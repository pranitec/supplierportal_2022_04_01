export const environment = {
  production: true,
   baseUrl:'https://supplier.craftsmanautomation.com/',
   apiBaseUrl: 'https://supplier.craftsmanautomation.com/api',
  asnDays: 90
};
