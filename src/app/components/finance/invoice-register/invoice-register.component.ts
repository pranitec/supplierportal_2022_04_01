import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { Router } from '@angular/router';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { FinanceService } from '../service/finance.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { profileData } from 'src/app/config/config'

declare var jQuery: any;
@Component({
  selector: 'app-invoice-register',
  templateUrl: './invoice-register.component.html',
  styleUrls: ['./invoice-register.component.css']
})
export class InvoiceRegisterComponent implements OnInit {

  public fullInvListHead = [];
  public fullInvListLine = [];
  public fullSummary = [];
  public summary = []
  public invListHead = [];
  public invListLine = [];
  public email = '';
  public showHide = true;
  public tableView;
  public page = 1;
  public viewCount = 10;
  public searchText = '';
  public lineId = '';
  public shipmentView = false;
  public noOfInvoiceCreated = 0;
  public noOfInvoiceApproved = 0;
  public noOfInvoicePaid = 0;
  public noOfInvoiceNotPaid = 0;
  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();
  public action = false;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst
  public hsnsacCode;
  public fieldName =''
  public paymentSchedule =[]
  public paymentScheduleFlag = false
  // public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' +
    // this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

  public selectedmenu;
  public selectedmenuFilter =[];
  public oneyearstartingDate : Date;
  private notifier: NotifierService;
  public noOfMonths;

  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

  constructor(private purchaseService: PurchaseService, private chRef: ChangeDetectorRef,
    public breadCrumbServices: BreadcrumbService, private router: Router, private notifierService: NotifierService,
    public rsaService: RsaService, public financeService: FinanceService, private spinner: NgxSpinnerService,) {
      this.notifier = notifierService;
     }
  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));

    // this.rsaService.numberValueformatNumber()
    this.tableView = true;
    localStorage.setItem('INDF', this.fromDate);
    localStorage.setItem('INDT', this.toDate);

    this.selectedmenu= this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
    this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(({MENUACCESS})=> MENUACCESS.MENU_CODE === 'INVOICE_REGISTER');
    this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START)
    this.noOfMonths  = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE /31)

    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/finance/invoiceRegister') {
        this.setFilteredData();
      }
    });



    this.getInvRegister();

    (function ($, _this) {

      $(document).ready(function () {
        // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        // const today = new Date();
        // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
        //   months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();

        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
          value: _this.fromDate,
          minDate : _this.oneyearstartingDate,
          change(e) {
            localStorage.setItem('INDF', e.target.value);
            _this.fromDate = e.target.value
          }

        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
          value: _this.toDate,
          maxDate: new Date(),
          change(e) {
            localStorage.setItem('INDT', e.target.value);
            _this.toDate = e.target.value
          }
        });
      });
    })(jQuery, this);
    this.setAsDataTable();

  }

  getInvRegister() {
    // this.fromDate = localStorage.getItem('INDF');
    // this.toDate = localStorage.getItem('INDT');
    this.spinner.show();
    // const details = { email: this.email, financeType: 'INVOICE_DETAILS', date1: this.fromDate, date2: this.toDate };
    // this.financeService.getFinance(details).subscribe((data: any) => {
      var time_difference =  Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
      var months_difference =  Math.floor(time_difference / (1000 * 60 * 60 * 24)/31);

      if((-(months_difference)) >this.noOfMonths){
        // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
        this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
        this.spinner.hide();
      } else{
      var query = this.purchaseService.inputJSON('INVOICE_DETAILS', this.fromDate, this.toDate)
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      // console.log("getInvRegister",data);
      this.spinner.hide();
      if (data) {

        // this.fullInvListHead = Array.isArray(data.INVOICES.INVOICE_HEADERS.INVOICE_HEADER) ? data.INVOICES.INVOICE_HEADERS.INVOICE_HEADER : (data.INVOICES.INVOICE_HEADERS.INVOICE_HEADER) ? [data.INVOICES.INVOICE_HEADERS.INVOICE_HEADER] : [];
        // this.fullInvListLine = Array.isArray(data.INVOICES.INVOICE_LINES.INVOICE_LINE) ? data.INVOICES.INVOICE_LINES.INVOICE_LINE : (data.INVOICES.INVOICE_LINES.INVOICE_LINE) ? [data.INVOICES.INVOICE_LINES.INVOICE_LINE] : [];
        this.fullInvListHead = data.INVOICES.INVOICE_HEADERS ? data.INVOICES.INVOICE_HEADERS.map(x=>(x.INVOICE_HEADER)) :[]
        this.fullInvListLine = data.INVOICES.INVOICE_LINES ? data.INVOICES.INVOICE_LINES.map(x=>(x.INVOICE_LINE)) :[]
        this.fullSummary = data.INVOICES.INVOICE_SUMMARIES ? data.INVOICES.INVOICE_SUMMARIES.map(x=>(x.INVOICE_SUMMARY)) :[]
        this.invListHead = this.fullInvListHead;
        this.invListLine = this.fullInvListLine;
        this.summary = this.fullSummary;
        // console.log(" fullInvListHead : ", this.fullInvListHead)
        this.invListHead.forEach(
          (x, i) => {
            // this.invListHead[i].INVOICE_AMOUNT =this.formatNumber.transform(this.invListHead[i].INVOICE_AMOUNT, null)
            this.invListHead[i].filteredList = this.invListLine.filter(y => y.INVOICE_ID === x.INVOICE_ID);
          }
        );

        this.setPoSummary();
        this.setFilteredData();
        this.setAsDataTable();
      } else {
        this.spinner.hide();
        console.error('NULL VALUE');
      }

    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }
  }

  setPoSummary() {

    this.summary = this.fullSummary.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      // if (this.searchText) {
      //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      // }
      return a;
    });
      // if (this.searchText) {
      //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      // }
    this.setSummaryData();
  }

  setSummaryData() {
    let InvoiceCreated = 0, InvoiceApproved = 0, InvoicePaid = 0, InvoiceNotPaid = 0;
      this.summary.forEach(f => {
        InvoiceCreated = InvoiceCreated + Number(f.NO_OF_INV_CREATED);
        InvoiceApproved = InvoiceApproved + Number(f.NO_OF_INV_APPROVED);
        InvoicePaid = InvoicePaid + Number(f.NO_OF_INV_PAID);
        InvoiceNotPaid = InvoiceNotPaid + Number(f.NO_OF_INV_NOT_PAID);
    });

    // this.poCounts.forEach(f=>{
    //   ordersReceived = ordersReceived + Number(f.PAST_PO_COUNT);
    // })

    this.noOfInvoiceCreated = InvoiceCreated;
    this.noOfInvoiceApproved = InvoiceApproved;
    this.noOfInvoicePaid = InvoicePaid;
    this.noOfInvoiceNotPaid = InvoiceNotPaid;
  }

  setFilteredData() {
    this.invListHead = this.fullInvListHead.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }

      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });
    this.invListHead.forEach((f, i) => {
      this.invListHead[i].list = this.fullInvListLine.filter(x => x.INVOICE_ID == f.INVOICE_ID);
    });

    this.setAsDataTable();
  }

  setAsDataTable() {

    this.chRef.detectChanges();
    this.invListHead.forEach((f, i) => {
      (function ($) {
        $('#test' + i).DataTable({
          responsive: true,
          bLengthChange: false,
          retrieve: true,
          pageLength: 5,
        });
      })(jQuery);
    });
  }


  setAsDataTableList() {


    this.chRef.detectChanges();
    (function ($) {


      $('#Pending-table').DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        pageLength: 20,
      });

    })(jQuery);
  }

  showHideTable() {
    this.showHide = false;
    setTimeout(() => {
      this.showHide = true;
      this.setFilteredData();
    }, 10);
  }

  gridView() {
    this.tableView = true;
    this.setAsDataTable();
  }

  listView() {
    this.tableView = false;
    this.setAsDataTableList();
  }

  public onPageChanged(event) {
    this.page = event;
  }

  exportExcel() {

    const details = { POLINES: this.fullInvListHead, title: 'Invoice Report' };
    this.financeService.getExcel(details);
  }
  clickedLineId(lineId, name) {
    this.lineId = lineId;
    // this.shipmentView = true;
    this.router.navigateByUrl('/craftsmanautomation/purchase/pastPurchaseOrders', { queryParams: { invoiceId: this.lineId, fieldName: name }, skipLocationChange: profileData.hideURL });
  }
  clickedLineId1(lineId, name) {
    this.lineId = lineId;
    // this.shipmentView = true;
    this.router.navigateByUrl('/craftsmanautomation/finance/paymentRegister', { queryParams: { invoiceId: this.lineId, fieldName: name }, skipLocationChange: profileData.hideURL });


  }

  goBackShip() {
    this.shipmentView = false;
    this.revive();
  }
  revive() {
    (function ($) {

      $(document).ready(function () {
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        const today = new Date();
        const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
          months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();

        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
          value: localStorage.getItem('INDF'),
          change(e) {
            localStorage.setItem('INDF', e.target.value);
          }

        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
          value: localStorage.getItem('INDT'),
          change(e) {
            localStorage.setItem('INDT', e.target.value);
          }
        });
      });
    })(jQuery);
    this.setAsDataTable();
  }


  viewTaxButton1(element, i, j){
    this.action = false;
    // this.poHeader[i].filteredList[j].action = false;
    this.invListHead[i].filteredList[j].action = false

  }
  viewTaxButton2(element,i, j){
    // console.log("ele : ", element)
    this.action = true;
    // this.poHeader[i].filteredList[j].action = true
    this.invListHead[i].filteredList[j].action= true
    // const details = { email: this.email, attachmentString: element.TAX_IDENTIFICATION_STRING, purchaseType: 'TAX_LINES'};
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
      var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', element.TAX_IDENTIFICATION_STRING)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log("tax details : ", data)
      this.taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.myGst = this.taxData[0].MY_TAX_REG_NO
      this.hsnsacCode = this.taxData[0].HSN_SAC_CODE

      this.invListHead[i].filteredList[j].taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.invListHead[i].filteredList[j].taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.invListHead[i].filteredList[j].taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.invListHead[i].filteredList[j].myGst = this.taxData[0].MY_TAX_REG_NO
      this.invListHead[i].filteredList[j].hsnsacCode = this.taxData[0].HSN_SAC_CODE
      this.invListHead[i].filteredList[j].hsnsacCodeDesc = this.taxData[0].HSN_SAC_CODE_DESC

      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  getInvoiceAmountData(invoiceId){
    console.log("inn")
    var query = this.purchaseService.inputJSON("PAYMENT_SCHEDULES", undefined, undefined,"INVOICE_ID", invoiceId);
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      this.paymentSchedule = data.PAYMENT_SCHEDULES ? data.PAYMENT_SCHEDULES.map(x => x.PAYMENT_SCHEDULE) :[]
    this.paymentScheduleFlag= true
    console.log("payment schedule : ", this.paymentSchedule )
  },
  (err) => {
    this.spinner.hide();
    console.log(err);
  });
  }
}
