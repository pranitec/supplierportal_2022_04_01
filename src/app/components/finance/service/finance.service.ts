import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpParams, HttpClient } from '@angular/common/http';
import { saveAs } from 'file-saver';
@Injectable({
  providedIn: 'root'
})
export class FinanceService {

  public baseUrl = environment.apiBaseUrl + '/finance' ;

  constructor(private http: HttpClient) { }

  getFinance(email) {
    return this.http.post(`${this.baseUrl}`, email);
  }

  getExcel(data){
    // return this.http.post(`${this.baseUrl}/getExcel`, data);
    const title = data.title;
    return this.http.post(`${this.baseUrl}/getExcel`, data, {
      params: new HttpParams().append('token', localStorage.getItem('1')),
      observe: 'response', responseType: 'text'
    }).subscribe(r => { saveAs(new Blob([r.body], { type: 'text/csv' }), title + '.csv'); });

  }
  

  getLedgerExcel(data) {
    // return this.http.post(`${this.baseUrl}/getExcel`, data);
    const title = data.title;
    return this.http.post(`${this.baseUrl}/getLedgerExcel`, data, {
      params: new HttpParams().append('token', localStorage.getItem('1')),
      observe: 'response', responseType: 'text'
    }).subscribe(r => { saveAs(new Blob([r.body], { type: 'text/csv' }), title + '.csv'); });

  }

  getAdvancePaymentExcel(data){
    // return this.http.post(`${this.baseUrl}/getExcel`, data);
    const title = data.title;
    return this.http.post(`${this.baseUrl}/getAdvancePaymentExcel`, data, {
      params: new HttpParams().append('token', localStorage.getItem('1')),
      observe: 'response', responseType: 'text'
    }).subscribe(r => { saveAs(new Blob([r.body], { type: 'text/csv' }), title + '.csv'); });

  }

  getPaymentExcel(data){
    // return this.http.post(`${this.baseUrl}/getExcel`, data);
    const title = data.title;
    return this.http.post(`${this.baseUrl}/getPaymentExcel`, data, {
      params: new HttpParams().append('token', localStorage.getItem('1')),
      observe: 'response', responseType: 'text'
    }).subscribe(r => { saveAs(new Blob([r.body], { type: 'text/csv' }), title + '.csv'); });

  }
}
