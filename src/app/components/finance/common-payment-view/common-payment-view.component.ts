import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { ShipmentService } from '../../shipment/service/shipment.service';
declare var jQuery:any;

@Component({
  selector: 'app-common-payment-view',
  templateUrl: './common-payment-view.component.html',
  styleUrls: ['./common-payment-view.component.css']
})
export class CommonPaymentViewComponent implements OnInit {

  @Input() shipmentId: string;
  @Input() shipmentView: boolean;
  @Output("goBackShip") goBackShip: EventEmitter<any> = new EventEmitter();
  public email;
  public poHeadersList = [];
  public fullpoHeaderList = [];
  public poLineList = [];
  public fullpolines = [];
  public fullSummary = [];
  public viewShipmenList = [];

  public searchText = '';
  public page: any;
  public viewCount = 10;
  public viewGrid = true;

  public uom = ''
  // public fromDate;
  // public toDate;
  constructor(private shipmentService: ShipmentService , private chRef: ChangeDetectorRef,
    public breadCrumbServices: BreadcrumbService, private router: Router, public rsaService: RsaService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
    });

    this.getViewReceipts();
  }


  getViewReceipts() {
    this.spinner.show();

    const details = { email: this.email, shipmentType: 'INVOICE_PO_DETAILS', lineId: this.shipmentId };
    this.shipmentService.getShipmentsById(details).subscribe((data: any) => {
      this.spinner.hide();
      if (data) {
        this.fullpoHeaderList = Array.isArray(data.PODETAILS.POHEADERS.POHEADER) ? data.PODETAILS.POHEADERS.POHEADER : (data.PODETAILS.POHEADERS.POHEADER) ? [data.PODETAILS.POHEADERS.POHEADER] : [];
        this.fullpolines = Array.isArray(data.PODETAILS.POLINES.POLINE) ? data.PODETAILS.POLINES.POLINE : (data.PODETAILS.POLINES.POLINE) ? [(data.PODETAILS.POLINES.POLINE)] : [];
        //this.fullSummary = Array.isArray(data.PODETAILS.RECEIPTLOTS.RECEIPTLOT) ? data.PODETAILS.RECEIPTLOTS.RECEIPTLOT : (data.PODETAILS.RECEIPTLOTS.RECEIPTLOT) ? [data.PODETAILS.RECEIPTLOTS.RECEIPTLOT] : [];
        this.setFilteredData();
       // this.setSummaryData();
      } else {
        console.error('NULL VALUE');
      }

    }, error => {
      this.spinner.hide();
      console.log(error);
    });



  }

  setFilteredData() {
    this.poHeadersList = this.fullpoHeaderList.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }


      // if (this.fromDate) {
      //   a = a && this.fromDate < f.PO_DATE;
      // }

      // if (this.toDate) {
      //   a = a && this.toDate > f.PO_DATE;
      // }

      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });
    //   this.poHeadersList = this.poHeadersList.slice(0, 20);
    this.poHeadersList.forEach((f, i) => {
      this.poHeadersList[i].list = this.fullpolines.filter(x => x.SHIPMENT_HEADER_ID == f.SHIPMENT_HEADER_ID);
    });
    this.setAsDataTable();
  }

  setAsDataTable() {
    this.chRef.detectChanges();
    this.poHeadersList.forEach((f, i) => {
      (function ($) {
        $('#testvi' + i).DataTable({
          responsive: true,
          bLengthChange: false,
          retrieve: true,
          pageLength: 5,
        });
      })(jQuery);
    });
  }
  clickedLineId (id) {

  }

  // setAsDataTableList() {
  //   this.chRef.detectChanges();
  //   (function ($) {
  //     $('#Past-table').DataTable({
  //       responsive: true,
  //       iDisplayLength: 5,
  //       bLengthChange: false,
  //       retrieve: true,
  //       pageLength: 5,
  //     });

  //     $('#Past-table').draw();
  //   })(jQuery);

  // }

  public onPageChanged(event) {
    this.page = event;
  }
  onViewGrid() {
    this.viewGrid = true;
    this.searchText = '';
    //this.setAsDataTable();

  }

  onViewList() {
    this.viewGrid = false;
    this.searchText = '';
    //this.setAsDataTableList();

  }

  exportExcel() {
    const poLines = [];
    this.poHeadersList.forEach(f => {
      poLines.push(...f.list);
    });
    const details = { POLINES: poLines };
    this.shipmentService.getExcel(details);
  }




  changeView(shipment_id) {

    this.viewShipmenList = this.fullSummary.filter(f => f.SHIPMENT_LINE_ID === shipment_id)
    this.viewGrid = false;
  }
  goBack() {
    this.viewGrid = true;
  }

  goBackFromShip() {
    this.shipmentView = false;
    this.goBackShip.emit();
  }

}
