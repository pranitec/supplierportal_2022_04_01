import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonPaymentViewComponent } from './common-payment-view.component';

describe('CommonPaymentViewComponent', () => {
  let component: CommonPaymentViewComponent;
  let fixture: ComponentFixture<CommonPaymentViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonPaymentViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonPaymentViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
