import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FinanceService } from '../service/finance.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { Router } from '@angular/router';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { NotifierService } from 'angular-notifier';


declare var jQuery: any;

@Component({
  selector: 'app-advance-payment',
  templateUrl: './advance-payment.component.html',
  styleUrls: ['./advance-payment.component.css']
})
export class AdvancePaymentComponent implements OnInit {

  public email;
  public viewCount = 10;
  public searchText = '';
  public page: any;
  public view;
  public payment = [];
  public paymentLine = [];
  public paymentAll = [];
  public paymentLineAll = [];
  public summary = [];
  public noOfinvoicePaid = 0;
  public noOfpaymentApproved = 0;
  public noOfpaymentCreated = 0;
  public noOfpaymentVoid = 0;
  public selectedmenu;
  public selectedmenuFilter =[];
  public oneyearstartingDate : Date;
  private notifier: NotifierService;
  public noOfMonths;

  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();
  // public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' +
  //   this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() -30));
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

  constructor(private financeService: FinanceService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService,
    private breadCrumbServices: BreadcrumbService, private router: Router, private rsaService: RsaService,
    public purchaseService : PurchaseService, private notifierService: NotifierService) {
      this.notifier = notifierService;
    }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    localStorage.setItem('ARDF', this.fromDate);
    localStorage.setItem('ARDT', this.toDate);
    this.view = true;

    this.selectedmenu= this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
    this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(({MENUACCESS})=> MENUACCESS.MENU_CODE === 'VIEW_ADVANCE_PAYMENTS');
    this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START)
    this.noOfMonths  = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE /31)

    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/finance/advancePayment') {
        this.setFilteredData();
      }
    });

    this.getPaymentRegister();

    (function ($, _this) {
      $(document).ready(function () {
        // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        // const today = new Date();
        // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
        //   months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          value: _this.fromDate,
          minDate : _this.oneyearstartingDate,
          change(e) {
            localStorage.setItem('ARDF', e.target.value);
            _this.fromDate = e.target.value;
          }
        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          value: _this.toDate,
          maxDate: new Date(),
          change(e) {
            localStorage.setItem('ARDT', e.target.value);
            _this.toDate = e.target.value
          }
        });
      });
    })(jQuery, this);
  }
  getPaymentRegister() {
    this.spinner.show();
    // this.fromDate = localStorage.getItem('ARDF');
    // this.toDate = localStorage.getItem('ARDT');
    // const details = { email: this.email, financeType: 'ADVANCE_PAYMENT', date1: this.fromDate, date2: this.toDate };
    // this.financeService.getFinance(details).subscribe((data: any) => {
      var time_difference =  Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
      var months_difference =  Math.floor(time_difference / (1000 * 60 * 60 * 24)/31);
      if((-(months_difference)) >this.noOfMonths){
        // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
        this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
        this.spinner.hide();
      } else{
      var query = this.purchaseService.inputJSON('ADVANCE_PAYMENT', this.fromDate, this.toDate)
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      // console.log("getPaymentRegister", data);
      this.spinner.hide();
      // this.paymentLineAll = Array.isArray(data.ADVPAYMENT.PAYMENTLINES.PAYMENTLINE) ? data.ADVPAYMENT.PAYMENTLINES.PAYMENTLINE :
      //   (data.ADVPAYMENT.PAYMENTLINES.PAYMENTLINE) ? [data.ADVPAYMENT.PAYMENTLINES.PAYMENTLINE] : [];
      // this.paymentAll = Array.isArray(data.ADVPAYMENT.PAYMENTS.PAYMENT) ?
      //   data.ADVPAYMENT.PAYMENTS.PAYMENT : (data.ADVPAYMENT.PAYMENTS.PAYMENT) ? [(data.ADVPAYMENT.PAYMENTS.PAYMENT)] : [];

        this.paymentLineAll = data.ADVPAYMENT.PAYMENTLINES ? data.ADVPAYMENT.PAYMENTLINES.map(x=>(x.PAYMENTLINE)) :[]
        this.paymentAll = data.ADVPAYMENT.PAYMENTS ? data.ADVPAYMENT.PAYMENTS.map(x=>(x.PAYMENT)) :[]
        // this.paymentAll = data.ADVPAYMENT ? data.ADVPAYMENT.PAYMENTS.map(x=>(x.PAYMENT)) :[]
        this.summary = data.PAYMENT_SUMMARIES? data.REGPAYMENT.PAYMENT_SUMMARIES.map(x=>(x.PAYMENT_SUMMARY)) :[]
        this.payment = this.paymentAll;
      this.paymentLine = this.paymentLineAll;
      this.payment.forEach((f, i) => {
        this.payment[i].filteredList = this.paymentLineAll.filter(x => x.CHECK_ID == f.CHECK_ID);
      });
    }, err => {
      console.log(err);
    });
  }
  }

  setFilteredData() {

    this.payment = this.paymentAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });
    this.setSummaryData();
    // this.poHeader.forEach((f, i) => {
    //   this.poHeader[i].list = this.poLineAll.filter(x => x.PO_HEADER_ID == f.PO_HEADER_ID);
    // });


    this.payment.forEach((f, i) => {
      this.payment[i].list = this.paymentLineAll.filter(x => x.CHECK_ID == f.CHECK_ID);
    });
    this.setAsDataTable();
  }

  setSummaryData() {
    let invoicePaid = 0, paymentApproved = 0, paymentCreated = 0, paymentVoid = 0;
      this.summary.forEach(f => {
        invoicePaid = invoicePaid + Number(f.NO_OF_INV_PAID);
        paymentApproved = paymentApproved + Number(f.NO_OF_PAY_APPROVED);
        paymentCreated = paymentCreated + Number(f.NO_OF_PAY_CREATED);
        paymentVoid = paymentVoid + Number(f.NO_OF_PAY_VOID);
    });

    // this.poCounts.forEach(f=>{
    //   ordersReceived = ordersReceived + Number(f.PAST_PO_COUNT);
    // })

    this.noOfinvoicePaid = invoicePaid;
    this.noOfpaymentApproved = paymentApproved;
    this.noOfpaymentCreated = paymentCreated;
    this.noOfpaymentVoid = paymentVoid;
  }

  setAsDataTable() {

    this.chRef.detectChanges();

    (function ($) {

      $("#listtest").DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        pageLength: 20,
      });
    })(jQuery);

  }

  exportExcel() {
    const details = { POLINES: this.paymentLineAll, title: 'Advance Payment Report' };
    this.financeService.getAdvancePaymentExcel(details);

  }
  getTableDetails(paymentId) {
    this.view = false;
    this.paymentLine = this.paymentLineAll.filter(x => x.CHECK_ID == paymentId);
    this.setAsDataTable();

  }
  getBack() {
    this.view = true;
  }
  onPageChanged(event) {
    this.page = event;
  }

}
