import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoginService } from '../../auth/service/login.service';
import { Upload } from '../../purchase/model/upload';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { saveAs } from 'file-saver';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NotifierService } from 'angular-notifier';
import { Router } from '@angular/router';

@Component({
  selector: 'app-file-view',
  templateUrl: './file-view.component.html',
  styleUrls: ['./file-view.component.css']
})
export class FileViewComponent implements OnInit {

  @Input() headerId: string;
  @Input() type: string;
  @Input() attachmentString: string;
  @Input() printDocString : string;
  @Input() communicationString : string;

  public attachments: any[] = [];
  public upload: Upload;
  fileData: File = null;
  public email;
  notifier: NotifierService;
  public fileInput1;

  constructor(private purchaseService: PurchaseService, private spinner: NgxSpinnerService, private router: Router,
              private loginService: LoginService, public rsaService: RsaService,  private notifierService: NotifierService) {
                this.notifier = notifierService;
              }

  @ViewChild('myFileInput', { static: false }) myInputVariable: ElementRef;

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.upload = new Upload();
  }

  getDocumentList() {
    this.spinner.show();
    // 247380
    if (this.type === 'INVOICE_DOC_ATTACHMENTS') {
      const details = { purchaseType: 'INVOICE_DOC_ATTACHMENTS', headerId: this.headerId };
      this.purchaseService.getDocs(details).subscribe((data: any) => {
        // console.log('kkk', data);
        // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
        //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
        this.attachments = data.ATTACHMENTS ? data.ATTACHMENTS.map(x=>x.ATTACHMENT) : [];
        this.spinner.hide();
      }, error => {
        console.log(error);
        this.spinner.hide();
      });
    }
    else  if (this.type === 'AP_INVOICE_ATTACHMENTS'){
      const details = { email: this.email, purchaseType: 'ATTACHMENT_LIST', attachmentString: this.attachmentString };
      // const details = { purchaseType: this.type, headerId: this.headerId };
      // console.log('detaiks', details);
      this.purchaseService.getDocs(details).subscribe((data: any) => {
        // console.log('invoice', data);
        this.attachments = data.ATTACHMENTS ? data.ATTACHMENTS.map(x=>x.ATTACHMENT) : [];
        this.spinner.hide();
      }, error => {
        console.log(error);
        this.spinner.hide();
      });

    }
    else if (this.type !== 'ASN_DETAILS') {
      const details = { purchaseType: 'PO_ATTACHMENTS', headerId: this.headerId };
      this.purchaseService.getDocs(details).subscribe((data: any) => {
        // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
        //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
        this.attachments = data ? data.ATTACHMENTS.map(x=>x.ATTACHMENT) : [];
        this.spinner.hide();

      }, error => {
        console.log(error);
        this.spinner.hide();
      });
    } else if (this.type === 'ASN_DETAILS') {
      const details = { purchaseType: 'ASN_ATTACHMENTS', headerId: this.headerId };
      this.purchaseService.getDocs(details).subscribe((data: any) => {
        this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
          data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
        this.spinner.hide();
      }, error => {
        console.log(error);
        this.spinner.hide();
      });

    }



  }


  downloadfile(element) {
    this.spinner.show();
    const dat = { documentId: element.DOCUMENT_ID, fileName: element.FILE_NAME };
    this.purchaseService.downloadAttachment(dat).subscribe((data) => {
      const file = new Blob([data]);
      saveAs(file, element.FILE_NAME);
      // var fileURL = URL.createObjectURL(file);
      // window.open(fileURL);
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });
  }

  // onFilesAdded(fileInput: any) {
  //   this.spinner.show();
  //   this.fileData = fileInput.target.files as File;
  //   const length = fileInput.target.files.length;
  //   const file = new FormData();
  //   for (let i = 0; i < length; i++) {
  //     file.append('file', this.fileData[0], this.fileData[0].name);
  //   }
  //   this.loginService.FileUpload(file).subscribe((data) => {
  //     this.spinner.hide();
  //     let i;
  //     this.upload.file = this.fileData[0].name;
  //   }, err => {
  //     this.spinner.hide();
  //     this.resetFile();
  //     if (err.error.message) {

  //     } else {
  //     }
  //   });

  // }

  // resetFile() {
  //   this.myInputVariable.nativeElement.value = '';
  // }

  // fileUpload() {
  //   this.spinner.show();
  //   const query = {
  //     DOCUMENT_ID: this.headerId, FILE_NAME: this.upload.file, TYPE: this.type,
  //     TITLE: this.upload.documentCategory, DESCRIPTION: this.upload.description
  //   };
  //   this.purchaseService.FileUpload(query).subscribe((data) => {
  //     if (data === 'S') {
  //       this.upload.documentCategory = '';
  //       this.upload.description = '';
  //       this.resetFile();
  //     }
  //     this.spinner.hide();
  //   }, (error) => {
  //     console.log(error);
  //     this.spinner.hide();
  //   });


  // }

  onFilesAdded(fileInput: any) {
    this.spinner.show();
    this.loginService.type = '/temp'
    this.loginService.fileType ='No'
    this.fileInput1=fileInput;
    this.fileData = fileInput.target.files as File;
    const length = fileInput.target.files.length;
    const file = new FormData();
    for (let i = 0; i < length; i++) {
      file.append('file', this.fileData[0], this.fileData[0].name);
    }
    this.loginService.FileUpload(file).subscribe((data) => {
      this.spinner.hide();
      let i;
      this.upload.file = this.fileData[0].name;
    }, err => {
      this.spinner.hide();
      this.resetFile();
      if (err) {
        console.log("error : ", err)
      } else {
      }
    });

  }

  resetFile() {
    this.myInputVariable.nativeElement.value = '';
  }

  fileUpload() {
    var temp=''
    this.spinner.show();
    const query = {
      DOCUMENT_ID: this.headerId, FILE_NAME: this.upload.file, TYPE: this.type,
      TITLE: this.upload.documentCategory, DESCRIPTION: this.upload.description,
      USER_ID:this.email
    };
    this.purchaseService.FileUpload(query).subscribe((data :any) => {
      var file1 = data.filename
      var data1=data.path
      this.loginService.type = data1;
      this.loginService.fileType = 'Yes'
      if(data.o === 'S')
      {
          this.fileData = this.fileInput1.target.files as File;
          const length = this.fileInput1.target.files.length;
          const file3 = new FormData();
          for (let i = 0; i < length; i++) {
            file3.append('file', this.fileData[0], file1);
          }
        this.loginService.FileUpload(file3).subscribe((data) => {
          this.spinner.hide();
          let i;
          // this.upload.file = this.fileData[0].name;
        }, err => {
          this.spinner.hide();
          this.resetFile();
          if (err.error.message) {

          } else {
          }
        });

        this.upload.documentCategory = '';
        this.upload.description = '';
        this.resetFile();
        this.notifier.notify('success', 'File being uploaded!');
      }
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });


  }

  cancel() {
    this.upload.documentCategory = '';
    this.upload.description = '';
    this.resetFile();
  }

  getPrintDocument(){
      // if(this.type === 'ASN_DETAILS'){
      //   this.generatePdf(this.headerId)
      // }else{
      this.spinner.show();
      const details = {  printDocumentString: this.printDocString};
      this.purchaseService.getDocPrint(details).subscribe((data: any) => {
        this.spinner.hide();
        const file = new Blob([data]);
        saveAs(file, "this.headerId"+'.pdf');

      }, error => {
        console.log(error);
        this.notifier.notify('warning', 'Print document request has been submitted, You will be notified to download the document shortly.!');
        this.spinner.hide();
      });
    // }
  }
  getTransacationCommunication(){
    this.router.navigateByUrl('/craftsmanautomation/administration/communication', { queryParams: { communicationString : this.communicationString}, skipLocationChange: true });
  }
}
