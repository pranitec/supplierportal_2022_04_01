import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceRegisterComponent } from './invoice-register/invoice-register.component';
import {PaymentRegisterComponent} from './payment-register/payment-register.component';
import { AdvancePaymentComponent } from './advance-payment/advance-payment.component';
import {MyLedgerComponent} from './my-ledger/my-ledger.component';
import { UploadInvoiceComponent } from './upload-invoice/upload-invoice.component';
import { CreateInvoiceComponent } from './create-invoice/create-invoice.component';
import { AuthGuardService } from 'src/app/shared/service/auth-guard.service';


const routes: Routes = [
  {
    path: 'invoiceRegister',
    component: InvoiceRegisterComponent,
    data: {
      title: 'Invoice Register',
      breadcrumb: 'Invoice Register'
    } ,
    canActivate: [AuthGuardService]
  },
    {
      path: 'paymentRegister',
      component: PaymentRegisterComponent,
      data: {
        title: 'Payment Register',
        breadcrumb: 'Payment Register'
      },
      canActivate: [AuthGuardService]
    },
    {
      path: 'advancePayment',
      component: AdvancePaymentComponent,
      data: {
        title: 'View Advance Payment',
        breadcrumb: 'View Advance Payment'
      },
      canActivate: [AuthGuardService]
     },
      {
        path: 'myLedger',
        component: MyLedgerComponent,
        data: {
          title: 'My Ledger',
          breadcrumb: 'My Ledger'
        },
        canActivate: [AuthGuardService]
       },
        {
          path: 'uploadinvoice',
          component: UploadInvoiceComponent,
          data: {
            title: 'Upload Invoice',
            breadcrumb: 'Upload Invoice'
          },
          canActivate: [AuthGuardService]
         },
          {
            path: 'createinvoice',
            component: CreateInvoiceComponent,
            data: {
              title: 'Create Invoice',
              breadcrumb: 'Create Invoice'
            },
            canActivate: [AuthGuardService] },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinanceRoutingModule { }
