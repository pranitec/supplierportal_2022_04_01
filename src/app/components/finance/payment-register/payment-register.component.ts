import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { FinanceService } from '../service/finance.service';
import { Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { NotifierService } from 'angular-notifier';
import { profileData } from 'src/app/config/config'

declare var jQuery: any;


@Component({
  selector: 'app-payment-register',
  templateUrl: './payment-register.component.html',
  styleUrls: ['./payment-register.component.css']
})
export class PaymentRegisterComponent implements OnInit {

  public email;
  public viewCount = 10;
  public searchText = '';
  public page =1 ;
  public view;
  public payment = [];
  public paymentLine = [];
  public paymentAll = [];
  public paymentLineAll = [];
  public paymentSummaryALL = [];
  public summary = [];
  public lineId = '';
  public shipmentView= false;
  public noOfinvoicePaid = 0;
  public noOfpaymentApproved = 0;
  public noOfpaymentCreated = 0;
  public noOfpaymentVoid = 0;
  public selectedmenu;
  public selectedmenuFilter =[];
  public oneyearstartingDate : Date;
  private notifier: NotifierService;
  public noOfMonths;

  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();
  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' +
    this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

  constructor(private financeService: FinanceService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService,
    private breadCrumbServices: BreadcrumbService, private router: Router, private rsaService: RsaService, private notifierService: NotifierService,
    public purchaseService : PurchaseService) {
      this.notifier = notifierService;
     }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    localStorage.setItem('PRDF', this.fromDate);
    localStorage.setItem('PRDT', this.toDate);
    this.view = true;
    this.selectedmenu= this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
    this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(({MENUACCESS})=> MENUACCESS.MENU_CODE === 'PAYMENT_REGISTER');
    this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START)
    this.noOfMonths  = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE /31)


    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/finance/paymentRegister') {
        this.setFilteredData();
      }
    });

    this.getPaymentRegister();

    (function ($, _this) {
      $(document).ready(function () {
        // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        // const today = new Date();
        // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
        //   months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          value: _this.fromDate,
          minDate : _this.oneyearstartingDate,
          change(e) {
            localStorage.setItem('PRDF', e.target.value);
            _this.fromDate = e.target.value;
          }
        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          value: _this.toDate,
          maxDate: new Date(),
          change(e) {
            localStorage.setItem('PRDT', e.target.value);
            _this.toDate = e.target.value
          }
        });
      });
    })(jQuery, this);
  }
  getPaymentRegister() {
    this.spinner.show();
    // this.fromDate = localStorage.getItem('PRDF');
    // this.toDate = localStorage.getItem('PRDT');
    // const details = { email: this.email, financeType: 'REGULAR_PAYMENT', date1: this.fromDate, date2: this.toDate };
    // this.financeService.getFinance(details).subscribe((data: any) => {
      var time_difference =  Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
      var months_difference =  Math.floor(time_difference / (1000 * 60 * 60 * 24)/31);

      if((-(months_difference)) >this.noOfMonths){
        // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
        this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
        this.spinner.hide();
      } else{
       var query = this.purchaseService.inputJSON('REGULAR_PAYMENT', this.fromDate, this.toDate)
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      console.log("getPaymentRegister", data);
      this.spinner.hide();
      // this.paymentLineAll = Array.isArray(data.REGPAYMENT.PAYMENTLINES.PAYMENTLINE) ? data.REGPAYMENT.PAYMENTLINES.PAYMENTLINE :
      //   (data.REGPAYMENT.PAYMENTLINES.PAYMENTLINE) ? [data.REGPAYMENT.PAYMENTLINES.PAYMENTLINE] : [];
      // this.paymentAll = Array.isArray(data.REGPAYMENT.PAYMENTS.PAYMENT) ?
      //   data.REGPAYMENT.PAYMENTS.PAYMENT : (data.REGPAYMENT.PAYMENTS.PAYMENT) ? [(data.REGPAYMENT.PAYMENTS.PAYMENT)] : [];
      this.paymentLineAll = data.REGPAYMENT.PAYMENTLINES ? data.REGPAYMENT.PAYMENTLINES.map(x=>(x.PAYMENTLINE)) :[]
      this.paymentAll = data.REGPAYMENT.PAYMENTS? data.REGPAYMENT.PAYMENTS.map(x=>(x.PAYMENT)) :[]
      this.paymentSummaryALL = data.REGPAYMENT.PAYMENT_SUMMARIES? data.REGPAYMENT.PAYMENT_SUMMARIES.map(x=>(x.PAYMENT_SUMMARY)) :[]

      this.payment = this.paymentAll;
      this.paymentLine = this.paymentLineAll;
      this.summary = this.paymentSummaryALL;
      this.payment.forEach((f, i) => {
        this.payment[i].filteredList = this.paymentLineAll.filter(x => x.CHECK_ID == f.CHECK_ID);
      });
      console.log("payment reg : ", this.payment)
      this.setInvoiceSummary();
      this.setAsDataTable();
      this.setFilteredData();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
  }


  setInvoiceSummary() {

    this.summary = this.paymentSummaryALL.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      // if (this.searchText) {
      //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      // }
      return a;
    });
      // if (this.searchText) {
      //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      // }
    this.setSummaryData();
  }

  setSummaryData() {
    let invoicePaid = 0, paymentApproved = 0, paymentCreated = 0, paymentVoid = 0;
      this.summary.forEach(f => {
        invoicePaid = invoicePaid + Number(f.NO_OF_INV_PAID);
        paymentApproved = paymentApproved + Number(f.NO_OF_PAY_APPROVED);
        paymentCreated = paymentCreated + Number(f.NO_OF_PAY_CREATED);
        paymentVoid = paymentVoid + Number(f.NO_OF_PAY_VOID);
    });

    // this.poCounts.forEach(f=>{
    //   ordersReceived = ordersReceived + Number(f.PAST_PO_COUNT);
    // })

    this.noOfinvoicePaid = invoicePaid;
    this.noOfpaymentApproved = paymentApproved;
    this.noOfpaymentCreated = paymentCreated;
    this.noOfpaymentVoid = paymentVoid;
  }

  setFilteredData() {

    this.payment = this.paymentAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });

    this.payment.forEach((f, i) => {
      this.payment[i].list = this.paymentLineAll.filter(x => x.CHECK_ID == f.CHECK_ID);
    });
    this.setAsDataTable();

  }

  setAsDataTable() {

    this.chRef.detectChanges();
    this.payment.forEach((f, i) => {
      (function($) {
        $('#test' + i).DataTable({
          responsive: true,
          bLengthChange: false,
          retrieve: true,
          pageLength: 5,
        });
      })(jQuery);
    });

  }

  exportExcel() {
    const details = { POLINES: this.paymentLineAll, title: 'Payment Report' };
    this.financeService.getPaymentExcel(details);

  }
  // getTableDetails(paymentId) {
  //   this.view = false;
  //   this.paymentLine = this.paymentLineAll.filter(x => x.CHECK_ID == paymentId);
  //   this.setAsDataTable();

  // }
  // getBack() {
  //   this.view = true;
  // }
  onPageChanged(event) {
    this.page = event;
  }

  clickedLineId(lineId,name) {
    this.lineId = lineId;
    // this.shipmentView = true;
     this.router.navigateByUrl('/craftsmanautomation/purchase/pastPurchaseOrders', { queryParams: { invoiceId: this.lineId, fieldName: name }, skipLocationChange: profileData.hideURL });

  }
  goBackShip() {
    this.shipmentView = false;
this.revive();
  }
  revive(){
    (function ($) {
      $(document).ready(function () {
        // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        // const today = new Date();
        // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
        //   months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          value:   localStorage.getItem('PRDF'),
          change(e) {
            localStorage.setItem('PRDF', e.target.value);
            this.fromDate = e.target.value;
          }
        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          value: localStorage.getItem('PRDT'),
          change(e) {
            localStorage.setItem('PRDT', e.target.value);
          }
        });
      });
    })(jQuery);
    this.setAsDataTable();
  }
}
