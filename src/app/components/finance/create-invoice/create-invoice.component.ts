import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { EmailService } from '../../auth/service/email.service';
import { LoginService } from '../../auth/service/login.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { profileData } from 'src/app/config/config'

declare var jQuery: any;

@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.css']
})
export class CreateInvoiceComponent implements OnInit {

  purchasedList = [];

  public email = '';

  public poHeaderAll = [];
  //public poLineAll = [];
  public poHeader = [];
  //public poLine = [];


  public file;
  fileData: File = null;
  public file64;

  public selectedPO = '';

  public currPOlines = [];

  public invoiceLines = [];

  public beforeUpdateInvLine = [];

  public uniquePOs = [];

  public createInv: Invoice;

  public invoiceMode = '';


  public edit = false;
  private notifier: NotifierService;
  constructor(private purchaseService: PurchaseService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService,
    private breadCrumbServices: BreadcrumbService, private router: Router, private rsaService: RsaService, private notifierService: NotifierService, private loginService: LoginService, public emailService: EmailService) {
    this.notifier = notifierService;


  }
  @ViewChild('myFileInput', { static: false }) myInputVariable: ElementRef;
  ngOnInit(): void {
    this.createInv = new Invoice();
    // this.createInv.CURRENCY = "MYR";
    (function ($,thi) {
      // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      // const today = new Date();

      // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
      // $('#datepicker1').datepicker({
      //   uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
      //   value: cur.NEED_BY_DATE,
      //   change(e) {
      //     localStorage.setItem('ADDO', e.target.value);
      //     cur.NEED_BY_DATE = e.target.value;
      //   }
      // });

      $('.nav-tabs > li a[title]').tooltip();

      //Wizard

      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        var target = $(e.target);

        if (target.parent().hasClass('disabled')) {

          return false;

        }

      });

      $(".next-step").click(function (e) {

        var active = $('.wizard .nav-tabs li.active');

        active.next().removeClass('disabled');

        nextTab(active);

      });

      $(".prev-step").click(function (e) {

        var active = $('.wizard .nav-tabs li.active');

        prevTab(active);

      });

      function nextTab(elem) {
        $('#datepicker2').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
          value: thi.createInv.INVOICE_DATE,
          change(e) {
            thi.createInv.INVOICE_DATE = e.target.value;
          }
        });

        $(elem).next().find('a[data-toggle="tab"]').click();

      }



      function prevTab(elem) {

        $(elem).prev().find('a[data-toggle="tab"]').click();

      }

      $('.nav-tabs').on('click', 'li', function () {

        $('.nav-tabs li.active').removeClass('active');

        $(this).addClass('active');

      });

      $('#datepicker2').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'dd-mmm-yyyy',
        // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
        value: thi.createInv.INVOICE_DATE,
        change(e) {
          thi.createInv.INVOICE_DATE = e.target.value;
        }
      });

    })(jQuery,this);

    // (function ($, thi) {

    // })(jQuery, this);

    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.getPendingPurchase();

  }

  AddPOClicked() {

  }


  onPOModelClose() {

  }


  setPo(value) {

    let a = this.uniquePOs.filter(f => f === value)
    if (a.length === 1) {
      this.selectedPO = value;
      this.setLinesList();
    }
  }

  getPendingPurchase() {
    this.spinner.show();
    // const details = { email: this.email, purchaseType: 'PENDING_PO_LINE_FOR_INVOICE' };
    const details = this.purchaseService.inputJSON('PENDING_PO_LINE_FOR_INVOICE', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('ppp', data);
      this.spinner.hide();
      ///     this.getAllFreightTerms();
      this.poHeaderAll = Array.isArray(data.PENDING_INV_LINES.PENDING_INV_LINE) ? data.PENDING_INV_LINES.PENDING_INV_LINE : (data.PENDING_INV_LINES.PENDING_INV_LINE) ? [data.PENDING_INV_LINES.PENDING_INV_LINE] : [];
      // this.poLineAll = Array.isArray(data.PENDINGPO.POLINES.POLINE) ? data.PENDINGPO.POLINES.POLINE : (data.PENDINGPO.POLINES.POLINE) ? [(data.PENDINGPO.POLINES.POLINE)] : [];
      this.poHeader = this.poHeaderAll;
      // this.poLine = this.poLineAll;
      // this.poHeader.forEach(
      //   (x, i) => {

      //     this.poHeader[i].filteredList = this.poLine.filter(y => y.PO_HEADER_ID === x.PO_HEADER_ID);
      //     this.poHeader[i].filteredList = this.poHeader[i].filteredList.map(y => {
      //       y.BUYER_EMAIL =   this.poHeader[i].BUYER_EMAIL

      //     return y;
      //     });
      //   }
      // );

      this.uniquePOs = Array.from(new Set(this.poHeader.map(m => m.PO_NUMBER)))
      //console.log(this.uniquePOs.length);
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }


  editLine(item) {

    this.selectedPO = item.DOCUMENT_NO;
    this.edit = true;

    this.beforeUpdateInvLine = JSON.parse(JSON.stringify(this.invoiceLines));

    this.setLinesList()

  }



  setLinesList() {
    let line = this.poHeaderAll.filter(f => f.PO_NUMBER === this.selectedPO);
    this.currPOlines = line ? line : [];
    if (line.length > 0) {
      this.invoiceMode = line[0].INVOICE_MODE;
    }
  }


  deleteLine(item) {

    // console.log('Delete Called',item)
    this.invoiceLines = this.invoiceLines.filter(f => f.LINE_LOCATION_ID !== item.LINE_LOCATION_ID);
  }


  checkValue(value, index) {
    //console.log(value,index);
    if (!value) {
      this.currPOlines[index].QUANTITY = undefined;




    } else {


      this.currPOlines[index].QUANTITY =   Number(this.currPOlines[index].DELIVERED_QTY) -    Number(this.currPOlines[index].BILLED_QUANTITY)
    }

  }


  checkQuantityAndUpdate(i) {
    if (Number(this.currPOlines[i].QUANTITY) > (Number(this.currPOlines[i].DELIVERED_QTY) - Number(this.currPOlines[i].BILLED_QUANTITY))) {

      this.notifier.notify('error', 'Invoice Qty Exceeds Delivered Qty');
      this.currPOlines[i].QUANTITY = 0;
    }

  }



  AddButtonClicked() {
    if (!this.edit) {
      let linesSelected = [];
      let selectedLineIds = [];



      linesSelected = this.currPOlines.filter(f => f.selected && Number(f.QUANTITY) > 0);

      selectedLineIds = linesSelected.map(m => m.LINE_LOCATION_ID);



      //console.log('LineSelected Length',linesSelected.length);
      this.invoiceLines.push(...linesSelected);
      if (this.invoiceLines, length > 0) {
        this.createInv.CURRENCY = this.invoiceLines[0].PO_CURRENCY;
        this.invoiceMode = this.invoiceLines[0].INVOICE_MODE;
      }
      //console.log('LEN',this.asnLines.length,'LEN!',this.asnLots.length);
    }
    else {
      // this.edit= false;
      let selectedLineIds = [];
      this.invoiceLines = this.invoiceLines.filter(f => f.selected && Number(f.QUANTITY) > 0);

      selectedLineIds = this.invoiceLines.map(m => m.LINE_LOCATION_ID);

      if (this.invoiceLines, length > 0) {
        this.createInv.CURRENCY = this.invoiceLines[0].PO_CURRENCY;
        this.invoiceMode = this.invoiceLines[0].INVOICE_MODE;
      }


      // console.log('LEN',this.asnLines.length,'LEN!',this.asnLots.length);
    }

  }

  onModelClose() {
    //  console.log('Called');
    this.invoiceLines = this.beforeUpdateInvLine;
  }
  AddLineCalled() {

    this.selectedPO = '';
    this.currPOlines = [];

    this.edit = false;
    this.beforeUpdateInvLine = JSON.parse(JSON.stringify(this.invoiceLines));

    let invLines = [];
    if (this.invoiceLines.length > 0) {
      invLines = this.poHeaderAll.filter(f => f.INVOICE_MODE === this.invoiceLines[0].INVOICE_MODE && f.BUYER_UNIT_ID === this.invoiceLines[0].BUYER_UNIT_ID && f.VENDOR_SITE_ID === this.invoiceLines[0].VENDOR_SITE_ID && f.PO_CURRENCY === this.invoiceLines[0].PO_CURRENCY)

      this.uniquePOs = Array.from(new Set(invLines.map(m => m.PO_NUMBER)));
      //console.log(this.uniquePOs.length);
    }


  }


  onFilesAdded(fileInput: any) {
    // this.spinner.show();
    this.fileData = fileInput.target.files as File;
    //const length = fileInput.target.files.length;
    // this.length = fileInput.target.files.length;
    const file = new FormData();
    for (let i = 0; i < 1; i++) {
      file.append('file', this.fileData[0], this.fileData[0].name);
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData[0]);
    reader.onload = () => {
      this.file64 = reader.result;
    };
    // this.createInv.FILE_NAME = this.fileData[0].name;
    this.loginService.FileUpload(file).subscribe((data) => {
      this.spinner.hide();
      let i;
      this.createInv.FILE_NAME = this.fileData[0].name;
    }, err => {
      this.spinner.hide();
      this.resetFile();
      if (err.error.message) {

      } else {
      }
    });

  }

  resetFile() {
    this.myInputVariable.nativeElement.value = '';

  }

  cancel() {

    this.resetFile();
  }




  saveInvoice() {

    //console.log(this.asnHeaders,this.asnLines,this.asnLots,this.email);
    let emailList = [];
    let uniqueEmailList = [];
    // emailList = this.asnLines.map(m=>m.BUYER_EMAIL);
    // uniqueEmailList =Array.from(new Set(emailList));

    //console.log(uniqueEmailList);
    let query = {
      INVOICE: {
        HEADER: { ...this.createInv, USER_NAME: this.email },
        LINES: {
          LINE: [...this.invoiceLines.map((m, i) => {
            return {
              LINE_NUMBER: '' + (i + 1),
              PO_HEADER_ID: m.PO_HEADER_ID,
              PO_LINE_ID: m.PO_LINE_ID,
              LINE_LOCATION_ID: m.LINE_LOCATION_ID,
              RCV_TRANSACTION_ID: m.RCV_TRANSACTION_ID,
              ACTUAL_DELIVERY_DATE: m.ACTUAL_DELIVERY_DATE,
              QUANTITY: m.QUANTITY,
              LINE_PRICE: m.PRICE
            }
          })]
        }, FILE_NAME: this.createInv.FILE_NAME
      }
    };

    this.spinner.show()
    this.purchaseService.createInvoice(query).subscribe((data: any) => {

      // console.log(data);
      this.spinner.hide();
      this.reset();
      this.notifier.notify('success', 'Submitted Sucessfully');
      setTimeout(() => {
        this.redirectTo(this.router.url);
      }, 2000);

      // this.asn_id = data;

      //console.log('ASN_ID',this.asn_id);
      //       this.makePdf(emailList,`ASN ${this.asn_id} has been generated successfully generated by Supplier ${this.supplierName}`,`
      //       Dear Team,

      // ASN ${this.asn_id} has been generated successfully generated by Supplier ${this.supplierName}.

      // Please find attached ASN document for further details.

      // Regards,
      // ${this.supplierName}

      //       `,data);

    }, error => {

      console.log(error);
    })
    console.log(query);
  }


  reset() {

    this.selectedPO = '';
    this.poHeaderAll = [];
    this.poHeader = [];
    this.uniquePOs = [];
    this.createInv = new Invoice();
    this.invoiceLines = [];

  (function ($, thi) {
    $('#datepicker2').datepicker({
      uiLibrary: 'bootstrap4',
      format: 'dd-mmm-yyyy',
      // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
      value: thi.createInv.INVOICE_DATE,
      change(e) {
        thi.createInv.INVOICE_DATE = e.target.value;
      }
    });
  })(jQuery, this);
    this.getPendingPurchase();

  }
  redirectTo(uri) {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this.router.navigateByUrl(uri, {skipLocationChange : profileData.hideURL}));
  }

  // setDatePicker(){
  //   //this.showHide = false;
  //   let setDate1 = (i,value)=>{
  //   //  console.log('CAlled Date 1',i,value)
  //     this.currPOlines[i].ACTUAL_DELIVERY_DATE = value;
  //   }


  //   this.chRef.detectChanges();
  //   this.lotsEntered.forEach((f,i)=>{
  //     (function($,i,setDate1,setDate2){
  //  //     console.log(i,"Called");
  //       $("#mfgDate"+i).datepicker({
  //         uiLibrary: 'bootstrap4',
  //         format: 'dd-mmm-yyyy',
  //         value: f.MFG_DATE,
  //         change(e) {
  //           setDate1(i, e.target.value);
  //         }
  //     });
  //     $("#expDate"+i).datepicker({
  //       uiLibrary: 'bootstrap4',
  //       format: 'dd-mmm-yyyy',
  //       value: f.EXPIRATION_DATE,
  //       change(e) {


  //         setDate2(i, e.target.value);
  //       }
  //   });

  //     })(jQuery,i,setDate1,setDate2);
  //   })

  // }

}


class Invoice {
  INVOICE_NUMBER = ''
  INVOICE_DATE = ''
  CURRENCY = ''
  DESCRIPTION = ''
  INVOICE_AMOUNT = ''
  INVOICE_ID = ''
  REASON_DESCRIPTION = ''
  ADDITIONAL_NOTES = ''
  USER_NAME = ''
  FILE_NAME = ''

}
