import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { EmailService } from '../../auth/service/email.service';
import { LoginService } from '../../auth/service/login.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
declare var jQuery: any;

@Component({
  selector: 'app-upload-invoice',
  templateUrl: './upload-invoice.component.html',
  styleUrls: ['./upload-invoice.component.css']
})
export class UploadInvoiceComponent implements OnInit {

  public email;
  public supplierName;
  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();
  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
  public file;
  fileData: File = null;
  public file64;
  public uploadInv: UploadInvoice;
  public uniquePos = [];
  public units = [];
  public showDoc = [];
  public showDocAll = [];
  public currencyList = [];
  public searchText;
  public length;
  public accountType;
  public showHide = true;

  private notifier: NotifierService;
  constructor( public emailService: EmailService, private router: Router, public breadCrumbServices: BreadcrumbService, private purchaseService: PurchaseService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService,
               private rsaService: RsaService, private notifierService: NotifierService, private loginService: LoginService) {
      this.notifier = notifierService; ;
    }
 @ViewChild('myFileInput', { static: false }) myInputVariable: ElementRef;
  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.accountType = this.rsaService.decrypt(localStorage.getItem('6'));
    // this.accountType = 'SUPPLIER';
    this.uploadInv = new UploadInvoice();

    (function($, thi) {

      $(document).ready(function() {
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        const today = new Date();
        const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' + months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();

        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
          value: thi.fromDate,
          change(e) {
            localStorage.setItem('LDF', e.target.value);

            thi.fromDate = e.target.value;
          }

        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
          value: thi.toDate,
          change(e) {
           thi.toDate = e.target.value;
          }
        });
      });

    })(jQuery, this);
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/finance/uploadinvoice') {
        this.showHideTable();
      }
    });

    this.getSelectValues();
    this.getPastPurchases();
    this.getCurrencyList();
  }

  setAsDataTable() {

    this.chRef.detectChanges();
    (function($) {
      $('#Pending-table').DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        pageLength: 5,
      });
    })(jQuery);

  }


  getCurrencyList(){
    // const details = { email: this.email, purchaseType: 'ALL_CURRENCIES' };
    const details = this.purchaseService.inputJSON('ALL_CURRENCIES', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data:any)=>{
      // console.log(
      //   'data',data
      // );
      this.currencyList = Array.isArray(data.FREIGHT_TERMS.FREIGHT_TERM) ? data.FREIGHT_TERMS.FREIGHT_TERM : (data.FREIGHT_TERMS.FREIGHT_TERM) ? [data.FREIGHT_TERMS.FREIGHT_TERM] : [];
    },error=>{
      console.log(error);
    })
  }

  getPastPurchases() {
    this.spinner.show();
    // this.fromDate = localStorage.getItem('PODF');
    // this.toDate = localStorage.getItem('PODT');
    //console.log(this.fromDate,this.toDate);
    // const details = { email: this.email, purchaseType: 'INVOICE_DOCUMENTS', date1: this.fromDate, date2: this.toDate };
    const details = this.purchaseService.inputJSON('INVOICE_DOCUMENTS', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      //console.log('ppp', data);
      this.spinner.hide();
      if (data) {
        this.showDocAll = Array.isArray(data.UPLOADED_INVOICES.UPLOADED_INVOICE) ? data.UPLOADED_INVOICES.UPLOADED_INVOICE : (data.UPLOADED_INVOICES.UPLOADED_INVOICE) ? [data.UPLOADED_INVOICES.UPLOADED_INVOICE] : [];

        this.showHideTable();
        //this.setAsDataTable();
        // this.fullpoHeaderList = Array.isArray(data.PASTPO.POHEADERS.POHEADER) ? data.PASTPO.POHEADERS.POHEADER : (data.PASTPO.POHEADERS.POHEADER) ? [data.PASTPO.POHEADERS.POHEADER] : [];
        // this.fullpolines = Array.isArray(data.PASTPO.POLINES.POLINE) ? data.PASTPO.POLINES.POLINE : (data.PASTPO.POHEADERS.POHEADER) ? [(data.PASTPO.POHEADERS.POHEADER)] : [];
        // this.fullSummary = Array.isArray(data.PASTPO.POSUMMARIES.POSUMMARY) ? data.PASTPO.POSUMMARIES.POSUMMARY : (data.PASTPO.POSUMMARIES.POSUMMARY) ? [data.PASTPO.POSUMMARIES.POSUMMARY] : [];
        // this.summary = this.fullSummary;

        // this.poCountsAll = Array.isArray(data.PASTPO.POCOUNTS.POCOUNT) ? data.PASTPO.POCOUNTS.POCOUNT : (data.PASTPO.POCOUNTS.POCOUNT) ? [data.PASTPO.POCOUNTS.POCOUNT] : [];
        // this.poCounts = this.poCountsAll;

        // this.setPoSummary();
        // this.setFilteredData();
        // this.setSummaryData();
      } else {
      }

    }, error => {
      this.spinner.hide();
      console.log(error);
    });



  }

  getPOList() {

    this.spinner.show();
    // const details = { email: this.email, purchaseType: this.uploadInv.BUYER_UNIT };
    const details = this.purchaseService.inputJSON(this.uploadInv.BUYER_UNIT, undefined, undefined)
    this.purchaseService.getPurchaseOrderList(details).subscribe((data: any) => {
      this.spinner.hide();
      if (data) {
        this.uniquePos = Array.isArray(data.PO_LISTS.PO_LIST) ? data.PO_LISTS.PO_LIST : (data.PO_LISTS.PO_LIST) ? [data.PO_LISTS.PO_LIST] : [];
        // this.fullpoHeaderList = Array.isArray(data.PASTPO.POHEADERS.POHEADER) ? data.PASTPO.POHEADERS.POHEADER : (data.PASTPO.POHEADERS.POHEADER) ? [data.PASTPO.POHEADERS.POHEADER] : [];
        // this.fullpolines = Array.isArray(data.PASTPO.POLINES.POLINE) ? data.PASTPO.POLINES.POLINE : (data.PASTPO.POHEADERS.POHEADER) ? [(data.PASTPO.POHEADERS.POHEADER)] : [];
        // this.fullSummary = Array.isArray(data.PASTPO.POSUMMARIES.POSUMMARY) ? data.PASTPO.POSUMMARIES.POSUMMARY : (data.PASTPO.POSUMMARIES.POSUMMARY) ? [data.PASTPO.POSUMMARIES.POSUMMARY] : [];
        // this.summary = this.fullSummary;

        // this.poCountsAll = Array.isArray(data.PASTPO.POCOUNTS.POCOUNT) ? data.PASTPO.POCOUNTS.POCOUNT : (data.PASTPO.POCOUNTS.POCOUNT) ? [data.PASTPO.POCOUNTS.POCOUNT] : [];
        // this.poCounts = this.poCountsAll;

        // this.setPoSummary();
        // this.setFilteredData();
        // this.setSummaryData();
      } else {
      }

    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }

  onFilesAdded(fileInput: any) {
    // this.spinner.show();
    this.fileData = fileInput.target.files as File;
    const length = fileInput.target.files.length;
    this.length = fileInput.target.files.length;
    const file = new FormData();
    for (let i = 0; i < length; i++) {
      file.append('file', this.fileData[0], this.fileData[0].name);
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData[0]);
    reader.onload = () => {
      this.file64 = reader.result;
    };
    this.loginService.FileUpload(file).subscribe((data) => {
      this.spinner.hide();
      let i;
      this.uploadInv.FILE_NAME = this.fileData[0].name;
    }, err => {
      this.spinner.hide();
      this.resetFile();
      if (err.error.message) {

      } else {
      }
    });

  }

  resetFile() {
    this.myInputVariable.nativeElement.value = '';
    this.length = 0;
  }

  cancel() {

    this.resetFile();
  }
  setPo(value) {
    const a = this.uniquePos.filter(f => f.PO_NUMBER === value);
    if (a.length === 1) {
  this.uploadInv.PO_NUMBER = value;
  this.setCurrency(a);
}
  }
  setCurrency(a) {
    this.uploadInv.CURRENCY_CODE = a[0].CURRENCY_CODE;
    this.supplierName = a[0].VENDOR_NAME;
    //console.log('ccc', this.uploadInv.CURRENCY_CODE);
  }


  fileUpload() {
    let toMail = '';
    if (['GP1 OU', 'GPL OU'].includes( this.uploadInv.BUYER_UNIT)) {
      toMail = 'hemavathi.raman@ytygroup.com.my';

    }
    if (['YTY OU', 'YTA OU'].includes(this.uploadInv.BUYER_UNIT)) {
      toMail = 'narjeet.singh@ytygroup.com.my';
    }
    this.spinner.show();
// &lt;Invoice Number&gt;
// &lt;PO Number&gt;
    this.emailService.sendMailWithAttachments(toMail, `Craftsman Automation Supply: Invoice uploaded by ${this.supplierName} against PO ${this.uploadInv.PO_NUMBER}`, `<p>Dear Invoice Process Team,</p>
    <p>We have uploaded the invoice ${this.uploadInv.INVOICE_NUMBER}  against the Purchase Order ${this.uploadInv.PO_NUMBER} and the details are given below.</p>
    <table border="1">
    <tbody>
    <tr style="height: 35px;">
    <td style="height: 35px; width: 160px;">
    <p><strong>Buyer Unit</strong></p>
    </td>
    <td style="height: 35px; width: 441px;">
    <p>${this.uploadInv.BUYER_UNIT}</p>
    </td>
    </tr>
    <tr style="height: 35px;">
    <td style="height: 35px; width: 160px;">
    <p><strong>Purchase Order</strong></p>
    </td>
    <td style="height: 35px; width: 441px;">
    <p>${this.uploadInv.PO_NUMBER}</p>
    </td>
    </tr>
    <tr style="height: 35px;">
    <td style="height: 35px; width: 160px;">
    <p><strong>Invoice Number</strong></p>
    </td>
    <td style="height: 35px; width: 441px;">
    <p>${this.uploadInv.INVOICE_NUMBER}</p>
    </td>
    </tr>
    <tr style="height: 35px;">
    <td style="height: 35px; width: 160px;">
    <p><strong>Invoice Date</strong></p>
    </td>
    <td style="height: 35px; width: 441px;">
    <p>${this.uploadInv.INVOICE_DATE}</p>
    </td>
    </tr>
    <tr style="height: 35px;">
    <td style="height: 35px; width: 160px;">
    <p><strong>Currency</strong></p>
    </td>
    <td style="height: 35px; width: 441px;">
    <p>${this.uploadInv.CURRENCY_CODE}</p>
    </td>
    </tr>
    <tr style="height: 35px;">
    <td style="height: 35px; width: 160px;">
    <p><strong>Amount</strong></p>
    </td>
    <td style="height: 35px; width: 441px;">
    <p>${this.uploadInv.INVOICE_AMOUNT}</p>
    </td>
    </tr>
    <tr style="height: 35px;">
    <td style="height: 35px; width: 160px;">
    <p><strong>Description</strong></p>
    </td>
    <td style="height: 35px; width: 441px;">
    <p>${this.uploadInv.INVOICE_DESCRIPTION}</p>
    </td>
    </tr>
    </tbody>
    </table>
    <p>&nbsp;</p>
    <p>Best Regards,</p>
    <p>${this.supplierName}</p>
    <p>This is an auto generated email. Please do not reply to this email.</p>`, this.fileData[0].name, this.file64).
    subscribe(message => {
      // this.spinner.hide();
      if (message === 'OK') {
        const id = 1;
        // this.router.navigate(['/auth/view'], { queryParams: { id } });
        // this.text = false;
        // this.email = '';
      } else {
        // this.notifier.notify('warning', 'Email Send Failed');
      }
    },error=>{
      console.log(error);
    });
    this.uploadInv.USER_NAME = this.email;
    this.purchaseService.saveInvoice({INVOICE: this.uploadInv}).subscribe(data => {
      this.spinner.hide();
      this.resetPage();
      this.getPastPurchases();
    }, error => {
      console.log(error);
    });

  }

getSelectValues() {
 this.units = Array.from(new Set(this.breadCrumbServices.selectValues.map(x => x.ERP_ORGANIZATION_NAME))); // this.breadCrumService.selectValues
}

resetPage() {
  this.uploadInv = new UploadInvoice();
  this.resetFile();
}

onOpenModel() {
  this.uploadInv = new UploadInvoice();
  this.resetFile();
  (function($, thi) {
  $('#datepicker2').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'dd-mmm-yyyy',
    // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
    value: thi.uploadInv.INVOICE_DATE,
    change(e) {
      thi.uploadInv.INVOICE_DATE = e.target.value;
    }
  });
})(jQuery, this);
}

getInvoice(){
  if(this.uploadInv.INVOICE_MODE === 'direct'){
    this.uploadInv.PO_NUMBER = '';
    this.uploadInv.CURRENCY_CODE = '';
  }
  // console.log('fff', this.uploadInv.INVOICE_MODE, (!this.uploadInv.BUYER_UNIT && (this.uploadInv.INVOICE_MODE ==='direct')));
}


showHideTable() {
  this.showHide = false;
  setTimeout(() => {
    this.showHide = true;
    this.setFilteredData();
  }, 10);
}


setFilteredData() {

  this.showDoc = this.showDocAll.filter(f => {
    let a = true;

    if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
      a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

    }
    // if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
    //   a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
    // }
    // if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
    //   a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
    // }
    if (this.searchText) {
      a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
    }
    return a;
  });

  this.setAsDataTable();
  // this.showHideTable();
}
}

class UploadInvoice {
  BUYER_UNIT = '';
  PO_NUMBER = '';
  INVOICE_NUMBER = '';
  INVOICE_DATE = '';
  CURRENCY_CODE = '';
  INVOICE_AMOUNT = '';
  INVOICE_DESCRIPTION = '';
  FILE_NAME = '';
  USER_NAME = '';
  SUBJECT = '';
  BODY = '';
  TO = '';
  INVOICE_MODE = '';
}
