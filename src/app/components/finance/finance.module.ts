import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';

import { FinanceRoutingModule } from './finance-routing.module';
import { InvoiceRegisterComponent } from './invoice-register/invoice-register.component';
import {PaymentRegisterComponent} from './payment-register/payment-register.component';
import { AdvancePaymentComponent } from './advance-payment/advance-payment.component';
import { MyLedgerComponent } from './my-ledger/my-ledger.component';
import { CommonPaymentViewComponent } from './common-payment-view/common-payment-view.component';
import { UploadInvoiceComponent } from './upload-invoice/upload-invoice.component';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { FileViewComponent } from './file-view/file-view.component';
import { CreateInvoiceComponent } from './create-invoice/create-invoice.component';
// import { NumberFormattingPipe} from '../../shared/service/number-formatting.pipe'
import { SharedModule } from 'src/app/shared/shared.module';

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'middle',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 99,
      gap: 15
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 1200,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 800,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 1000,
      easing: 'ease',
      offset: 150
    },
    shift: {
      speed: 500,
      easing: 'ease'
    },
    overlap: 90
  }
};
@NgModule({
  declarations: [InvoiceRegisterComponent, PaymentRegisterComponent, AdvancePaymentComponent, MyLedgerComponent,CommonPaymentViewComponent, UploadInvoiceComponent, FileViewComponent, CreateInvoiceComponent],
  imports: [
    CommonModule,
    FinanceRoutingModule,
    FormsModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxPaginationModule,
    NgxSpinnerModule,
    SharedModule
  ]
})
export class FinanceModule { }
