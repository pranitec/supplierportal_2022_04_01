import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { FinanceService } from '../service/finance.service';
import { Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { NotifierService } from 'angular-notifier';

declare var jQuery: any;

@Component({
  selector: 'app-my-ledger',
  templateUrl: './my-ledger.component.html',
  styleUrls: ['./my-ledger.component.css']
})
export class MyLedgerComponent implements OnInit {

  public email;
  public ledgerHeaderAll = [];
  public ledgerHeader = [];
  public ledgerLineAll = [];
  public ledgerLine = [];
  public searchText = '';
  public closingBal = 0;
  public openingBal = 0;
  public debit = 0;
  public credit = 0;
  public showHide = true;
  public ledgerLength;
  public viewCount= 10;
  public page =1

  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();
  // public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

  public selectedmenu;
  public selectedmenuFilter =[];
  public oneyearstartingDate : Date;
  private notifier: NotifierService;
  public noOfMonths;

  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();


  constructor(private financeService: FinanceService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService,
    private breadCrumbServices: BreadcrumbService, private router: Router, private rsaService: RsaService,
    private purchaseService : PurchaseService, private notifierService: NotifierService) {
      this.notifier = notifierService;
    }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    localStorage.setItem('LDF', this.fromDate);
    localStorage.setItem('LDT', this.toDate);
    this.selectedmenu= this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
    this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(({MENUACCESS})=> MENUACCESS.MENU_CODE === 'SUPPLIER_LEDGER');
    this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START)
    this.noOfMonths  = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE /31)
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/finance/myLedger') {
        this.showHideTable();
        this.setHeader();
      }
    });
    let i=0
    setTimeout(() => {
      this.getMyLedger();
    }, 100);

    (function ($, _this) {

      $(document).ready(function () {
        // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        // const today = new Date();
        // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' + months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();

        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
          value: _this.fromDate,
          minDate : _this.oneyearstartingDate,
          change(e) {
            localStorage.setItem('LDF', e.target.value);
            _this.fromDate = e.target.value;
          }
        });

        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
         value: _this.toDate,
          maxDate: new Date(),
          change(e) {
            localStorage.setItem('LDT', e.target.value);
            _this.toDate = e.target.value
          }
        });

      });
    })(jQuery, this);
  }

  getMyLedger() {
    this.fromDate = localStorage.getItem('LDF');
    this.toDate = localStorage.getItem('LDT');
    this.spinner.show();
    // const details = { email: this.email, financeType: 'LEDGER_DETAILS', date1: this.fromDate, date2: this.toDate };
    // this.financeService.getFinance(details).subscribe((data: any) => {

      var time_difference =  Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
    var months_difference =  Math.floor(time_difference / (1000 * 60 * 60 * 24)/31);

    if((-(months_difference)) >this.noOfMonths){
      // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
      this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
      this.spinner.hide();
    } else{
    var query = this.purchaseService.inputJSON('LEDGER_DETAILS', this.fromDate, this.toDate)
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      this.spinner.hide();
      if(data){

      // this.ledgerHeaderAll = Array.isArray(data.LEDGERS.LEDGER_HEADERS.LEDGER_HEADER) ?
      //   data.LEDGERS.LEDGER_HEADERS.LEDGER_HEADER : (data.LEDGERS.LEDGER_HEADERS.LEDGER_HEADER) ?
      //     [data.LEDGERS.LEDGER_HEADERS.LEDGER_HEADER] : [];
      // this.ledgerLineAll = Array.isArray(data.LEDGERS.LEDGER_LINES.LEDGER_LINE) ?
      //   data.LEDGERS.LEDGER_LINES.LEDGER_LINE : (data.LEDGERS.LEDGER_LINES.LEDGER_LINE) ? [data.LEDGERS.LEDGER_LINES.LEDGER_LINE] : [];
      this.ledgerHeaderAll = data.LEDGERS.LEDGER_HEADERS? data.LEDGERS.LEDGER_HEADERS.map(x=>(x.LEDGER_HEADER)) :[]
      this.ledgerLineAll = data.LEDGERS.LEDGER_LINES? data.LEDGERS.LEDGER_LINES.map(x=>(x.LEDGER_LINE)) :[]
      this.ledgerHeader = this.ledgerHeaderAll;
      this.ledgerLine = this.ledgerLineAll;
      this.ledgerLength =this.ledgerHeaderAll ?this.ledgerHeaderAll.length :0
      //  this.setHeader();
      //  this.setFilteredData();
      this.setHeaderData();
      //  this.setAsDataTable();
      this.showHideTable();
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
  }

  setFilteredData() {

    this.ledgerLine = this.ledgerLineAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }
      return a;
    });

    this.setAsDataTable();
    // this.showHideTable();
  }

  setHeader() {
    this.ledgerHeader = this.ledgerHeaderAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;
    });
    this.setHeaderData();
  }

  setAsDataTable() {

    this.chRef.detectChanges();

    (function ($) {

      $("#Pending-table").DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        sorting : false,
        paging : false,
        info : false,
        // pageLength: 10,
      });
    })(jQuery);

  }

  setHeaderData() {
    let closingBal = 0, openingBal = 0, credit = 0, debit = 0;

    this.ledgerHeader.forEach(f => {
      closingBal = closingBal + Number(f.CLOSING);
      openingBal = openingBal + Number(f.OPENING);
      credit = credit + Number(f.CREDIT);
      debit = debit + Number(f.DEBIT);
    });
    this.closingBal = closingBal;
    this.openingBal = openingBal;
    this.credit = credit;
    this.debit = debit;
  }

  showHideTable() {
    this.showHide = false;
    setTimeout(() => {
      this.showHide = true;
      this.setFilteredData();
    }, 100);
  }

  exportExcel() {
    const details = { POLINES: this.ledgerLineAll, title: 'Ledger Report' };
    this.financeService.getLedgerExcel(details);

  }

  public onPageChanged(event) {
    this.page = event;
  }

}
