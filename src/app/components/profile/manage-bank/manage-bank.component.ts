import { Component, OnInit } from '@angular/core';
import { BankAccounts } from '../../auth/models/bank-accounts';
import { ProfileService } from '../service/profile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { Router } from '@angular/router';
import { PurchaseService } from '../../purchase/service/purchase.service';

@Component({
  selector: 'app-manage-bank',
  templateUrl: './manage-bank.component.html',
  styleUrls: ['./manage-bank.component.css']
})
export class ManageBankComponent implements OnInit {

  public email;
  public VENDOR_ID;
  public vendorBankAll = [];
  public vendorBanks = [];
  public bank: BankAccounts;

  constructor(private profileService: ProfileService, private spinner: NgxSpinnerService, private purchaseService : PurchaseService,
              private breadCrumbServices: BreadcrumbService, private rsaService: RsaService, private router: Router) { }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.getVendorId();
    this.bank = new BankAccounts();
    this.getProfileBank();
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/profile/managebank') {
        this.setFilteredData();
      }
    });
    this.spinner.show();
  }

  getVendorId() {
    const vendorID = JSON.parse(this.rsaService.decrypt(localStorage.getItem('USERACCESS')));
    if (vendorID.length > 0) {
    this.VENDOR_ID = vendorID[0].VENDOR_ID;
    }
  }

  getProfileBank() {
    // const details = { email: this.email, purchaseType: 'VENDOR_BANKS' };
    // this.profileService.getProfile(details).subscribe((data: any) => {
       var query = this.purchaseService.inputJSON('VENDOR_BANKS', undefined, undefined)
      this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      this.spinner.hide();
      // console.log("getProfileBank : ", data);
      // this.vendorBankAll =  Array.isArray(data.VENDOR_BANKS.VENDOR_BANK) ? data.VENDOR_BANKS.VENDOR_BANK :
      // (data.VENDOR_BANKS.VENDOR_BANK) ? [data.VENDOR_BANKS.VENDOR_BANK] : [];
      this.vendorBankAll = data ? data.VENDOR_BANKS.map(x =>(x.VENDOR_BANK)) :[]
      this.vendorBanks = this.vendorBankAll;
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  setFilteredData() {

    // this.vendorBanks = this.vendorBankAll.filter(f => {
    //   let a = true;

    //   if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
    //     a = a && this.breadCrumbServices.select1 === f.ERP_ORGANIZATION_NAME;

    //   }
    //   if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
    //     a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
    //   }
    //   if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
    //     a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
    //   }
    //   console.log("aaaaa : ", a)
    //   return a;

    // });
    this.vendorBanks = this.vendorBankAll.filter(f => {
      let a = true;
      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.UNIT;
      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      return a;
    });

  }

  getColor(value) {
    switch (value) {
      case 'Active':
        return 'act';
        break;

      case 'Inactive':
        return 'inact';
        break;
    }
  }

  addBank() {
    this.bank = new BankAccounts();
    // this.bank.intermidiate = true;
  }

  editBank(element) {
    this.bank = element;
    this.bank.intermidiate = (element.ADD_INTERMIDIATE_BANK === 'YES');

  }

  addAccount() {

    this.spinner.show();
    const element = this.bank;
    const detail = {type: 'BANK', ACTION: 'CREATE', details: {
      VENDOR_ID: element.VENDOR_ID, COUNTRY: element.COUNTRY,
      BANK: element.BANK_NAME, BRANCH_NUMBER: element.BANK_BRANCH_NUMBER, BRANCH_NAME: element.BANK_BRANCH_NAME,
      BIC: element.BIC, ACCOUNT_NUMBER: element.BANK_ACCOUNT_NUM, ACCOUNT_NAME: element.BANK_ACCOUNT_NAME,
      ACCOUNT_TYPE: element.BANK_ACCOUNT_TYPE, IBAN: element.IBAN, CURRENCY: element.CURRENCY, ADD_INTERMIDIATE_BANK: element.ADD_INTERMIDIATE_BANK,
      INT_COUNTRY: element.INT_BANK_COUNTRY, INT_BANK: element.INT_BANK, INT_BRANCH_NUMBER: element.INT_BANK_BRANCH_NUMBER,
      INT_BRANCH_NAME: element.INT_BANK_BRANCH_NAME, INT_BIC: element.INT_BIC, INT_ACCOUNT_NUMBER: element.INT_BANK_ACCOUNT_NUMBER,
      INT_ACCOUNT_NAME: element.INT_BANK_ACCOUNT_NAME, INT_ACCOUNT_TYPE: element.INT_BANK_ACCOUNT_TYPE,
      INT_IBAN: element.INT_IBAN, INT_CURRENCY: element.INT_CURRENCY
    }};
    this.profileService.editProfile(detail).subscribe((data: any) => {
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });


  }

  changeIntermediate() {
    this.bank.ADD_INTERMIDIATE_BANK = this.bank.intermidiate ? 'YES' : 'NO';
  }

  updateAccount() {

    this.spinner.show();
    const element = this.bank;
    const detail = {type: 'BANK', ACTION: 'EDIT', details: {
      VENDOR_ID: element.VENDOR_ID, VENDOR_SITE_ID: element.VENDOR_SITE_ID, COUNTRY: element.COUNTRY,
      BANK: element.BANK_NAME, BRANCH_NUMBER: element.BANK_BRANCH_NUMBER, BRANCH_NAME: element.BANK_BRANCH_NAME,
      BIC: element.BIC, ACCOUNT_NUMBER: element.BANK_ACCOUNT_NUM, ACCOUNT_NAME: element.BANK_ACCOUNT_NAME,
      ACCOUNT_TYPE: element.BANK_ACCOUNT_TYPE, IBAN: element.IBAN, CURRENCY: element.CURRENCY, ADD_INTERMIDIATE_BANK: element.ADD_INTERMIDIATE_BANK,
      INT_COUNTRY: element.INT_BANK_COUNTRY, INT_BANK: element.INT_BANK, INT_BRANCH_NUMBER: element.INT_BANK_BRANCH_NUMBER,
      INT_BRANCH_NAME: element.INT_BANK_BRANCH_NAME, INT_BIC: element.INT_BIC, INT_ACCOUNT_NUMBER: element.INT_BANK_ACCOUNT_NUMBER,
      INT_ACCOUNT_NAME: element.INT_BANK_ACCOUNT_NAME, INT_ACCOUNT_TYPE: element.INT_BANK_ACCOUNT_TYPE,
      INT_IBAN: element.INT_IBAN, INT_CURRENCY: element.INT_CURRENCY
    }};
    this.profileService.editProfile(detail).subscribe((data: any) => {
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });

  }

}
