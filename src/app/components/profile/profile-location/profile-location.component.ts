import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { ProfileService } from '../service/profile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Location } from '../model/location';
import { PurchaseService } from '../../purchase/service/purchase.service';


@Component({
  selector: 'app-profile-location',
  templateUrl: './profile-location.component.html',
  styleUrls: ['./profile-location.component.css']
})
export class ProfileLocationComponent implements OnInit {
  public email;
  public VENDOR_ID;
  public vendorSitesAll = [];
  public vendorSites = [];
  public location: Location;

  constructor(private profileService: ProfileService, private spinner: NgxSpinnerService, private purchaseService: PurchaseService,
    private breadCrumbServices: BreadcrumbService, private rsaService: RsaService, private router: Router) { }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.getVendorId();
    this.location = new Location();
    this.getProfileLocation();
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/profile/profileLocation') {
        this.setFilteredData();
      }
    });
    this.spinner.show();
  }

  getVendorId(){
    let vendorID = JSON.parse(this.rsaService.decrypt(localStorage.getItem('USERACCESS')));
    if (vendorID.length>0){
    this.VENDOR_ID = vendorID[0].VENDOR_ID;
    }
  }

  getProfileLocation() {
    // const details = { email: this.email, purchaseType: 'VENDOR_SITES' };
    // this.profileService.getProfile(details).subscribe((data: any) => {
      var query = this.purchaseService.inputJSON('VENDOR_SITES', undefined, undefined)
      this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      this.spinner.hide();
      this.vendorSitesAll = data.VENDOR_SITES.map(x =>(x.VENDOR_SITE));
      this.vendorSites = this.vendorSitesAll;
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  setFilteredData() {
    this.vendorSites = this.vendorSitesAll.filter(f => {
      let a = true;
      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.UNIT;
      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      return a;
    });

  }

  getColor(value) {
    switch (value) {
      case 'Active':
        return 'act';
        break;

      case 'Inactive':
        return 'inact';
        break;

    }
  }

  addLocation() {
    this.location = new Location();
   }

  editLocation(element) {
       this.location = element;
     }

  updateLocation() {
    this.spinner.show();
    let element = this.location;
    const detail = {type: 'LOCATION', ACTION: 'EDIT', details: {
      LOCATION_NAME: element.STATE, COUNTRY: element.COUNTRY, ADDRESS_LINE_1: element.ADDRESS_LINE1,
      ADDRESS_LINE_2: element.ADDRESS_LINE2, CITY: element.CITY, STATE: element.STATE, POSTAL_CODE: element.POSTAL_CODE,
      PHONE: element.PHONE, FAX: element.FAX, VENDOR_ID: element.VENDOR_ID, VENDOR_SITE_ID: element.VENDOR_SITE_ID
    }};
    this.profileService.editProfile(detail).subscribe((data: any) => {
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });

  }

  saveLocation(){

    this.spinner.show();
    let element = this.location;
    const details = {type: 'LOCATION', ACTION: 'CREATE', details: {
      LOCATION_NAME: element.STATE, COUNTRY: element.COUNTRY, ADDRESS_LINE_1: element.ADDRESS_LINE1,
      ADDRESS_LINE_2: element.ADDRESS_LINE2, CITY: element.CITY, STATE: element.STATE, POSTAL_CODE: element.POSTAL_CODE,
      PHONE: element.PHONE, FAX: element.FAX, VENDOR_ID: this.VENDOR_ID}};
    this.profileService.editProfile(details).subscribe((data: any) => {
      this.spinner.hide();

    }, err => {
      this.spinner.hide();
      console.log(err);
    });

  }

}
