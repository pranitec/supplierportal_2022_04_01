import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { ProfileService } from '../service/profile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Contact } from '../model/contact';
import { PurchaseService } from '../../purchase/service/purchase.service';

@Component({
  selector: 'app-profile-contact',
  templateUrl: './profile-contact.component.html',
  styleUrls: ['./profile-contact.component.css']
})
export class ProfileContactComponent implements OnInit {

  public email;
  public VENDOR_ID;
  public vendorContactsAll = [];
  public vendorContacts = [];
  public contact: Contact;

  constructor(private profileService: ProfileService, private spinner: NgxSpinnerService, private purchaseService: PurchaseService,
              private breadCrumbServices: BreadcrumbService, private rsaService: RsaService, private router: Router) { }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.getVendorId();
    this.contact = new Contact();
    this.getProfileContact();
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/profile/profileContact') {
        this.setFilteredData();
      }
    });
    this.spinner.show();
  }

  getVendorId() {
    const vendorID = JSON.parse(this.rsaService.decrypt(localStorage.getItem('USERACCESS')));
    if (vendorID.length > 0) {
    this.VENDOR_ID = vendorID[0].VENDOR_ID;
    }
  }

  getProfileContact() {
    // const details = { email: this.email, purchaseType: 'VENDOR_CONTACTS' };
    // this.profileService.getProfile(details).subscribe((data: any) => {
       var query = this.purchaseService.inputJSON('VENDOR_CONTACTS', undefined, undefined)
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      // console.log("getProfileContact", data)
      this.spinner.hide();
      this.vendorContactsAll = data.VENDOR_CONTACTS ? data.VENDOR_CONTACTS.map(x =>(x.VENDOR_CONTACT)) : [];
      this.vendorContacts = this.vendorContactsAll;
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  setFilteredData() {

    this.vendorContacts = this.vendorContactsAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.ERP_ORGANIZATION_NAME;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }

      return a;

    });

  }

  getColor(value) {
    switch (value) {
      case 'Active':
        return 'act';
        break;

      case 'Inactive':
        return 'inact';
        break;
    }
  }

  addContact() {
    this.contact = new Contact();
  }

  editContact(element) {
    this.contact = element;

  }

  saveContact() {
    this.spinner.show();
    const element = this.contact;
    const detail = {type: 'CONTACT', ACTION: 'CREATE', details: {
       VENDOR_ID: this.VENDOR_ID, SALUTATION: element.SALUTATION,
       FIRST_NAME: element.PERSON_FIRST_NAME, MIDDLE_NAME: element.MIDDLE_NAME, LAST_NAME: element.PERSON_LAST_NAME,
       JOB_TITLE: element.JOB_TITLE, PHONE: element.PHONE, FAX: element.FAX, EMAIL: element.EMAIL_ADDRESS
    }};
    this.profileService.editProfile(detail).subscribe((data: any) => {
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });

  }

  updateContact() {
    this.spinner.show();
    const element = this.contact;
    const detail = {type: 'CONTACT', ACTION: 'EDIT', details: {
       VENDOR_ID: element.VENDOR_ID, VENDOR_SITE_ID: element.VENDOR_SITE_ID, SALUTATION: element.SALUTATION,
       FIRST_NAME: element.PERSON_FIRST_NAME, MIDDLE_NAME: element.MIDDLE_NAME, LAST_NAME: element.PERSON_LAST_NAME,
       JOB_TITLE: element.JOB_TITLE, PHONE: element.PHONE, FAX: element.FAX, EMAIL: element.EMAIL_ADDRESS
    }};
    this.profileService.editProfile(detail).subscribe((data: any) => {
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });

  }


}
