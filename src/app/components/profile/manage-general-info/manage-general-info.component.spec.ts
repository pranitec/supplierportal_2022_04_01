import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageGeneralInfoComponent } from './manage-general-info.component';

describe('ManageGeneralInfoComponent', () => {
  let component: ManageGeneralInfoComponent;
  let fixture: ComponentFixture<ManageGeneralInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageGeneralInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageGeneralInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
