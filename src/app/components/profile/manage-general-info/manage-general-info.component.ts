import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { ProfileService } from '../service/profile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { Router } from '@angular/router';
import {Finance} from '../model/finance';
import {Employee} from '../model/employee';
import {Organization} from '../model/organization';
import {UploadFile} from '../model/upload-file';
import {LoginService} from '../../auth/service/login.service';
import {PurchaseService} from '../../purchase/service/purchase.service';
import { saveAs } from 'file-saver';
import {Tax } from '../model/tax'
import { CommonFileViewComponent} from '../../purchase/common-file-view/common-file-view.component'

@Component({
  selector: 'app-manage-general-info',
  templateUrl: './manage-general-info.component.html',
  styleUrls: ['./manage-general-info.component.css']
})
export class ManageGeneralInfoComponent implements OnInit {
  public email;
  public profile;
  public organisation: any = {};
  public employee: any = {};
  public attachments;
  public upload: UploadFile;
  fileData: File = null;
  public vendorId;
  public organization: Organization;
  public finance: Finance;
  public employees: Employee;
  public tax =[];
  public headers=[];
  public headersLength;
  constructor(private profileService: ProfileService, private spinner: NgxSpinnerService, private loginService: LoginService,
              private breadCrumbServices: BreadcrumbService, private rsaService: RsaService, private router: Router,
              private purchaseService: PurchaseService) { }

    @ViewChild('myFileInput', { static: false }) myInputVariable: ElementRef;

  ngOnInit(): void {
    this.upload = new UploadFile();
    this.finance = new Finance();
    this.organization = new Organization();
    this.employees = new Employee();
    // this.tax = new Tax();
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.employee = 'kalidas1998@gmail.com';
    this.getGeneralInfo();

    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/profile/generalInfo') {
        // this.setFilteredData();
      }
    });
    this.spinner.show();
  }

  getGeneralInfo() {
    // const details = { email: this.email, purchaseType: 'VENDOR_PROFILE' };
    // this.profileService.getProfile(details).subscribe((data: any) => {
      var query = this.purchaseService.inputJSON('VENDOR_PROFILE', undefined, undefined)
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      this.spinner.hide();
      if(data){
      // console.log("getGeneralInfo", data)
      // this.profile = data.VENDOR_PROFILE;
      // this.organization = Array.isArray(data.VENDOR_PROFILE.HEADERS.HEADER) ?
      //   data.VENDOR_PROFILE.HEADERS.HEADER[0] : (data.VENDOR_PROFILE.HEADERS.HEADER) ? data.VENDOR_PROFILE.HEADERS.HEADER : [];
      // this.employees = Array.isArray(data.VENDOR_PROFILE.EMPLOYEES.EMPLOYEE) ?
      //   data.VENDOR_PROFILE.EMPLOYEES.EMPLOYEE[0] : (data.VENDOR_PROFILE.EMPLOYEES.EMPLOYEE) ? data.VENDOR_PROFILE.EMPLOYEES.EMPLOYEE : [];
      // this.finance = Array.isArray(data.VENDOR_PROFILE.OTHERS.OTHER) ?
      //   data.VENDOR_PROFILE.OTHERS.OTHER[0] : (data.VENDOR_PROFILE.OTHERS.OTHER) ? data.VENDOR_PROFILE.OTHERS.OTHER : [];
      // this.vendorId = data.VENDOR_PROFILE.HEADERS.HEADER.VENDOR_ID;
      // this.tax = Array.isArray(data.VENDOR_PROFILE.TAXES.TAX) ?
      // data.VENDOR_PROFILE.TAXES.TAX : (data.VENDOR_PROFILE.TAXES.TAX) ? [data.VENDOR_PROFILE.TAXES.TAX] : [];
      this.organization = data? data.VENDOR_PROFILE.HEADERS.map(x=>(x.HEADER)) :[]
      this.employees = data? data.VENDOR_PROFILE.EMPLOYEES.map(x=>(x.EMPLOYEE)) :[]
      this.finance = data? data.VENDOR_PROFILE.OTHERS.map(x=>(x.OTHER)) :[]
      this.vendorId = data? data.VENDOR_PROFILE.HEADERS.HEADER : 0
      this.tax =data? data.VENDOR_PROFILE.TAXES.map(x =>x.TAX):[]
      this.headers =data? data.VENDOR_PROFILE.HEADERS.map(x =>x.HEADER):[]
      this.headersLength = this.headers? this.headers.length : 0;
      this.getDocumentList();
      }
        //  this.vendorSitesAll = data.VENDOR_SITES.VENDOR_SITE;
      //  this.vendorSites = this.vendorSitesAll;
    }, err => {
      console.log(err);
    });
  }

  // setFilteredData() {

  //   this.vendorSites = this.vendorSitesAll.filter(f => {
  //     let a = true;

  //     if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
  //       a = a && this.breadCrumbServices.select1 === f.UNIT;

  //     }
  //     if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
  //       a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
  //     }
  //     if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
  //       a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
  //     }

  //     return a;

  //   });

  // }

  getColor(value) {
    switch (value) {
      case 'Active':
        return 'act';
        break;

      case 'Inactive':
        return 'inact';
        break;

    }
  }


  onFilesAdded(fileInput: any) {
    this.spinner.show();
    this.fileData = fileInput.target.files as File;
    const length = fileInput.target.files.length;
    const file = new FormData();
    for (let i = 0; i < length; i++) {
      file.append('file', this.fileData[0], this.fileData[0].name);
    }
    this.loginService.FileUpload(file).subscribe((data) => {
      this.spinner.hide();
      let i;
      this.upload.file = this.fileData[0].name;
    }, err => {
      this.spinner.hide();
      this.resetFile();
      if (err.error.message) {

      } else {
      }
    });

  }

  resetFile() {
    this.myInputVariable.nativeElement.value = '';
  }

  cancel() {
    this.upload.documentCategory = '';
    this.upload.description = '';
    this.resetFile();
  }

  fileUpload() {
    this.spinner.show();
    const query = {
      DOCUMENT_ID: this.vendorId, FILE_NAME: this.upload.file, TYPE: 'VENDOR_ATTACHMENTS',
       DESCRIPTION: this.upload.description, TITLE: this.upload.documentCategory
    };
    this.purchaseService.FileUpload(query).subscribe((data) => {
      if (data === 'S') {
        this.getDocumentList();
        this.upload.documentCategory = '';
        this.upload.description = '';
        this.resetFile();
      }
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });
  }



  getDocumentList() {
    this.spinner.show();
    const details = { purchaseType: 'VENDOR_ATTACHMENTS', headerId: this.vendorId};
    this.purchaseService.getDocs(details).subscribe((data: any) => {
      this.attachments = data? data.ATTACHMENTS.map(x => x.ATTACHMENT) :[];
      // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
      //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
      this.spinner.hide();
    }, error => {
      console.log(error);
      this.spinner.hide();
    });

  }

  downloadfile(element) {
    this.spinner.show();
    const dat = { documentId: element.DOCUMENT_ID, fileName: element.FILE_NAME };
    this.purchaseService.downloadAttachment(dat).subscribe((data) => {
      const file = new Blob([data]);
      saveAs(file, element.FILE_NAME);
      // var fileURL = URL.createObjectURL(file);
      // window.open(fileURL);
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });
  }

updateOrganization() {
  this.spinner.show();
  const element = this.organization;
  const detail = {
    type: 'ORGANIZATION', ACTION: 'EDIT', details: {
      VENDOR_ID: element.VENDOR_ID, COMPANY_TYPE: element.COMPANY_TYPE, SUPPLIER_TYPE: element.SUPPLIER_TYPE,
      CEO_NAME: element.CEO, YEAR_ESTABLISHED: element.YEAR_ESTABLISHED, INCORPORATION_YEAR: element.INCORP_CONTROL_YEAR,
      WEBSITE: element.WEBSITE
    }
  };
  this.profileService.editProfile(detail).subscribe((data: any) => {
    this.spinner.hide();
  }, err => {
    this.spinner.hide();
    console.log(err);
  });

}

cancel2() {

}

updateEmployee() {
  this.spinner.show();
  const element = this.employees;
  const detail = {
    type: 'TOTAL_EMPLOYEES', ACTION: 'EDIT', details: {
      VENDOR_ID: element.VENDOR_ID, CORPORATE_TOTAL: element.CORPORATE_TOTAL, CORPORATE_TOTAL_TYPE: element.CORP_TOTAL_TYPE,
      ORGANIZATION_TOTAL: element.ORGANIZATION_TOTAL, ORGANIZATION_TOTAL_TYPE: element.ORG_TOTAL_TYPE
    }
  };
  this.profileService.editProfile(detail).subscribe((data: any) => {
    this.spinner.hide();
    }, err => {
    this.spinner.hide();
    console.log(err);
  });

}

cancel3() {

}

updateFinance() {
  this.spinner.show();
  const element = this.finance;
  const detail = {
    type: 'TAX_FINANCE', ACTION: 'EDIT', details: {
      VENDOR_ID: element.VENDOR_ID, CURRENCY: element.CUR_PREFERENCE, FINANCIAL_YEAR_FROM: element.FIN_YEAR_FROM,
      FINANCIAL_YEAR_TO: element.FIN_YEAR_TO, ANNUAL_REVENUE: element.ANNUAL_REVENUE
    }
  };
  this.profileService.editProfile(detail).subscribe((data: any) => {
    this.spinner.hide();
    }, err => {
    this.spinner.hide();
    console.log(err);
  });

}

cancel4() {

}
}
