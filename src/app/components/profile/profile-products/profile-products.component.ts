import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import {ProfileService} from '../service/profile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import {Product} from '../model/product';


@Component({
  selector: 'app-profile-products',
  templateUrl: './profile-products.component.html',
  styleUrls: ['./profile-products.component.css']
})
export class ProfileProductsComponent implements OnInit {

  public email;
  public vendorSitesAll = [];
  public vendorSites = [];
  public product: Product;
  public searchText = '';

  constructor(private profileService: ProfileService, private spinner: NgxSpinnerService,
    private breadCrumbServices: BreadcrumbService, private rsaService: RsaService, private router: Router) { }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.product = new Product();
    this.getProfileLocation();
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/profile/profileLocation') {
        this.setFilteredData();
      }
    });
    this.spinner.show();
  }

  getProfileLocation() {
    const details = { email: this.email, purchaseType: 'VENDOR_SITES' };
    this.profileService.getProfile(details).subscribe((data: any) => {
     this.spinner.hide();
    //  console.log('data', data);
     this.vendorSitesAll = data.VENDOR_SITES.VENDOR_SITE;
     this.vendorSites = this.vendorSitesAll;
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  setFilteredData() {

    this.vendorSites = this.vendorSitesAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });

  }

  editProduct(){

  }

  exportExcel() {
    const details = { POLINES: this.vendorSitesAll, title: 'Vendor Report' };
    this.profileService.getExcel(details);

  }

}
