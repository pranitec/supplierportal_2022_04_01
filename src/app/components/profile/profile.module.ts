import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileLocationComponent } from './profile-location/profile-location.component';
import { ProfileContactComponent } from './profile-contact/profile-contact.component';
import { ManageGeneralInfoComponent } from './manage-general-info/manage-general-info.component';
import { ManageBankComponent } from './manage-bank/manage-bank.component';
import {ProfileProductsComponent} from './profile-products/profile-products.component';
import { PurchaseModule} from '../purchase/purchase.module'

@NgModule({
  declarations: [ProfileLocationComponent, ProfileContactComponent,
    ManageGeneralInfoComponent, ManageBankComponent, ProfileProductsComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
    NgxSpinnerModule,
    PurchaseModule
  ]
})
export class ProfileModule { }
