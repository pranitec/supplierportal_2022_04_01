import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProfileContactComponent} from './profile-contact/profile-contact.component';
import {ProfileLocationComponent} from './profile-location/profile-location.component';
import { ManageGeneralInfoComponent } from './manage-general-info/manage-general-info.component';
import { ManageBankComponent } from './manage-bank/manage-bank.component';
import {ProfileProductsComponent} from './profile-products/profile-products.component';
import { AuthGuardService } from 'src/app/shared/service/auth-guard.service';


const routes: Routes = [
  {
    path: 'profileContact',
    component: ProfileContactComponent,
    data: {
      title: 'Manage Contacts',
      breadcrumb: 'Manage Contacts'
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'profileLocation',
    component: ProfileLocationComponent,
    data: {
      title: 'Manage Locations',
      breadcrumb: 'Manage Locations'
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'generalInfo',
    component: ManageGeneralInfoComponent,
    data: {
      title: 'Manage General Information',
      breadcrumb: 'Manage General Information'
    },
    canActivate: [AuthGuardService]
  },
  // {
  //   path: 'profileProduct',
  //   component: ProfileProductsComponent,
  //   data: {
  //     title: 'Manage Product and Services',
  //     breadcrumb: 'Manage Product and Services'
  //   }
  // },
  {
    path:'managebank',
    component:ManageBankComponent,
    data:{
      title:'Manage Bank Account',
      breadcrumb:'Manage Bank Account'
    },
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
