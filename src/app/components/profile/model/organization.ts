export class Organization {

    CEO: string;
    COMPANY_TYPE: string;
    INCORP_CONTROL_YEAR: string;
    LEGAL_NAME: string;
    SUPPLIER_TYPE: string;
    VENDOR_NAME: string;
    WEBSITE: string;
    YEAR_ESTABLISHED: string;
    VENDOR_ID: string;
}
