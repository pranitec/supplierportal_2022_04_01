export class UploadFile {
    file: string;
    description: string;
    documentCategory: string;
}
