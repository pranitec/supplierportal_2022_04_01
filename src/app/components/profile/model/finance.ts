export class Finance {

    ANNUAL_REVENUE: string;
    CUR_PREFERENCE: string;
    FIN_YEAR_FROM: string;
    FIN_YEAR_TO: string;
    VENDOR_ID: string;
}
