export class Contact {
    VENDOR_ID: string;
    EMAIL_ADDRESS: string;
    FAX: string;
    PHONE: string;
    JOB_TITLE: string;
    PERSON_FIRST_NAME: string;
    PERSON_LAST_NAME: string;
    MIDDLE_NAME: string;
    SALUTATION: string;
    VENDOR_SITE_ID: string;
}
