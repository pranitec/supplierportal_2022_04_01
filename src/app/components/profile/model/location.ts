export class Location {
    
    COUNTRY:string;
    ADDRESS_LINE1:string;
    ADDRESS_LINE2:string;
    ADDRESS_LINE3:string;
    NAME: string;
    CITY:string;
    STATE:string;
    POSTAL_CODE:string;
    PHONE:string;
    FAX:string
    EMAIL_ADDRESS:string;
    UNIT_ID: string;
    VENDOR_ID: string;
    VENDOR_SITE_ID: string;
}
