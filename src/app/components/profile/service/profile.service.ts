import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpClient, HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  public BaseUrl = environment.apiBaseUrl + '/profile';

  constructor(private http: HttpClient) { }

  getProfile(email){
    return this.http.post(`${this.BaseUrl}`, email);
  }

  getExcel(data) {
    const title = data.title;
    return this.http.post(`${this.BaseUrl}/getExcel`, data, {
      params: new HttpParams().append('token', localStorage.getItem('1')),
      observe: 'response', responseType: 'text'
    }).subscribe(r => { saveAs(new Blob([r.body], { type: 'text/csv' }), title + '.csv'); });

  }

  // addProfile(profile){
  //   return this.http.post(`${this.BaseUrl}/addProfile`, profile); 
  // }

  editProfile(profile) {
    return this.http.put(`${this.BaseUrl}`, profile); 
  }

}


