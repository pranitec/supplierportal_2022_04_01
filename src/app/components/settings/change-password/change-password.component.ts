import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { profileData } from 'src/app/config/config';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { LoginService } from '../../auth/service/login.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  public oldPassword = '';
  public newPassword = '';
  public confirmPassword = '';
  public email;
  public showOldPass= true;
  notifier: NotifierService;
  public id;
  public text;
  public content;
  public logo;
  constructor(private route: ActivatedRoute, private rsaService: RsaService, private router: Router,
    private spinner: NgxSpinnerService, private loginService: LoginService, private notifierService: NotifierService,) {
    this.notifier = notifierService;
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
  }

  ngOnInit(): void {
    this.text = true;
this.logo =profileData.LogoFile

  }

  checkPassword(){


    if (this.oldPassword) {
      this.spinner.show();
      const detail = { email: this.email, password: this.oldPassword };
      this.loginService.getEmail(detail).subscribe((data: any) => {
        console.log('kk', data);
        this.spinner.hide();
        if (!data.message) {
        this.notifier.notify('success','Checked');
        this.showOldPass = false;
        } else {
          this.notifier.notify('error', data.message);
        }
      }, err => {
        this.spinner.hide();
        console.log(err);
      });
    } else {
      this.notifier.notify('error', 'Please enter valid Password');
    }
  }

  submitPassword() {

    if (this.newPassword === this.confirmPassword) {
      this.spinner.show();
      const subject = 'Craftsman Automation Limited Supplier Portal - Account Updation';
      const body = `
      Dear Supplier,<br/><br/>

      Congratulations! Your Supplier Portal password has been updated successfully. <br/><br/>

       Thank You, <br/>
       Craftsman Automation Team
        `;

      const details = { email: this.email, password: this.confirmPassword ,subject: subject, content: body};
      this.loginService.getUserDetails(details).subscribe((data: any) => {
        this.spinner.hide();
        if (data) {
          setTimeout(()=>{
            this.logout()
          },1000)
          this.notifier.notify('success','Password Changed Sucessfully');

          this.text = false;
          // this.router.navigate(['/auth/view'], { queryParams: { id: this.id } });
          if (this.id == 3) {
            this.content = true;
          }
          else {
            this.content = false;
          }
          this.email = '';
        }
      }, err => {
        console.log(err);
      });
    } else {
      this.notifier.notify('warning', 'Both password must be same');
    }
  }
  logout(){
    localStorage.clear();
    this.router.navigate(['']);
  }
}
