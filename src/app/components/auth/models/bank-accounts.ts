import { BankDetails } from './bank-details';

export class BankAccounts {
    ADD_INTERMIDIATE_BANK: string;
    COUNTRY:string;
    BANK:string;
    BANK_NAME: string;
    BANK_BRANCH_NUMBER:string;
    BANK_BRANCH_NAME:string;
    BIC:string;
    BANK_ACCOUNT_NUMBER:string;
    BANK_ACCOUNT_NUM: string;
    BANK_ACCOUNT_NAME:string;
    BANK_ACCOUNT_TYPE:string;
    IBAN:string;
    CURRENCY:string;
    FILE_NAME:string;
    intermidiate:Boolean;
    INT_BANK_COUNTRY:string;
    INT_BANK:string;
    INT_BANK_BRANCH_NUMBER:string;
    INT_BANK_BRANCH_NAME:string;
    INT_BIC:string;
    INT_BANK_ACCOUNT_NUMBER:string;
    INT_BANK_ACCOUNT_NAME: string;
    INT_BANK_ACCOUNT_TYPE: string;
    INT_IBAN:string;
    INT_CURRENCY: string;
    VENDOR_ID:string;
    VENDOR_SITE_ID: string;
}
