export class BankDetails {
    COUNTRY:string;
    BANK:string;
    BANK_BRANCH_NUMBER:string;
    BANK_BRANCH_NAME:string;
    BIC:string;
    BANK_ACCOUNT_NUMBER:string;
    BANK_ACCOUNT_NAME:string;
    BANK_ACCOUNT_TYPE:string;
    IBAN:string;
    CURRENCY:string;
    FILE_NAME:string;
}

