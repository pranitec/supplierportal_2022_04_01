export class Address {
    ADDRESS_NAME:string;
    COUNTRY:string;
    ADDRESS_LINE1:string;
    ADDRESS_LINE2:string;
    ADDRESS_LINE3:string;
    TAX_REGISTRATION_NUMBER: string;
    CITY:string;
    STATE:string;
    POSTAL_CODE:string;
    PHONE:string;
    FAX:string
    EMAIL_ADDRESS:string;
}
