import { Address } from './address';
import { Contacts } from './contacts';
import { BankAccounts } from './bank-accounts';

export class Registration {
    HEADER: HEADERS;
    ADDRESSES: ADDRESSES;
    CONTACTS: CONTACTS;
    BANKS: BANKS;
    file1:string;
    file2:string;
}

export class ADDRESSES {
    ADDRESS: Address[];
}
export class CONTACTS {
    CONTACT: Contacts[];
}
export class BANKS {
    BANK: BankAccounts[];
}

export class HEADERS {
    VENDOR_NAME: string;
    LEGAL_NAME: string;
    REQUEST_REASON: string;
    JUSTIFICATION: string;
    COMPANY_TYPE: string;
    VENDOR_TYPE: string;
    WEBSITE: string;
    TAX_COUNTRY: string;
    TAX_REGISTRATION_NO: string;
    TAX_PAYER_ID: string;
    FILE_NAME: string;
    file: string;
}
