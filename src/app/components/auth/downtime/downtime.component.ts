import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-downtime',
  templateUrl: './downtime.component.html',
  styleUrls: ['./downtime.component.css']
})
export class DowntimeComponent implements OnInit {

  downtimeText =''
  constructor(private httpClient : HttpClient) {

   }

  ngOnInit(): void {
    this.httpClient.get('assets/npmdown.txt', { responseType: 'text' })
    .subscribe(data => {
      if(data.length>4) {
        this.downtimeText = data
      }
       });
  }

}
