import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { LoginService } from '../service/login.service';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { profileData } from 'src/app/config/config'

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {

  public newPassword = '';
  public confirmPassword = '';
  public email;
  notifier: NotifierService;
  public id;
  public text;
  public content;
  public logo;
  public image;
  public copyright;
  public name;
  public website

  constructor(private route: ActivatedRoute, private rsaService: RsaService, private router: Router,
    private spinner: NgxSpinnerService, private loginService: LoginService, private notifierService: NotifierService,) {
    this.notifier = notifierService;
    this.route.queryParams.subscribe((data) => {
      let email = data['page'];
      this.id = data['id'];
      email = decodeURIComponent(email);
      this.email = this.rsaService.decrypt(email);
    });
  }

  ngOnInit(): void {
    this.text = true;
    this.logo =profileData.LogoFile
    this.image=profileData.BannerImage
    this.copyright = profileData.copyright
    this.name = this.rsaService.name
    this.website = profileData.websiteURL
  }

  submitPassword() {

    if (this.newPassword === this.confirmPassword) {
      this.spinner.show();
      const subject = 'Craftsman Automation Limited Supplier Portal - Account Updation';
      const body = `
      Dear Supplier,<br/><br/>

      Congratulations! Your Supplier Portal password has been updated successfully. <br/><br/>

       Thank You, <br/>
       Craftsman Automation Team
        `;

      const details = { email: this.email, password: this.confirmPassword, subject: subject, content: body};
      this.loginService.getUserDetails(details).subscribe((data: any) => {
        this.spinner.hide();
        if (data) {
          this.text = false;
          // this.router.navigate(['/auth/view'], { queryParams: { id: this.id } });
          if (this.id == 3) {
            this.content = true;
          }
          else {
            this.content = false;
          }
          this.email = '';
        }
      }, err => {
        console.log(err);
      });
    } else {
      this.notifier.notify('warning', 'Both password must be same');
    }
  }
}
