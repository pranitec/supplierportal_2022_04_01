import { Component, OnInit, ViewChild, ElementRef, VERSION} from '@angular/core';
import { Login } from '../models/login';
import { NotifierService } from 'angular-notifier';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { profileData } from 'src/app/config/config'
import { profile } from 'console';
import { RsaService } from 'src/app/shared/service/rsa.service';
// import { PurchaseService } from '../service/purchase.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { DeviceDetectorService } from "ngx-device-detector";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('canvasView', { static: false })
  canvas: ElementRef;
  // @ViewChild('wizard',{static: true})
  // stepper:ElementRef<HTMLDivElement>;
  private ctx: CanvasRenderingContext2D;

  public login: Login;

  public user;
  public pass;
  public originalCaptcha;
  public captchaEntered;
  public logo;
  public image;
  public copyright;
  public website;
  public ipAddress:string;
  name = "Angular " + VERSION.major;
  deviceInfo = null;
  isDesktopDevice: boolean;
  isTablet: boolean;
  isMobile: boolean;
  private notifier: NotifierService;
  public newsList=[];
  public downtimeFlag = false;
  public downtimeText=''
  public companyName;

   // private deviceService: DeviceDetectorService
  constructor(private notifierService: NotifierService, private router: Router, private spinner: NgxSpinnerService, private deviceService: DeviceDetectorService,
    private loginService: LoginService, public breadcrumbService: BreadcrumbService, private rsaService : RsaService, private purchaseService: PurchaseService,
    private httpClient : HttpClient) {
    this.notifier = notifierService;
    this.epicFunction();
    this.getAnnouncements();
  }

  ngOnInit() {
    this.user = true;
    this.pass = false;
    this.login = new Login();
    this.logo =profileData.LogoFile
    this.image=profileData.BannerImage
    this.copyright = profileData.copyright
    this.website = profileData.websiteURL
    this.companyName = profileData.companyName
    this.readDowntimeFile()
  }
  readDowntimeFile(){
    this.httpClient.get('assets/downtime.txt', { responseType: 'text' })
    .subscribe(data => {
      if(data.length>4) {
        this.downtimeFlag = true
        this.downtimeText = data
      }
       });
  }
  getPassword() {

    if (this.login.email) {
      this.spinner.show();
      const details = { email: this.login.email };
      // let query = {
      //   INPUT_HEADER :{ MAIL_ID :(this.login.email).toUpperCase()},
      //   FILTER_DETAILS :{ MENU_NAME:"LOGIN_PAGE", DOCUMENT_TYPE: "LOGIN_PAGE", PASSWORD : undefined, SUPPLIER_ID :undefined }
      // }
      // console.log("query : ", query)
      this.loginService.getEmail(details).subscribe((data: any) => {
        // console.log("getPassword : ", data)
        this.spinner.hide();
        // console.log('lll getpassword', data, data.MAIL_ID);
        // localStorage.setItem('3', this.rsaService.encrypt(data.MAIL_ID));
        localStorage.setItem('3', this.rsaService.encrypt(data.MAIL_ID));
        this.login.userName = data.USER_NAME;
        if (data.VALID_FLAG !== 'Y' && data.ACCOUNT_ACTIVE_FLAG !== 'Y') {
          this.notifier.notify('error', 'Please enter valid UserName');

        } else if (data.ACCOUNT_ACTIVE_FLAG !== 'Y') {
          this.notifier.notify('error', 'Please Activate Your Account');
        } else if (data.VALID_FLAG === 'Y' && data.ACCOUNT_ACTIVE_FLAG === 'Y') {
          this.user = false;
          this.pass = true;
          setTimeout(() => {this.onEvent();
          }, 100);
        }

      }, err => {
        this.spinner.hide();
        console.log(err);
      });

    } else {
      this.notifier.notify('error', 'Please enter valid UserName');
    }
    // this.getIP();
  }

  onEvent() {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.setCanvas();
  }

  setCanvas() {

    // const charsArray =
    //   '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*';
      const charsArray =
      '0123456789';
    const lengthOtp = 7;
    const captcha = [];
    for (let i = 0; i < lengthOtp; i++) {
      // below code will not allow Repetition of Characters
      const index = Math.floor(Math.random() * charsArray.length + 1); // get the next character from the array
      if (captcha.indexOf(charsArray[index]) == -1) {
        captcha.push(charsArray[index]);
      } else { i--; }
    }

    // var ctx = .getContext("2d");
    this.ctx.font = '25px Georgia';
    this.ctx.clearRect(0, 0, 100, 50);
    this.ctx.strokeText(captcha.join(''), 0, 30);
    this.originalCaptcha = captcha.join('');
  }

  loginPage() {
    console.log("login page");

    if (this.captchaEntered === this.originalCaptcha) {
      if (this.login.password) {
        this.spinner.show();
        const detail = { email: this.login.email, password: this.login.password };
        this.loginService.getEmail(detail).subscribe((data: any) => {
          // console.log("users : ", data)
          this.spinner.hide();
          if (!data.message) {
            if(data.user.USER.ACCOUNT_TYPE === 'SECURITY'){
              this.router.navigateByUrl('/craftsmanautomation/gate', {skipLocationChange : profileData.hideURL});
            } else{
              console.log("login page else");
            this.router.navigateByUrl('/craftsmanautomation/dashboard', {skipLocationChange : profileData.hideURL});
            }
          } else {
            this.notifier.notify('error', data.message);
          }
        }, err => {
          this.spinner.hide();
          console.log(err);
        });
      } else {
        this.notifier.notify('error', 'Please enter valid Password');
      }
    } else {
      alert('Enter Correct Captcha');
      this.setCanvas();

    }

  }
  epicFunction() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.isMobile = this.deviceService.isMobile();
    this.isTablet = this.deviceService.isTablet();
    this.isDesktopDevice = this.deviceService.isDesktop();
    // console.log("login version : ", this.deviceInfo, this.isMobile, this.isTablet, this.isDesktopDevice)
    // console.log("version : ",this.deviceInfo.deviceType, this.deviceInfo.userAgent, this.deviceInfo.os, this.deviceInfo.os_version,
    // this.deviceInfo.browser_version)
  }
  getAnnouncements(){
    const details = this.purchaseService.inputJSON('SUPPLIER_NEWS', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      console.log("news data : ", data)
      this.newsList = data.SUPPLIER_NEWS_ALL ? data.SUPPLIER_NEWS_ALL.map(x=>x. SUPPLIER_NEWS) : []
    }, err => {
      this.spinner.hide();
      console.log(err);
    });

  }
}
