import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ActivateAccountComponent } from './activate-account/activate-account.component';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { PasswordComponent } from './password/password.component';
import { ViewTemplateComponent } from './view-template/view-template.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DeviceDetectorService } from "ngx-device-detector";
import { DowntimeComponent } from './downtime/downtime.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from 'src/app/shared/shared.module';
import { PurchaseModule } from '../purchase/purchase.module';


const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'middle',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 99,
      gap: 15
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 1200,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 800,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 1000,
      easing: 'ease',
      offset: 150
    },
    shift: {
      speed: 500,
      easing: 'ease'
    },
    overlap: 90
  }
};


@NgModule({
  declarations: [LoginComponent, RegisterComponent, ForgetPasswordComponent, ActivateAccountComponent, PasswordComponent, ViewTemplateComponent, DowntimeComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxSpinnerModule,
    NgxPaginationModule,
    SharedModule,
    PurchaseModule
  ]

})
export class AuthModule { }
