import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-view-template',
  templateUrl: './view-template.component.html',
  styleUrls: ['./view-template.component.css']
})
export class ViewTemplateComponent implements OnInit {

  public id;
  public active = false;
  public forget = false;
  public active1 = false;
  public forget1 = false;

  constructor(private route: ActivatedRoute) {
    this.route.queryParams.subscribe((data) => {
      this.id = data['id'];
      });
   }

  ngOnInit(): void {
    
    this.viewContent();
  }

  viewContent(){
   if(this.id==1){
    this.active = true;
   }
   else if(this.id==2){
    this.forget = true;
   }
   else if(this.id==3){
    this.active1 = true;
   }
   else{
    this.forget1 = true;
   }
  }

}
