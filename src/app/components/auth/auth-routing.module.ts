import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ActivateAccountComponent } from './activate-account/activate-account.component';
import { RegisterComponent } from './register/register.component';
import { PasswordComponent } from './password/password.component';
import { ViewTemplateComponent } from './view-template/view-template.component';
import { DowntimeComponent } from './downtime/downtime.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'forget-pass', component: ForgetPasswordComponent },
  { path: 'activate', component: ActivateAccountComponent },
  {
    path: 'registration',
    component: RegisterComponent,
  },
  {
    path: 'password', component: PasswordComponent
  },
  {
    path: 'view', component: ViewTemplateComponent
  },
  // {
  //   path: 'downtime', component: DowntimeComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
