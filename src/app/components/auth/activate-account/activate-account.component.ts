import { Component, OnInit } from '@angular/core';

import './../../../../assets/vendor/smtp/smtp.js';
import { NotifierService } from 'angular-notifier';
import { EmailService } from '../service/email.service.js';
import { LoginService } from '../service/login.service';
import { RsaService } from 'src/app/shared/service/rsa.service.js';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { profileData } from 'src/app/config/config'
declare let Email: any;

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.css']
})
export class ActivateAccountComponent implements OnInit {

  public email = '';
  public text;
  public text1;
  notifier: NotifierService;
  public logo;
  public image;
  public copyright;
  public website

  constructor(private notifierService: NotifierService, public emailService: EmailService, private spinner: NgxSpinnerService,
    private loginService: LoginService, private rsaService: RsaService, private router: Router) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    this.text = true;
    this.logo =profileData.LogoFile
    this.image=profileData.BannerImage
    this.copyright = profileData.copyright
    this.website = profileData.websiteURL
  }

  activateAccount() {

    if (this.email.length > 0) {
      this.spinner.show();
      // const detail = { email: this.login.email, password: this.login.password };
      // this.loginService.getEmail(detail).subscribe((data: any) => {

      const details = { email: this.email };
      this.loginService.getEmail(details).subscribe((data: any) => {
        // console.log('lll email', data);
        this.spinner.hide();
        let emailId = this.email;
        let name = data.USER_NAME;
        this.rsaService.name = data.USER_NAME
        emailId = this.rsaService.encrypt(emailId);
        emailId = encodeURIComponent(emailId);
        //if(true){
        if (data.VALID_FLAG === 'Y' && data.ACCOUNT_ACTIVE_FLAG === 'N') {
          this.spinner.show();
          const subject = 'Craftsman Automation Limited Supplier Portal - Account Activation';
          const body = `
          Dear ` + name + `,<br/><br/>

          Congratulations! Your Supplier Portal account has been successfully created. <br/><br/>

          Please follow the link below to activate your account and reset your password <br/><br/>

          `+ environment.baseUrl + `/#/auth/password?page=` + emailId + `&id=3<br/><br/>

          For any issues, contact your system administrator.<br/><br/>

           Thank You, <br/>
           Craftsman Automation Team
            `;
          const smsType="Supplier Portal Account Activation"
          this.emailService.sendMail(this.email, subject, body, smsType).subscribe(message => {
            this.spinner.hide();
            // console.log('data',message);
            if (message === 'OK') {
              const id = 1;
              // this.router.navigate(['/auth/view'], { queryParams: { id } });
              this.text = false;
              this.text1 = true;
              this.email = '';
            } else {
              this.notifier.notify('warning', 'Email Delivery Failure!');
            }
          },error=>{
            this.notifier.notify('warning', 'Email Delivery Failure!');
          });
        }
        else if (data.VALID_FLAG === 'Y' && data.ACCOUNT_ACTIVE_FLAG === 'Y') {
           this.text = false;
           this.text1 = false;
        }
        else {
          this.notifier.notify('error', 'Please enter valid email address');
        }
      }, err => {
        this.spinner.hide();
        console.log(err);
      });
    } else {
      this.notifier.notify('error', 'Please enter email address');
    }
  }
}

