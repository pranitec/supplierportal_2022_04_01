import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { tap } from 'rxjs/operators';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { PurchaseService } from '../../purchase/service/purchase.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public baseUrl = environment.apiBaseUrl + '/users';
  public user;
  public type : string;
  public fileType : string;
  public menuaccess;
  constructor(private http: HttpClient, private rsaService: RsaService, private breadcrumbService: BreadcrumbService,
    ) { }

  getEmail(email) {
    return this.http.post(`${this.baseUrl}`, email).pipe(tap(x => {
      if(x){
        this.setToken(x)
      }else{
        setTimeout(() => {
          this.setToken(x);
        }, 3600);
      }

      }));
  }

  getUserDetails(data) {
    return this.http.post(`${this.baseUrl}/account`, data);
  }

  setToken(data) {
    // console.log("setToken data : ", data, data.user.USERACCESS)
    if (data.token) {
      // console.log("USERACCESS : ", data.user.USERACCESS)
      const token = data.token;
      this.user = data.user;
      // this.getMenu();
      localStorage.clear();
      localStorage.setItem('1', token);
      localStorage.setItem('2', this.rsaService.encrypt(JSON.stringify(this.user)));
      // var useraccess = (data.user.USERACCESSES.map(x=>x.USERACCESS))
      // useraccess.sort((a, b) =>a.ERP_ORGANIZATION_NAME < b.ERP_ORGANIZATION_NAME ? -1 : a.ERP_ORGANIZATION_NAME > b.ERP_ORGANIZATION_NAME ? 1 : 0);
      // localStorage.setItem('USERACCESS', this.rsaService.encrypt(JSON.stringify(useraccess)));
      localStorage.setItem('3', this.rsaService.encrypt(data.user.USER.MAIL_ID));
      localStorage.setItem('4',this.rsaService.encrypt(data.user.USER.USER_NAME));
      localStorage.setItem('5',data.user.USER.NO_OF_PARTIES);
      localStorage.setItem('6',this.rsaService.encrypt(data.user.USER.ACCOUNT_TYPE));
      // this.breadcrumbService.setSelectValues(data.user.USERACCESSES.map(x=>x.USERACCESS),data.user.USER.NO_OF_PARTIES ||0);
    }
  }

  logout() {
    localStorage.clear();
  }

  tokenVerfiy() {
    return this.http.get(`${this.baseUrl}/tokenVerfiy`);
  }

  registerUser(data){
    return this.http.post(`${this.baseUrl}/userRegister`,data);
  }

  FileUpload(files) {
    const httpOptions = { headers: new HttpHeaders({ type : this.type, filetype : this.fileType}) }
    return this.http.post(`${this.baseUrl}/fileUpload`, files, httpOptions);
    }


  }
