import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { error } from 'protractor';
// import './../../../../assets/vendor/smtp/smtp.js';
declare let Email: any;

@Injectable({
  providedIn: 'root'
})
export class EmailService {


  public baseUrl = environment.apiBaseUrl + '/mail';
  constructor(private http: HttpClient) { }

  sendMail(to, subject, body, smsType, mobile?, field1?, field2?, field3?) {
    // console.log("rrrr : ", smsType, mobile, field1, field2)
    // return Email.send({
    // //  SecureToken: '9e359c94-801c-44b3-98f9-f47aafb4031e',
    //  // SecureToken: 'b880bdf7-bb84-4078-9990-499e7bf85453',
    //  SecureToken: '796cffa6-0fd5-40e9-844e-75f3f3aed6b8',
    //   To: `${to}`,
    //   From: `info.yty@ytygroup.com.my`,
    //   Subject: `${subject}`,
    //   Body: `${body}`
    // });

   return this.http.post(`${this.baseUrl}/`, {to:to,subject,content:body, SMSType:smsType, mobileNo:mobile, field1: field1, field2:field2, field3:field3})

  }

  sendMailWithAttachments(to,subject,body,fileName,base64, smsType?, mobile?, field1?, field2?, field3?){
    // console.log("rrrr : ", to,subject,body,fileName,base64, smsType, mobile, field1, field2, field3)
    // return Email.send({
    //   SecureToken: '796cffa6-0fd5-40e9-844e-75f3f3aed6b8',
    //   To: `${to}`,
    //   From: `info.yty@ytygroup.com.my`,
    //   Subject': `${subject}`,
    //   Body: `${body}`,
    //   Attachments : [
    //     {
    //       name:fileName,
    //         data:base64
    //     }
    //   ]
    // });


    return this.http.post(`${this.baseUrl}/attachments`, {to:to,subject,content:body,filename:fileName,base64:base64,
      SMSType:smsType, mobileNo:mobile, field1: field1, field2:field2, field3:field3})
  }

  // sendSMS(mobile, type, field1, field2){
  //   return this.http.post(`${this.baseUrl}/sms`, {mobileNo:mobile,smstype:type, field1: field1, field2:field2})
  // }
}
