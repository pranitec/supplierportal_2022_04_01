import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Registration, ADDRESSES, CONTACTS, BANKS, HEADERS } from '../models/registration';
import { Address } from '../models/address';
import { Contacts } from '../models/contacts';
import { BankAccounts } from '../models/bank-accounts';
import { BankDetails } from '../models/bank-details';
import { LoginService } from '../service/login.service';
import { PurchaseService} from '../../purchase/service/purchase.service'
import { NotifierService } from 'angular-notifier';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { profileData } from 'src/app/config/config';
import { AnyNsRecord } from 'dns';
declare var jQuery: any;
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, AfterViewInit {

  @ViewChild('canvasView', { static: false })
  canvas: ElementRef;
  // @ViewChild('wizard',{static: true})
  // stepper:ElementRef<HTMLDivElement>;
  private ctx: CanvasRenderingContext2D;
  public register: any =[];
  public address: Address;
  public HEADERS:HEADERS;
  public ADDRESSES: ADDRESSES;
  public CONTACTS: CONTACTS;
  public BANKS: BANKS;
  public isAddress = false;
  public contact: Contacts;
  public bankAccounts: BankAccounts;
  public originalCaptcha;
  public captchaEntered;
  public email;
  public id;
  public logo;
  public copyright;
  public image;
  public countryList = [];
  public currencyList = [];
  public stateList = [];
  public fileInput1;
  fileData: File = null;
  fileData1: File = null;
  private notifier: NotifierService;
  public taxCountry;
  public filenameWithoutno =[];
  public filenameWithno =[];
  public sortedCountryList =[];
  public sortedStateList =[]
  public filenameWithno1 =[];
  public filenameWithno2 =[];
  public filenameWithno3 =[];
  public filenameWithno4 =[];
  public filenameWithoutno1 =[];
  public filenameWithoutno2 =[];
  public filenameWithoutno3 =[];
  public filenameWithoutno4 =[];
  public description;
  public headerFlag= false;
  public headerFlag1 = false;
  public addressFlag = false;
  public bankFlag = false;

  constructor(private loginService: LoginService, private purchaseService: PurchaseService, private notifierService: NotifierService, private router: Router, private spinner: NgxSpinnerService,private route: ActivatedRoute,private rsaService: RsaService, ) {
    this.notifier = notifierService;
  }

  @ViewChild('myFileInput', { static: false }) myInputVariable: ElementRef;
  @ViewChild('myFileInput', { static: false }) myInputVariable1: ElementRef;
  ngOnInit(): void {
    // CANVAS
    this.logo =profileData.LogoFile
    this.image=profileData.BannerImage
    this.copyright = profileData.copyright

    // console.log(encodeURI(this.rsaService.encrypt(''+ (((new Date()).getTime())-4.5*60*60*1000))));

    this.route.queryParams.subscribe((data) => {
      let email = data['page'];
      let id = data['id'];
      id = decodeURIComponent(id);
      email = decodeURIComponent(email);
      this.email = this.rsaService.decrypt(email);
      this.id  = this.rsaService.decrypt(id);
      // console.log('Data',this.email,this.id);
      if(this.email.length>0){
        let fourhtime = Number(this.id)+4*60*60*1000;
        if((new Date()).getTime()<fourhtime && this.id.length>0){

        }
        else{
          if(confirm("Session TimeOut")) {
            // console.log("Implement delete functionality here");
            this.router.navigateByUrl('/auth/login', );
          }
          else{
            this.router.navigateByUrl('/auth/login');
          }
        }

      }else{
        if(confirm("Invalid url")) {
          // console.log("Implement delete functionality here");
          this.router.navigateByUrl('/auth/login');
        } else{
          this.router.navigateByUrl('/auth/login');
        }
      }
    });


    this.getCountryList();
    this.getCurrencyList();
    // this.getStateList();

    this.register = new Registration();
    this.register.HEADER = new HEADERS();
    this.ADDRESSES = new ADDRESSES();
    this.ADDRESSES.ADDRESS = [];
    this.register.ADDRESSES = this.ADDRESSES;
    this.CONTACTS = new CONTACTS();
    this.CONTACTS.CONTACT = [];
    this.register.CONTACTS = this.CONTACTS;
    this.BANKS = new BANKS();
    this.BANKS.BANK = [];
    this.register.BANKS = this.BANKS;
    this.address = new Address();
    this.contact = new Contacts();
    this.bankAccounts = new BankAccounts();
    // this.bankAccounts. = new BankDetails();

    (function($) {
      $(document).ready(function() {
        $('.pass').hide();

        $('.Intermidiate').hide();
        $('#ibaccount').click(function() {

          if ($(this).is(':checked')) {
            $('.Intermidiate').show();
          } else {
            $('.Intermidiate').hide();
          }

        });

        $('#myBtn').click(function() {
          $('.pass').show();
          $('.user').hide();

          return false;
        });
      });

      $(function() {
        $('#wizard').steps({
          headerTag: 'h4',
          bodyTag: 'section',
          transitionEffect: 'fade',
          enableAllSteps: true,
          transitionEffectSpeed: 500,
          onStepChanging(event, currentIndex, newIndex) {
            if (newIndex === 1) {
              $('.steps ul').addClass('step-2');
            } else {
              $('.steps ul').removeClass('step-2');
            }
            if (newIndex === 2) {
              $('.steps ul').addClass('step-3');
            } else {
              $('.steps ul').removeClass('step-3');
            }

            if (newIndex === 3) {
              $('.steps ul').addClass('step-4');
            } else {
              $('.steps ul').removeClass('step-4');
            }

            if (newIndex === 4) {
              $('.steps ul').addClass('step-5');
              $('.actions ul').addClass('step-last');
              const len = $('#wizard .actions li:last-child').children().length;
              if (len == 1) {
                $('#wizard .actions li:last-child').append('<button type="submit" id="reg" style="padding:0;padding-left: 47.5px;width: 150px;font-weight: 600;border: none;display: inline-flex;height: 51px;align-items: center;background: #0f96f9;cursor: pointer;color: #fff !important;position: relative;text-decoration: none;transform: perspective(1px) translateZ(0);transition-duration: 0.3s;font-weight: 400;">Register</button>');
              }
              // remove default #finish button
              // $("#registerForm").submit(function( event ) {
              //   event.preventDefault();
              // });
              $('#wizard').find('a[href="#finish"]').attr('style', 'display:none;');
              // append a submit type button

            } else {
              $('.steps ul').removeClass('step-5');
              $('.actions ul').removeClass('step-last');
            }
            return true;
          },
          labels: {
            finish: 'Register',
            next: 'Next',
            previous: 'Previous'
          }
        });
        // Custom Steps Jquery Steps
        $('.wizard > .steps li a').click(function() {
          $(this).parent().addClass('checked');
          $(this).parent().prevAll().addClass('checked');
          $(this).parent().nextAll().removeClass('checked');
        });
        // Custom Button Jquery Steps
        $('.forward').click(function() {
          $('#wizard').steps('next');
        });
        $('.backward').click(function() {
          $('#wizard').steps('previous');
        });
        // Checkbox
        $('.checkbox-circle label').click(function() {
          $('.checkbox-circle label').removeClass('active');
          $(this).addClass('active');
        });
      });
      $(document).ready(function() {
        // createCaptcha()
        $('.file_input_button').mouseover(function() {
          $(this).addClass('file_input_button_hover');
        });

        $('.file_input_button').mouseout(function() {
          $(this).removeClass('file_input_button_hover');
        });

        $('.file_input_hidden').change(function() {

          const fileInputVal = $(this).val();

          // fileInputVal = fileInputVal.replace("C:"\"fakepath"\"", "");

          $(this).parent().prev().val(fileInputVal);

        });
      });


      let code;
      function createCaptcha() {
        // clear the contents of captcha div first
        document.getElementById('captcha').setAttribute('innerHTML', '');
        const charsArray =
          '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*';
        const lengthOtp = 6;
        const captcha = [];
        for (let i = 0; i < lengthOtp; i++) {
          // below code will not allow Repetition of Characters
          const index = Math.floor(Math.random() * charsArray.length + 1); // get the next character from the array
          if (captcha.indexOf(charsArray[index]) == -1) {
            captcha.push(charsArray[index]);
          } else { i--; }
        }
        const canv = document.createElement('canvas');
        canv.id = 'captcha';
        canv.width = 100;
        canv.height = 50;
        const ctx = canv.getContext('2d');
        ctx.font = '25px Georgia';
        ctx.strokeText(captcha.join(''), 0, 30);
        // storing captcha so that can validate you can save it somewhere else according to your specific requirements
        code = captcha.join('');
        this.originalCaptcha = captcha.join('');
        document.getElementById('captcha').appendChild(canv); // adds the canvas to the body element
      }
      // function validateCaptcha() {
      //   event.preventDefault();
      //   debugger
      //   if (document.getElementById("cpatchaTextBox")) {
      //     alert("Valid Captcha")
      //   }else{
      //     alert("Invalid Captcha. try Again");
      //     createCaptcha();
      //   }
      // }

    })(jQuery);
  }

  ngAfterViewInit(): void {
    // Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    // Add 'implements AfterViewInit' to the class.

  }

  getCountryList(){
    // const details = { email: 'senthilkumar.r@pranitec.com', purchaseType: 'ALL_COUNTRIES' };
    const details = this.purchaseService.inputJSON('ALL_COUNTRIES', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      console.log('country', data);
      this.countryList = data? data.ALL_COUNTRIES.map(x=>(x.ALL_COUNTRY)) :[];
      // this.countryList = this.countryList.sort();
      this.sortedCountryList = this.countryList.map((m) => m.DESCRIPTION);
        this.sortedCountryList.sort((a,b) => 0 - (a > b ? -1 : 1));
        // console.log('country', this.sortedCountryList);
    }, error => {
      console.log(error);
      });
    }

    getCurrencyList(){

      // const details = { email: 'senthilkumar.r@pranitec.com', purchaseType: 'ALL_CURRENCIES' };
      const details = this.purchaseService.inputJSON('ALL_CURRENCIES', undefined, undefined)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
        // console.log('currency', data);
        this.currencyList = data? data.ALL_CURRENCIES.map(x=>(x.ALL_CURRENCY)) :[];
      }, error => {
        console.log(error);
        });
      }

      getStateList(cname){
        let country:any =''
        if(cname){
        country = this.countryList.find(f=>f.DESCRIPTION === cname)
        console.log("countryc code : ", country)
        // const details = { email: 'senthilkumar.r@pranitec.com', purchaseType: 'ALL_COUNTRY_STATES', attachmentString: country.COUNTRY_CODE };
        // const details = { email: 'senthilkumar.r@pranitec.com', purchaseType: 'ALL_CURRENCIES' };
        // this.purchaseService.getDocs(details).subscribe((data: any) => {
          const details = this.purchaseService.inputJSON('ALL_COUNTRY_STATES', undefined, undefined, 'COUNTRY_CODE', country.COUNTRY_CODE)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
        console.log("statelist : ", data)
        if(data) {
          this.stateList = data? data.ALL_STATES.map(x=>(x.ALL_STATE)) :[];
          this.sortedStateList = this.stateList.map((m) => m.DESCRIPTION);
          this.sortedStateList.sort((a,b) => 0 - (a > b ? -1 : 1));
          // console.log('state', this.stateList);
        }
        }, error => {
          console.log(error);
          });
        }
        }

  addAddress() {
    this.register.ADDRESSES.ADDRESS.push(this.address);
    this.address = new Address();
    this.isAddress = true;
    // (function($){
    //   $('.steps ul').removeClass('step-3');
    //   $('.steps ul').addClass('step-3');

    // })(jQuery);

  }

  addContact() {
    this.register.CONTACTS.CONTACT.push(this.contact);
    this.contact = new Contacts();
  }

  addAccount() {
    this.register.BANKS.BANK.push(this.bankAccounts);
    this.bankAccounts = new BankAccounts();
    this.onEvent();
  }

  addIntermidiateBankAccount() {
    if (this.bankAccounts.intermidiate) {
    } else {
      this.bankAccounts.INT_BANK, this.bankAccounts.INT_BANK_ACCOUNT_NUMBER,
      this.bankAccounts.INT_BANK_BRANCH_NAME, this.bankAccounts.INT_BANK_COUNTRY,
      this.bankAccounts.INT_IBAN, this.bankAccounts.INT_BIC,
      this.bankAccounts.INT_BANK_ACCOUNT_NUMBER = undefined;
    }
  }


  registeration() {
    this.spinner.show();
    console.log("this.register : ", this.register)
    // let query = {PO_ACCEPTANCE :{ PO_HEADER_ID: headerId, PO_RELEASE_ID:element.PO_RELEASE_ID, USER_NAME: this.email, ACTION: action, NOTES: this.reason,
    //   LINES: [{LINE: line}] }};
     //  console.log(query);
    //  this.register.HEADER.FILE_ATTACHMENTS = this.register.HEADER.FILE_ATTACHMENTS.push(this.register.HEADER.FILE_ATTACHMENTS1)
     let query = { SUPPLIER_REGISTRATION :{ ...this.register}};
    console.log("query : ", query)
this.purchaseService.saveASN(query).subscribe(data => {
    // this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
    // this.loginService.registerUser(this.register).subscribe(data => {
      console.log("rgister : ", data)
      this.spinner.hide(); this.notifier.notify('success', 'Registered Successfully');
      // this.router.navigateByUrl('/auth/login');
    }, err => {
      this.spinner.hide();
      this.notifier.notify('error', 'Something Went Wrong! Please Try again');
      console.log(err);

    });
  }

  setCanvas() {

    const charsArray =
      '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*';
    const lengthOtp = 6;
    const captcha = [];
    for (let i = 0; i < lengthOtp; i++) {
      // below code will not allow Repetition of Characters
      const index = Math.floor(Math.random() * charsArray.length + 1); // get the next character from the array
      if (captcha.indexOf(charsArray[index]) == -1) {
        captcha.push(charsArray[index]);
      } else { i--; }
    }

    // var ctx = .getContext("2d");
    this.ctx.font = '25px Georgia';
    this.ctx.clearRect(0, 0, 100, 50);
    this.ctx.strokeText(captcha.join(''), 0, 30);
    this.originalCaptcha = captcha.join('');
  }
  onEvent() {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.setCanvas();
  }

  submitterdForm() {
    if (this.captchaEntered === this.originalCaptcha) {
      this.registeration();
    } else {
      alert('Enter Correct Captcha');
      this.setCanvas();

    }
  }

  // onFilesAdded(fileInput: any) {
  //   this.fileData = fileInput.target.files as File;
  //   const length = fileInput.target.files.length;
  //   let file = new FormData();
  //   for (let i = 0; i < length; i++) {
  //     file = new FormData();
  //     this.loginService.type = '/GENERAL';
  //   this.loginService.fileType = 'Yes'
  //     file.append('file', this.fileData[i], this.fileData[i].name);
  //     this.loginService.FileUpload(file).subscribe((data) => {
  //       if(i=== length-1){
  //       this.register.file1 = this.register.file1 + this.fileData[i].name;
  //       }
  //     }, err => {
  //       if(i=== length-1){
  //       this.resetFile();
  //       }
  //       if (err) {

  //       } else {
  //       }
  //     });
  //   }
  // }

  onFilesAdded(fileInput: any, type) {
    console.log("type : ", type)
    this.filenameWithoutno1 =[];
    this.filenameWithno1 =[]
    this.filenameWithoutno2 =[];
    this.filenameWithno2 =[]
    this.filenameWithoutno4 =[];
    this.filenameWithno4 =[]
    this.filenameWithoutno3 =[];
    this.filenameWithno3 =[]
    this.spinner.show();
    this.fileInput1 = fileInput;
    this.fileData = fileInput.target.files as File;
    const length = fileInput.target.files.length;

    let file = new FormData();
    for (let i = 0; i < length; i++) {
      file = new FormData();
      this.loginService.type = "/TEMP";
      this.loginService.fileType = "Yes";
      const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      file.append("file", this.fileData[0], S4+"_"+this.fileData[0].name);
      console.log("file : ", file)
      this.loginService.FileUpload(file).subscribe(
        (data) => {
          console.log("onFilesadded : ", data)
          if (i === length - 1) {
            // this.asnHeaders.file1 =
            //   this.asnHeaders.file1 + this.fileData[i].name;

            // this.filenameWithoutno.push(this.fileData[i].name);
            // this.filenameWithno.push("/TEMP/"+S4+"_"+this.fileData[i].name)
            if(type === 1 ){
              console.log("first");
              this.filenameWithoutno1.push({FILE_NAME :this.fileData[i].name, DESCRIPTION : this.description})
              this.filenameWithno1.push({FILE_NAME :"/TEMP/"+S4+"_"+this.fileData[i].name, DESCRIPTION : this.description} )
              // this.filenameWithoutno1.push(this.fileData[i].name);
            // this.filenameWithno1.push("/TEMP/"+S4+"_"+this.fileData[i].name)
             console.log("1" , this.filenameWithno1, this.filenameWithoutno1)
            }else if(type === 2){
              console.log("second")
            //   this.filenameWithoutno2.push(this.fileData[i].name);
            // this.filenameWithno2.push("/TEMP/"+S4+"_"+this.fileData[i].name)
            this.filenameWithoutno2.push({FILE_NAME :this.fileData[i].name, DESCRIPTION : this.description})
            this.filenameWithno2.push({FILE_NAME :"/TEMP/"+S4+"_"+this.fileData[i].name, DESCRIPTION : this.description} )
            console.log("2" , this.filenameWithno2)
             }
             else if(type === 3){
               console.log("third ", this.fileData[i].name)
            //   this.filenameWithoutno3.push(this.fileData[i].name);
            // this.filenameWithno3.push("/TEMP/"+S4+"_"+this.fileData[i].name)
            this.filenameWithoutno3.push({FILE_NAME :this.fileData[i].name, DESCRIPTION : this.description})
            this.filenameWithno3.push({FILE_NAME :"/TEMP/"+S4+"_"+this.fileData[i].name, DESCRIPTION : this.description} )

              console.log("3" , this.filenameWithno3)
            }
            else if(type === 4){
              this.filenameWithoutno4.push({FILE_NAME :this.fileData[i].name, DESCRIPTION : this.description})
              this.filenameWithno4.push({FILE_NAME :"/TEMP/"+S4+"_"+this.fileData[i].name, DESCRIPTION : this.description} )

              // this.filenameWithoutno4.push(this.fileData[i].name);
            // this.filenameWithno4.push("/TEMP/"+S4+"_"+this.fileData[i].name)
            console.log("4" , this.filenameWithno4)
            }
          }
          this.spinner.hide();
          // console.log("filename : ", this.filenameWithno.length)

          // this.register.FILE_ATTACHMENTS=this.filenameWithno;
          // this.resetFile();
          // this.finalSubmitButton = true;
          if(type ===1)this.register.HEADER.FILE_ATTACHMENTS=this.filenameWithno1 || [];
          if(type ===2) this.register.HEADER.FILE_ATTACHMENTS1=this.filenameWithno2 || [];
          if(type ===3) this.register.ADDRESSES.FILE_ATTACHMENTS=this.filenameWithno3 || [];
          if(type ===4)this.register.BANKS.FILE_ATTACHMENTS=this.filenameWithno4 || [];
        },
        //     this.fileName = this.fileData[0].name;
        //     this.fileUpload();
        //   },
        (err) => {
          this.spinner.hide();
          if (i === length - 1) {
            this.resetFile();
          }
          if (err) {
          } else {
          }
        }
      );
      }
  }

  resetFile() {
    this.myInputVariable.nativeElement.value = '';
  }
  removeFile(i, type){
    console.log("remove :", i, this.filenameWithoutno1)
    // this.filenameWithno.splice(i, 1);
    // this.filenameWithoutno.splice(i, 1);
    if(type ===1){
      console.log("inn")
      this.filenameWithno1.splice(i, 1);
      this.filenameWithoutno1.splice(i, 1);
    }
    if(type ===2) {
      this.filenameWithno2.splice(i, 1);
      this.filenameWithoutno2.splice(i, 1);
    }
    if(type ===3) {
      this.filenameWithno3.splice(i, 1);
      this.filenameWithoutno3.splice(i, 1);
    }
    if(type ===4){
      this.filenameWithno4.splice(i, 1);
      this.filenameWithoutno4.splice(i, 1);
    }
    console.log("remove :", i, this.filenameWithoutno1, this.filenameWithno1)
  }

  // onFilesAdded1(fileInput: any) {
  //   this.fileData1 = fileInput.target.files as File;
  //   const length = fileInput.target.files.length;
  //   let file = new FormData();
  //   for (let i = 0; i < length; i++) {
  //     file = new FormData();
  //     this.loginService.type = '/GENERAL';
  //   this.loginService.fileType = 'Yes'
  //     file.append('file', this.fileData1[i], this.fileData1[i].name);
  //     this.loginService.FileUpload(file).subscribe((data) => {
  //       if(i=== length-1){
  //       this.register.file2 = this.register.file2 +this.fileData1[i].name;
  //       }
  //     }, err => {
  //       if(i=== length-1){
  //       this.resetFile1();
  //       }
  //       if (err.error.message) {

  //       } else {

  //       }
  //     });
  //   }

  // }

  resetFile1() {
    this.myInputVariable1.nativeElement.value = '';
  }

  submitAttchment(type){
    console.log("ok clicked", this.register.HEADER.FILE_ATTACHMENTS)
    if(type === 1){ if(this.filenameWithoutno1.length>0) { this.headerFlag = true} }
    if(type === 2){ if(this.filenameWithoutno2.length>0) { this.headerFlag1 = true} }
    if(type === 3){ if(this.filenameWithoutno3.length>0) { this.addressFlag = true} }
    if(type === 4){ if(this.filenameWithoutno4.length>0) { this.bankFlag = true} }
  }

}
