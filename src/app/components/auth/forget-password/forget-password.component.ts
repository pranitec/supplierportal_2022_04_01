import { Component, OnInit } from '@angular/core';
import './../../../../assets/vendor/smtp/smtp.js';
import { NotifierService } from 'angular-notifier';
import { EmailService } from '../service/email.service.js';
import { LoginService } from '../service/login.service';
import { RsaService } from 'src/app/shared/service/rsa.service.js';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { error } from 'protractor';
import { profileData } from 'src/app/config/config'
declare let Email: any;

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  public email = '';
  notifier: NotifierService;
  public text;
  public logo;
  public image;
  public copyright;
  public mobile;
  public website;

  constructor(private notifierService: NotifierService, public emailService: EmailService, private spinner: NgxSpinnerService,
    private loginService: LoginService, private rsaService: RsaService, private router: Router) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    this.text = true;
    this.logo =profileData.LogoFile
    this.image=profileData.BannerImage
    this.copyright = profileData.copyright
    this.website = profileData.websiteURL
    // this.mobile = localStorage.getItem('7');
    // console.log("monbile : ", this.mobile)
  }

  resetPassword() {
    if (this.email.length > 0) {
      this.spinner.show();
      const details = { email: this.email };
      this.loginService.getEmail(details).subscribe((data: any) => {
        // console.log('lll email', data);
        this.spinner.hide();
        let emailId = this.email;
        // localStorage.setItem('7', data.PRIMARY_PHONE);
        emailId = this.rsaService.encrypt(emailId);
        emailId = encodeURIComponent(emailId);
        if (data.VALID_FLAG === 'Y') {
          this.spinner.show();
          const subject = 'Craftsman Automation Limited Supplier Portal - Reset Password';
          const body = `
          Dear ` + name + `,<br/><br/>

          You have requested to reset your password for Supplier Portal through Self Service Portal <br/><br/>

          Please follow the link below to reset your password <br/><br/>

          `+ environment.baseUrl + `/#/auth/password?page=` + emailId + `&id=4<br/><br/>

          For any issues, contact your system administrator.<br/><br/>

          Thank You, <br/>
          Craftsman Automation Team
        `;
        const smsType="Supplier Portal Account Activation"
          this.emailService.sendMail(this.email, subject, body, smsType, data.PRIMARY_PHONE).subscribe(message => {
            // console.log('message email', message);
            this.spinner.hide();
            if (message === 'OK') {
              const id = 2;
              // this.router.navigate(['/auth/view'], { queryParams: { id } });
              this.text=false;
              this.email = '';
            } else {
              this.notifier.notify('warning', 'Email Delivery Failure!');
            }
          },error=>{
            this.notifier.notify('warning', 'Email Delivery Failure!');
          });
        } else {
          this.notifier.notify('error', 'Please enter valid UserName');
        }
      }, err => {
        console.log(err);
      });
    }
    }

}
