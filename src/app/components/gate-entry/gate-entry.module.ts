import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GateEntryRoutingModule } from './gate-entry-routing.module';
import { GateEntryComponent } from './gate-entry/gate-entry.component';
import { FormsModule } from '@angular/forms';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'middle',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 99,
      gap: 15
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 1200,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 800,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 1000,
      easing: 'ease',
      offset: 150
    },
    shift: {
      speed: 500,
      easing: 'ease'
    },
    overlap: 90
  }
};
@NgModule({
  declarations: [GateEntryComponent],
  imports: [
    CommonModule,
    GateEntryRoutingModule,
    FormsModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxPaginationModule,
    NgxSpinnerModule,
  ]
})
export class GateEntryModule { }
