import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { EmailService } from '../../auth/service/email.service';
import { LoginService } from '../../auth/service/login.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
declare var jQuery: any;

@Component({
  selector: 'app-gate-entry',
  templateUrl: './gate-entry.component.html',
  styleUrls: ['./gate-entry.component.css']
})
export class GateEntryComponent implements OnInit {

  public email;
  public supplierName;
  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();
  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
  public file;
  fileData: File = null;
  public file64;
  public uploadInv: UploadInvoice;
  public uniquePos = [];
  public units = [];
  public showDoc = [];
  public showDocAll = [];
  public currencyList = [];
  public searchText;
  public length;
  public accountType;
  public showHide = true;
  public nextPage = false;

///
public asnEntered ='';
public asnHeaderAll =[];
public asnLineAll = [];
public asnHeader = [];
public asnLine = [];
public asnLotsAll = [];
public asnLots = [];
public lotListView = false;
public lotDetails = [];
public notes = '';
public reason = '';


  private notifier: NotifierService;
  constructor( public emailService: EmailService, private router: Router, public breadCrumbServices: BreadcrumbService, private purchaseService: PurchaseService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService,
               private rsaService: RsaService, private notifierService: NotifierService, private loginService: LoginService) {
      this.notifier = notifierService; ;
    }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.accountType = this.rsaService.decrypt(localStorage.getItem('6'));
    // this.accountType = 'SUPPLIER';
    this.uploadInv = new UploadInvoice();

    (function($, thi) {

      $(document).ready(function() {

      });

    })(jQuery, this);
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/finance/uploadinvoice') {
        this.showHideTable();
      }
    });

    (function ($) {

      $(document).ready(function () {
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        const today = new Date();
        const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
          months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();

        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
          value: fromDate,
          change(e) {
            localStorage.setItem('INDF', e.target.value);
          }

        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
          value: toDate,
          change(e) {
            localStorage.setItem('INDT', e.target.value);
          }
        });
      });
    })(jQuery);

    this.getGateEntries();
    this.getCurrencyList();
  }

  setAsDataTable() {

    this.chRef.detectChanges();
    (function($) {
      $('#Pending-table').DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        pageLength: 5,
      });
    })(jQuery);
    this.asnHeader.forEach((f, i) => {
      (function ($) {
        $('#test' + i).DataTable({
          responsive: true,
          bLengthChange: false,
          retrieve: true,
          pageLength: 5,
        });
      })(jQuery);

    });

  }


  getCurrencyList(){
    // const details = { email: this.email, purchaseType: 'ALL_CURRENCIES' };
    const details = this.purchaseService.inputJSON('ALL_CURRENCIES', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data:any)=>{
      // console.log(
      //   'data',data
      // );
      this.currencyList = Array.isArray(data.FREIGHT_TERMS.FREIGHT_TERM) ? data.FREIGHT_TERMS.FREIGHT_TERM : (data.FREIGHT_TERMS.FREIGHT_TERM) ? [data.FREIGHT_TERMS.FREIGHT_TERM] : [];
    },error=>{
      console.log(error);
    })
  }

  getGateEntries() {
    this.spinner.show();
    // this.fromDate = localStorage.getItem('PODF');
    // this.toDate = localStorage.getItem('PODT');
    //console.log(this.fromDate,this.toDate);
    this.fromDate = localStorage.getItem('INDF');
    this.toDate = localStorage.getItem('INDT');
    // const details = { email: this.email, purchaseType: 'GATE_ENTRY_LIST', date1: this.fromDate, date2: this.toDate };
    const details = this.purchaseService.inputJSON('GATE_ENTRY_LIST', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('ppp', data);
      this.spinner.hide();
      if (data) {
        this.showDocAll = Array.isArray(data.GATE_ENTRIES.GATE_ENTRY) ? data.GATE_ENTRIES.GATE_ENTRY : (data.GATE_ENTRIES.GATE_ENTRY) ? [data.GATE_ENTRIES.GATE_ENTRY] : [];

        this.showHideTable();
        //this.setAsDataTable();
        // this.fullpoHeaderList = Array.isArray(data.PASTPO.POHEADERS.POHEADER) ? data.PASTPO.POHEADERS.POHEADER : (data.PASTPO.POHEADERS.POHEADER) ? [data.PASTPO.POHEADERS.POHEADER] : [];
        // this.fullpolines = Array.isArray(data.PASTPO.POLINES.POLINE) ? data.PASTPO.POLINES.POLINE : (data.PASTPO.POHEADERS.POHEADER) ? [(data.PASTPO.POHEADERS.POHEADER)] : [];
        // this.fullSummary = Array.isArray(data.PASTPO.POSUMMARIES.POSUMMARY) ? data.PASTPO.POSUMMARIES.POSUMMARY : (data.PASTPO.POSUMMARIES.POSUMMARY) ? [data.PASTPO.POSUMMARIES.POSUMMARY] : [];
        // this.summary = this.fullSummary;

        // this.poCountsAll = Array.isArray(data.PASTPO.POCOUNTS.POCOUNT) ? data.PASTPO.POCOUNTS.POCOUNT : (data.PASTPO.POCOUNTS.POCOUNT) ? [data.PASTPO.POCOUNTS.POCOUNT] : [];
        // this.poCounts = this.poCountsAll;

        // this.setPoSummary();
        // this.setFilteredData();
        // this.setSummaryData();
      } else {
      }

    }, error => {
      this.spinner.hide();
      console.log(error);
    });



  }

  getASNDetails() {

    this.spinner.show();
    const details = { purchaseType: 'ASN_DETAILS_FOR_ORDER', headerId: this.asnEntered };
    this.purchaseService.getDocs(details).subscribe((data: any) => {
      this.spinner.hide();
      // console.log('DAADADA',data);
      this.lotListView = true;
      if (data) {
        //this.uniquePos = Array.isArray(data.PO_LISTS.PO_LIST) ? data.PO_LISTS.PO_LIST : (data.PO_LISTS.PO_LIST) ? [data.PO_LISTS.PO_LIST] : [];

        this.asnHeaderAll = Array.isArray(data.ASNDETAILS.ASN_HEADERS.ASN_HEADER) ? data.ASNDETAILS.ASN_HEADERS.ASN_HEADER : (data.ASNDETAILS.ASN_HEADERS.ASN_HEADER) ? [data.ASNDETAILS.ASN_HEADERS.ASN_HEADER] : [];
        this.asnLineAll = Array.isArray(data.ASNDETAILS.ASN_LINES.ASN_LINE) ? data.ASNDETAILS.ASN_LINES.ASN_LINE : (data.ASNDETAILS.ASN_LINES.ASN_LINE) ? [(data.ASNDETAILS.ASN_LINES.ASN_LINE)] : [];
        this.asnLotsAll = Array.isArray(data.ASNDETAILS.ASN_LOTS.ASN_LOT) ? data.ASNDETAILS.ASN_LOTS.ASN_LOT : (data.ASNDETAILS.ASN_LOTS.ASN_LOT) ? [data.ASNDETAILS.ASN_LOTS.ASN_LOT] : [];
        this.asnHeader = this.asnHeaderAll;
        this.asnLine = this.asnLineAll;
        this.asnLots = this.asnLotsAll;
        this.asnHeader.forEach((f, i) => {
          this.asnHeader[i].list = this.asnLineAll.filter(x => x.ASN_ID == f.ASN_ID);
        });
        this.asnHeader = [this.asnHeader[0]];
        this.setFilteredData();
        // this.fullpoHeaderList = Array.isArray(data.PASTPO.POHEADERS.POHEADER) ? data.PASTPO.POHEADERS.POHEADER : (data.PASTPO.POHEADERS.POHEADER) ? [data.PASTPO.POHEADERS.POHEADER] : [];
        // this.fullpolines = Array.isArray(data.PASTPO.POLINES.POLINE) ? data.PASTPO.POLINES.POLINE : (data.PASTPO.POHEADERS.POHEADER) ? [(data.PASTPO.POHEADERS.POHEADER)] : [];
        // this.fullSummary = Array.isArray(data.PASTPO.POSUMMARIES.POSUMMARY) ? data.PASTPO.POSUMMARIES.POSUMMARY : (data.PASTPO.POSUMMARIES.POSUMMARY) ? [data.PASTPO.POSUMMARIES.POSUMMARY] : [];
        // this.summary = this.fullSummary;

        // this.poCountsAll = Array.isArray(data.PASTPO.POCOUNTS.POCOUNT) ? data.PASTPO.POCOUNTS.POCOUNT : (data.PASTPO.POCOUNTS.POCOUNT) ? [data.PASTPO.POCOUNTS.POCOUNT] : [];
        // this.poCounts = this.poCountsAll;

        // this.setPoSummary();
        // this.setFilteredData();
        // this.setSummaryData();
      } else {
      }

    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }




  cancel() {


  }




  Accept() {

    this.spinner.show();
// &lt;Invoice Number&gt;
// &lt;PO Number&gt;

const details = {
  GATE_ENTRY:{
    ASN_ID:this.asnHeader[0].ASN_ID,
    NOTES:this.notes,
    STATUS:'A',
    USER_NAME:this.email,

  }
}

// console.log('DET',details);
    this.purchaseService.createGateEntry(details).subscribe(data => {
      // console.log('accept', data);
      this.spinner.hide();

      if(data){

      this.notifier.notify('success', 'Gate Entry No.' + data + ' accepted successfully')
      }
      setTimeout(() => {
        this.resetPage();
        this.getGateEntries();
      }, 1600);

    }, error => {
      console.log(error);
    });

  }

  Reject() {

    this.spinner.show();
// &lt;Invoice Number&gt;
// &lt;PO Number&gt;

const details = {
  GATE_ENTRY:{
    ASN_ID:this.asnHeader[0].ASN_ID,
    NOTES:this.notes,
    STATUS:'R',
    REASON_DESCRIPTION:this.notes,
    USER_NAME:this.email,
  }
}
// console.log('DET',details);
    this.purchaseService.createGateEntry(details).subscribe(data => {
      // console.log('reject', data);
      this.spinner.hide();
      if(data){

        this.notifier.notify('success', 'Gate Entry No.' + data + ' rejected successfully')
        }
        setTimeout(() => {
          this.resetPage();
          this.getGateEntries();
        }, 1600);

    }, error => {
      console.log(error);
    });

  }



resetPage() {
  this.uploadInv = new UploadInvoice();

}

onOpenModel() {
  this.uploadInv = new UploadInvoice();
  this.lotListView = false ;
  this.asnHeader = [];
  this.asnHeaderAll = [];
  this.asnEntered = '';
  this.notes = '';


  (function($, thi) {

})(jQuery, this);
}




showHideTable() {
  this.showHide = false;
  setTimeout(() => {
    this.showHide = true;
    this.setFilteredData();
  }, 10);
}


setFilteredData() {

  this.asnHeader = this.asnHeaderAll.filter(f => {
    let a = true;

        if (this.searchText) {
      a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
    }

    return a;

  });
  //   this.poHeadersList = this.poHeadersList.slice(0, 20);
  this.asnHeader.forEach((f, i) => {
    this.asnHeader[i].list = this.asnLineAll.filter(x => x.ASN_ID == f.ASN_ID);
  });

  this.asnHeader = [this.asnHeader[0]];

  this.showDoc = this.showDocAll.filter(f => {
    let a = true;


    if (this.searchText) {
      a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
    }
    return a;
  });
  this.showDoc =this.showDoc.sort((a,b)=> Number(a.GATE_ENTRY_ID)-Number(b.GATE_ENTRY_ID))
  // console.log('show', this.showDoc);

  this.setAsDataTable();
  // this.showHideTable();
}
goNextPage(row) {
  this.nextPage = true;
  this.asnEntered = row.ASN_ID;
  this.getASNDetails();
}
goBack(){
  this.nextPage = false;
  this.onOpenModel();
  (function ($) {

    $(document).ready(function () {
      const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      const today = new Date();
      const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
      const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
        months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
      const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();

      $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
        // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
        value: fromDate,
        change(e) {
          localStorage.setItem('INDF', e.target.value);
        }

      });
      $('#datepicker1').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'dd-mmm-yyyy',
        // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
        value: toDate,
        change(e) {
          localStorage.setItem('INDT', e.target.value);
        }
      });
    });
  })(jQuery);
}
}

class UploadInvoice {
  BUYER_UNIT = '';
  PO_NUMBER = '';
  INVOICE_NUMBER = '';
  INVOICE_DATE = '';
  CURRENCY_CODE = '';
  INVOICE_AMOUNT = '';
  INVOICE_DESCRIPTION = '';
  FILE_NAME = '';
  USER_NAME = '';
  SUBJECT = '';
  BODY = '';
  TO = '';
  INVOICE_MODE = '';
}

