import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GateEntryComponent } from './gate-entry/gate-entry.component';


const routes: Routes = [
  {
    path: '',
    component: GateEntryComponent,
    data: {
      title: 'Gate Entry',
      breadcrumb: 'Gate Entry'
    } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GateEntryRoutingModule { }
