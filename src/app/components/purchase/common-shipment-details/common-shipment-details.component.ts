import { Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { ShipmentService } from '../../shipment/service/shipment.service';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PurchaseService } from '../service/purchase.service';
declare var jQuery: any;

@Component({
  selector: 'app-common-shipment-details',
  templateUrl: './common-shipment-details.component.html',
  styleUrls: ['./common-shipment-details.component.css']
})
export class CommonShipmentDetailsComponent implements OnInit {
  @Input() shipmentId: string;
  @Input() shipmentView: boolean;
  @Input() fieldName: string;
  @Output("goBackShip") goBackShip: EventEmitter<any> = new EventEmitter();
  public email;
  public poHeadersList = [];
  public fullpoHeaderList = [];
  public poLineList = [];
  public fullpolines = [];
  public fullSummary = [];
  public viewShipmenList = [];

  public searchText = '';
  public page: any;
  public viewCount = 10;
  public viewGrid = true;

  public uom = ''
  // public fromDate;
  // public toDate;
  constructor(private shipmentService: ShipmentService, private chRef: ChangeDetectorRef,
    public breadCrumbServices: BreadcrumbService, private router: Router, public rsaService: RsaService, private spinner: NgxSpinnerService,
    public purchaseService : PurchaseService) { }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
    });

    this.getViewReceipts();

    (function ($) {

      $(document).ready(function () {
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        const today = new Date();
        const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
          months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();

        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
          value: fromDate,
          change(e) {
            // $('#datepicker').val(e.target.value);
            // $('#datepicker').trigger('input');
            // $('#datepicker').trigger('ngModelChange');
            localStorage.setItem('PODF', e.target.value);

            this.fromDate = e.target.value;
          }

        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
          value: toDate,
          change(e) {
            localStorage.setItem('PODT', e.target.value);
          }
        });
        // $('#grid-sections').show();
        // $('#list-sections').hide();,m
        // $('#grid').click(function() {
        //   $('#list-sections').hide();
        //   $('#grid-sections').show();
        //   $('#list').removeClass('btn-success');
        //   $('#list').addClass('btn-light');
        //   $(this).removeClass('btn-light');
        //   $(this).addClass('btn-success');
        // });
        // $('#list').click(function() {
        //   $('#list-sections').show();
        //   $('#grid-sections').hide();

        //   $('#Past-details-view').hide();
        //   $('#Past-details').show();

        //   $('#grid').removeClass('btn-success');
        //   $('#grid').addClass('btn-light');

        //   $(this).removeClass('btn-light');
        //   $(this).addClass('btn-success');
        // });
        // $('#Past-details-view').hide();
        // $('#goback').hide();

        // $('#goback').click(function() {
        //   $('#Past-details-view').hide();
        //   $('#Past-details').show();
        // });
      });
    })(jQuery);
  }


  getViewReceipts() {
    this.spinner.show();

    // const details = { email: this.email, shipmentType: 'PO_RECEIPT', lineId: this.shipmentId };
    const details = this.purchaseService.inputJSON('PO_RECEIPT', undefined, undefined, this.fieldName, this.shipmentId)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log("getViewReceipts : ", data)
    // this.shipmentService.getShipmentsById(details).subscribe((data: any) => {
      this.spinner.hide();
      if (data) {
        // this.fullpoHeaderList = Array.isArray(data.PORECEIPT.RECEIPTHEADERS.RECEIPTHEADER) ? data.PORECEIPT.RECEIPTHEADERS.RECEIPTHEADER : (data.PORECEIPT.RECEIPTHEADERS.RECEIPTHEADER) ? [data.PORECEIPT.RECEIPTHEADERS.RECEIPTHEADER] : [];
        // this.fullpolines = Array.isArray(data.PORECEIPT.RECEIPTLINES.RECEIPTLINE) ? data.PORECEIPT.RECEIPTLINES.RECEIPTLINE : (data.PORECEIPT.RECEIPTLINES.RECEIPTLINE) ? [(data.PORECEIPT.RECEIPTLINES.RECEIPTLINE)] : [];
        // this.fullSummary = Array.isArray(data.PORECEIPT.RECEIPTLOTS.RECEIPTLOT) ? data.PORECEIPT.RECEIPTLOTS.RECEIPTLOT : (data.PORECEIPT.RECEIPTLOTS.RECEIPTLOT) ? [data.PORECEIPT.RECEIPTLOTS.RECEIPTLOT] : [];
        this.fullpoHeaderList = data.PORECEIPT.RECEIPTHEADERS ? data.PORECEIPT.RECEIPTHEADERS.map(x=>x.RECEIPTHEADER) :[]
        this.fullpolines = data.PORECEIPT.RECEIPTLINES ? data.PORECEIPT.RECEIPTLINES.map(x=>x.RECEIPTLINE) :[]
        this.fullSummary = data.PORECEIPT.RECEIPTLOTS ? data.PORECEIPT.RECEIPTLOTS.map(x=>x.RECEIPTLOT) :[]
        this.setFilteredData();
        this.setSummaryData();
      } else {
        console.error('NULL VALUE');
      }

    }, error => {
      this.spinner.hide();
      console.log(error);
    });

  }

  setFilteredData() {
    this.poHeadersList = this.fullpoHeaderList.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }


      // if (this.fromDate) {
      //   a = a && this.fromDate < f.PO_DATE;
      // }

      // if (this.toDate) {
      //   a = a && this.toDate > f.PO_DATE;
      // }

      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });
    //   this.poHeadersList = this.poHeadersList.slice(0, 20);
    this.poHeadersList.forEach((f, i) => {
      this.poHeadersList[i].list = this.fullpolines.filter(x => x.SHIPMENT_HEADER_ID == f.SHIPMENT_HEADER_ID);
    });
    this.setAsDataTable();
  }

  setAsDataTable() {
    this.chRef.detectChanges();
    this.poHeadersList.forEach((f, i) => {
      (function ($) {
        $('#test' + i).DataTable({
          responsive: true,
          bLengthChange: false,
          retrieve: true,
          pageLength: 5,
        });
      })(jQuery);
    });
  }

  setAsDataTableList() {
    this.chRef.detectChanges();
    (function ($) {
      $('#Past-table').DataTable({
        responsive: true,
        iDisplayLength: 5,
        bLengthChange: false,
        retrieve: true,
        pageLength: 5,
      });

      $('#Past-table').draw();
    })(jQuery);

  }

  public onPageChanged(event) {
    this.page = event;
  }
  onViewGrid() {
    this.viewGrid = true;
    this.searchText = '';
    this.setAsDataTable();

  }

  onViewList() {
    this.viewGrid = false;
    this.searchText = '';
    this.setAsDataTableList();

  }

  exportExcel() {
    const poLines = [];
    this.poHeadersList.forEach(f => {
      poLines.push(...f.list);
    });
    const details = { POLINES: poLines };
    this.shipmentService.getExcel(details);
  }


  setSummaryData() {
    let pendingQuantity = 0, orderedQuantity = 0, ordersReceived = 0, deliveredQuantity = 0;

    this.fullSummary.forEach(f => {
      pendingQuantity = pendingQuantity + Number(f.PENDING_PO_QUANTITY);
      orderedQuantity = orderedQuantity + Number(f.ORDERED_QUANTITY);
      ordersReceived = ordersReceived + Number(f.PENDING_PO_COUNT);
      deliveredQuantity = deliveredQuantity + Number(f.DELIVERED_QUANTITY);
      if (f.UOM) {
        this.uom = f.UOM;
      }
    });

    // this.pendingQuantity = pendingQuantity;
    // this.orderedQuantity = orderedQuantity;
    // this.ordersReceived = ordersReceived;
    // this.deliveredQuantity = deliveredQuantity;
  }
  fromDateChanged() {

  }


  changeView(shipment_id) {

    this.viewShipmenList = this.fullSummary.filter(f => f.SHIPMENT_LINE_ID === shipment_id)
    this.viewGrid = false;
  }
  goBack() {
    this.viewGrid = true;
  }

  goBackFromShip() {
    this.shipmentView = false;
    this.goBackShip.emit();
  }

}
