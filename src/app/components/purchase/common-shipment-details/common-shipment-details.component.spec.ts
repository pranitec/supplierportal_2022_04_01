import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonShipmentDetailsComponent } from './common-shipment-details.component';

describe('CommonShipmentDetailsComponent', () => {
  let component: CommonShipmentDetailsComponent;
  let fixture: ComponentFixture<CommonShipmentDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonShipmentDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonShipmentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
