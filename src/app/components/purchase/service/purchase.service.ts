import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpClient, HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { RsaService } from 'src/app/shared/service/rsa.service';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {

  public baseUrl = environment.apiBaseUrl + '/purchase';
  public email;
  constructor(private http: HttpClient, private rsaService: RsaService) {
   }

inputJSON(doctype, dateFrom?, dateTo?, fieldName?, docId?){
  this.email = this.rsaService.decrypt(localStorage.getItem('3'));
  let query = {
      INPUT_HEADER :{ MAIL_ID :this.email},
      FILTER_DETAILS :{ MENU_NAME:doctype, DOCUMENT_TYPE: doctype, DATE_FROM : dateFrom, DATE_TO: dateTo , FIELD_NAME: fieldName, FIELD_VALUE : docId }
    }

return query
}

  getPurchaseOrder(email) {
    return this.http.post(`${this.baseUrl}`, email);
  }

  getExcel(data) {
    const title = data.title;
    return this.http.post(`${this.baseUrl}/getExcel`, data, {
      params: new HttpParams().append('token', localStorage.getItem('1')),
      observe: 'response', responseType: 'text'
    }).subscribe(r => { saveAs(new Blob([r.body], { type: 'text/csv' }), title + '.csv'); });

  }

  getDocs(query) {
    return this.http.post(`${this.baseUrl}/docs`, query);
  }
  getDocPrint(query) {
    return this.http.post(`${this.baseUrl}/print`, query,{
      responseType: 'arraybuffer'
    });
  }

  downloadAttachment(dataFile) {

    return this.http.post(`${this.baseUrl}/attachment`, dataFile, {
      responseType: 'arraybuffer'
    })
  }

  setAccept(query){
    return this.http.post(`${this.baseUrl}/accept`,query)
  }

  FileUpload(file) {
    return this.http.post(`${this.baseUrl}/updateAttachment`, file);
}

saveASN(data){
  return this.http.post(`${this.baseUrl}/saveASN`,data);
}

getPurchaseOrderList(data){
  return this.http.post(`${this.baseUrl}/list`,data);
}
createInvoice(data){
  return this.http.post(`${this.baseUrl}/createInvoice`,data);
}
saveInvoice(data){
  return this.http.post(`${this.baseUrl}/saveInvoice`,data);
}

createGateEntry(data){
  return this.http.post(`${this.baseUrl}/createGateEntry`,data);
}
updateMenu(data){
  return this.http.put(`${this.baseUrl}/updateMenu`,data);
}
}
