import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastPurchaseReleaseComponent } from './past-purchase-release.component';

describe('PastPurchaseReleaseComponent', () => {
  let component: PastPurchaseReleaseComponent;
  let fixture: ComponentFixture<PastPurchaseReleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastPurchaseReleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastPurchaseReleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
