import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastAgreementsComponent } from './past-agreements.component';

describe('PastAgreementsComponent', () => {
  let component: PastAgreementsComponent;
  let fixture: ComponentFixture<PastAgreementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastAgreementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastAgreementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
