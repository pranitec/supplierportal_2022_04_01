import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PurchaseService } from '../service/purchase.service';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
declare var jQuery: any;

@Component({
  selector: 'app-past-agreements',
  templateUrl: './past-agreements.component.html',
  styleUrls: ['./past-agreements.component.css']
})
export class PastAgreementsComponent implements OnInit {

  public email;
  public shipmentView = false;
  public lineId = '';
  public poHeadersList = [];
  public fullpoHeaderList = [];
  public poLineList = [];
  public fullpolines = [];
  public fullSummary = [];
  public summary = [];
  public poCountsAll = [];
  public poCounts = [];
  public searchText = '';
  public page = 1;
  public viewCount = 10;
  public viewGrid = true;
  public agreementsReceived = 0;
  public agreementQuantity = 0;
  public deliveredQuantity = 0;
  public pendingQuantity = 0;
  public uom = '';
  public modal;
  public modalHeader;
  public modalLine1;
  public modalLine2;
  public modalLine3;
  public modalLine4;
  public action= false;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst
  public selectedmenu;
  public selectedmenuFilter =[];
  public oneyearstartingDate : Date;
  private notifier: NotifierService;

  // public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  // public today = new Date();
  // public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
  public noOfMonths;
  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();
  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

  constructor(private purchaseService: PurchaseService, private chRef: ChangeDetectorRef,private notifierService: NotifierService,
    public breadCrumbServices: BreadcrumbService, private router: Router, public rsaService: RsaService, private spinner: NgxSpinnerService) {
      this.page=1;
      this.notifier = notifierService;
     }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.selectedmenu= this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
    this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(({MENUACCESS})=> MENUACCESS.MENU_CODE === 'PAST_PO_AGREEMENTS');
    localStorage.setItem('PODF', this.fromDate);
    localStorage.setItem('PODT', this.toDate);

    this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START)
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.noOfMonths  = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE /31)
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/purchase/pastAgreements') {
        this.setFilteredData();
        this.setPoSummary();
      }
    });

    this.getPastAgreements();

    (function ($, _this) {
      $(document).ready(function () {
        // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        // const today = new Date();
        // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-'
        //   + months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          value: _this.fromDate,
          minDate : _this.oneyearstartingDate,
          change(e) {
            // $('#datepicker').val(e.target.value);
            // $('#datepicker').trigger('input');
            // $('#datepicker').trigger('ngModelChange');
            localStorage.setItem('PADF', e.target.value);
            _this.fromDate = e.target.value;
          }
        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          value: _this.toDate,
          maxDate: new Date(),
          change(e) {
            localStorage.setItem('PADT', e.target.value);
            _this.toDate = e.target.value
          }
        });
        // $('#grid-sections').show();
        // $('#list-sections').hide();
        // $('#grid').click(function() {
        //   $('#list-sections').hide();
        //   $('#grid-sections').show();
        //   $('#list').removeClass('btn-success');
        //   $('#list').addClass('btn-light');
        //   $(this).removeClass('btn-light');
        //   $(this).addClass('btn-success');
        // });
        // $('#list').click(function() {
        //   $('#list-sections').show();
        //   $('#grid-sections').hide();

        //   $('#Past-details-view').hide();
        //   $('#Past-details').show();

        //   $('#grid').removeClass('btn-success');
        //   $('#grid').addClass('btn-light');

        //   $(this).removeClass('btn-light');
        //   $(this).addClass('btn-success');
        // });
        // $('#Past-details-view').hide();
        // $('#goback').hide();

        // $('#goback').click(function() {
        //   $('#Past-details-view').hide();
        //   $('#Past-details').show();
        // });
      });
    })(jQuery, this);
  }


  getPastAgreements() {
    this.spinner.show();
    // this.fromDate = localStorage.getItem('PADF');
    // this.toDate = localStorage.getItem('PADT');

    // const details = { email: this.email, purchaseType: 'PAST_AGREEMENT', date1: this.fromDate, date2: this.toDate };
    var time_difference =  Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
    var months_difference =  Math.floor(time_difference / (1000 * 60 * 60 * 24)/31);
    // console.log("entered date : ", this.fromDate, this.toDate, time_difference, months_difference);

    if((-(months_difference)) >this.noOfMonths){
      // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
      this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
      this.spinner.hide();
    } else{
    const details = this.purchaseService.inputJSON('PAST_AGREEMENT', this.fromDate, this.toDate)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('lll getPastAgreeents', data);
      // this.fullpoHeaderList = Array.isArray(data.PASTRELEASE.POHEADERS.POHEADER) ? data.PASTRELEASE.POHEADERS.POHEADER : (data.PASTRELEASE.POHEADERS.POHEADER) ? [data.PASTRELEASE.POHEADERS.POHEADER] : [];
      // this.fullpolines = Array.isArray(data.PASTRELEASE.POLINES.POLINE) ? data.PASTRELEASE.POLINES.POLINE : (data.PASTRELEASE.POHEADERS.POHEADER) ? [(data.PASTRELEASE.POHEADERS.POHEADER)] : [];
      // this.fullSummary = Array.isArray(data.PASTRELEASE.POSUMMARIES.POSUMMARY) ? data.PASTRELEASE.POSUMMARIES.POSUMMARY : (data.PASTRELEASE.POSUMMARIES.POSUMMARY) ? [data.PASTRELEASE.POSUMMARIES.POSUMMARY] : [];

      this.fullpoHeaderList = data.PASTAGREEMENT.POHEADERS ? data.PASTAGREEMENT.POHEADERS.map(x=>(x.POHEADER)) :[]
      this.fullpolines = data.PASTAGREEMENT.POLINES ? data.PASTAGREEMENT.POLINES.map(x=>(x.POLINE)) : []
      this.fullSummary = data.PASTAGREEMENT.POSUMMARIES ? data.PASTAGREEMENT.POSUMMARIES.map(x=>(x.POSUMMARY)) :[]

      this.summary = this.fullSummary;
      // this.poCountsAll = Array.isArray(data.PASTRELEASE.POCOUNTS.POCOUNT) ? data.PASTRELEASE.POCOUNTS.POCOUNT : (data.PASTRELEASE.POCOUNTS.POCOUNT) ? [data.PASTRELEASE.POCOUNTS.POCOUNT] : [];

      // this.poCountsAll = data.PASTRELEASE.POCOUNTS ? data.PASTRELEASE.POCOUNTS.map(x=>(x.POCOUNT)) : []


      this.spinner.hide();
      this.setFilteredData();
      this.setSummaryData();
      this.setPoSummary();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
    }
  }

  setFilteredData() {
    this.poHeadersList = this.fullpoHeaderList.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }


      // if (this.fromDate) {
      //   a = a && this.fromDate < f.PO_DATE;
      // }

      // if (this.toDate) {
      //   a = a && this.toDate > f.PO_DATE;
      // }

      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });
    //   this.poHeadersList = this.poHeadersList.slice(0, 20);
    this.poHeadersList.forEach((f, i) => {
      // this.deliveredQty = this.deliveredQty + Number(f.DELIVERED_QUANTITY);
      // this.pendingQty = this.pendingQty + Number(f.PENDING_PO_QUANTITY);
      // this.pendingCount = this.pendingCount + Number(f.PENDING_PO_COUNT);
      // this.orderedQty = this.orderedQty + Number(f.ORDERED_QUANTITY);
      this.poHeadersList[i].list = this.fullpolines.filter(x => x.PO_HEADER_ID === f.PO_HEADER_ID);
    });
    this.setAsDataTable();
  }

  setAsDataTable() {
    this.chRef.detectChanges();
    this.poHeadersList.forEach((f, i) => {
      (function ($) {
        $('#test' + i).DataTable({
          responsive: true,
          bLengthChange: false,
          retrieve: true,
          pageLength: 5,
        });
      })(jQuery);
    });
  }

  setAsDataTableList() {
    this.chRef.detectChanges();
    (function ($) {
      $('#Past-table').DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        pageLength: 5,
      });
    })(jQuery);

  }

  public onPageChanged(event) {
    this.page = event;
  }
  onViewGrid() {
    this.viewGrid = true;
    this.searchText = '';
    this.setAsDataTable();

  }

  onViewList() {
    this.viewGrid = false;
    this.searchText = '';
    this.setAsDataTableList();

  }

  exportExcel() {
    const poLines = [];
    this.poHeadersList.forEach(f => {
      poLines.push(...f.list);
    });
    const details = { POLINES: poLines, title: 'Past Agreement Report' };
    this.purchaseService.getExcel(details);
  }

  setPoSummary() {

    this.summary = this.fullSummary.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      // if (this.searchText) {
      //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      // }

      return a;

    });

    // this.poCounts = this.poCountsAll.filter(f => {
    //   let a = true;

    //   if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
    //     a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

    //   }
    //   if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
    //     a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
    //   }
    //   if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
    //     a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
    //   }
    //   // if (this.searchText) {
    //   //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
    //   // }

    //   return a;

    // });

    this.setSummaryData();
  }



  setSummaryData() {
    let agreementsReceived = 0; let agreementQuantity = 0; let deliveredQuantity = 0;
    let pendingQuantity = 0;

    this.summary.forEach(f => {
      agreementQuantity = agreementQuantity + Number(f.ORDERED_QUANTITY);
      deliveredQuantity = deliveredQuantity + Number(f.DELIVERED_QUANTITY);
      pendingQuantity = pendingQuantity + Number(f.PENDING_PO_COUNT);
      if (f.UOM) {
        this.uom = f.UOM;
      }

    });
    // this.poCounts.forEach(f=>{
    //   agreementsReceived = agreementsReceived + Number(f.PAST_RELEASE_COUNT);
    // })
    this.agreementsReceived = agreementsReceived;
    this.agreementQuantity = agreementQuantity;
    this.deliveredQuantity = deliveredQuantity;
    this.pendingQuantity = pendingQuantity;
  }

  clickedLineId(lineId) {
    this.lineId = lineId;
    this.shipmentView = true;
  }
  goBackShip() {
    this.shipmentView = false;
  }

  fromDateChanged(){

  }

  getSummaryDetails(data) {
    this.modalLine1 = false;
    this.modalLine2 = false;
    this.modalLine3 = false;
    this.modalLine4 = false;
    if (data === 'received') {
      this.modal = 'Agreements Received';
      this.modalHeader = 'Agreement Received Count';
      this.modalLine1 = true;
    } else if (data === 'orderQty') {
      this.modal = 'Agreement Quantity Details';
      this.modalHeader = 'Agreement Quantity';
      this.modalLine2 = true;
    } else if (data === 'deliverQty') {
      this.modal = 'Delivered Quantity Details';
      this.modalHeader = 'Delivered Quantity';
      this.modalLine3 = true;
    } else if (data === 'pendingQty') {
      this.modal = 'Pending Quantity Details';
      this.modalHeader = 'Pending Quantity';
      this.modalLine4 = true;
    }

  }
  viewTaxButton1(element, i, j){
    this.action = false;
    this.poHeadersList[i].list[j].action = false;

  }
  viewTaxButton2(element,i, j){
    this.action = true;
    this.poHeadersList[i].list[j].action = true
    // const details = { email: this.email, attachmentString: element.TAX_IDENTIFICATION_STRING, purchaseType: 'TAX_LINES'};
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
      var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', element.TAX_IDENTIFICATION_STRING)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.myGst = this.taxData[0].MY_TAX_REG_NO

      this.poHeadersList[i].list[j].taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.poHeadersList[i].list[j].taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.poHeadersList[i].list[j].taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.poHeadersList[i].list[j].myGst = this.taxData[0].MY_TAX_REG_NO
      this.poHeadersList[i].list[j].hsnsacCode = this.taxData[0].HSN_SAC_CODE
      this.poHeadersList[i].list[j].hsnsacCodeDesc = this.taxData[0].HSN_SAC_CODE_DESC

      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
}
