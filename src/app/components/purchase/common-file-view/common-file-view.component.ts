import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { PurchaseService } from '../service/purchase.service';
import { saveAs } from 'file-saver';
import { NgxSpinnerService } from 'ngx-spinner';
import { Upload } from '../model/upload';
import { LoginService } from '../../auth/service/login.service';
import { NotifierService } from 'angular-notifier';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { Router } from '@angular/router';
import pdfMake from "pdfmake/build/pdfmake";
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { EmailService } from '../../auth/service/email.service';
import { SSL_OP_SINGLE_DH_USE } from 'constants';
import { profileData } from 'src/app/config/config'


@Component({
  selector: 'app-common-file-view',
  templateUrl: './common-file-view.component.html',
  styleUrls: ['./common-file-view.component.css']
})
export class CommonFileViewComponent implements OnInit {
  @Input() headerId: string;
  @Input() type: string;
  @Input() attachmentString: string;
  @Input() printDocString : string;
  @Input() headerData : any
  @Input() poNumber: string;
  @Input() communicationString : string;


  public attachments: any[] = [];
  public upload: Upload;
  fileData: File = null;
  notifier: NotifierService;
  public fileInput1;
  public email;
  public bodyFontSize = 10;
  public asnLots = [];
  public asnLotsAll = []
  public asnTransporter = [];
  public asnLines =[]
  public supplierName
  public editReason;
  public deleteReason;
  public communication=[];
  public addUpdateFlag = false;
  public updateText =''
  public title=''

  constructor(private purchaseService: PurchaseService, private spinner: NgxSpinnerService,
    private loginService: LoginService, private notifierService: NotifierService,public rsaService: RsaService,
    private router: Router, private breadCrumbServices: BreadcrumbService, public emailService: EmailService) {
      this.notifier = notifierService;
     }

  @ViewChild('myFileInput', { static: false }) myInputVariable: ElementRef;

  ngOnInit(): void {
    // console.log("communicationString ", this.communicationString, this.type, this.headerId)
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.supplierName = this.breadCrumbServices.filteredSelection[0].VENDOR_NAME;
    this.upload = new Upload();
  }

  getDocumentList() {
    this.spinner.show();
    // 247380
    if(this.type === 'INVOICE_DOC_ATTACHMENTS') {
      const details = { email: this.email, purchaseType: 'INVOICE_DOC_ATTACHMENTSS', headerId: this.headerId };
      this.purchaseService.getDocs(details).subscribe((data: any) => {
        this.attachments = data.ATTACHMENTS? data.ATTACHMENTS.map(x=>(x.ATTACHMENT)) :[]
        // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
        //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
        this.spinner.hide();

      }, error => {
        console.log(error);
        this.spinner.hide();
      });
    }else if(this.type !== 'ASN_DETAILS') {
    // const details = { email: this.email, purchaseType: 'PO_ATTACHMENTS', headerId: this.headerId };
    // const details = { email: this.email,  purchaseType: 'ATTACHMENT_LIST', attachmentString: this.attachmentString };
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
      var query = this.purchaseService.inputJSON('ATTACHMENT_LIST', undefined, undefined,'ATTACHMENT_LIST',this.attachmentString )
      this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
        // console.log("attachments : ", data)
      this.attachments = data.ATTACHMENTS? data.ATTACHMENTS.map(x=>(x.ATTACHMENT)) :[]
      // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
      //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
      this.spinner.hide();

    }, error => {
      console.log(error);
      this.spinner.hide();
    });
  }

  else if(this.type === 'ASN_DETAILS'){
    // const details = { email: this.email, purchaseType: 'ATTACHMENT_LIST', attachmentString: this.attachmentString };
  // const details = { email: this.email, purchaseType: 'ASN_ATTACHMENTS', headerId: this.headerId,
  // attachmentString: this.attachmentString };
    // this.purchaseService.getDocs(details).subscribe((data: any) => {

      var query = this.purchaseService.inputJSON('ATTACHMENT_LIST', undefined, undefined,'ATTACHMENT_LIST',this.attachmentString )
      this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      this.attachments = data.ATTACHMENTS? data.ATTACHMENTS.map(x=>(x.ATTACHMENT)) :[]
      // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
      //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
      this.spinner.hide();
    }, error => {
      console.log(error);
      this.spinner.hide();
    });

  }
  // else {
  //   console.log("COMMUNICATION frrr")
  //     var query = this.purchaseService.inputJSON('ATTACHMENT_LIST', undefined, undefined,'ATTACHMENT_LIST',this.attachmentString )
  //     this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
  //       console.log("Download comm : ", data)
  //     this.attachments = data.ATTACHMENTS? data.ATTACHMENTS.map(x=>(x.ATTACHMENT)) :[]
  //     // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
  //     //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
  //     this.spinner.hide();
  //   }, error => {
  //     console.log(error);
  //     this.spinner.hide();
  //   });

  // }


  }


  downloadfile(element) {
    var downloadFile;
    this.spinner.show();
    // var fileFolder = element.ATTACHMENT_PATH.split('\\');
    const dat = { documentId: element.DOCUMENT_ID, fileName: element.FILE_NAME, folder: element.ATTACHMENT_PATH };
    this.purchaseService.downloadAttachment(dat).subscribe((data) => {
      const file = new Blob([data]);
      saveAs(file, element.FILE_NAME);
      // var fileURL = URL.createObjectURL(file);
      // window.open(fileURL);
      this.spinner.hide();
    }, (error) => {
      this.notifier.notify('warning', 'File Not found!');
      console.log(error);
      this.spinner.hide();
    });
  }

  onFilesAdded(fileInput: any) {
    this.spinner.show();
    this.loginService.type = '/temp'
    this.loginService.fileType ='No'
    this.fileInput1=fileInput;
    this.fileData = fileInput.target.files as File;
    const length = fileInput.target.files.length;
    const file = new FormData();
    for (let i = 0; i < length; i++) {
      file.append('file', this.fileData[0], this.fileData[0].name);
    }
    this.loginService.FileUpload(file).subscribe((data) => {
      this.spinner.hide();
      let i;
      this.upload.file = this.fileData[0].name;
    }, err => {
      this.spinner.hide();
      this.resetFile();
      if (err) {
        console.log("error : ", err)
      } else {
      }
    });

  }

  resetFile() {
    this.myInputVariable.nativeElement.value = '';
  }

  fileUpload() {
    var temp=''
    this.spinner.show();
    const query = {
      DOCUMENT_ID: this.headerId, FILE_NAME: this.upload.file, TYPE: this.type,
      TITLE: this.upload.documentCategory, DESCRIPTION: this.upload.description,
      USER_ID:this.email
    };
    this.purchaseService.FileUpload(query).subscribe((data :any) => {
      var file1 = data.filename
      var data1=data.path
      this.loginService.type = data1;
      this.loginService.fileType = 'Yes'
      if(data.o === 'S')
      {
          this.fileData = this.fileInput1.target.files as File;
          const length = this.fileInput1.target.files.length;
          const file3 = new FormData();
          for (let i = 0; i < length; i++) {
            file3.append('file', this.fileData[0], file1);
          }
        this.loginService.FileUpload(file3).subscribe((data) => {
          this.spinner.hide();
          let i;
          // this.upload.file = this.fileData[0].name;
        }, err => {
          this.spinner.hide();
          this.resetFile();
          if (err.error.message) {

          } else {
          }
        });

        this.upload.documentCategory = '';
        this.upload.description = '';
        this.resetFile();
        this.notifier.notify('success', 'File being uploaded!');
      }
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });


  }

  cancel(){
    this.upload.documentCategory = '';
    this.upload.description = '';
    this.resetFile();
  }

  getPrintDocument() {
    if(this.type === 'ASN_DETAILS'){
      this.generatePdf(this.headerId);
    }else{
    this.spinner.show();
    const details = {  printDocumentString: this.printDocString};
    this.purchaseService.getDocPrint(details).subscribe((data: any) => {
      console.log("print asn : ", data);

      this.spinner.hide();
      const file = new Blob([data]);
      saveAs(file, this.poNumber+'.pdf');

    }, error => {
      console.log(error);
      this.notifier.notify('warning', 'Print document request has been submitted, You will be notified to download the document shortly.!');
      this.spinner.hide();
    });
  }
  }

  editASN(){
    this.router.navigateByUrl('/craftsmanautomation/shipment/editasn', { queryParams: { headerId: this.headerId, reason: this.editReason } ,skipLocationChange : profileData.hideURL});
  }
  deleteASN(){
    let query = {
      ASN: {
        HEADER: { OLD_ASN_ID: this.headerId, REASON_DESCRIPTION: this.deleteReason },
      }
    };
    var ASN_NUMBER=this.headerData.ASN_NUMBER;
    var mobileNo =this.headerData.list.map(x=>x.BUYER_PHONE_NUM)
    var email =this.headerData.list.map(x=>x.BUYER_EMAIL)
    mobileNo = (Array.from(new Set(mobileNo))).toString();
    email = (Array.from(new Set(email))).toString();
    this.spinner.show();
    this.purchaseService.saveASN(query).subscribe((data: any) => {
      this.spinner.hide();
      window.location.reload();
      const smsType="ASN Cancel";
      const subject=`ASN ${ASN_NUMBER} has been cancelled successfully by Supplier ${this.supplierName}`;
     const body= `
Dear Team,

ASN ${ASN_NUMBER} has been cancelled successfully by Supplier ${this.supplierName}.

Regards,
${this.supplierName}

`;
      this.emailService.sendMail(email, subject, body, smsType,mobileNo,ASN_NUMBER,this.supplierName )
      // .sendMailWithAttachments(email, subject, body, "ASN.pdf", encodedString, smsType, mobileNo, asn_id, supplierName)
      .subscribe(
        (message) => {
          // this.resetPage();
          this.spinner.hide();
          if (message === "OK") {
            const id = 1;
            // this.router.navigate(['/auth/view'], { queryParams: { id } });
            // this.text = false;
            // this.email = '';
          } else {
            // this.notifier.notify('warning', 'Email Send Failed');
          }
        },
        (error) => {
          this.spinner.hide();
        }
      );
  });

      // this.router.navigate([[this.router.url]]);
  }

 generatePdf(asnid){
    this.asnLines = this.headerData.list
    this.asnTransporter = this.headerData.list1
    this.asnLots = this.headerData.list2
    const documentDefinition = this.getDocumentDefinition();
    const temp = JSON.stringify(documentDefinition);
    const json = JSON.parse(temp);
    pdfMake.createPdf(json).open();
    this.router.navigateByUrl('/craftsmanautomation/shipment/advanceShipments', { skipLocationChange: true });

  }
  getDocumentDefinition() {
    return {
      content: [
        this.getQRRow(),
        // {
        //   text: this.headerData.ASN_NUMBER,
        //   fontSize: this.bodyFontSize + 2,
        //   bold: true,
        //   alignment: "right",
        // },
        this.getFirstTable(),
        {
          text: "ASN Items",
          fontSize: this.bodyFontSize + 2,
          bold: true,
          margin: [20, 20, 0, 0],
        },
        this.getSecondTable(),
        {
          text: this.asnLots.length ? "Lot Details" : "",
          fontSize: this.bodyFontSize + 2,
          bold: true,
          margin: [20, 20, 0, 0],
        },
        this.asnLots.length ? this.getThirdTable() : "",
        {
          text: this.asnTransporter.length ? "Transporter Details" : "",
          fontSize: this.bodyFontSize + 2,
          bold: true,
          margin: [20, 20, 0, 0],
        },
        this.asnTransporter.length ? this.getFourthTable() : "",
      ],
      info: {
        title: "ASN INVOICE",
      },
      styles: {
        header: {
          fontSize: 16,
          bold: true,
          margin: [0, 20, 0, 10],
          decoration: "underline",
        },
        alignment: {
          alignment: "right",
        },
        tableHeader: {
          bold: true,
        },
        tableExample: {
          margin: [0, 5, 0, 15],
        },
      },
    };
  }

  getQRRow() {
    return { qr: " " + this.headerId, fit: "75", alignment: "right"};
  }

  getFirstTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths: [136, 136, 136, 136],
        body: [
          [
            {
              text: "Advance Shipment Notice",
              bold: true,
              fontSize: this.bodyFontSize + 2,
              colSpan: 4,
              alignment: "center",
            },
           "",
            "",
            "",
          ],
          [
            {
              text: " Unit Name : Craftsman Automation Limited - "+this.headerData.ORG_NAME+" \t\t\t\t\t\t     ASN Number : "+this.headerData.ASN_NUMBER,
              bold: true,
              italic: true,
              fontSize: this.bodyFontSize + 2,
              colSpan: 4,
            },

          ],
          [
            { text: "Supplier", bold: true, fontSize: this.bodyFontSize },
            {
              text: "Invoice Number",
              bold: true,
              fontSize: this.bodyFontSize,
            },
            {
              text: "Invoice Date",
              bold: true,
              fontSize: this.bodyFontSize,
            },
            {
              text: "Expected Receipt Date",
              bold: true,
              fontSize: this.bodyFontSize,
            },
          ],
          [
            { text: this.supplierName || "-", fontSize: this.bodyFontSize },
            {
              text: this.headerData.DELIVERY_ORDER_NUMBER || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.headerData.DELIVERY_ORDER_DATE || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.headerData.EXPECTED_RECEIPT_DATE || "-",
              fontSize: this.bodyFontSize,
            },

          ],
          [
            { text: "Bill of Lading", bold: true, fontSize: this.bodyFontSize },
            { text: "Waybill Number", bold: true, fontSize: this.bodyFontSize },
            { text: "E-Invoice No", bold: true, fontSize: this.bodyFontSize },
            {
              text: "Shipment Method",
              bold: true,
              fontSize: this.bodyFontSize,
            },
          ],
          [
            {
              text: this.headerData.BILL_OF_LADING || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.headerData.WAYBILL_NUMBER || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.headerData.EINVOICE_NUMBER || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.headerData.SHIPPING_METHOD || "-",
              fontSize: this.bodyFontSize,
            },
          ],
          [
            { text: "Line Amount", bold: true, fontSize: this.bodyFontSize },
            {
              text: "Other Amount",
              bold: true,
              fontSize: this.bodyFontSize,
            },
            {
              text: "Tax Amount",
              bold: true,
              fontSize: this.bodyFontSize,
            },
            {
              text: "Total Value",
              bold: true,
              fontSize: this.bodyFontSize,
            },
          ],
          [
            { text: this.headerData.BASE_AMOUNT || "-", fontSize: this.bodyFontSize },
            {
              text: this.headerData.OTHER_AMOUNT || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.headerData.TAX_AMOUNT || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.headerData.TOTAL_VALUE || "-",
              fontSize: this.bodyFontSize,
            },
          ],
        ],
      },
      layout: {
        hLineWidth(i, node) {
          return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return i === 0 || i === node.table.body.length ? "white" : "white";
        },
        vLineColor(i, node) {
          return i === 0 || i === node.table.widths.length ? "white" : "white";
        },
      },
    };
  }

  getSecondTableBody() {
    return this.asnLines.map((m) => {
      return [
        { text: m.PO_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.PO_LINE_NUMBER || "-", fontSize: this.bodyFontSize },
        // { text: m.PO_SHIPMENT_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.ITEM_DESCRIPTION || "-", fontSize: this.bodyFontSize },
        // { text: m.ORDERED_QTY || "-", fontSize: this.bodyFontSize },
        { text: m.UOM || "-", fontSize: this.bodyFontSize },
        { text: m.RECEIVED_QTY || "-", fontSize: this.bodyFontSize },
        { text: (m.PRICE *m.RECEIVED_QTY).toFixed(2) || "-", fontSize: this.bodyFontSize },
      ];
    });
  }

  getSecondTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths: [120, 40, 190, 45, 65, 65],
        // widths: [136,136,136,136],
        body: [
          [
            { text: "PO Number", fontSize: this.bodyFontSize, bold: true },
            { text: "Line No.", fontSize: this.bodyFontSize, bold: true },
            // { text: "Shipment No.", fontSize: this.bodyFontSize, bold: true },
            { text: "Item", fontSize: this.bodyFontSize, bold: true },
            // { text: "Ordered Qty", fontSize: this.bodyFontSize, bold: true },
            { text: "UOM", fontSize: this.bodyFontSize, bold: true },
            { text: "Shipped Qty", fontSize: this.bodyFontSize, bold: true },
            { text: "Line Amount", fontSize: this.bodyFontSize, bold: true },
          ],
          ...this.getSecondTableBody(),
        ],
      },
      layout: {
        hLineWidth(i, node) {
          return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return i === 0 || i === node.table.body.length ? "white" : "white";
        },
        vLineColor(i, node) {
          return i === 0 || i === node.table.widths.length ? "white" : "white";
        },
      },
    };
  }

  getThirdBody() {
    return this.asnLots.map((m) => {
      return [
        { text: m.PO_NO || "-", fontSize: this.bodyFontSize },
        { text: m.PO_LINE_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.PO_SHIPMENT_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.LOT_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.GRADE || "-", fontSize: this.bodyFontSize },
        { text: m.QUANTITY || "-", fontSize: this.bodyFontSize },
        { text: m.REMARKS || "-", fontSize: this.bodyFontSize },
      ];
    });
  }

  getThirdTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths: [80, 40, 65, 180, 55, 40, 55],
        // widths: [136,136,136,136],
        body: [
          [
            { text: "PO Number", fontSize: this.bodyFontSize, bold: true },
            { text: "Line No.", fontSize: this.bodyFontSize, bold: true },
            { text: "Shipment No.", fontSize: this.bodyFontSize, bold: true },
            { text: "Lot", fontSize: this.bodyFontSize, bold: true },
            { text: "Grade", fontSize: this.bodyFontSize, bold: true },
            { text: "Quantity", fontSize: this.bodyFontSize, bold: true },
            { text: "Remarks", fontSize: this.bodyFontSize, bold: true },
          ],
          ...this.getThirdBody(),
        ],
      },
      layout: {
        hLineWidth(i, node) {
          return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return i === 0 || i === node.table.body.length ? "white" : "white";
        },
        vLineColor(i, node) {
          return i === 0 || i === node.table.widths.length ? "white" : "white";
        },
      },
    };
  }

  getFourthTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths: [55, 40, 65, 80, 65, 62, 60, 80],
        // widths: [136,136,136,136],
        body: [
          [
            { text: "Transporter", fontSize: this.bodyFontSize, bold: true },
            { text: "Vehicle No.", fontSize: this.bodyFontSize, bold: true },
            { text: "E-Way Bill No.", fontSize: this.bodyFontSize, bold: true },
            { text: "LR No.", fontSize: this.bodyFontSize, bold: true },
            { text: "LR Date", fontSize: this.bodyFontSize, bold: true },
            { text: "Driver", fontSize: this.bodyFontSize, bold: true },
            { text: "Contact No.", fontSize: this.bodyFontSize, bold: true },
            { text: "Remarks", fontSize: this.bodyFontSize, bold: true },
          ],
          ...this.getFourthBody(),
        ],
      },
      layout: {
        hLineWidth(i, node) {
          return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return i === 0 || i === node.table.body.length ? "white" : "white";
        },
        vLineColor(i, node) {
          return i === 0 || i === node.table.widths.length ? "white" : "white";
        },
      },
    };
  }

  getFourthBody() {
    return this.asnTransporter.map((m) => {
      return [
        { text: m.TRANSPORTER_NAME || "-", fontSize: this.bodyFontSize },
        { text: m.VEHICLE_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.EWAY_BILL_NO || "-", fontSize: this.bodyFontSize },
        { text: m.LR_NO || "-", fontSize: this.bodyFontSize },
        { text: m.LR_DATE || "-", fontSize: this.bodyFontSize },
        { text: m.DRIVER_NAME || "-", fontSize: this.bodyFontSize },
        { text: m.DRIVER_CONTACT_NO || "-", fontSize: this.bodyFontSize },
        { text: m.REMARKS || "-", fontSize: this.bodyFontSize },
      ];
    });
  }

getTransacationCommunication(){
  this.router.navigateByUrl('/craftsmanautomation/administration/communication', { queryParams: { communicationString : this.communicationString}, skipLocationChange : profileData.hideURL });
}
}
