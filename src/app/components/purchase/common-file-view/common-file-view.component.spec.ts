import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonFileViewComponent } from './common-file-view.component';

describe('CommonFileViewComponent', () => {
  let component: CommonFileViewComponent;
  let fixture: ComponentFixture<CommonFileViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonFileViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonFileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
