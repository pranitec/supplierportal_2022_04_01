import { Component, OnInit , ChangeDetectorRef} from '@angular/core';
import { PurchaseService } from '../service/purchase.service';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jQuery: any;
import { profileData } from 'src/app/config/config'

@Component({
  selector: 'app-pending-purchase-release',
  templateUrl: './pending-purchase-release.component.html',
  styleUrls: ['./pending-purchase-release.component.css']
})
export class PendingPurchaseReleaseComponent implements OnInit {

  public email = '';
  public shipmentView = false;
  public lineId = '';
  public fieldName='';
  public poSummaryAll = [];
  public poSummary = [];
  public poHeaderAll = [];
  public poLineAll = [];
  public poHeader = [];
  public poLine = [];
  public viewCount = 10;
  public orderedQty = 0;
  public deliveredQty = 0;
  public pendingQty = 0;
  public pendingCount = 0;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst
  //

  public poCountsAll =[];
  public poCounts = []
  // public viewLength: number;
  public page =1;
  public searchText = '';
  public tableView;
  public modal;
  public modalHeader;
  public modalLine1;
  public modalLine2;
  public modalLine3;
  public modalLine4;
  public action= false;

  constructor(private purchaseService: PurchaseService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService,
    private breadCrumbServices: BreadcrumbService, private router: Router, private rsaService: RsaService) { }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));

    this.tableView = true;
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/purchase/pendingAgreement') {
        this.setFilteredData();
        this.setPoSummary();
      }
    });
    this.getPendingRelease();
    this.spinner.show();
  }

  getPendingRelease() {
    const deliveredQty = 0, pendingQty = 0, orderedQty = 0, pendingCount = 0;
    // let query = {
    //   INPUT_HEADER :{ MAIL_ID :this.email},
    //   FILTER_DETAILS :{ MENU_NAME:"PENDING_PO", DOCUMENT_TYPE: "PENDING_PO"}
    // }
    var query = this.purchaseService.inputJSON('PENDING_RELEASE', undefined, undefined)
    // const details = { email: this.email, purchaseType: 'PENDING_PO'};
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      // console.log('ppp PENDING_RELEASE', data);
      this.spinner.hide();

      this.poCountsAll = data.PENDINGRELEASE.POCOUNTS ? data.PENDINGRELEASE.POCOUNTS.map(x=>(x.POCOUNT)) :[]
      this.poSummaryAll = data.PENDINGRELEASE.POSUMMARIES ? data.PENDINGRELEASE.POSUMMARIES.map(x=>(x.POSUMMARY)) : []
      this.poHeaderAll = data.PENDINGRELEASE.POHEADERS ? data.PENDINGRELEASE.POHEADERS.map(x =>(x.POHEADER)) : []
      this.poLineAll = data.PENDINGRELEASE.POLINES ? data.PENDINGRELEASE.POLINES.map(x =>(x.POLINE)) : []

      this.poHeader = this.poHeaderAll;
      this.poCounts = this.poCountsAll;
      this.poLine = this.poLineAll;
      this.poHeader.forEach(
        (x, i) => {
          this.poHeader[i].filteredList = this.poLine.filter(y => y.PO_HEADER_ID === x.PO_HEADER_ID);
        }
      );
      this.poSummary = this.poSummaryAll;
      this.poSummaryDetails();
      this.setAsDataTable();
      this.setFilteredData();
      this.setPoSummary();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
  poSummaryDetails() {
    let deliveredQty = 0, pendingQty = 0, orderedQty = 0, pendingCount = 0;
    this.poSummary.forEach(x => {
      deliveredQty = deliveredQty + Number(x.DELIVERED_QUANTITY);
      pendingQty = pendingQty + Number(x.PENDING_PO_QUANTITY);

      orderedQty = orderedQty + Number(x.ORDERED_QUANTITY);
    });
    this.poCounts.forEach(x => {
      pendingCount = pendingCount + Number(x.PENDING_PO_COUNT);
    });
    this.pendingCount = pendingCount;
    this.deliveredQty = deliveredQty;
    this.pendingQty = pendingQty;
    this.orderedQty = orderedQty;

  }

  setPoSummary() {

    this.poSummary = this.poSummaryAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      // if (this.searchText) {
      //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      // }

      return a;
    });
    this.poCounts = this.poCountsAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      // if (this.searchText) {
      //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      // }

      return a;
    });


    this.poSummaryDetails();
  }

  setAsDataTable() {

    this.chRef.detectChanges();
    this.poHeader.forEach((f, i) => {
      (function ($) {
        $('#test' + i).DataTable({
          responsive: true,
          // bLengthChange: false,
          retrieve: true,
          // pageLength: 5,
        });
      })(jQuery);
    });
  }

  setAsDataTable1() {

    this.chRef.detectChanges();

    (function ($) {

      $('#Pending-table').DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        pageLength: 20,
      });
    })(jQuery);

  }

  setFilteredData() {

    this.poHeader = this.poHeaderAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });

    this.poHeader.forEach((f, i) => {
      this.poHeader[i].list = this.poLineAll.filter(x => x.PO_HEADER_ID == f.PO_HEADER_ID);
    });
    this.setAsDataTable();
  }

  public onPageChanged(event) {
    this.page = event;
  }

  gridView() {
    this.tableView = true;
    this.setAsDataTable();
  }

  listView() {
    this.tableView = false;
    this.setAsDataTable1();
  }

  exportExcel() {
    const details = { POLINES: this.poLineAll, title: 'Pending Agreement Report'  };
    this.purchaseService.getExcel(details);
  }

  clickedLineId(lineId, name, qty) {
    this.lineId = lineId;
    this.fieldName = name
    // this.shipmentView = true;
    if(qty != 0){
     this.router.navigateByUrl('/craftsmanautomation/shipment/viewreceipts', { queryParams: { lineId: this.lineId, fieldName: name }, skipLocationChange : profileData.hideURL });
    }
  }
  goBackShip() {
    this.shipmentView = false;
  }

  getSummaryDetails(data) {
    this.modalLine1 = false;
    this.modalLine2 = false;
    this.modalLine3 = false;
    this.modalLine4 = false;

    if (data === 'pending') {
      this.modal = 'Pending Agreement Details';
      this.modalHeader = 'Pending Agreement Count';
      this.modalLine1 = true;
    } else if (data === 'orderQty') {
      this.modal = 'Agreement Quantity Details';
      this.modalHeader = 'Agreement Quantity';
      this.modalLine2 = true;
    } else if (data === 'deliverQty') {
      this.modal = 'Delivered Quantity Details';
      this.modalHeader = 'Delivered Quantity';
      this.modalLine3 = true;
    } else if (data === 'pendingQty') {
      this.modal = 'Pending Quantity Details';
      this.modalHeader = 'Pending Quantity';
      this.modalLine4 = true;
    }

  }
  viewTaxButton1(element, i, j){
    this.action = false;
    this.poHeader[i].filteredList[j].action = false;

  }
  viewTaxButton2(element,i, j){
    this.action = true;
    this.poHeader[i].filteredList[j].action = true
    // const details = { email: this.email, attachmentString: element.TAX_IDENTIFICATION_STRING, purchaseType: 'TAX_LINES'};
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
      var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', element.TAX_IDENTIFICATION_STRING)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.myGst = this.taxData[0].MY_TAX_REG_NO

      this.poHeader[i].filteredList[j].taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.poHeader[i].filteredList[j].taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.poHeader[i].filteredList[j].taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.poHeader[i].filteredList[j].myGst = this.taxData[0].MY_TAX_REG_NO
      this.poHeader[i].filteredList[j].hsnsacCode = this.taxData[0].HSN_SAC_CODE
      this.poHeader[i].filteredList[j].hsnsacCodeDesc = this.taxData[0].HSN_SAC_CODE_DESC

      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
}
