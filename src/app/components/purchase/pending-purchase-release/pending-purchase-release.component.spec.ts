import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingPurchaseReleaseComponent } from './pending-purchase-release.component';

describe('PendingPurchaseReleaseComponent', () => {
  let component: PendingPurchaseReleaseComponent;
  let fixture: ComponentFixture<PendingPurchaseReleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingPurchaseReleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingPurchaseReleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
