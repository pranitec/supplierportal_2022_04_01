import { Component, OnInit } from '@angular/core';
declare var jQuery: any;

@Component({
  selector: 'app-create-quote',
  templateUrl: './create-quote.component.html',
  styleUrls: ['./create-quote.component.css']
})
export class CreateQuoteComponent implements OnInit {

  constructor() { }
  public quote:any ={};
  ngOnInit(): void {
    this.setDatePicker();
  }


  setDatePicker(){
    //this.showHide = false;
    let setDate1 = (value)=>{
    //  console.log('CAlled Date 1',i,value)
      this.quote.QUOTATION_DATE = value;
    }
    (function($,i,setDate1,setDate2){
      //     console.log(i,"Called");
           $("#datepicker1").datepicker({
             uiLibrary: 'bootstrap4',
             format: 'dd-mmm-yyyy',
             value: i,
             change(e) {
               setDate1( e.target.value);
             }
         });
     
       })(jQuery,this.quote.QUOTATION_DATE,setDate1);
  }

}
