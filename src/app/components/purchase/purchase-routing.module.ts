import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PastPurchaseOrdersComponent } from './past-purchase-orders/past-purchase-orders.component';
import { PendingPurchaseOrderComponent } from './pending-purchase-order/pending-purchase-order.component';
import { PendingPurchaseAgreementComponent } from './pending-purchase-agreement/pending-purchase-agreement.component';
import { PastAgreementsComponent } from './past-agreements/past-agreements.component';
import {PendingAcceptanceComponent} from './pending-acceptance/pending-acceptance.component';
import { CreateQuoteComponent } from './create-quote/create-quote.component';
import { PendingPurchaseReleaseComponent } from './pending-purchase-release/pending-purchase-release.component';
import { PastPurchaseReleaseComponent } from './past-purchase-release/past-purchase-release.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { AuthGuardService } from 'src/app/shared/service/auth-guard.service';

const routes: Routes = [
  {
    path: 'pastPurchaseOrders',
    component: PastPurchaseOrdersComponent,
    data: {
      title: 'Past Purchase Orders',
      breadcrumb: 'Past Purchase Orders'
    },
    canActivate: [AuthGuardService],
  },
  {
    path: 'pendingOrder',
    component: PendingPurchaseOrderComponent,
    data: {
      title: 'Pending Purchase Orders',
      breadcrumb: 'Pending Purchase Orders'
    },
    canActivate: [AuthGuardService],
  },
  {
    path: 'pendingRelease',
    component:  PendingPurchaseReleaseComponent,
    data: {
      title: 'Pending Purchase Release',
      breadcrumb: 'Pending Purchase Release'
    },
    canActivate: [AuthGuardService],
  },
  {
    path: 'pastRelease',
    component: PastPurchaseReleaseComponent,
    data: {
      title: 'Past Purchase Release',
      breadcrumb: 'Past Purchase Release'
    },
    canActivate: [AuthGuardService],
  },
  {
    path: 'pendingAgreement',
    component: PendingPurchaseAgreementComponent,
    data: {
      title: 'Pending Purchase Agreements',
      breadcrumb: 'Pending Purchase Agreements'
    },
    canActivate: [AuthGuardService],
  },
  {
    path: 'pastAgreements',
    component: PastAgreementsComponent,
    data: {
      title: 'Past Purchase Agreements',
      breadcrumb: 'Past Purchase Agreements'
    },
    canActivate: [AuthGuardService]
  },
    {
      path: 'pendingAcceptance',
      component: PendingAcceptanceComponent,
      data: {
        title: 'PO Acknowledgement',
        breadcrumb: 'PO Acknowledgement'
      },
      canActivate: [AuthGuardService]
  },
      {
        path:'createquote',
        component:CreateQuoteComponent,
        data:{
          title:'Create Quote',
          breadcrumb:'Create Quote'
        },
        canActivate: [AuthGuardService]
      }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseRoutingModule { }
