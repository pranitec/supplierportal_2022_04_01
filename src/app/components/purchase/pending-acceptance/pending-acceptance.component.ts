import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { PurchaseService } from '../service/purchase.service';
import { Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import {EmailService} from '../../auth/service/email.service';
import { getMaxListeners } from 'process';

declare var jQuery: any;

@Component({
  selector: 'app-pending-acceptance',
  templateUrl: './pending-acceptance.component.html',
  styleUrls: ['./pending-acceptance.component.css']
})
export class PendingAcceptanceComponent implements OnInit {

  public email;
  public viewCount = 10;
  public searchText = '';
  public page = 1;
  public view;
  public poHeader = [];
  public poLine = [];
  public poHeaderAll = [];
  public poLineAll = [];
  private notifier: NotifierService;
  public reason = '';
  public show1 = false;
  public show2 = false;
  public id;
  public promisedDate;
  public lineId;
  public selectedHeaderId;
  public updatedLines = [];
  public modalDate = true;

  public headerDetailsReject;
  public action= false;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst;
  public supplierName;

  constructor(private purchaseService: PurchaseService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService, private emailService: EmailService,
    private breadCrumbServices: BreadcrumbService, private router: Router, private rsaService: RsaService,private notifierService: NotifierService) {

      this.notifier = notifierService;
    }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.supplierName = this.breadCrumbServices.filteredSelection[0].VENDOR_NAME;
    this.view = true;
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/purchase/pendingAcceptance') {
        this.setFilteredData();
      }
    });
    this.getPendingAcceptance();
    this.spinner.show();
  }

  getPendingAcceptance() {
    this.spinner.show();
    // const details = { email: this.email, purchaseType: 'PO_ACCEPTANCE' };
    var details = this.purchaseService.inputJSON('PO_ACCEPTANCE',  undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.spinner.hide();
      // console.log('getPendingAcceptance', data);
      // this.poHeaderAll = Array.isArray(data.POACCEPTANCE.POHEADERS.POHEADER) ? data.POACCEPTANCE.POHEADERS.POHEADER : (data.POACCEPTANCE.POHEADERS.POHEADER) ? [data.POACCEPTANCE.POHEADERS.POHEADER] : [];
      // this.poLineAll = Array.isArray(data.POACCEPTANCE.POLINES.POLINE) ? data.POACCEPTANCE.POLINES.POLINE : (data.POACCEPTANCE.POLINES.POLINE) ? [(data.POACCEPTANCE.POLINES.POLINE)] : [];

      this.poHeaderAll = data.POACCEPTANCE.POHEADERS ? data.POACCEPTANCE.POHEADERS.map(x=>(x.POHEADER)) :[]
      this.poLineAll = data.POACCEPTANCE.POLINES ? data.POACCEPTANCE.POLINES.map(x=>(x.POLINE)) : []

      this.poHeader = this.poHeaderAll;
      this.poLine = this.poLineAll;
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  setFilteredData() {

    this.poHeader = this.poHeaderAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });

  }

  setAsDataTable() {

    this.chRef.detectChanges();

    (function ($) {

      $("#listtest").DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        pageLength: 20,
      });
    })(jQuery);

  }

  exportExcel() {
    const details = { POLINES: this.poLineAll, title: 'Pending Acceptance Report'  };
    this.purchaseService.getExcel(details);

  }
  getTableDetails(headerId) {
    this.selectedHeaderId = headerId;
    this.view = false;
    this.poLine = this.poLineAll.filter(x => x.PO_HEADER_ID == headerId);
    this.setAsDataTable();
  }
  getBack() {
    this.view = true;
  }
  onPageChanged(event) {
    this.page = event;
  }

  actionClicked(headerId,action, element){
    this.spinner.show();
    let line = [];
    line = (this.updatedLines.filter(x => x.headerId === headerId)).map(a => {return {LINE_LOCATION_ID:a.LINE_LOCATION_ID, NEW_PROMISE_DATE: a.NEW_PROMISE_DATE }});
    let query = {PO_ACCEPTANCE :{ PO_HEADER_ID: headerId, PO_RELEASE_ID:element.PO_RELEASE_ID, USER_NAME: this.email, ACTION: action, NOTES: this.reason,
                   LINES: [{LINE: line}] }};
                  //  console.log(query);

    this.purchaseService.saveASN(query).subscribe(data => {

      // this.purchaseService.saveASN(query).subscribe(
      this.spinner.hide();
      // if (data === 'S') {
        var message = '';
        if (action === 'ACCEPT') {
          message = 'Accepted Successfully';
          this.sendAcceptMail(element);
          this.notifier.notify('success', message);
        }
        else {
          message = "Rejected Successfully";
          this.sendRejectMail(element);
          this.notifier.notify('error', message);
        }
        setTimeout(() => {
          this.getPendingAcceptance();
        }, 1000);
      // }
      // else {
      //   var message = '';
      //   if (action === 'ACCEPT') {
      //     message = 'Accept Failed';
      //   }
      //   else {
      //     message = "Reject Failed";
      //   }
      //   this.notifier.notify('error', message);
      //   //this.getPendingAcceptance();
      // }


    }, error => {
      console.log(error);
      this.spinner.hide();
    })
  }

  getReason(){
    let remark = this.reason.trim();
    if (remark) {
      this.show1 = true;
    }
    else {
      this.show1 = false;
    }
  }

  rejection(id, data) {
    this.headerDetailsReject = data;
    this.reason = '';
    this.id = id;
  }

  showHideTable() {
    this.modalDate = false;
    setTimeout(() => {
      this.modalDate = true;
      (function ($) {
        $(document).ready(function () {
          const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
          const today = new Date();
          const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
          const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-'
            + months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
          const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
          // console.log(localStorage.getItem('PRD'));
          $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
            value: localStorage.getItem('PRD'),
            change(e) {

              localStorage.setItem('PRD', e.target.value);
              this.promisedDate = e.target.value;
            }
          });
        });
      })(jQuery);
    }, 10);
  }

  getLineDetails(data){
    // console.log(data);
    this.lineId = data.PO_LINE_ID;
    localStorage.setItem('PRD', data.PROMISED_DATE);
    this.showHideTable();
  }

  submit(){
    this.actionClicked(this.id,'REJECT', this.headerDetailsReject);
  }

  submit2(){
    this.promisedDate = localStorage.getItem('PRD');
    // console.log('ppp', this.promisedDate);
    let query = {LINE_LOCATION_ID: this.lineId, headerId: this.selectedHeaderId, NEW_PROMISE_DATE: this.promisedDate};
    this.updatedLines = this.updatedLines.filter(x => !(x.headerId === this.selectedHeaderId && x.LINE_LOCATION_ID === this.lineId))
    this.updatedLines.push(query);
    // this.getLineDetails();
  }

  cancel(){
    this.reason = '';
  }

  cancel2(){
    this.promisedDate = '';
  }

  sendAcceptMail(element) {
    const email = element.BUYER_EMAIL;
    const smsType="PO Acceptance"
    const subject = `Supplier Portal - PO ` + element.PO_NO + ` has been acknowledged`;
    const body = `
          Dear ` + element.BUYER + `,<br/><br/>

         Supplier ` + element.BUYER_UNIT_ID + ` has been acknowledged the receipt of your purchase order
         number ` + element.PO_NO + `.<br/><br/>

          Thank You, <br/>
           Craftsman Automation Team
              `;

    this.emailService.sendMail(email, subject, body, smsType, element.BUYER_PHONE_NUM, element.BUYER_UNIT_ID, element.PO_NO).subscribe(message => {
      // console.log('aaaa', message);

    }, err => {
      console.log(err);
    });

  }

  sendRejectMail(element){
    const email = element.BUYER_EMAIL;
    const subject = `Supplier Portal - PO ` + element.PO_NO + ` has been rejected`;
    const body = `
          Dear ` + element.BUYER + `,<br/><br/>

         Supplier ` + element.BUYER_UNIT_ID[1] + ` has been rejected your purchase order
         number ` + element.PO_NO + ` with reason ` + this.reason + `.<br/><br/>

          Thank You, <br/>
          Craftsman Automation Team
              `;
    const smsType="PO Rejected"
    this.emailService.sendMail(email, subject, body, smsType, element.BUYER_PHONE_NUM, element.BUYER_UNIT_ID, element.PO_NO).subscribe(message => {

    }, err => {
      console.log(err);
    });
  }

  viewTaxButton1(element, j){
    this.action = false;
    this.poLine[j].action = false;

  }
  viewTaxButton2(element, j){
    this.action = true;
    this.poLine[j].action = true
    // const details = { email: this.email, attachmentString: element.TAX_IDENTIFICATION_STRING, purchaseType: 'TAX_LINES'};
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
      var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', element.TAX_IDENTIFICATION_STRING)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.taxData = data.TAX_DETAILS? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.myGst = this.taxData[0].MY_TAX_REG_NO

      this.poLine[j].taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.poLine[j].taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.poLine[j].taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.poLine[j].myGst = this.taxData[0].MY_TAX_REG_NO
      this.poLine[j].hsnsacCode = this.taxData[0].HSN_SAC_CODE
      this.poLine[j].hsnsacCodeDesc = this.taxData[0].HSN_SAC_CODE_DESC

      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
}
