import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingAcceptanceComponent } from './pending-acceptance.component';

describe('PendingAcceptanceComponent', () => {
  let component: PendingAcceptanceComponent;
  let fixture: ComponentFixture<PendingAcceptanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingAcceptanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingAcceptanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
