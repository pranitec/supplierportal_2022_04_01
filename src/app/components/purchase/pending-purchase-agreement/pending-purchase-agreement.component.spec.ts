import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingPurchaseAgreementComponent } from './pending-purchase-agreement.component';

describe('PendingPurchaseAgreementComponent', () => {
  let component: PendingPurchaseAgreementComponent;
  let fixture: ComponentFixture<PendingPurchaseAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingPurchaseAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingPurchaseAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
