import { Component, OnInit, ChangeDetectorRef, ViewChild, Renderer2 } from '@angular/core';
import { PurchaseService } from '../service/purchase.service';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {ToolbarComponent} from '../../../shared/components/toolbar/toolbar.component'
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { NotifierService } from "angular-notifier";

declare var jQuery: any;

@Component({
  selector: 'app-past-purchase-orders',
  templateUrl: './past-purchase-orders.component.html',
  styleUrls: ['./past-purchase-orders.component.css']
})
export class PastPurchaseOrdersComponent implements OnInit {
  public email;
  public shipmentView = false;
  public lineId = '';
  public fieldName =''
  public poHeadersList = [];
  public fullpoHeaderList = [];
  public poLineList = [];
  public fullpolines = [];
  public fullSummary = [];
  public summary = [];
  public modal;
  public modalHeader;
  public modalLine1;
  public modalLine2;
  public modalLine3;
  public modalLine4;

  public searchText = '';
  public page = 1;
  public viewCount = 10;
  public viewGrid = true;
  public ordersReceived = 0;
  public orderedQuantity = 0;
  public deliveredQuantity = 0;
  public pendingQuantity = 0;

  public poCountsAll = [];
  public poCounts = []
  public action= false;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst;
  public invoiceId;
  public invoicefieldName ;
  public selectedmenu;
  public selectedmenuFilter =[];
  public oneyearstartingDate : Date;
  private notifier: NotifierService;
  public noOfMonths;

  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();
  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate()-30));
    // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();


  public uom = '';
  // public fromDate;
  // public toDate;

  // @ViewChild('datetimepicker1') input1;

  constructor(private purchaseService: PurchaseService, private chRef: ChangeDetectorRef, private route : ActivatedRoute, private notifierService: NotifierService,
              public breadCrumbServices: BreadcrumbService, private router: Router, public rsaService: RsaService, private spinner: NgxSpinnerService) {
                this.route.queryParams.subscribe((data) => {
                  this.invoiceId = data['invoiceId'];
                  this.invoicefieldName = data['fieldName'];
                });
                this.page = 1;
                this.notifier = notifierService;
                console.log("oneMonthBefore : ", this.oneMonthBefore)
}

// ngAfterViewInit(){
//   this.renderer.invokeElementMethod(
//       this.input1.nativeElement,
//       'focus',
//       []
//   )
// }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.selectedmenu= this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
    this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(({MENUACCESS})=> MENUACCESS.MENU_CODE === 'PAST_PO');
    localStorage.setItem('PODF', this.fromDate);
    localStorage.setItem('PODT', this.toDate);

    this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START)
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.noOfMonths  = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE /31)

    // const oneyearendingDate = new Date(new Date(this.oneyearstartingDate).setDate((new Date(this.oneyearstartingDate).getDate())))
    // this.fromDateview = '' + ('0' + new Date(this.oneyearstartingDate).getDate()).slice(-2) + '-' + months[new Date(this.oneyearstartingDate).getMonth()] + '-' + new Date(this.oneyearstartingDate).getFullYear();
    // this.toDateview = '' + ('0' + new Date(oneyearendingDate).getDate()).slice(-2) + '-' + months[new Date(oneyearendingDate).getMonth() + this.noOfMonths] + '-' + new Date(oneyearendingDate).getFullYear();

    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/purchase/pastPurchaseOrders') {
        this.setFilteredData();
        this.setPoSummary();
      }
    });

    (function($,_this) {

      $(document).ready(function() {
        const today = new Date();
        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          value: _this.fromDate,
          minDate : _this.oneyearstartingDate,
          change(e) {
            // $('#datepicker').val(e.target.value);
            // $('#datepicker').trigger('input');
            // $('#datepicker').trigger('ngModelChange');
            localStorage.setItem('PODF', e.target.value);
            _this.fromDate = e.target.value;
          }
        });

        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          value: _this.toDate,
          maxDate: new Date(),
          change(e) {
            localStorage.setItem('PODT', e.target.value);
            _this.toDate = e.target.value
          }
        });
        // $('#grid-sections').show();
        // $('#list-sections').hide();,m
        // $('#grid').click(function() {
        //   $('#list-sections').hide();
        //   $('#grid-sections').show();
        //   $('#list').removeClass('btn-success');
        //   $('#list').addClass('btn-light');
        //   $(this).removeClass('btn-light');
        //   $(this).addClass('btn-success');
        // });
        // $('#list').click(function() {
        //   $('#list-sections').show();
        //   $('#grid-sections').hide();

        //   $('#Past-details-view').hide();
        //   $('#Past-details').show();

        //   $('#grid').removeClass('btn-success');
        //   $('#grid').addClass('btn-light');

        //   $(this).removeClass('btn-light');
        //   $(this).addClass('btn-success');
        // });
        // $('#Past-details-view').hide();
        // $('#goback').hide();

        // $('#goback').click(function() {
        //   $('#Past-details-view').hide();
        //   $('#Past-details').show();
        // });
      });
    })(jQuery,this);


    this.getPastPurchases();
  }


  getPastPurchases() {
    this.spinner.show();
    // this.fromDate = localStorage.getItem('PODF');
    // this.toDate = localStorage.getItem('PODT');

    var time_difference =  Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
    var months_difference =  Math.floor(time_difference / (1000 * 60 * 60 * 24)/31);

    if((-(months_difference)) >this.noOfMonths){
      // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
      this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
      this.spinner.hide();
    } else{
    if(this.invoicefieldName && this.invoiceId){
      var details = this.purchaseService.inputJSON('INVOICE_PO_DETAILS', undefined, undefined, this.invoicefieldName, this.invoiceId)
    }else{
      var details = this.purchaseService.inputJSON('PAST_PO',  this.fromDate, this.toDate, this.invoicefieldName, this.invoiceId)
    }
     // let query = {
    //   INPUT_HEADER :{ MAIL_ID :this.email},
    //   FILTER_DETAILS :{ MENU_NAME:"PASTPO", DOCUMENT_TYPE: "PASTPO"}
    // }

    // const details = { email: this.email, purchaseType: 'PAST_PO', date1: this.fromDate, date2: this.toDate };
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log("PAST_PO : ", data)
      this.spinner.hide();
      if (data) {
        this.fullpoHeaderList = data.PASTPO.POHEADERS ? data.PASTPO.POHEADERS.map(x=>(x.POHEADER)) :[]
        this.fullpolines = data.PASTPO.POLINES ? data.PASTPO.POLINES.map(x=>(x.POLINE)) : []
        this.fullSummary = data.PASTPO.POSUMMARIES ? data.PASTPO.POSUMMARIES.map(x =>(x.POSUMMARY)) : []
        // this.fullpoHeaderList = Array.isArray(data.PASTPO.POHEADERS.POHEADER) ? data.PASTPO.POHEADERS.POHEADER : (data.PASTPO.POHEADERS.POHEADER) ? [data.PASTPO.POHEADERS.POHEADER] : [];
        // this.fullpolines = Array.isArray(data.PASTPO.POLINES.POLINE) ? data.PASTPO.POLINES.POLINE : (data.PASTPO.POHEADERS.POHEADER) ? [(data.PASTPO.POHEADERS.POHEADER)] : [];
        // this.fullSummary = Array.isArray(data.PASTPO.POSUMMARIES.POSUMMARY) ? data.PASTPO.POSUMMARIES.POSUMMARY : (data.PASTPO.POSUMMARIES.POSUMMARY) ? [data.PASTPO.POSUMMARIES.POSUMMARY] : [];
        this.summary = this.fullSummary;
        this.poCountsAll = data.PASTPO.POCOUNTS ? data.PASTPO.POCOUNTS.map(x=>(x.POCOUNT)) :[]
        // this.poCountsAll = Array.isArray(data.PASTPO.POCOUNTS.POCOUNT) ? data.PASTPO.POCOUNTS.POCOUNT : (data.PASTPO.POCOUNTS.POCOUNT) ? [data.PASTPO.POCOUNTS.POCOUNT] : [];
        this.poCounts = this.poCountsAll;

        this.setPoSummary();
        this.setFilteredData();
        this.setSummaryData();
      } else {
      }
    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }


  }

  setFilteredData() {
    this.poHeadersList = this.fullpoHeaderList.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }


      // if (this.fromDate) {
      //   a = a && this.fromDate < f.PO_DATE;
      // }

      // if (this.toDate) {
      //   a = a && this.toDate > f.PO_DATE;
      // }

      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });
    //   this.poHeadersList = this.poHeadersList.slice(0, 20);
    this.poHeadersList.forEach((f, i) => {
      this.poHeadersList[i].list = this.fullpolines.filter(x => x.PO_HEADER_ID == f.PO_HEADER_ID);
    });
    this.setAsDataTable();
  }

  setAsDataTable() {
    this.chRef.detectChanges();
    this.poHeadersList.forEach((f, i) => {
      (function($) {
        $('#test' + i).DataTable({
          responsive: true,
          // bLengthChange: false,
          retrieve: true,
          // pageLength: 5,
        });
      })(jQuery);
    });
  }

  setAsDataTableList() {
    this.chRef.detectChanges();
    (function($) {
      $('#Past-table').DataTable({
        responsive: true,
        iDisplayLength: 5,
        bLengthChange: false,
        retrieve: true,
        pageLength: 5,
      });

      $('#Past-table').draw();
    })(jQuery);

  }

  public onPageChanged(event) {
    this.page = event;
  }
  onViewGrid() {
    this.viewGrid = true;
    this.searchText = '';
    this.setAsDataTable();

  }

  onViewList() {
    this.viewGrid = false;
    this.searchText = '';
    this.setAsDataTableList();

  }

  exportExcel() {
    const poLines = [];
    this.poHeadersList.forEach(f => {
      poLines.push(...f.list);
    });
    const details = { POLINES: poLines , title: 'Past Purchase Report' };
    this.purchaseService.getExcel(details);
  }

  setPoSummary() {

    this.summary = this.fullSummary.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      // if (this.searchText) {
      //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      // }

      return a;

    });
    this.poCounts = this.poCountsAll.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      // if (this.searchText) {
      //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      // }

      return a;

    });
    this.setSummaryData();
  }



  setSummaryData() {
    let pendingQuantity = 0, orderedQuantity = 0, ordersReceived = 0, deliveredQuantity = 0;

    this.summary.forEach(f => {
      pendingQuantity = pendingQuantity + Number(f.PENDING_PO_QUANTITY);
      orderedQuantity = orderedQuantity + Number(f.ORDERED_QUANTITY);

      deliveredQuantity = deliveredQuantity + Number(f.DELIVERED_QUANTITY);

    });

    this.poCounts.forEach(f=>{
      ordersReceived = ordersReceived + Number(f.PAST_PO_COUNT);
    })

    this.pendingQuantity = pendingQuantity;
    this.orderedQuantity = orderedQuantity;
    this.ordersReceived = ordersReceived;
    this.deliveredQuantity = deliveredQuantity;
  }

  fromDateChanged() {
    // var date = new Date("2013-07-15");
    // var currentMonth = date.getMonth();
    // var currentDate = date.getDate();
    // var currentYear = date.getFullYear();
    // $("#datepicker" ).datepicker( "option", "maxDate", new Date(currentYear, currentMonth, currentDate));

  }

  // clickedLineId(lineId, name) {
  //   console.log("lineId : ", lineId, name);
  //   this.lineId = lineId;
  //   this.fieldName = name
  //   // this.shipmentView = true;
  //    this.router.navigate(['/craftsmanautomation/shipment/viewreceipts'], { queryParams: { lineId: this.lineId, fieldName: name } });
  // }
  clickedLineId(lineId, name, qty) {
    this.lineId = lineId;
    // this.shipmentView = true;
    this.fieldName = name
    if(qty != 0){
    // this.shipmentView = true;
     this.router.navigate(['/craftsmanautomation/shipment/viewreceipts'], { queryParams: { lineId: this.lineId, fieldName: name } });
    }
  }
  goBackShip() {
    this.shipmentView = false;
  }

  getSummaryDetails(data) {
    this.modalLine1 = false;
    this.modalLine2 = false;
    this.modalLine3 = false;
    this.modalLine4 = false;
    if (data === 'orders') {
      this.modal = 'Order Received Details';
      this.modalHeader = 'Orders Received';
      this.modalLine1 = true;
    } else if (data === 'orderQty') {
      this.modal = 'Ordered Quantity Details';
      this.modalHeader = 'Ordered Quantity';
      this.modalLine2 = true;
    } else if (data === 'deliverQty') {
      this.modal = 'Delivered Quantity Details';
      this.modalHeader = 'Delivered Quantity';
      this.modalLine3 = true;
    } else if (data === 'pendingQty') {
      this.modal = 'Pending Quantity Details';
      this.modalHeader = 'Pending Quantity';
      this.modalLine4 = true;
    }

  }

  viewTaxButton1(element, i, j){
    this.action = false;
    this.poHeadersList[i].list[j].action = false;
  }

  viewTaxButton2(element,i, j){
    this.action = true;
    this.poHeadersList[i].list[j].action = true
    // const details = { email: this.email, attachmentString: element.TAX_IDENTIFICATION_STRING, purchaseType: 'TAX_LINES'};
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
      var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', element.TAX_IDENTIFICATION_STRING)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
        // console.log("tax lines : ", data)
      this.taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.myGst = this.taxData[0].MY_TAX_REG_NO

      this.poHeadersList[i].list[j].taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.poHeadersList[i].list[j].taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.poHeadersList[i].list[j].taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.poHeadersList[i].list[j].myGst = this.taxData[0].MY_TAX_REG_NO
      this.poHeadersList[i].list[j].hsnsacCode = this.taxData[0].HSN_SAC_CODE
      this.poHeadersList[i].list[j].hsnsacCodeDesc = this.taxData[0].HSN_SAC_CODE_DESC

      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
}
