import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastPurchaseOrdersComponent } from './past-purchase-orders.component';

describe('PastPurchaseOrdersComponent', () => {
  let component: PastPurchaseOrdersComponent;
  let fixture: ComponentFixture<PastPurchaseOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastPurchaseOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastPurchaseOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
