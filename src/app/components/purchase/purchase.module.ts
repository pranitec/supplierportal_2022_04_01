import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchaseRoutingModule } from './purchase-routing.module';
import { PastPurchaseOrdersComponent } from './past-purchase-orders/past-purchase-orders.component';
//import { CusfilterPipe } from 'src/app/shared/service/cusfilter.pipe';
import {PendingPurchaseOrderComponent} from './pending-purchase-order/pending-purchase-order.component';
// import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PendingPurchaseAgreementComponent } from './pending-purchase-agreement/pending-purchase-agreement.component';
import { PastAgreementsComponent } from './past-agreements/past-agreements.component';
import { PendingAcceptanceComponent } from './pending-acceptance/pending-acceptance.component';
import { CommonShipmentDetailsComponent } from './common-shipment-details/common-shipment-details.component';
import { SummaryDetailsComponent } from './summary-details/summary-details.component';
import { CommonFileViewComponent } from './common-file-view/common-file-view.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { CreateQuoteComponent } from './create-quote/create-quote.component';
import { PendingPurchaseReleaseComponent } from './pending-purchase-release/pending-purchase-release.component';
import { PastPurchaseReleaseComponent } from './past-purchase-release/past-purchase-release.component';
// import { NumberFormattingPipe} from '../../shared/service/number-formatting.pipe'
import { SharedModule} from '../../shared/shared.module'
const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'middle',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 99,
      gap: 15
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 1200,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 800,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 1000,
      easing: 'ease',
      offset: 150
    },
    shift: {
      speed: 500,
      easing: 'ease'
    },
    overlap: 90
  }
};
@NgModule({
  declarations: [PastPurchaseOrdersComponent, PendingPurchaseOrderComponent,
    PendingPurchaseAgreementComponent,PastAgreementsComponent, PendingAcceptanceComponent, CommonShipmentDetailsComponent, SummaryDetailsComponent, CommonFileViewComponent, InvoiceComponent,CreateQuoteComponent, PendingPurchaseReleaseComponent, PastPurchaseReleaseComponent ],
  imports: [
    CommonModule,
    PurchaseRoutingModule,
    FormsModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxPaginationModule,
    NgxSpinnerModule,
    SharedModule
  ],
  exports: [CommonFileViewComponent]
})
export class PurchaseModule { }
