import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild,Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { LoginService } from '../../auth/service/login.service';
import { PurchaseService } from '../../purchase/service/purchase.service';

import { Address } from '../../auth/models/address';
import { Contacts } from '../../auth/models/contacts';
import { BankAccounts } from '../../auth/models/bank-accounts';
import { BankDetails } from '../../auth/models/bank-details';
import { Registration, ADDRESSES, CONTACTS, BANKS, HEADERS } from '../../auth/models/registration';
declare var jQuery: any;
import { profileData } from 'src/app/config/config'

@Component({
  selector: 'app-view-supplier',
  templateUrl: './view-supplier.component.html',
  styleUrls: ['./view-supplier.component.css']
})
export class ViewSupplierComponent implements OnInit {

  @Input() supplier: [];
  @Input() viewFlag : boolean;


  notifier: NotifierService;
  public viewSupplierList =[];
  // public viewFlag=false;
  public AddressesAll =[];
  public banksAll =[];
  public contactsAll =[];
  public Addresses =[];
  public banks =[];
  public contacts =[];
  public headers;
  public attachments=[];
  public register: any =[];
  public address: Address;
  // public HEADERS:HEADERS;
  public ADDRESSES: ADDRESSES;
  public CONTACTS: CONTACTS;
  public BANKS: BANKS;
  public isAddress = false;
  public contact: Contacts;
  public bankAccounts: BankAccounts;
  public originalCaptcha;
  public captchaEntered;

  constructor(private notifierService: NotifierService, private spinner: NgxSpinnerService, private route : ActivatedRoute,
    private rsaService: RsaService, private router: Router, private loginService: LoginService, private purchaseService: PurchaseService ) {
    this.notifier = notifierService;
    // this.route.queryParams.subscribe((data) => {
    //   this.supplier = data['supplier'];
    // });
    // console.log("supplier : ", this.supplier)

  }

  ngOnInit(): void {
    (function($) {
      $(document).ready(function() {
        $('.pass').hide();

        $('.Intermidiate').hide();
        $('#ibaccount').click(function() {

          if ($(this).is(':checked')) {
            $('.Intermidiate').show();
          } else {
            $('.Intermidiate').hide();
          }

        });

        $('#myBtn').click(function() {
          $('.pass').show();
          $('.user').hide();

          return false;
        });
      });

      $(function() {
        $('#wizard').steps({
          headerTag: 'h4',
          bodyTag: 'section',
          transitionEffect: 'fade',
          enableAllSteps: true,
          transitionEffectSpeed: 500,
          onStepChanging(event, currentIndex, newIndex) {
            if (newIndex === 1) {
              $('.steps ul').addClass('step-2');
            } else {
              $('.steps ul').removeClass('step-2');
            }
            if (newIndex === 2) {
              $('.steps ul').addClass('step-3');
            } else {
              $('.steps ul').removeClass('step-3');
            }

            if (newIndex === 3) {
              $('.steps ul').addClass('step-4');
            } else {
              $('.steps ul').removeClass('step-4');
            }

            if (newIndex === 4) {
              $('.steps ul').addClass('step-5');
              $('.actions ul').addClass('step-last');
              const len = $('#wizard .actions li:last-child').children().length;
              if (len == 1) {
                $('#wizard .actions li:last-child').append('<button type="submit" id="reg" style="padding:0;padding-left: 47.5px;margin-right: 25px;width: 150px;font-weight: 600;border: none;display: inline-flex;height: 51px;align-items: center;background: #0f96f9;cursor: pointer;color: #fff !important;position: relative;text-decoration: none;transform: perspective(1px) translateZ(0);transition-duration: 0.3s;font-weight: 400;">Approve</button>');
                $('#wizard .actions li:last-child').append('<button type="submit" id="reg" style="padding:0;padding-left: 47.5px;width: 150px;font-weight: 600;border: none;display: inline-flex;height: 51px;align-items: center;background: #0f96f9;cursor: pointer;color: #fff !important;position: relative;text-decoration: none;transform: perspective(1px) translateZ(0);transition-duration: 0.3s;font-weight: 400;">Reject</button>');
              }
              // remove default #finish button
              // $("#registerForm").submit(function( event ) {
              //   event.preventDefault();
              // });
              $('#wizard').find('a[href="#finish"]').attr('style', 'display:none;');
              // append a submit type button

            }
            else {
              $('.steps ul').removeClass('step-5');
              // $('.actions ul').removeClass('step-last');
            }
            if (newIndex === 5) {
              $('.steps ul').addClass('step-6');
            } else {
              $('.actions ul').removeClass('step-last');
            }
            return true;
          },
          labels: {
            finish: 'Approve',
            next: 'Next',
            previous: 'Previous'
          }
        });
        // Custom Steps Jquery Steps
        $('.wizard > .steps li a').click(function() {
          $(this).parent().addClass('checked');
          $(this).parent().prevAll().addClass('checked');
          $(this).parent().nextAll().removeClass('checked');
        });
        // Custom Button Jquery Steps
        $('.forward').click(function() {
          $('#wizard').steps('next');
        });
        $('.backward').click(function() {
          $('#wizard').steps('previous');
        });
        // Checkbox
        $('.checkbox-circle label').click(function() {
          $('.checkbox-circle label').removeClass('active');
          $(this).addClass('active');
        });
      });
      $(document).ready(function() {
        // createCaptcha()
        $('.file_input_button').mouseover(function() {
          $(this).addClass('file_input_button_hover');
        });

        $('.file_input_button').mouseout(function() {
          $(this).removeClass('file_input_button_hover');
        });

        $('.file_input_hidden').change(function() {

          const fileInputVal = $(this).val();

          // fileInputVal = fileInputVal.replace("C:"\"fakepath"\"", "");

          $(this).parent().prev().val(fileInputVal);

        });
      });

    })(jQuery);

    console.log("supplier : ", this.supplier)
    // this.viewSupplier();
    if(this.supplier){
      this.viewData(this.supplier)
    }

  }

  viewSupplier(){
    this.spinner.show();
    const details = this.purchaseService.inputJSON('SUPPLIER_REGISTRATION', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      console.log('lll VIEW_SUPPLIER', data);
      this.viewSupplierList = data.SUPPLIER_REGISTRATION.HEADERS ? data.SUPPLIER_REGISTRATION.HEADERS.map(x=>(x.HEADER)) :[]
      this.AddressesAll = data.SUPPLIER_REGISTRATION.ADDRESSES ? data.SUPPLIER_REGISTRATION.ADDRESSES.map(x=>(x.ADDRESS)) :[]
      this.banksAll = data.SUPPLIER_REGISTRATION.BANKS ? data.SUPPLIER_REGISTRATION.BANKS.map(x=>(x.BANK)) :[]
      this.contactsAll = data.SUPPLIER_REGISTRATION.CONTACTS ? data.SUPPLIER_REGISTRATION.CONTACTS.map(x=>(x.CONTACT)) :[]

      this.Addresses = this.AddressesAll;
      this.banks = this.banksAll;
      this.contacts = this.contactsAll;

      this.viewSupplierList.forEach(
        (x, i) => {
          this.viewSupplierList[i].address = this.AddressesAll.filter(y => y.SUPPLIER_REGISTRATION_ID === x.SUPPLIER_REGISTRATION_ID);
          this.viewSupplierList[i].bank = this.banksAll.filter(y => y.SUPPLIER_REGISTRATION_ID === x.SUPPLIER_REGISTRATION_ID);
          this.viewSupplierList[i].contact = this.contactsAll.filter(y => y.SUPPLIER_REGISTRATION_ID === x.SUPPLIER_REGISTRATION_ID);
        }
      );
      console.log("viewSupplierList : ", this.viewSupplierList)
      // this.addressList  = data.SUPPLIER_REGISTRATION.ADDRESSES ? data.SUPPLIER_REGISTRATION.ADDRESSES.map(x=>(x.ADDRESS)) :[]
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  viewData(supplier){
    // console.log("vew comm: element : ", supplier);
    // this.router.navigate(['/craftsmanautomation/administration/viewSupplier'], { queryParams: {  SUPPLIER_REGISTRATION_ID: supplier.SUPPLIER_REGISTRATION_ID } });
    // this.viewFlag = true;
    // this.headers= this.viewSupplierList.find(x => x.SUPPLIER_REGISTRATION_ID === supplier.SUPPLIER_REGISTRATION_ID)
    this.headers = this.supplier
    this.getFileAttchment(this.headers.ATTACH_DOCUMENT_STRING)
    // this.getFileAttchment(this.headers.ATTACH_DOCUMENT_STRING)
    // console.log("headers : ", this.headers)
  }
  goBack(){
    this.viewFlag = false;
    // this.router.navigate(['/craftsmanautomation/administration/authorizeSupplier']);
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
      this.router.navigateByUrl('/craftsmanautomation/administration/authorizeSupplier', {skipLocationChange : profileData.hideURL}));
  }

  getFileAttchment(attchmentString){
    var query = this.purchaseService.inputJSON('ATTACHMENT_LIST', undefined, undefined,'ATTACHMENT_LIST',attchmentString )
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      console.log("attachments : ", data)
    this.attachments = data.ATTACHMENTS? data.ATTACHMENTS.map(x=>(x.ATTACHMENT)) :[]
    // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
    //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
    this.spinner.hide();

  }, error => {
    console.log(error);
    this.spinner.hide();
  });
  }
  downloadfile(item){

  }
}
