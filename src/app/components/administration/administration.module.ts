import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrationRoutingModule } from './administration-routing.module';
import { InviteSupplierComponent } from './invite-supplier/invite-supplier.component';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CommunicationComponent } from './communication/communication.component';
import { MenusComponent } from './menus/menus.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { OnlineForumComponent } from './online-forum/online-forum.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PurchaseModule } from '../purchase/purchase.module';
import { AuthorizeSupplierComponent } from './authorize-supplier/authorize-supplier.component';
import { ViewSupplierComponent } from './view-supplier/view-supplier.component';
import { ListNotificationComponent } from './list-notification/list-notification.component';


const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'middle',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 99,
      gap: 15
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 1200,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 800,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 1000,
      easing: 'ease',
      offset: 150
    },
    shift: {
      speed: 500,
      easing: 'ease'
    },
    overlap: 90
  }
};

@NgModule({
  declarations: [InviteSupplierComponent, CommunicationComponent, MenusComponent, RolesComponent, UsersComponent, OnlineForumComponent, AuthorizeSupplierComponent, ViewSupplierComponent, ListNotificationComponent],
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    FormsModule,
    NgxPaginationModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxSpinnerModule,
    SharedModule,
    PurchaseModule,
  ]
})
export class AdministrationModule { }
