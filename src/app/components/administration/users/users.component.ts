import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import { NotifierService } from "angular-notifier";
import { NgxSpinnerService } from "ngx-spinner";
import { PurchaseService } from "../../purchase/service/purchase.service";
import { EmailService } from "../../auth/service/email.service";
import { RsaService } from "src/app/shared/service/rsa.service";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public email;
  public shipmentView = false;
  public page: any;
  public searchText = '';
  public user: any = {};
  private notifier: NotifierService;
  public userList;
  // public toggleButton = false;
  public USER_ID;
  public rolesList=[];
  public userAssignMentList=[];
  public userAssignMentfilter;
  public selectedItemsList = [];
  public checkedIDs = [];
  public supplieralreadyAssigned=[];
  public supplieralreadyAssignedwithYes =[];
  public supplierFlag = false;
  public activeflagNList=[];
  public AssignToUSER_ID=''
  public tempflag=false;
  public selectParty=[];

  constructor(
    private purchaseService: PurchaseService,
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService,
    public emailService: EmailService,
    private rsaService: RsaService
  ) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
    this.getRoles();
    this.getUser();
  }

  getRoles(){
    this.spinner.show();
    const details = this.purchaseService.inputJSON('ROLE_LIST', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.rolesList = data.ROLES ? data.ROLES.map(x=> x.ROLE) :[]
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  getUser(){
    this.spinner.show();
    // const details = { email: this.email, purchaseType: "ASN_PENDING_PO" };
    const details = this.purchaseService.inputJSON('USER_LIST', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
        // console.log('ppp userList', data);
      this.userList = data.USERS ? data.USERS.map(x=> x.USER) :[]
      this.userList.sort((a, b) =>a.USER_ID < b.USER_ID ? -1 : a.USER_ID > b.USER_ID ? 1 : 0);
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }
  addUser(){
    this.user.ALL_SUPPLIER_FLAG = this.user.ALL_SUPPLIER_FLAG ? 'Y' :'N'
    this.user.ACTIVE_FLAG = this.user.ACTIVE_FLAG ? 'Y' :'N'
      this.user.ACTION = 'UPDATE';
      let query = { USER_LIST: { ...this.user, USER_ID : this.USER_ID }}
      this.purchaseService.saveASN(query).subscribe((data: any) => {
      this.notifier.notify("success", "Updated Sucessfully");
      this.getUser();
    },
      (error) => {
        console.log(error);
      }
    );
  }

  editUser(item, i){
    this.USER_ID = item.USER_ID
    this.user = item
  }

  AssignSupplier(item){
    this.AssignToUSER_ID = item.USER_ID
    this.spinner.show();
    this.supplieralreadyAssigned =[];
    var details = this.purchaseService.inputJSON('USER_ASSIGNMENT_LIST', undefined, undefined, 'USER_ASSIGNMENT_LIST', item.USER_ID)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.userAssignMentList = data ? data.USER_ASSOCIATIONS.map(x=>x.USER_ASSOCIATION) :[]
      this.spinner.hide();
      this.userAssignMentList.forEach(e => {
        if(e.ACTIVE_FLAG ==='Y') {
          this.supplieralreadyAssignedwithYes.push(e)
          this.checkedIDs.push(e.SUPPLIER_ID)
          e.isChecked = true
         }else{
          this.supplieralreadyAssigned.push(e)
         }
      });
      if(this.supplieralreadyAssignedwithYes.length === 0){
        this.supplierFlag =true;
      }
    },
      (error) => {
        console.log(error);
      }
    );
  }

  AddMoreButton(){
      this.supplierFlag=true;
      this.supplieralreadyAssigned=[];
      this.supplieralreadyAssigned =this.userAssignMentList.filter((x ,i)=> (x.ACTIVE_FLAG ==='N'))
  }

 addAssignMenuToRole(){
    const details ={ USER_ASSIGNMENT_LIST :{ USER_ID : this.AssignToUSER_ID, SUPPLIER_ID :  this.checkedIDs}}
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
    // console.log("addAssignMenuToRole : ", data)
    },
    (error) => {
    console.log(error);
    });
}

setSupplier(value){
    var currentSelectedSuppliers
        this.supplieralreadyAssigned.filter((x,i) => {
          if (x.SUPPLIER_NAME === value) {
            this.checkedIDs.push(x.SUPPLIER_ID);
            currentSelectedSuppliers=x
          }
        })
        let temp = this.supplieralreadyAssignedwithYes.find(x=> x.SUPPLIER_NAME === value)
          if(!temp){
            currentSelectedSuppliers.isChecked = true
            this.supplieralreadyAssignedwithYes.push(currentSelectedSuppliers);
          }else{
          this.notifier.notify('warning', "Supplier already selected !!!")
          }
        this.checkedIDs= Array.from(new Set(this.checkedIDs))
  }

  changeSelection(i){
    if(this.supplieralreadyAssignedwithYes[i].isChecked === false){
       this.supplieralreadyAssignedwithYes.splice(i,1)
  }
  this.checkedIDs= this.supplieralreadyAssignedwithYes.filter(x=>(x.SUPPLIER_ID))
}
}
