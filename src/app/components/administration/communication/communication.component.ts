import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
declare var jQuery: any;

@Component({
  selector: 'app-communication',
  templateUrl: './communication.component.html',
  styleUrls: ['./communication.component.css']
})
export class CommunicationComponent implements OnInit {

  public okButton = false;
  public viewButton= true;
  public title;
  public messages;
  public messageQueue : any=[];
  public allSupplierFlag;
  public readOnlyFlag;
  public validTillDate;
  public allSupplierButton = false;
  public partyList=[];
  public checkedIDs=[];
  public supplierList =[];
  private notifier: NotifierService;
  public email;
  public supplierNews;
  public highImportance;
  public startDate;
  public today = new Date();
  public downTime;

  constructor(private purchaseService : PurchaseService, private notifierService: NotifierService, public rsaService: RsaService) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem("3"));
    this.getAllPartyList();
    let setstartDate = (date) => {
      this.startDate = date;
    };
    let setvalidTillDate = (date) => {
      this.validTillDate = date;
    };
    (function ($, setstartDate, setvalidTillDate) {
      $(document).ready(function () {
        var date1 = new Date();

        $("#datepicker1").datepicker({
          uiLibrary: "bootstrap4",
          format: "yyyy-mm-dd",
          maxDate: date1,
          // initialDate : date1,
          enableTime : true,
          // endDate : this.today,
          change(e) {
            setstartDate(e.target.value);
          },
        });

        // $('#datepicker1').datetimepicker('setStartDate',date1);

        $("#datepicker2").datepicker({
          uiLibrary: "bootstrap4",
          format: "yyyy-mm-dd",
          minDate: date1,
          change(e) {
            setvalidTillDate(e.target.value);
          },
        });
      });
    })(jQuery, setstartDate, setvalidTillDate);
  }

  pressOk(){
    this.viewButton = false;
    this.okButton=true;
  }

  submitPage(){
    if(!(this.title && this.messages)){
      this.notifier.notify("warning", "Please enter discussion title & texts");
    } else{
    this.allSupplierFlag = this.allSupplierFlag ? 'Y' :'N'
    this.readOnlyFlag = this.readOnlyFlag ? 'Y' :'N'
    this.highImportance = this.highImportance ? 'Y' :'N'
    this.supplierNews = this.supplierNews ? 'Y' :'N'
    this.downTime = this.downTime ? 'Y' :'N'

    let query = { COMMUNICATIONS: { TITLE: this.title, ALL_SUPPLIER_FLAG : this.allSupplierFlag, READONLY_FLAG : this.readOnlyFlag ,
      STATUS : 'NEW', VALID_TILL_DATE :this.validTillDate, SUPPLIER_ID : this.checkedIDs, CONVERSATION_TEXT :this.messages,
      MODULE_NAME : 'COMMON', USER_ID :this.email, HIGH_IMPORTANCE : this.highImportance, SUPPLIER_NEWS : this.supplierNews,
    START_DATE : this.startDate, DOWNTIME : this.downTime }}
    // console.log("query : ", query)
    this.purchaseService.saveASN(query).subscribe((data: any) => {
      // console.log("data : ", data);

      if(data){
        this.notifier.notify("success", "Posted Sucessfully");
        this.clear();
      }
    },(error) => {
        console.log(error);
      }
    );
    }
    // this.addUpdateFlag = false;
  }

  changeAllSupplierFlag(){
    if(this.allSupplierFlag ===true){
      this.allSupplierButton = true;
  }else{
    this.allSupplierButton = false;
    }
  }

  getAllPartyList(){
  const details = this.purchaseService.inputJSON('SUPPLIER_LIST', undefined, undefined)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      if(data){
          this.partyList = (data.VENDORS) ? data.VENDORS.map(x=>(x.VENDOR)) : [];
          // this.partyList = this.partyList.sort();

        }
      })
  }

  setSupplier(value){
        let temp = this.partyList.find(x=> x.VENDOR_NAME === value)
          if(temp){
            this.supplierList.push(temp)
            this.checkedIDs.push(temp.VENDOR_ID);
          }
        this.supplierList= Array.from(new Set(this.supplierList))
        this.checkedIDs= Array.from(new Set(this.checkedIDs))
  }

  removeSupplier(i){
    this.checkedIDs.splice(i, 1);
    this.supplierList.splice(i, 1);
    }
    clear(){
      this.title ='';
      this.allSupplierFlag =false;
      this.readOnlyFlag= false;
      this.validTillDate =false;
      this.checkedIDs =[];
      this.messages ='';
      this.supplierList =[];
    }
  }
