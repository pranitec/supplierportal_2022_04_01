import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteSupplierComponent } from './invite-supplier.component';

describe('InviteSupplierComponent', () => {
  let component: InviteSupplierComponent;
  let fixture: ComponentFixture<InviteSupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviteSupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
