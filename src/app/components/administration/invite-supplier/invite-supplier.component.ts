import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild  } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { environment } from 'src/environments/environment';
import { EmailService } from '../../auth/service/email.service';
import { LoginService } from '../../auth/service/login.service';

@Component({
  selector: 'app-invite-supplier',
  templateUrl: './invite-supplier.component.html',
  styleUrls: ['./invite-supplier.component.css']
})
export class InviteSupplierComponent implements OnInit {
  public email = '';
  public justification = '';
  public text = true;
  public registrationType;
  notifier: NotifierService;
  public fileName = '';
  fileData: File = null;
  public file64;


  @ViewChild('myFileInput', { static: false }) myInputVariable: ElementRef;
  constructor(private notifierService: NotifierService, public emailService: EmailService, private spinner: NgxSpinnerService,
    private rsaService: RsaService, private router: Router, private loginService: LoginService) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
  }

  invite() {
    // console.log((new Date()).getTime());


    if (this.email.length > 0) {
      this.spinner.show();

      this.spinner.hide();
      let emailId = this.email;
      emailId = this.rsaService.encrypt(emailId);
      emailId = encodeURIComponent(emailId);
      let time = encodeURIComponent(this.rsaService.encrypt('' + (new Date()).getTime()));
      this.spinner.show();
      const subject = ' Craftsman Automation Supplier Portal - New Supplier Request';
      const body = `
          Hi ` + name + `,<br/><br/>

          Please follow the below link for registering new Supplier. <br/><br/>

          `+ environment.baseUrl + `/#/auth/registration?page=` + emailId + `&id=` + time + `<br/><br/>

          For any issues, contact your system administrator.<br/><br/>

           Thank You, <br/>
           Craftsman Automation Team
            `;

      this.emailService.sendMailWithAttachments(this.email, subject, body, this.fileData[0].name, this.file64).subscribe(message => {
        console.log("msg : ", message)
        this.spinner.hide();
        if (message === 'OK') {
          const id = 1;
          this.text = false;
          this.email = '';
        } else {
          this.notifier.notify('warning', 'Email Delivery Failure!');
        }
      },error=>{
        this.spinner.hide();
        this.notifier.notify('warning', 'Email Delivery Failure!');
      });
      this.spinner.hide();
      // window.open('http://localhost:4200/#/auth/registration?page=U2FsdGVkX19RUeDuGWaPuz%2BstnxToHnlPCxLK1WYl5g7Nqoj9zG2MDWoF4cp9l%2Bl&id=U2FsdGVkX1%2FZEq2xTx8yPo3tqsJnvM0UhxjAN6WcgXQ%3D');
    } else {
      this.notifier.notify('error', 'Please enter email address');
    }

  }
  cancel() {

  }

  onFilesAdded(fileInput: any) {
    this.spinner.show();
    this.fileData = fileInput.target.files as File;
    const length = fileInput.target.files.length;
    const file = new FormData();
    this.loginService.type = '/GENERAL';
    this.loginService.fileType = 'Yes'
    for (let i = 0; i < length; i++) {
      file.append('file', this.fileData[0], this.fileData[0].name);
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData[0]);
    reader.onload = () => {
      this.file64 = reader.result;
    };
    this.loginService.FileUpload(file).subscribe((data) => {
      this.spinner.hide();
      let i;
      //console.log('iiii');
     // this.fileUpload();
      this.fileName = this.fileData[0].name;
    }, err => {
      this.spinner.hide();
      this.resetFile();
      if (err) {

      } else {
      }
    });

  }

  resetFile() {
    this.myInputVariable.nativeElement.value = '';
  }

}
