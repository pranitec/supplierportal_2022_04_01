import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineForumComponent } from './online-forum.component';

describe('OnlineForumComponent', () => {
  let component: OnlineForumComponent;
  let fixture: ComponentFixture<OnlineForumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineForumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineForumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
