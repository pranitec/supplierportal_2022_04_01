import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { BreadcrumbService } from "src/app/shared/service/breadcrumb.service";
import { NavService } from "src/app/shared/service/nav.service";
import { RsaService } from "src/app/shared/service/rsa.service";
import { SidebarService } from "src/app/shared/service/sidebar.service";
import { PurchaseService } from "../../purchase/service/purchase.service";
import { NotifierService } from 'angular-notifier';
declare var jQuery: any;

@Component({
  selector: "app-online-forum",
  templateUrl: "./online-forum.component.html",
  styleUrls: ["./online-forum.component.css"],
})
export class OnlineForumComponent implements OnInit {
  public communication;
  public historyFlag = false;
  public historyDetailsFlag = false;
  public communicationDetails;
  public addUpdateFlag = false;
  public communication_id;
  public updateText;
  public communication_title = "";
  public email = "";
  public attachmentDocumentString = "";
  public communicationString = "";
  public communicationStringTitle = [];
  public title;
  public messages;
  public status='';
  public modulename='';
  public searchText='';
  public communication1 =[];
  public newConversationflag=false;
  public communicationstatus =''
  public viewCount = 10;
  public page = 1;
  public viewFlag = false;
  public enableUpdateButton = true;
  private notifier: NotifierService;

  constructor(
    private purchaseService: PurchaseService,
    private spinner: NgxSpinnerService,
    private rsaService: RsaService,
    public router: Router,  private breadCrumbServices: BreadcrumbService,
    public navServices: NavService, public sidebarService : SidebarService,
    private route: ActivatedRoute, private chRef: ChangeDetectorRef, private notifierService: NotifierService,
  ) {
    this.navServices.collapseSidebar = true;
    this.notifier = notifierService;
    this.route.queryParams.subscribe((data) => {
      this.communicationString = data["communicationString"];
      this.communicationStringTitle = this.communicationString
        ? this.communicationString.split("~")
        : [];
    });
  }

  ngOnInit(): void {
    this.newConversationflag= this.sidebarService.checkMenuAccess('COMMON_DISCUSSION');
    // if(this.communicationString ){
    //   this.newConversationflag = true;
    // }
    this.email = this.rsaService.decrypt(localStorage.getItem("3"));
    this.getHistory();
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/administration/communication') {
        this.getHistory();
      }
    });


  }

  getHistory() {
    this.historyFlag = true;
    this.spinner.show();
    if (this.communicationString) {
      var query = this.purchaseService.inputJSON("COMMUNICATIONS", undefined, undefined,"COMMUNICATION_STRING", this.communicationString);
    } else {
      var query = this.purchaseService.inputJSON("COMMUNICATIONS", undefined, undefined);
    }
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      // console.log("communication : ", data);
      this.spinner.hide();
        this.communication = data.COMMUNICATIONS ? data.COMMUNICATIONS.map((x) => x.COMMUNICATION) : [];
        this.communication1 = this.communication
        this.communication.forEach((x) => {
          if (x.PARENT_FLAG === "Y") {
            x.isChecked = true;
          }
        });
        if(this.communication){
          this.viewFlag = true
          this.viewTable();
        }
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  viewTable(){
    // this.chRef.detectChanges();
    (function ($) {
      $('#onlineDiscussiontable').DataTable({
        responsive: false,
        // iDisplayLength: 5,
        // bLengthChange: false,
        // retrieve: true,
        pageLength: 10,
        paging : false,
        // stateSave : false,
       info : false
      });
    })(jQuery);
  }
  getCommunicationDetails(communication_id, title, attachmentString, status) {
    console.log("inside getcommunication details")
    this.spinner.show();
    this.communication_id = communication_id;
    this.communication_title = title;
    this.attachmentDocumentString = attachmentString;
    this.communicationstatus = status
    this.historyFlag = false;
    this.historyDetailsFlag = true;
    var query = this.purchaseService.inputJSON("COMMUNICATION_DETAILS", undefined, undefined, "COMMUNICATION_ID",
      communication_id
    );
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
        this.spinner.hide();
        console.log("getCommunicationDetails : ", data)
        this.communicationDetails = data.COMMUNICATION_DETAILS
          ? data.COMMUNICATION_DETAILS.map((x) => x.COMMUNICATION_DETAIL) : [];
        // console.log("COMMUNICATION_details : ", data);
        // this.communicationDetails.forEach((x) => {
        //   x.firstLetter = x.CREATED_BY.substring(0, 1);
        // });
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }
  goback() {
    this.historyDetailsFlag = false;
    this.historyFlag = true;
    this.addUpdateFlag = false
    this.getHistory();
  }
  addUpdate() {
    this.addUpdateFlag = true;
    this.updateText = ''
  }

  reply() {
console.log("inn reply")
    if(this.updateText.length<=5){
      this.notifier.notify("warning", "Please enter discussion texts");
      // this.enableUpdateButton =  false
    }
    else{
      // this.enableUpdateButton =  true
    var query;
    if (this.communicationString) {
      query = {
        COMMUNICATIONS: {
          TITLE: this.title,
          CONVERSATION_TEXT: this.messages,
          COMMUNICATION_STRING: this.communicationString,
          USER_ID: this.email,
        },
      };
    } else {
      query = {
        COMMUNICATION_DETAILS: {
          COMMUNICATION_ID: this.communication_id,
          CONVERSATION_TEXT: this.updateText,
          USER_ID: this.email,
        },
      };
    }
    console.log("query", query)
       this.purchaseService.saveASN(query).subscribe((data: any) => {
      // console.log("reply update : ", data, this.updateText)
      this.updateText ='';
      this.addUpdateFlag = false;
       this.getCommunicationDetails(this.communication_id, this.communication_title,this.attachmentDocumentString, this.communicationstatus);
      },
      (error) => {
        console.log(error);
      }
    );
    this.addUpdateFlag = false;
  }
  }

  submitPage() {
    console.log("inn submit page", this.title, this.messages)
    if(!(this.title&& this.messages)){
      this.notifier.notify("warning", "Please enter discussion title & texts");
      // this.enableUpdateButton =  false
    } else{
    let query = {
      COMMUNICATIONS: {
        TITLE: this.title,
        CONVERSATION_TEXT: this.messages,
        COMMUNICATION_STRING: this.communicationString,
        USER_ID: this.email,
      },
    };
    this.purchaseService.saveASN(query).subscribe((data: any) => {
      this.getHistory();
      },
      (error) => {
        console.log(error);
      }
    );
  }
  }
  close(){
    let query = {
      COMMUNICATIONS_UPDATE: {
        COMMUNICATION_ID: this.communication_id,
        USER_ID: this.email,
      },
    };
    this.purchaseService.saveASN(query).subscribe((data: any) => {
        this.getCommunicationDetails(this.communication_id, this.title, this.communicationString, this.communicationstatus)
       },
      (error) => {
        console.log(error);
      }
    );

  }

  searchData(){
    if(this.modulename === 'ALL' || this.status === 'ALL'){
      this.communication = this.communication1
    }
    else if(this.modulename || this.status ){
      this.communication = this.communication1.filter(x=>
       (this.modulename === '' || x.MODULE_NAME === this.modulename)
         &&
        (this.status === '' || x.STATUS === this.status)
       )};
    if(this.searchText) {
      this.communication = this.communication1.filter(f=> JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()))
    }

    if(this.communication){
      this.viewFlag = true
      this.viewTable()
    }
  }

  clear(){
    this.modulename='';
    this.status='';
    this.searchText='';
    this.getHistory();
  }

  public onPageChanged(event) {
    this.page = event;
  }
}
