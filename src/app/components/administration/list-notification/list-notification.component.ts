import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { profileData } from 'src/app/config/config'

@Component({
  selector: 'app-list-notification',
  templateUrl: './list-notification.component.html',
  styleUrls: ['./list-notification.component.css']
})
export class ListNotificationComponent implements OnInit {

  public notificationList=[];
  public viewCount =10;
  public page= 1;

  constructor(private purchaseService: PurchaseService, private spinner: NgxSpinnerService, private router: Router) { }

  ngOnInit(): void {
    this.getAllNotification();
  }

  getAllNotification(){
    this.spinner.show();
    var query = this.purchaseService.inputJSON('NOTIFICATION_LIST', undefined, undefined)
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      console.log('ppp NOTIFICATION_LIST', data);
      this.notificationList = data ? data.NOTIFICATION_LISTS.map(x => x.NOTIFICATION_LIST) : [];
      // this.count = data.NOTIFICATION_COUNT;
      this.spinner.hide();
      // let markallread = this.notificationList.filter(y => (y.NOTIFICATION_STATUS === 'U'))
      // markallread.forEach(x => {
      //   this.markallReadIds.push(x.NOTIFICATION_ID)
      // });
      // console.log('ppp markallReadIds', this.markallReadIds);
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
  onPageChanged(event){
    this.page = event;
  }

  clickedLink(id, url, docid) {
   let temp = this.notificationList.find(x => x.NOTIFICATION_ID === id && x.NOTIFICATION_STATUS === 'U')
    console.log("temp : ", temp);

    if(temp){
      let query = { NOTIFICATION_LIST: { NOTIFICATION_ID: id} };
      this.purchaseService.saveASN(query).subscribe((data: any) => {
        // console.log("notified data: ", data)
      },
        (error) => {
          console.log(error);
        });
    }
     this.router.navigateByUrl(url, { queryParams: { docid: docid }, skipLocationChange : profileData.hideURL});
  }

}
