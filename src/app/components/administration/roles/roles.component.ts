import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import { NotifierService } from "angular-notifier";
import { NgxSpinnerService } from "ngx-spinner";
import { PurchaseService } from "../../purchase/service/purchase.service";
import { EmailService } from "../../auth/service/email.service";
import { RsaService } from "src/app/shared/service/rsa.service";

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  public shipmentView = false;
  public page: any;
  public searchText = '';
  public roles: any = {};
  private notifier: NotifierService;
  public rolesList;
  public toggleButton = false;
  public email;
  public ROLE_ID;
  public roleAssignMentList;
  selectedItemsList = [];
  checkedIDs = [];
  public roleAssigedMenufilter;

  @ViewChild("mycheckbox") mycheckbox;
  constructor(
    private purchaseService: PurchaseService,
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService,
    public emailService: EmailService,
    private rsaService: RsaService
  ) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.getRoles();
  }

  getRoles(){
    this.spinner.show();
    // const details = { email: this.email, purchaseType: "ASN_PENDING_PO" };
    const details = this.purchaseService.inputJSON('ROLE_LIST', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
        // console.log('ppp rolesList', data);
      this.rolesList = data.ROLES ? data.ROLES.map(x=> x.ROLE) :[]
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }
  addRoles(){
    this.roles.ACTIVE_FLAG = this.roles.ACTIVE_FLAG ? 'Y' :'N'
    if(!this.toggleButton){
      this.roles.ACTION = 'CREATE';
      let query = { ROLE_LIST: { ...this.roles }}
      this.purchaseService.saveASN(query).subscribe((data: any) => {
      this.notifier.notify("success", "Submitted Sucessfully");
    },
      (error) => {
        console.log(error);
      }
    );
    }else{
      this.roles.ACTION = 'UPDATE';
      // var details = this.purchaseService.inputJSON('ROLE_ASSIGNMENT_LIST', undefined, undefined, 'ROLE_ASSIGNMENT_LIST', 'ADMIN')
      let query = { ROLE_LIST: { ...this.roles, ROLE_ID: this.ROLE_ID }}
      this.purchaseService.saveASN(query).subscribe((data: any) => {
      this.notifier.notify("success", "Updated Sucessfully");
    },
      (error) => {
        console.log(error);
      }
    );
    }

  }
  editRoles(item, i){
    this.roles = item
    this.toggleButton =true;
    this.ROLE_ID = item.ROLE_ID
    this.toggleButton =true;
  }
  AssignMenu(item){
    var details = this.purchaseService.inputJSON('ROLE_ASSIGNMENT_LIST', undefined, undefined, 'ROLE_ASSIGNMENT_LIST', item.ROLE_NAME)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.roleAssignMentList = data ? data.ROLE_ASSIGNMENTS.map(x=>x.ROLE_ASSIGNMENT) :[]

      this.roleAssignMentList.forEach(e => {
        if(e.ACTIVE_FLAG ==='Y') {
          e.isChecked = true
         }
      });
      this.roleAssigedMenufilter = this.rolesList.find(x =>(x.ROLE_NAME === item.ROLE_NAME))
    },
      (error) => {
        console.log(error);
      }
    );
  }

  changeSelection() {
    this.fetchSelectedItems()
    this.fetchCheckedIDs();
  }

  fetchSelectedItems() {
    this.selectedItemsList = this.roleAssignMentList.filter((value, index) => {
      return value.isChecked
    });
  }

  fetchCheckedIDs() {
    this.checkedIDs = []
    this.roleAssignMentList.forEach((value, index) => {
      if (value.isChecked) {
        this.checkedIDs.push(value.MENU_ID);
      }
    });
  }

  addAssignMenuToRole(){
    const details ={ ROLE_ASSIGNMENT_LIST :{ ROLE_NAME : this.roleAssigedMenufilter.ROLE_NAME,
                                             MENUS :  this.checkedIDs}}
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
    },
      (error) => {
        console.log(error);
    }
      );
  }


}
