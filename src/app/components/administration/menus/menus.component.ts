import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import { NotifierService } from "angular-notifier";
import { NgxSpinnerService } from "ngx-spinner";
import { PurchaseService } from "../../purchase/service/purchase.service";
import { EmailService } from "../../auth/service/email.service";
import { RsaService } from "src/app/shared/service/rsa.service";

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {

  public email;
  public shipmentView = false;
  public page: any;
  public searchText = '';
  public menu: any = {};
  private notifier: NotifierService;
  public menuList;
  public addButton = true;
  public editButton = true;
  public MENU_ID;
  public parentmenuList;
  @ViewChild("closebutton") closebutton;

  constructor(
    private purchaseService: PurchaseService,
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService,
    public emailService: EmailService,
    private rsaService: RsaService
  ) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
    this.menu ={};
    // this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.getMenu();
  }

  getMenu(){
    this.spinner.show();
    // const details = { email: this.email, purchaseType: "ASN_PENDING_PO" };
    const details = this.purchaseService.inputJSON('MENU_LIST', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.menuList = data.MENUS ? data.MENUS.map(x=> x.MENU) :[]
      this.parentmenuList = this.menuList.filter(x =>(x.PARENT_MENU_ID !='null'))
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }
  addMenu(){
    console.log("before addButton, editbutton : ", this.addButton, this.editButton)
    this.menu.DISPLAY_FLAG = this.menu.DISPLAY_FLAG ? 'Y' :'N'
    this.menu.DATE_FILTER_FLAG = this.menu.DATE_FILTER_FLAG ? 'Y' :'N'
    this.menu.EXPORT_TO_EXCEL_FLAG = this.menu.EXPORT_TO_EXCEL_FLAG ? 'Y' :'N'
    this.menu.ENABLE_FILTER_FLAG = this.menu.ENABLE_FILTER_FLAG ? 'Y' :'N'
    this.menu.ACTIVE_FLAG = this.menu.ACTIVE_FLAG ? 'Y' :'N'
    if(this.addButton){
      this.menu.ACTION = 'CREATE';
      let query = { MENU_LIST: { ...this.menu }}
      this.purchaseService.saveASN(query).subscribe((data: any) => {
        if(data){
          this.notifier.notify("success", "Submitted Sucessfully");

          this.addButton =false;
          this.menu ={};
          this.getMenu();
        }
      },
        (error) => {
          console.log(error);
        }
      );
    }
    if(!this.editButton){
      this.menu.ACTION = 'UPDATE';
      let query = { MENU_LIST: { ...this.menu, MENU_ID : this.MENU_ID }}
      this.purchaseService.saveASN(query).subscribe((data: any) => {
        if(data){
          this.notifier.notify("success", "Updated Sucessfully");
          this.closebutton.nativeElement.click();
          this.editButton =false;
          this.menu ={};
          this.getMenu();
        }
      },(error) => {
          console.log(error);
        }
      );
    }
    let query = { MENU_LIST: { ...this.menu }}
    console.log("before addButton, editbutton : ", this.addButton, this.editButton)
  }
  editMenu(item, i){
    this.MENU_ID = item.MENU_ID
    this.menu = item
    this.editButton =false;
    this.addButton = false;
  }

  assignParentMenu(){
    this.menu.PARENT_MENU_ID =this.menu.PARENT_MENU
  }

}
