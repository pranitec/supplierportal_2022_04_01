import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/shared/service/auth-guard.service';
import { RegisterComponent } from '../auth/register/register.component';
import { AuthorizeSupplierComponent } from './authorize-supplier/authorize-supplier.component';
import { CommunicationComponent } from './communication/communication.component';
import { InviteSupplierComponent } from './invite-supplier/invite-supplier.component';
import { ListNotificationComponent } from './list-notification/list-notification.component';
import { MenusComponent } from './menus/menus.component';
import { OnlineForumComponent } from './online-forum/online-forum.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { ViewSupplierComponent } from './view-supplier/view-supplier.component';


const routes: Routes = [{
  path:'inviteSupplier',
  component:InviteSupplierComponent,
    data: {
      title: 'Invite New Supplier',
      breadcrumb: 'Invite New Supplier'
    }
},
{
  path:'communication',
  component:OnlineForumComponent,
    data: {
      title: 'Online Discussion',
      breadcrumb: 'Online Discussion'
    },
    canActivate: [AuthGuardService]
},
{
  path:'menus',
  component:MenusComponent,
    data: {
      title: 'Menus',
      breadcrumb: 'Menus'
    },
    canActivate: [AuthGuardService]
},
{
  path:'roles',
  component:RolesComponent,
    data: {
      title: 'Roles',
      breadcrumb: 'Roles'
    },
    canActivate: [AuthGuardService]
},
{
  path:'users',
  component:UsersComponent,
    data: {
      title: 'Users',
      breadcrumb: 'Users'
    },
    canActivate: [AuthGuardService]
},
{
  path:'newConversation',
  component:CommunicationComponent,
    data: {
      title: 'Online Forum',
      breadcrumb: 'Online Forum'
    },
    canActivate: [AuthGuardService]
},
{
  path:'authorizeSupplier',
  component:AuthorizeSupplierComponent,
    data: {
      title: 'Authorize Supplier',
      breadcrumb: 'Authorize Supplier'
    }
},
{
  path:'viewSupplier',
  component:ViewSupplierComponent,
    data: {
      title: 'View Supplier',
      breadcrumb: 'View Supplier'
    }
},
{
  path:'listNotification',
  component:ListNotificationComponent,
    data: {
      title: 'View Supplier',
      breadcrumb: 'View Supplier'
    }
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
