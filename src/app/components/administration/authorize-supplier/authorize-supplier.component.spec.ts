import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizeSupplierComponent } from './authorize-supplier.component';

describe('AuthorizeSupplierComponent', () => {
  let component: AuthorizeSupplierComponent;
  let fixture: ComponentFixture<AuthorizeSupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizeSupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizeSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
