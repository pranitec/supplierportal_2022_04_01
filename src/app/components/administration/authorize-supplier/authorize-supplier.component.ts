import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild  } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { LoginService } from '../../auth/service/login.service';
import { PurchaseService } from '../../purchase/service/purchase.service';

import { Address } from '../../auth/models/address';
import { Contacts } from '../../auth/models/contacts';
import { BankAccounts } from '../../auth/models/bank-accounts';
import { BankDetails } from '../../auth/models/bank-details';
import { Registration, ADDRESSES, CONTACTS, BANKS, HEADERS } from '../../auth/models/registration';

declare var jQuery: any;

@Component({
  selector: 'app-authorize-supplier',
  templateUrl: './authorize-supplier.component.html',
  styleUrls: ['./authorize-supplier.component.css']
})
export class AuthorizeSupplierComponent implements OnInit {

  notifier: NotifierService;
  public viewSupplierList =[];
  public viewFlag=false;
  public AddressesAll =[];
  public banksAll =[];
  public contactsAll =[];
  public Addresses =[];
  public banks =[];
  public contacts =[];
  public headers;

  public register: any =[];
  public address: Address;
  // public HEADERS:HEADERS;
  public ADDRESSES: ADDRESSES;
  public CONTACTS: CONTACTS;
  public BANKS: BANKS;
  public isAddress = false;
  public contact: Contacts;
  public bankAccounts: BankAccounts;
  public originalCaptcha;
  public captchaEntered;

  constructor(private notifierService: NotifierService, private spinner: NgxSpinnerService,
    private rsaService: RsaService, private router: Router, private loginService: LoginService, private purchaseService: PurchaseService ) {
    this.notifier = notifierService;
  }



  ngOnInit():void{
    this.register = new Registration();
    this.register.HEADER = new HEADERS();
    this.ADDRESSES = new ADDRESSES();
    this.ADDRESSES.ADDRESS = [];
    this.register.ADDRESSES = this.ADDRESSES;
    this.CONTACTS = new CONTACTS();
    this.CONTACTS.CONTACT = [];
    this.register.CONTACTS = this.CONTACTS;
    this.BANKS = new BANKS();
    this.BANKS.BANK = [];
    this.register.BANKS = this.BANKS;
    this.address = new Address();
    this.contact = new Contacts();
    this.bankAccounts = new BankAccounts();
    // this.bankAccounts. = new BankDetails();

    (function($) {
      $(document).ready(function() {
        $('.pass').hide();

        $('.Intermidiate').hide();
        $('#ibaccount').click(function() {

          if ($(this).is(':checked')) {
            $('.Intermidiate').show();
          } else {
            $('.Intermidiate').hide();
          }

        });

        $('#myBtn').click(function() {
          $('.pass').show();
          $('.user').hide();

          return false;
        });
      });

      $(function() {
        $('#wizard').steps({
          headerTag: 'h4',
          bodyTag: 'section',
          transitionEffect: 'fade',
          enableAllSteps: true,
          transitionEffectSpeed: 500,
          onStepChanging(event, currentIndex, newIndex) {
            if (newIndex === 1) {
              $('.steps ul').addClass('step-2');
            } else {
              $('.steps ul').removeClass('step-2');
            }
            if (newIndex === 2) {
              $('.steps ul').addClass('step-3');
            } else {
              $('.steps ul').removeClass('step-3');
            }

            if (newIndex === 3) {
              $('.steps ul').addClass('step-4');
            } else {
              $('.steps ul').removeClass('step-4');
            }

            if (newIndex === 4) {
              $('.steps ul').addClass('step-5');
              $('.actions ul').addClass('step-last');
              const len = $('#wizard .actions li:last-child').children().length;
              if (len == 1) {
                $('#wizard .actions li:last-child').append('<button type="submit" id="reg" style="padding:0;padding-left: 47.5px;width: 150px;font-weight: 600;border: none;display: inline-flex;height: 51px;align-items: center;background: #0f96f9;cursor: pointer;color: #fff !important;position: relative;text-decoration: none;transform: perspective(1px) translateZ(0);transition-duration: 0.3s;font-weight: 400;">Register</button>');
              }
              // remove default #finish button
              // $("#registerForm").submit(function( event ) {
              //   event.preventDefault();
              // });
              $('#wizard').find('a[href="#finish"]').attr('style', 'display:none;');
              // append a submit type button

            } else {
              $('.steps ul').removeClass('step-5');
              $('.actions ul').removeClass('step-last');
            }
            return true;
          },
          labels: {
            finish: 'Register',
            next: 'Next',
            previous: 'Previous'
          }
        });
        // Custom Steps Jquery Steps
        $('.wizard > .steps li a').click(function() {
          $(this).parent().addClass('checked');
          $(this).parent().prevAll().addClass('checked');
          $(this).parent().nextAll().removeClass('checked');
        });
        // Custom Button Jquery Steps
        $('.forward').click(function() {
          $('#wizard').steps('next');
        });
        $('.backward').click(function() {
          $('#wizard').steps('previous');
        });
        // Checkbox
        $('.checkbox-circle label').click(function() {
          $('.checkbox-circle label').removeClass('active');
          $(this).addClass('active');
        });
      });
      $(document).ready(function() {
        // createCaptcha()
        $('.file_input_button').mouseover(function() {
          $(this).addClass('file_input_button_hover');
        });

        $('.file_input_button').mouseout(function() {
          $(this).removeClass('file_input_button_hover');
        });

        $('.file_input_hidden').change(function() {

          const fileInputVal = $(this).val();

          // fileInputVal = fileInputVal.replace("C:"\"fakepath"\"", "");

          $(this).parent().prev().val(fileInputVal);

        });
      });


      let code;
      function createCaptcha() {
        // clear the contents of captcha div first
        document.getElementById('captcha').setAttribute('innerHTML', '');
        const charsArray =
          '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*';
        const lengthOtp = 6;
        const captcha = [];
        for (let i = 0; i < lengthOtp; i++) {
          // below code will not allow Repetition of Characters
          const index = Math.floor(Math.random() * charsArray.length + 1); // get the next character from the array
          if (captcha.indexOf(charsArray[index]) == -1) {
            captcha.push(charsArray[index]);
          } else { i--; }
        }
        const canv = document.createElement('canvas');
        canv.id = 'captcha';
        canv.width = 100;
        canv.height = 50;
        const ctx = canv.getContext('2d');
        ctx.font = '25px Georgia';
        ctx.strokeText(captcha.join(''), 0, 30);
        // storing captcha so that can validate you can save it somewhere else according to your specific requirements
        code = captcha.join('');
        this.originalCaptcha = captcha.join('');
        document.getElementById('captcha').appendChild(canv); // adds the canvas to the body element
      }
    })(jQuery);

    this.viewSupplier();
  }

  viewSupplier(){
    this.spinner.show();
    const details = this.purchaseService.inputJSON('SUPPLIER_REGISTRATION', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      console.log('lll VIEW_SUPPLIER', data);
      this.viewSupplierList = data.SUPPLIER_REGISTRATION.HEADERS ? data.SUPPLIER_REGISTRATION.HEADERS.map(x=>(x.HEADER)) :[]
      this.AddressesAll = data.SUPPLIER_REGISTRATION.ADDRESSES ? data.SUPPLIER_REGISTRATION.ADDRESSES.map(x=>(x.ADDRESS)) :[]
      this.banksAll = data.SUPPLIER_REGISTRATION.BANKS ? data.SUPPLIER_REGISTRATION.BANKS.map(x=>(x.BANK)) :[]
      this.contactsAll = data.SUPPLIER_REGISTRATION.CONTACTS ? data.SUPPLIER_REGISTRATION.CONTACTS.map(x=>(x.CONTACT)) :[]

      this.Addresses = this.AddressesAll;
      this.banks = this.banksAll;
      this.contacts = this.contactsAll;

      this.viewSupplierList.forEach(
        (x, i) => {
          this.viewSupplierList[i].address = this.AddressesAll.filter(y => y.SUPPLIER_REGISTRATION_ID === x.SUPPLIER_REGISTRATION_ID);
          this.viewSupplierList[i].bank = this.banksAll.filter(y => y.SUPPLIER_REGISTRATION_ID === x.SUPPLIER_REGISTRATION_ID);
          this.viewSupplierList[i].contact = this.contactsAll.filter(y => y.SUPPLIER_REGISTRATION_ID === x.SUPPLIER_REGISTRATION_ID);
        }
      );
      console.log("viewSupplierList : ", this.viewSupplierList)
      // this.addressList  = data.SUPPLIER_REGISTRATION.ADDRESSES ? data.SUPPLIER_REGISTRATION.ADDRESSES.map(x=>(x.ADDRESS)) :[]
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
  viewData(supplier){
    console.log("element : ", supplier);

    // this.router.navigate(['/craftsmanautomation/administration/viewSupplier'], { queryParams: {  SUPPLIER_REGISTRATION_ID: supplier.SUPPLIER_REGISTRATION_ID } });
    this.viewFlag = true;
    this.headers= this.viewSupplierList.find(x => x.SUPPLIER_REGISTRATION_ID === supplier.SUPPLIER_REGISTRATION_ID)
    console.log("headers : ", this.headers)
  }
  goBack(){
    this.viewFlag = false;
  }

}
