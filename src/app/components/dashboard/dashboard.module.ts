import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ChartsModule,
    NgxSpinnerModule,
    NgCircleProgressModule.forRoot({
      backgroundStrokeWidth: 0,
      backgroundPadding: -50,
      radius: 72,
      space: -7,
      outerStrokeGradient: true,
      outerStrokeWidth: 6,
       outerStrokeColor: 'green',
      outerStrokeGradientStopColor: 'green',
      outerStrokeLinecap: 'square',
      innerStrokeColor: '#e7e8ea',
      innerStrokeWidth: 6,
      title: 'UI',
      showSubtitle: false,
      imageHeight: 70,
      imageWidth: 92,
      animateTitle: false,
      animationDuration: 1000,
      showUnits: true,
      showBackground: false,
      startFromZero: false
    }),
    SharedModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class DashboardModule { }
