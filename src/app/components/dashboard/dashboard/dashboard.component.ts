import { Component, OnInit, HostListener, ChangeDetectorRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label, MultiDataSet } from 'ng2-charts';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';

declare var jQuery: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
@HostListener('window:scroll')
export class DashboardComponent implements OnInit {

  /*line graph*/
  lineChartData: ChartDataSets[] = [
    {
      // data: [0, 4, 3, 8, 15, 20, 24, 14, 34, 4, 24, 33],
      data:[],
      label: 'Earnings',
      lineTension: 0.3,
      // backgroundColor: '#8bdeac',
      // borderColor: '#8bdeac',
      pointRadius: 3,
      pointBackgroundColor: '#dc5257',
      pointBorderColor: '#dc5257',
      pointHoverRadius: 3,
      pointHoverBackgroundColor: 'rgba(78, 115, 223, 1)',
      pointHoverBorderColor: 'rgba(78, 115, 223, 1)',
      pointHitRadius: 10,
      pointBorderWidth: 2,
    },
  ];

  lineChartLabels: Label[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  lineChartOptions = {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    tooltips: {
      backgroundColor: 'rgb(255,255,255)',
      bodyFontColor: '#858796',
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label(tooltipItem, data) {
          var value = data.datasets[0].data[tooltipItem.index];
          value = Number(value).toFixed(0);
          value = value.toString();
          value = value.split(/(?=(?:...)*$)/);
          value = value.join(',');
          return value;
        }
      }
    },
    responsive: true,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7,

        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          userCallback(value, index, values) {
            value = value.toString();
            value = value.split(/(?=(?:...)*$)/);
            value = value.join(',');
            return value;
          }
        },
        gridLines: {
          display: false
        }
      }]
    }
  };


  lineChartColors: Color[] = [
    {

      borderColor: '#8bdeac',
      backgroundColor: 'white',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';
  public email;

  /*line graph*/

  /*pie chart*/

  doughnutChartLabels: Label[] = ['Acknowledgement', 'Delivery'];
  doughnutChartData: MultiDataSet = [
    [350, 450]
  ];
  doughnutChartType: ChartType = 'doughnut';
  chartOptions = {
    responsive: true,
    cutoutPercentage: 70,
    radius: '50%'
  };
  // public doughnutColors: Color[] = [{
  //   backgroundColor: 'green',
  // }];


  /*pie chart*/

  public date = new Date();
  public number = 0;
  public percent;
  public poAcceptAll = [];
  public poAccept = [];
  public poCount = 0;
  public poCount1 = 0;
  public poApproveAll = [];
  public poApprove = [];
  public poCountApprove = 0;


  public leadTimeAll = [];
  public leadTime = [];
  public leadTimeCount = 0;


  public defectRateAll = [];
  public defectRate = [];
  public defectRateCount = 0;

  public pendingInoiceAll = [];
  public pendingInoice = [];
  public pendingInoiceCount = 0;

  public pendingForPaymentsAll = [];
  public pendingForPayments = [];
  public pendingForPaymentsCount = 0;

  public imdAttnReqAll = [];
  public imdAttnReq = [];
  public imdAttnReqCount = 0;

  public suppCommAll = [];
  public suppComm = [];
  public suppCommCount = 0;

  public graphAllData = [];


  public graphData = [];
  public graphModified = [];



  public recentPurchaseAll = [];
  public recentPurchase = [];


  public loader1 = false;
  public loader2 = false;
  public loader3 = false;
  public loader4 = false;

  public loader5 = false;
  public loader6 = false;
  public loader7 = false;
  public loader8 = false;




  public graphload = false;
  public lineload = false;
  public dataTableload = false;
  public showHide = true;

  public pendingAckAll = [];
  public pendingAck = [];
  public pendingAckCount;

  public pendingDel = [];
  public pendingDelAll = [];
  public pendingDelCount;

  public lineProDataAll = [];
  public lineProData = [];
  public showLineData = [];
  public maxProValue = 0;
  public selectedLineType = 'Month';
  public classes = ['bg-warning', 'bg-success', 'bg-danger', 'bg-info', 'bg-success']
  public flag=false;

  constructor(public breadCrumbServices: BreadcrumbService, private router: Router,
    private purchaseService: PurchaseService, public rsaService: RsaService, private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    if (this.breadCrumbServices.subsVar === undefined) {
      this.breadCrumbServices.selectValuesChanged.subscribe(() => {
        if (this.router.url === '/craftsmanautomation/dashboard') {
          this.setFilteredData()
        }
      });
    }
    this.getApproveDetails();
    this.getAcceptDetails();
    this.getInvPendingPayment();
    this.getPendingLeadTime();
    this.getPendingForInvoice();
    this.getPODefectRate();
    this.getGraphData();
    this.getProgressData();
    this.getRecentsPayments();
    this.getImmediateActions();
    this.getImmediateActions2();

  }

  onClick() {
    this.percent = Math.random() * 100;
  }

  getAcceptDetails() {
    let poCount1 = 0;
    this.loader1 = true;
    // const details = { email: this.email, purchaseType: 'DB_PO_ACCEPTANCE' };
    const details = this.purchaseService.inputJSON('DB_PO_ACCEPTANCE', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      if(data.DBPOACCEPTS){
     this.poAcceptAll= data? data.DBPOACCEPTS.map(x=>(x.DBPOACCEPT)) :[]
      // this.poAcceptAll = Array.isArray(data.DBPOACCEPTS.DBPOACCEPT) ? data.DBPOACCEPTS.DBPOACCEPT : (data.DBPOACCEPTS.DBPOACCEPT) ? [data.DBPOACCEPTS.DBPOACCEPT] : [];
      this.poAccept = this.setFilteredContent(this.poAcceptAll)
      this.poAccept.forEach(f => {
        poCount1 = poCount1 + Number(f.PENDING_PO_COUNT);
      });
       }else{
        poCount1 =0;
      }
      this.poCount1 = poCount1;
      this.loader1 = false;
    }, error => {
      // this.spinner.hide();
      console.log(error);
      this.loader1 = false;
    });
  }

  getApproveDetails() {
    let poCount = 0;
    this.loader2 = true;
    // const details = { email: this.email, purchaseType: 'DB_PO_UNAPPROVED' };
    const details = this.purchaseService.inputJSON('DB_PO_UNAPPROVED', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      if(data.DBPOUNAPPROVES){
      this.poApproveAll= data.DBPOUNAPPROVES.map(x=>(x.DBPOUNAPPROVE))
      // this.poApproveAll = Array.isArray(data.DBPOUNAPPROVES.DBPOUNAPPROVE) ? data.DBPOUNAPPROVES.DBPOUNAPPROVE : (data.DBPOUNAPPROVES.DBPOUNAPPROVE) ? [data.DBPOUNAPPROVES.DBPOUNAPPROVE] : [];
        this.poApprove = this.setFilteredContent(this.poApproveAll)
        this.poApprove.forEach(f => {
          poCount = poCount + Number(f.PENDING_PO_COUNT);
        });
      }else{
        poCount =0;
      }
      this.poCountApprove = poCount;
      this.loader2 = false;
    }, error => {
      // this.spinner.hide();
      this.loader2 = false;
      console.log(error);
    });
  }


  getInvPendingPayment() {
    let poCount = 0;
    this.loader6 = true;
    let noOfLines2 =0
    // const details = { email: this.email, purchaseType: 'DB_INV_PENDING_PAYMENT' };
    const details = this.purchaseService.inputJSON('DB_INV_PENDING_PAYMENT', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      if(data.DBPENDINGPAYMENTS){
      this.pendingForPaymentsAll = data.DBPENDINGPAYMENTS.map(x=>(x.DBPENDINGPAYMENT))
      // this.pendingForPaymentsAll = Array.isArray(data.DBPENDINGPAYMENTS.DBPENDINGPAYMENT) ? data.DBPENDINGPAYMENTS.DBPENDINGPAYMENT : (data.DBPENDINGPAYMENTS.DBPENDINGPAYMENT) ? [data.DBPENDINGPAYMENTS.DBPENDINGPAYMENT] : [];
      this.pendingForPayments = this.setFilteredContent(this.pendingForPaymentsAll);
    //   this.pendingForPayments.forEach(f => {
    //     if (poCount) {
    //       poCount = (poCount + Number(f.PENDING_PAYMENT_COUNT)) / 2;
    //     } else {
    //       poCount = (poCount + Number(f.PENDING_PAYMENT_COUNT));
    //     }
    //   });
    // }else{
    //   poCount =0;
    // }
    this.pendingForPayments.forEach(f => {
      if(f.PENDING_PAYMENT_COUNT){
        poCount = poCount + Number(f.PENDING_PAYMENT_COUNT)
      noOfLines2 = noOfLines2 +1
      }
    });
  }else{
    poCount =0
  }
   if(noOfLines2 ===0 ){
    poCount =0
    }else{
      poCount = Math.round(poCount / noOfLines2);
    }
      this.pendingForPaymentsCount = Number(poCount.toFixed(2));
      this.loader6 = false;
    }, error => {
      // this.spinner.hide();
      this.loader6 = false;
    });
  }

  getImmediateActions() {
    let poCount = 0;
    this.loader7 = true;
    // const details = { email: this.email, purchaseType: 'DB_PO_ACK_LAST_WEEK' };
    const details = this.purchaseService.inputJSON('DB_PO_ACK_LAST_WEEK', undefined, undefined)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
        if(data.DBPOACKS){
      // this.pendingAckAll = Array.isArray(data.DBPOACKS.DBPOACK) ? data.DBPOACKS.DBPOACK : (data.DBPOACKS.DBPOACK) ? [data.DBPOACKS.DBPOACK] : [];
      this.pendingAckAll = data.DBPOACKS ? data.DBPOACKS.map(x=>(x.DBPOACK)) : []
      this.pendingAck = this.setFilteredContent(this.pendingAckAll);
      this.pendingAck.forEach(f => {
        poCount = (poCount + Number(f.PENDING_FOR_ACK));
      });
    }else{
      this.pendingAckCount =0;
    }
      this.pendingAckCount = poCount;
      this.loader7 = false;
      this.setPieChartData();

    }, error => {
      // this.spinner.hide();
      console.log(error);
      this.loader7 = false;
    });
  }

  getImmediateActions2() {
    let poCount = 0;
    this.loader7 = true;
    // const details = { email: this.email, purchaseType: 'DB_PENDING_RECEIPT_LAST_WEEK' };
    const details = this.purchaseService.inputJSON('DB_PENDING_RECEIPT_LAST_WEEK', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.pendingDelAll = data.DBPENDINGDELIVERIES ? data.DBPENDINGDELIVERIES.map(x=>(x.DBPENDINGDELIVERY)) :[]
      // this.pendingDelAll = Array.isArray(data.DBPENDINGDELIVERIES.DBPENDINGDELIVERY) ? data.DBPENDINGDELIVERIES.DBPENDINGDELIVERY : (data.DBPENDINGDELIVERIES.DBPENDINGDELIVERY) ? [data.DBPENDINGDELIVERIES.DBPENDINGDELIVERY] : [];
      this.pendingDel = this.setFilteredContent(this.pendingDelAll);
      this.pendingDel.forEach(f => {
        poCount = (poCount + Number(f.PENDING_FOR_DELIVERY));
      });
      this.pendingDelCount = poCount;
      this.loader7 = false;
      this.setPieChartData();
    }, error => {
      // this.spinner.hide();
      console.log(error);
      this.loader7 = false;
    });
  }

  setPieChartData() {
    this.doughnutChartData = [[this.pendingAckCount, this.pendingDelCount]];
  }

  getPendingLeadTime() {
    let poCount = 0;
    this.loader3 = true;
    let noOfLines =  0
    // const details = { email: this.email, purchaseType: 'DB_PO_LEAD_TIME' };
    const details = this.purchaseService.inputJSON('DB_PO_LEAD_TIME', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log("lead time")
      if(data){
      // this.leadTimeAll = Array.isArray(data.DBPOLEADTIMES.DBPOLEADTIME) ? data.DBPOLEADTIMES.DBPOLEADTIME : (data.DBPOLEADTIMES.DBPOLEADTIME) ? [data.DBPOLEADTIMES.DBPOLEADTIME] : [];
      this.leadTimeAll = data.DBPOLEADTIMES? data.DBPOLEADTIMES.map(x=>(x.DBPOLEADTIME)) : []
      this.leadTime = this.setFilteredContent(this.leadTimeAll);
      this.leadTime.forEach(f => {
        if(f.LEAD_TIME){
        poCount = poCount + Number(f.LEAD_TIME)
        noOfLines = noOfLines +1
        }
        // if (poCount) {
        //   poCount = (poCount + Number(f.LEAD_TIME));
        // } else {
        //   poCount = poCount + Number(f.LEAD_TIME);
        // }
      });

    }else{
        poCount = 0;
      }
      if(noOfLines ===0 ){
        this.leadTimeCount =0
      }else{
        this.leadTimeCount = Math.round(poCount / noOfLines);
      }
    //  console.log("lead Time : ",  poCount, noOfLines, this.leadTimeCount)
      this.loader3 = false;
    }, error => {
      // this.spinner.hide();
      console.log(error);
      this.loader3 = false;
    });
  }

  getPendingForInvoice() {
    let poCount = 0;
    this.loader5 = true;
    let noOfLines =0
    // const details = { email: this.email, purchaseType: 'DB_PENDING_FOR_INVOICE' };
    const details = this.purchaseService.inputJSON('DB_PENDING_FOR_INVOICE', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      if(data){
      // this.pendingInoiceAll = Array.isArray(data.DBPENDINGINVOICES.DBPENDINGINVOICE) ? data.DBPENDINGINVOICES.DBPENDINGINVOICE : (data.DBPENDINGINVOICES.DBPENDINGINVOICE) ? [data.DBPENDINGINVOICES.DBPENDINGINVOICE] : [];
      this.pendingInoiceAll = data.DBPENDINGINVOICES ? data.DBPENDINGINVOICES.map(x=>(x.DBPENDINGINVOICE)) :[]
      this.pendingInoice = this.setFilteredContent(this.pendingInoiceAll);
    //   this.pendingInoice.forEach(f => {
    //     if (poCount) {
    //       poCount = (poCount + Number(f.PENDING_FOR_INVOICE)) / 2;
    //     } else {
    //       poCount = poCount + Number(f.PENDING_FOR_INVOICE);
    //     }
    //   });
    // }else{
    //   poCount = 0;
    // }
    this.pendingInoice.forEach(f => {
      if(f.PENDING_FOR_INVOICE){
      poCount = poCount + Number(f.PENDING_FOR_INVOICE)
      noOfLines = noOfLines +1
      }
    });
  }else{
      poCount = 0;
    }
    if(noOfLines ===0 ){
      this.poCount =0
    }else{
      this.poCount = Math.round(poCount / noOfLines);
    }
      this.pendingInoiceCount = Number(poCount.toFixed(2));
      this.loader5 = false;
    }, error => {
      // this.spinner.hide();
      console.log(error);
      this.loader5 = false;
    });
  }

  getPODefectRate() {
    let poCount = 0;
    this.loader4 = true;
    let noOfLines=0
    // const details = { email: this.email, purchaseType: 'DB_PO_DEFECT_RATE' };
    const details = this.purchaseService.inputJSON('DB_PO_DEFECT_RATE', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      if(data){
      // this.defectRateAll = Array.isArray(data.DBDEFECTRATES.DBDEFECTRATE) ? data.DBDEFECTRATES.DBDEFECTRATE : (data.DBDEFECTRATES.DBDEFECTRATE) ? [data.DBDEFECTRATES.DBDEFECTRATE] : [];
      this.defectRateAll = data.DBDEFECTRATES ? data.DBDEFECTRATES.map(x=>(x.DBDEFECTRATE)) :[]
      this.defectRate = this.setFilteredContent(this.defectRateAll);
    //   this.defectRate.forEach(f => {
    //     if (poCount) {
    //       poCount = (poCount + Number(f.DEFECT_RATE)) / 2;
    //     } else {
    //       poCount = poCount + Number(f.DEFECT_RATE);
    //     }
    //   });
    // }else{
    //   poCount =0;
    // }
    this.defectRate.forEach(f => {
      if(f.DEFECT_RATE){
      poCount = poCount + Number(f.DEFECT_RATE)
      noOfLines = noOfLines +1
      }
    });
  }else{
      poCount = 0;
    }
    if(noOfLines ===0 ){
      this.poCount =0
    }else{
      this.poCount = Math.round(poCount / noOfLines);
    }
      this.defectRateCount = Number(poCount.toFixed(2));
      this.loader4 = false;
    }, error => {
      // this.spinner.hide();
      console.log(error);
      this.loader4 = false;
    });

  }

  getGraphData() {
    this.graphload = true;
    // const details = { email: this.email, purchaseType: 'DB_EARNING_TREAD' };
    const details = this.purchaseService.inputJSON('DB_EARNING_TREAD', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log("DB_EARNING_TREAD : ", data)
      this.graphAllData = data.DBEARNINGTRENDS ? data.DBEARNINGTRENDS.map(x =>(x.DBEARNINGTREND)) :[]
      // this.graphAllData = Array.isArray(data.DBEARNINGTRENDS.DBEARNINGTREND) ? data.DBEARNINGTRENDS.DBEARNINGTREND : (data.DBEARNINGTRENDS.DBEARNINGTREND) ? [data.DBEARNINGTRENDS.DBEARNINGTREND] : [];
      this.graphData = JSON.parse(JSON.stringify(this.setFilteredContent(this.graphAllData.sort((a, b) => Number(a.PERIOD_NUM) - Number(b.PERIOD_NUM)))));
      // console.table(this.graphData);
      //  this.defectRate = this.defectRateAll;
      //  this.defectRate.forEach(f => {
      //    poCount = poCount + Number(f.DEFECT_RATE);
      //    });
      let arr = {};
      // console.log("this.graphData : ", this.graphData)
      // this.graphData = null
      if(!this.graphData){
         this.flag=false
      }
      else{
        this.flag=true

      this.graphData.forEach((f, i) => {
        if (arr[f.PERIOD_CODE]) {
          arr[f.PERIOD_CODE].AMOUNT_PAID = Number(arr[f.PERIOD_CODE].AMOUNT_PAID) + Number(f.AMOUNT_PAID)
        } else {
          arr[f.PERIOD_CODE] = f;
        }
        if (i === this.graphData.length - 1) {
          this.setGraphData(arr);
        }
      });
    }

      this.graphload = false;
    }, error => {
      // this.spinner.hide();
      console.log(error);
      this.graphload = false;
    });

  }
  setGraphData(arr) {
    this.lineChartData[0].data = (Object.entries(arr)).map((m: any) => m[1].AMOUNT_PAID);
    this.lineChartLabels = (Object.entries(arr)).map(m => m[0]);
  }
  getBagdeColor(value: string) {
    switch (value) {
      case 'Pending':
        return 'badge-danger';
        break;

      case 'Processing':
        return 'badge-info';
        break;

      case 'Delivered':
        return 'badge-success';
        break;

      case 'Shipping':
        return 'badge-warning';
        break;
    }
  }

  gotoTop() {
    // window.scroll({
    //   top: 0,
    //   left: 0,
    //   behavior: 'smooth'
    // });
    (function smoothscroll() {
      const currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 5));
      }
    })();
  }

  setFilteredData() {
    //1
    this.loader1 = true;
    let acceptCount = 0;
    this.poAccept = this.setFilteredContent(this.poAcceptAll)
    this.poAccept.forEach(f => {
      acceptCount = acceptCount + Number(f.PENDING_PO_COUNT);
    });
    this.poCount = acceptCount;
    this.loader1 = false;
    //2
    this.loader2 = true;
    let pendingAppCount = 0;
    this.poApprove = this.setFilteredContent(this.poApproveAll);
    this.poApprove.forEach(f => {
      pendingAppCount = pendingAppCount + Number(f.PENDING_PO_COUNT);
    });
    this.poCountApprove = pendingAppCount;
    this.loader2 = false;
    //3
    this.loader3 = true;
    let leadTime = 0;
    this.leadTime = this.setFilteredContent(this.leadTimeAll);
    this.leadTime.forEach(f => {
      leadTime = leadTime + Number(f.LEAD_TIME);
    });

    this.leadTimeCount = leadTime;
    this.loader3 = false;

    //4
    this.loader4 = true;
    let defectRate = 0;
    this.defectRate = this.setFilteredContent(this.defectRateAll);
    this.defectRate.forEach(f => {
      defectRate = defectRate + Number(f.DEFECT_RATE);
    });
    this.defectRateCount = defectRate;
    this.loader4 = false;
    //5
    this.loader5 = true;
    let pendingForInv = 0;
    let noOfLines1 = 0
    let noOfLines2 = 0
    this.pendingInoice = this.setFilteredContent(this.pendingInoiceAll);
    // this.pendingInoice.forEach(f => {
    //   if (pendingForInv) {
    //     pendingForInv = (pendingForInv + Number(f.PENDING_FOR_INVOICE)) / 2;
    //   } else {
    //     pendingForInv = (pendingForInv + Number(f.PENDING_FOR_INVOICE));
    //   }
    // });
    this.pendingInoice.forEach(f => {
      if(f.PENDING_FOR_INVOICE){
        pendingForInv = pendingForInv + Number(f.PENDING_FOR_INVOICE)
      noOfLines1 = noOfLines1 +1
      }
    });
   if(noOfLines1 ===0 ){
      pendingForInv =0
    }else{
      pendingForInv = Math.round(pendingForInv / noOfLines1);
    }

    this.pendingInoiceCount = Number(pendingForInv.toFixed(2));
    this.loader5 = false;
    //6
    this.loader6 = true;
    let pendingForPay = 0;
    this.pendingForPayments = this.setFilteredContent(this.pendingForPaymentsAll)
    // this.pendingForPayments.forEach(f => {
    //   if (pendingForPay) {
    //     pendingForPay = (pendingForPay + Number(f.PENDING_PAYMENT_COUNT)) / 2;
    //   } else {
    //     pendingForPay = (pendingForPay + Number(f.PENDING_PAYMENT_COUNT));
    //   }
    // });
    this.pendingForPayments.forEach(f => {
      if(f.PENDING_PAYMENT_COUNT){
        pendingForPay = pendingForPay + Number(f.PENDING_PAYMENT_COUNT)
      noOfLines2 = noOfLines2 +1
      }
    });
   if(noOfLines2 ===0 ){
      pendingForPay =0
    }else{
      pendingForPay = Math.round(pendingForPay / noOfLines2);
    }
    this.pendingForPaymentsCount = Number(pendingForPay.toFixed(2));
    this.loader6 = false;

    //pie chart data1
    this.loader7 = true;
    let pendingAck = 0;
    this.pendingAck = this.setFilteredContent(this.pendingAckAll);
    this.pendingAck.forEach(f => {
      pendingAck = pendingAck + Number(f.PENDING_FOR_ACK);
    });

    this.pendingAckCount = pendingAck;
    this.loader7 = false;
    this.setPieChartData();
    //pie chart data2
    this.loader7 = true;
    let pendingDel = 0;
    this.pendingDel = this.setFilteredContent(this.pendingDelAll);
    this.pendingDel.forEach(f => {
      pendingDel = pendingDel + Number(f.PENDING_FOR_DELIVERY);
    });
    this.pendingDelCount = pendingDel;
    this.loader7 = false;
    this.setPieChartData();
    //Graph 1
    this.graphload = true;
    this.graphData = JSON.parse(JSON.stringify(this.setFilteredContent(this.graphAllData.sort((a, b) => Number(a.PERIOD_NUM) - Number(b.PERIOD_NUM)))));
    let arr = {};
    this.graphData.forEach(async (f, i) => {
      if (await arr[f.PERIOD_CODE]) {
        arr[f.PERIOD_CODE].AMOUNT_PAID = await Number(arr[f.PERIOD_CODE].AMOUNT_PAID) + Number(f.AMOUNT_PAID)
      } else {
        arr[f.PERIOD_CODE] = await JSON.parse(JSON.stringify(f));
      }
      if (i === this.graphData.length - 1) {
        this.setGraphData(arr);
      }
    });
    this.graphload = false;
    //Line Data
    this.lineload = true;
    let lineData = [];
    this.lineProData = this.setFilteredContent(this.lineProDataAll)
    this.selectLineType(this.selectedLineType);
    //Data Table
    this.showHide = false;
    setTimeout(() => {
      this.showHide = true;
      this.recentPurchase = this.setFilteredContent(this.recentPurchaseAll);
      this.setAsDataTableList();
    }, 10);
  }

  setFilteredContent(data) {
    return data.filter(f => {
      let a = true;
      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;
      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }
      // if (this.fromDate) {
      //   a = a && this.fromDate < f.PO_DATE;
      // }

      // if (this.toDate) {
      //   a = a && this.toDate > f.PO_DATE;
      // }
      return a;
    });
  }

  getProgressData() {
    this.lineload = true;
    // const details = { email: this.email, purchaseType: 'DB_PRODUCT_SOLD' };
    const details = this.purchaseService.inputJSON('DB_PRODUCT_SOLD', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.lineProDataAll = data.DBPRODUCTSALES? data.DBPRODUCTSALES.map(x =>(x.DBPRODUCTSALE)) :[]
      // this.lineProDataAll = Array.isArray(data.DBPRODUCTSALES.DBPRODUCTSALE) ? data.DBPRODUCTSALES.DBPRODUCTSALE : (data.DBPRODUCTSALES.DBPRODUCTSALE) ? [data.DBPRODUCTSALES.DBPRODUCTSALE] : [];
      this.lineProData = this.setFilteredContent(this.lineProDataAll);
      this.selectLineType(this.selectedLineType);
    }, err => {
      console.log(err);
      this.lineload = false;
    })
  }

  getRecentsPayments() {
    // const details = { email: this.email, purchaseType: 'DB_RECENT_PAYMENTS' };
    const details = this.purchaseService.inputJSON('DB_RECENT_PAYMENTS', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log("recent payment : ", data)
      this.recentPurchaseAll = data.DBRECENTPAYMENTS ? data.DBRECENTPAYMENTS.map(x =>(x.DBRECENTPAYMENT)) :[]
      // this.recentPurchaseAll = Array.isArray(data.DBRECENTPAYMENTS.DBRECENTPAYMENT) ? data.DBRECENTPAYMENTS.DBRECENTPAYMENT : (data.DBRECENTPAYMENTS.DBRECENTPAYMENT) ? [data.DBRECENTPAYMENTS.DBRECENTPAYMENT] : [];
      this.recentPurchase = this.recentPurchaseAll;
      this.recentPurchase.sort((a, b) =>a.PAYMENT_DATE < b.PAYMENT_DATE ? -1 : a.PAYMENT_DATE > b.PAYMENT_DATE ? 1 : 0);

      this.showHide = false;
      setTimeout(() => {
        this.showHide = true;
        this.setAsDataTableList2();
      }, 10);
    }, err => {
      console.log(err)
    })
  }

  setAsDataTableList2() {
    this.chRef.detectChanges();
    (function ($) {
      $('#recent').DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        pageLength: 5,
        searching: false,
      });
    })(jQuery);
  }

  setAsDataTableList() {
    this.chRef.detectChanges();
    (function ($) {
      $('#recent').DataTable({
        responsive: true,
        bLengthChange: false,
        retrieve: true,
        pageLength: 5,
        searching: false,
      });
    })(jQuery);
  }

  selectLineType(type) {
    this.lineload = true;
    this.selectedLineType = type;
    let showData = []
    let maxValue = 0;
    if (this.selectedLineType === 'Year') {
      showData = ([...this.lineProData, {
        BUYER_UNIT: "YTA OU",
        DAY_SALE: "0",
        ITEM: "GE70004000121",
        MONTH_SALE: "0",
        ORGANIZATION: "craftsman Automation Limited",
        VENDOR_ID: "87228",
        VENDOR_SITE_ID: "86387",
        WEEK_SALE: "0",
        YEAR_SALE: "10"
      }].sort((a, b) => { return Number(b.YEAR_SALE) - Number(a.YEAR_SALE) })).slice(0, 5);
      showData = showData.filter(f => {
        if (maxValue < Number(f.YEAR_SALE)) {
          maxValue = Number(f.YEAR_SALE);
        }
        return (Number(f.YEAR_SALE) > 0)
      });
    }


    if (this.selectedLineType === 'Month') {
      showData = (this.lineProData.sort((a, b) => { return Number(b.MONTH_SALE) - Number(a.MONTH_SALE) })).slice(0, 5)
      showData = showData.filter(f => {
        if (maxValue < Number(f.MONTH_SALE)) {
          maxValue = Number(f.MONTH_SALE);
        }
        return (Number(f.MONTH_SALE) > 0)
      });
    }
    if (this.selectedLineType === 'Week') {

      showData = (this.lineProData.sort((a, b) => { return Number(b.WEEK_SALE) - Number(a.WEEK_SALE) })).slice(0, 5)  //  points.sort(function(a, b){return a - b});
      showData = showData.filter(f => {
        if (maxValue < Number(f.WEEK_SALE)) {
          maxValue = Number(f.WEEK_SALE);
        }
        return (Number(f.WEEK_SALE) > 0)
      });
    }
    if (this.selectedLineType === 'Today') {
      showData = (this.lineProData.sort((a, b) => { return Number(b.DAY_SALE) - Number(a.DAY_SALE) })).slice(0, 5)  //  points.sort(function(a, b){return a - b});
      showData = showData.filter(f => {
        if (maxValue < Number(f.DAY_SALE)) {
          maxValue = Number(f.DAY_SALE);
        }
        return (Number(f.DAY_SALE) > 0)
      });
    }
    this.showLineData = showData;
    this.maxProValue = maxValue;
    this.lineload = false;
  }


}
