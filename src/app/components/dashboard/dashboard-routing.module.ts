import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/shared/service/auth-guard.service';
import { DashboardComponent} from './dashboard/dashboard.component';


const routes: Routes = [
  //  canActivate: [AuthGuardService]
  { path: '', component: DashboardComponent,
  data: {
    title: 'My Infolets',
    breadcrumb: 'My Infolets'
  } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
