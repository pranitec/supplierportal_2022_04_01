import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from "@angular/core";
import { Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
import { NgxSpinnerService } from "ngx-spinner";
import { BreadcrumbService } from "src/app/shared/service/breadcrumb.service";
import { RsaService } from "src/app/shared/service/rsa.service";
import { LoginService } from "../../auth/service/login.service";
import { PurchaseService } from "../../purchase/service/purchase.service";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { EmailService } from "../../auth/service/email.service";
import { saveAs } from "file-saver";
import { ParseTreeResult } from "@angular/compiler";
import * as moment from 'moment';
import { endianness } from "os";
import { AbstractControl, Form, FormControl, FormGroup, Validators } from "@angular/forms";
import { profileData } from 'src/app/config/config'
import { environment } from "src/environments/environment";

// import * as jsPDF from 'jspdf';
// import 'jspdf-autotable';
declare var jQuery: any;
pdfMake.vfs = pdfFonts.pdfMake.vfs;
pdfMake.fonts = {
  tamilfont: {
    normal:
      "https://cdn.jsdelivr.net/gh/Ganesan-Smart/tamil@main/HindMadurai-Regular.ttf",
    bold: "https://cdn.jsdelivr.net/gh/Ganesan-Smart/tamil@main/HindMadurai-Bold.ttf",
    italics:
      "https://cdn.jsdelivr.net/gh/Ganesan-Smart/tamil@main/HindMadurai-Light.ttf",
    bolditalics:
      "https://cdn.jsdelivr.net/gh/Ganesan-Smart/tamil@main/HindMadurai-Bold.ttf",
  },
  Roboto: {
    normal:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Regular.ttf",
    bold: "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Medium.ttf",
    italics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Italic.ttf",
    bolditalics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-MediumItalic.ttf",
  },
};
const fonts = {
  tamilfont: {
    normal:
      "https://cdn.jsdelivr.net/gh/Ganesan-Smart/tamil@main/HindMadurai-Regular.ttf",
    bold: "https://cdn.jsdelivr.net/gh/Ganesan-Smart/tamil@main/HindMadurai-Bold.ttf",
    italics:
      "https://cdn.jsdelivr.net/gh/Ganesan-Smart/tamil@main/HindMadurai-Light.ttf",
    bolditalics:
      "https://cdn.jsdelivr.net/gh/Ganesan-Smart/tamil@main/HindMadurai-Bold.ttf",
  },
  Roboto: {
    normal:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Regular.ttf",
    bold: "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Medium.ttf",
    italics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Italic.ttf",
    bolditalics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-MediumItalic.ttf",
  },
};

@Component({
  selector: "app-create-asn",
  templateUrl: "./create-asn.component.html",
  styleUrls: ["./create-asn.component.css"],
})
export class CreateAsnComponent implements OnInit {
  public email = "";

  public poHeaderAll = [];
  public poLineAll = [];
  public poHeader = [];
  public poLine = [];

  public uniquePOs = [];
  public selectedPO = "";
  public currPOlines = [];
  public selectedPOLines = [];
  public frightList = [];

  public fileName = "";
  public bodyFontSize = 10;

  fileData: File = null;
  fileData1 : File =null

  public lineClicked: any = {};

  public lotsEntered = [];

  public asnHeaders: any = {};

  public asnTransporter: any[] = [];
  public transporters: any = {};

  // private fieldArray: Array<any> = [];
  // private newAttribute: any = {};

  public asnLines: any[] = [];

  public asnLots: any[] = [];

  public showlot = false;

  public currentLotTotal = 0;

  public addLotButton = false;

  public allLotList = [];
  // public transporter = [];
  public edit = false;

  public showHide = true;

  public beforeUpdateAsnLine = [];

  public beforeUpdateAsnLots = [];

  public supplierName = "";

  public asn_id = "";
  public fileInput1;
  public enableEdit = false;
  public enableAdd = false;
  public enableEditindex = null;
  public finalSubmitButton = false;
  public filenameWithoutno =[];
  public filenameWithno =[];
  public filenameWithoutno1 =[];
  public filenameWithno1 =[];
  public action= false;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst;
  public totalTaxAmout =0;
  public today=new Date();
  public lotQty = 0;
  public lotbutton =true;
  public mobileNo =[];
  public enterqty=0;
  public lineNo=''
  public totalAsnquantity =0;
  public totalenertedASNquantity =0;
  public ORG_NAME=''
  asnQuantityForm: FormGroup;
  numRegex = /^-?\d*[.,]?\d{0,3}$/;
  asnQuantityForm1: FormGroup;
  numRegex1 = /^[0-9]+(\.?[0-9]+)?$/

  private notifier: NotifierService;
  urls = new Array<string>();
  showHide1 = true;

  @ViewChild("myFileInput", { static: false }) myInputVariable: ElementRef;
  @ViewChild("myFileInput1", { static: false }) myInputVariable1: ElementRef;
  @ViewChild("closebutton") closebutton;
  @ViewChild("closebutton1") closebutton1;

  constructor(
    private purchaseService: PurchaseService,
    private chRef: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private breadCrumbServices: BreadcrumbService,
    private router: Router,
    private rsaService: RsaService,
    private notifierService: NotifierService,
    private loginService: LoginService,
    public emailService: EmailService,
  ) {
    this.notifier = notifierService;
    // this.asnQuantityForm = new FormGroup({
    //   shipmentQuantity: new FormControl('', [Validators.required, Validators.pattern(this.numRegex)])
    // });
    // this.asnQuantityForm1 = new FormGroup({
    //   shipmentQuantity1: new FormControl('', [Validators.required, Validators.pattern(this.numRegex1)])
    // });
  }

  ngOnInit(): void {
    this.supplierName =
      this.breadCrumbServices.filteredSelection[0].VENDOR_NAME;
    this.email = this.rsaService.decrypt(localStorage.getItem("3"));
    this.getPendingPurchase();

    let setOrderDate = (date) => {
      this.asnHeaders.DELIVERY_ORDER_DATE = date;
    };

    let setExpectDate = (date) => {
      this.asnHeaders.EXPECTED_RECEIPT_DATE = date;
    };
    let setLRDate = (date) => {
      this.transporters.LR_DATE = date;
    };
    (function ($, setOrderDate, setExpectDate, setLRDate) {
      $(document).ready(function () {
        var date1 = new Date();
        date1.setDate(date1.getDate());
        //   $('.js-example-basic-single').select2({

        //   });

        //   $('#documentNo').on('select2:select', function (e) {
        //     var data = e.params.data;
        //     console.log(data.text);
        // });

        $(".asnlot").hide();

        $("#datepicker1").datepicker({
          // daysOfWeekDisabled: [0],
          uiLibrary: "bootstrap4",
          format: "dd-mmm-yyyy",
          // locale: 'pt-br',
          maxDate: date1,
          // endDate : this.today,
          change(e) {
            setOrderDate(e.target.value);
          },
        });

        $("#datepicker2").datepicker({
          uiLibrary: "bootstrap4",
          format: "dd-mmm-yyyy",
          change(e) {
            setExpectDate(e.target.value);
          },
        });

        $("#datepicker3").datepicker({
          uiLibrary: "bootstrap4",
          format: "dd-mmm-yyyy",
          change(e) {
            setLRDate(e.target.value);
          },
        });

        // $(".datepicker").datepicker();

        $("#test").click(function () {
          //console.log('Called');

          $(".asnlot").toggle();
        });

        $("#test1").click(function () {
          //console.log('Called');

          $(".asnlot").toggle();
        });
      });
    })(jQuery, setOrderDate, setExpectDate, setLRDate);

  }

  getPendingPurchase() {
    this.spinner.show();
    // const details = { email: this.email, purchaseType: "ASN_PENDING_PO" };
    const details = this.purchaseService.inputJSON('ASN_PENDING_PO', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe(
      (data: any) => {
        // console.log('ppp', data);
        this.spinner.hide();
        this.getAllFreightTerms();
        this.poHeaderAll = data ? data.PENDINGPO.POHEADERS.map((x) => x.POHEADER): [];
        this.poLineAll = data ? data.PENDINGPO.POLINES.map((x) => x.POLINE) : [];
        // this.poHeaderAll = Array.isArray(data.PENDINGPO.POHEADERS.POHEADER) ? data.PENDINGPO.POHEADERS.POHEADER : (data.PENDINGPO.POHEADERS.POHEADER) ? [data.PENDINGPO.POHEADERS.POHEADER] : [];
        // this.poLineAll = Array.isArray(data.PENDINGPO.POLINES.POLINE) ? data.PENDINGPO.POLINES.POLINE : (data.PENDINGPO.POLINES.POLINE) ? [(data.PENDINGPO.POLINES.POLINE)] : [];
        this.poHeader = this.poHeaderAll;
        this.poLine = this.poLineAll;
        this.poHeader.forEach((x, i) => {
          this.poHeader[i].filteredList = this.poLine.filter(
            (y) => y.PO_HEADER_ID === x.PO_HEADER_ID
          );
          this.poHeader[i].filteredList = this.poHeader[i].filteredList.map(
            (y) => {
              y.BUYER_EMAIL = this.poHeader[i].BUYER_EMAIL;
              y.BUYER_PHONE_NUM = this.poHeader[i].BUYER_PHONE_NUM
              return y;
            }
          );
        });

        this.uniquePOs = this.poHeader.map((m) => m.PO_NO);
        this.uniquePOs.sort((a,b) => 0 - (a > b ? -1 : 1));
      },
      (err) => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  setLinesList() {
    let line = this.poHeader.find((f) => f.PO_NO === this.selectedPO);
    this.ORG_NAME = line.BUYER_UNIT
    this.currPOlines = line.filteredList ? line.filteredList : [];
    // console.log("lne ddd : ", this.currPOlines)
    // this.enterqty = this.currPOlines.
    this.mobileNo.push(line.BUYER_PHONE_NUM);
  }

setTwoNumberDecimal(el) {
    el.value = parseFloat(el.value).toFixed(2);
    console.log("setTwoNumberDecimal : ", el.value)
}

// maxLength(item) {
//   let N = item.DECIMAL_ALLOWED_FLAG;
//   console.log("dddd :",item.shipmentQuantity, N)

// if (N &&item.shipmentQuantity.toString().split('.')[1].length === N) {
//       return false;
//     }
// }

  checkQuantityAndUpdate(i) {
    if (Number(this.currPOlines[i].shipmentQuantity) > Number(this.currPOlines[i].ORDERED_QTY) -
        Number(this.currPOlines[i].RECEIVED_QTY)) {
          this.notifier.notify("warning", "You have entered ASN quantity with tolerance");
        }

    if (Number(this.currPOlines[i].shipmentQuantity) > Number(this.currPOlines[i].BALANCE_QTY_WITH_TOL))
    {
      this.notifier.notify("error", "Shipment Qty Exceeds Order Qty");
      this.currPOlines[i].shipmentQuantity = 0;
    }

  }

  addLotClicked(i, lineNo) {
    this.showlot = true;
    this.lineNo =lineNo
    this.lineClicked = JSON.parse(JSON.stringify(this.currPOlines[i]));
    //  this.lotsEntered = this.asnLots.filter(f=>f.LINE_LOCATION_ID ===this.currPOlines[i].LINE_LOCATION_ID )

    this.lotsEntered = this.allLotList.filter(
      (f) => f.LINE_LOCATION_ID === this.currPOlines[i].LINE_LOCATION_ID
    );
    this.calculateTotal();
    this.setDatePicker();
  }

  checkValue(value, index) {
    if (!value) {
      this.currPOlines[index].shipmentQuantity = undefined;
      this.showlot = false;
      this.lotsEntered = this.lotsEntered.filter(
        (f) => f.LINE_LOCATION_ID !== this.lotsEntered[index].LINE_LOCATION_ID
      );
      this.allLotList = this.allLotList.filter(
        (f) => f.LINE_LOCATION_ID !== this.lotsEntered[index].LINE_LOCATION_ID
      );
      this.setDatePicker();
    } else {
      this.currPOlines[index].shipmentQuantity =
        Number(this.currPOlines[index].ORDERED_QTY) -
        Number(this.currPOlines[index].RECEIVED_QTY);
        // this.lotQty = this.currPOlines[index].shipmentQuantity
        this.enterqty = this.currPOlines[index].shipmentQuantity
        // console.log("check val", this.lotQty);

    }
  }

  addLotButtonClicked() {
    this.allLotList.push({
      LINE_LOCATION_ID: this.lineClicked.LINE_LOCATION_ID,
      PO_NO: this.selectedPO,
    });

    this.lotsEntered.push(this.allLotList[this.allLotList.length - 1]);
    this.setDatePicker();
    this.calculateTotal();
  }

  changedLotQuantity(i) {
    let lotquantity = 0;

    this.lotsEntered.forEach((f) => {
      lotquantity = lotquantity + Number(f.QUANTITY);
    });
    // console.log("this.lotesentered : ", lotquantity, this.lineClicked.shipmentQuantity)
    if (lotquantity > this.lineClicked.shipmentQuantity) {
      this.notifier.notify("error", "Lot Qunatity Exceeds Shipment Quantity");

      this.lotsEntered[i].QUANTITY = 0;
    }
    this.lotbutton = true;
    this.calculateTotal();
  }

  calculateTotal() {
    let lotquantity = 0;

    this.lotsEntered.forEach((f) => {
      lotquantity = lotquantity + Number(f.QUANTITY);
    });

    this.currentLotTotal = lotquantity;

    if (lotquantity >= this.lineClicked.shipmentQuantity) {
      this.addLotButton = true;
    } else {
      this.addLotButton = false;
    }
  }

  AddButtonClicked() {
    // console.log("clicked ", this.asnLines, this.asnLots, this.currPOlines)
    this.totalTaxAmout =0;
    if (!this.edit) {
      let linesSelected = [];
      let selectedLineIds = [];
      let filedLot = [];
      let check=false;
      let check1=false;
      let check2=false;
let check3 = false;
      // if(this.currentLotTotal !== this.lotQty){
      //   this.notifier.notify("error", "mismatch");
      //   this.lotbutton = false;
      // }else{

      linesSelected = this.currPOlines.filter(

        (f) => f.selected && Number(f.shipmentQuantity) > 0
      );
      selectedLineIds = linesSelected.map((m) => m.LINE_LOCATION_ID);

      filedLot = this.allLotList.filter((f) =>
        selectedLineIds.includes(f.LINE_LOCATION_ID)
      );
      // console.log("linesSelected ", linesSelected, selectedLineIds, filedLot)
      // console.log("this.currentLotTotal : ", this.currentLotTotal, this.lineClicked.shipmentQuantity)

      filedLot.forEach(x=>{
        if(new Date(x.MFG_DATE) >new Date(this.asnHeaders.DELIVERY_ORDER_DATE)){
         check=true;
        }
        if(new Date(x.MFG_DATE) >= new Date(x.EXPIRATION_DATE)){
          check2 = true;
        }
        if(Number(this.currentLotTotal) !== Number(this.lineClicked.shipmentQuantity)){
        check1 = true;
        }
        if(this.currentLotTotal === this.lineClicked.shipmentQuantity){
          check1=false;
        }
        if(isNaN(x.this.lineClicked.shipmentQuantity)){
          check3 =true;
        }
      })
      if(check){
        this.notifier.notify("warning","Manufacturing date should not exceed Invoice date ("+this.asnHeaders.DELIVERY_ORDER_DATE+")");
        return false;
      }
      if(check2){
        this.notifier.notify("warning"," Expiry date should be higher than Manufacturing date");
        return false;
      }
      if(check1){
        this.notifier.notify("warning","ASN Quantity mismatch");
        check1=false
        return false;
      }
      // console.log("this.asnLines.length : ", this.asnLines)
      if(this.asnLines.length === 0){
        this.asnLines.push(...linesSelected);
        this.asnLots.push(...filedLot);
      }else{
        linesSelected.forEach(y=>{
          let s =this.asnLines.filter(x => x.LINE_LOCATION_ID !=y.LINE_LOCATION_ID)
          this.asnLines = [...s,y]
      })
      filedLot.forEach(y=>{
        let s =this.asnLots.filter(x => x.LINE_LOCATION_ID !=y.LINE_LOCATION_ID)
        this.asnLots = [...s,y]
    })
      // this.asnLots.forEach(x=>{
      //   console.log("x : ", x)
      //   filedLot.forEach(y=>{
      //     console.log("y : ", y)
      //     console.log("loca di : ", x.LINE_LOCATION_ID, y.LINE_LOCATION_ID)
      //     if(x.LINE_LOCATION_ID != y.LINE_LOCATION_ID){
      //       console.log("loca di : ")
      //       this.asnLots.push(y);
      //       // this.asnLots.push(...filedLot);
      //     }
      //     else{
      //       // this.asnLines= this.asnLines;
      //       this.asnLots = this.asnLots
      //     }
      //   })
      // })
    }
      // this.asnLines.push(...linesSelected);
      // this.asnLots.push(...filedLot);
      this.asnLines.forEach(x=>{
        this.totalTaxAmout = this.totalTaxAmout + (x.shipmentQuantity/x.ORDERED_QTY)*x.ASN_TAX_AMOUNT
      });
      this.lotbutton = true;
      // console.log("dd  : ", this.asnLines, this.asnLots, this.totalTaxAmout)
    // }
    //    console.log("this.lotQty , this.currentLotTotal", this.lotQty, this.currentLotTotal)

    }
    else {
      let check =false;
      let check1 =false;
      let check2 =false;
      let shipmentqty=0;
      // this.edit= false;
      let selectedLineIds = [];
      let linesSelected = this.currPOlines.filter(f => f.selected && Number(f.shipmentQuantity) > 0);
      this.asnLines = this.asnLines.filter(f => f.selected && Number(f.shipmentQuantity) > 0);
      linesSelected .forEach((a)=>{
        let f = this.asnLines.find(x=>a.LINE_LOCATION_ID === x.LINE_LOCATION_ID);
        let i = this.asnLines.findIndex(x=>a.LINE_LOCATION_ID === x.LINE_LOCATION_ID);
        if(f){
          this.asnLines[i].shipmentQuantity = a.shipmentQuantity;
          // this.enterqty =Number(a.shipmentQuantity)
          this.totalTaxAmout = this.totalTaxAmout + (f.shipmentQuantity/f.ORDERED_QTY)*f.ASN_TAX_AMOUNT
        }
        else{
          // shipmentqty =this.lotsEntered
          // shipmentqty =this.enterqty
          this.asnLines.push(a)
        }
      })
      selectedLineIds = this.asnLines.map(m => m.LINE_LOCATION_ID);
      this.asnLots = this.allLotList.filter(f => selectedLineIds.includes(f.LINE_LOCATION_ID));

      // console.log("this.asnLines", this.asnLines)
      // this.asnLines.forEach(x =>{
      //   this.totalAsnquantity = this.totalAsnquantity + Number(x.shipmentQuantity)
      // })
      // console.log("this.totalAsnquantity", this.totalAsnquantity)
      this.asnLots.forEach(x=>{
        if(new Date(x.MFG_DATE) >new Date(this.asnHeaders.DELIVERY_ORDER_DATE)){
         check=true;
        }
        // console.log("edit currenttot ", this.currentLotTotal , shipmentqty, this.enterqty)
        if(Number(this.currentLotTotal) != Number(this.enterqty)){
          check1 = true;
        }
        if(new Date(x.MFG_DATE) >= new Date(x.EXPIRATION_DATE)){
          check2 = true;
        }
      })
      if(check){
        this.notifier.notify("warning","Manufacturing date should not exceed Invoice date ("+this.asnHeaders.DELIVERY_ORDER_DATE+")");
        return false;
      }
      if(check1){
        this.notifier.notify("warning","ASN Quantity mismatch");
        return false;
      }
      if(check2){
        this.notifier.notify("warning"," Expirate date should be higher than Manufacturing date");
        return false;
      }
    }

    // linesSelected.forEach(y=>{
    //   let s =this.asnLines.filter(x => x.LINE_LOCATION_ID !=y.LINE_LOCATION_ID)
    //   console.log("y : ", y)
    //   this.asnLines = [...s,y]
    // });
    // console.log("clicked ", this.asnLines, this.asnLots)
    this.closebutton1.nativeElement.click();
  }

  number(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  checkmfgdate(){
    this.lotbutton = true;
  }

  editLine(item) {
    this.selectedPO = item.DOCUMENT_NO;
    this.edit = true;
    this.showlot = false;
    this.beforeUpdateAsnLine = JSON.parse(JSON.stringify(this.asnLines));
    this.beforeUpdateAsnLots = JSON.parse(JSON.stringify(this.asnLots));
    this.setLinesList();
  }

  deleteLine(item, j) {
    this.totalTaxAmout =0
    // console.log("item", item, this.currPOlines)
    // let tempCurrentPo = this.currPOlines
    let tempCurrentPo = this.currPOlines.find((f) => {
      if(f.LINE_LOCATION_ID === item.LINE_LOCATION_ID){
        f.selected = false;
        f.shipmentQuantity = 0;
      }
      // this.currPOlines =tempCurrentPo
    });
    // this.asnLines.splice(j, 1);
    this.asnLines = this.asnLines.filter(
      (f) => f.LINE_LOCATION_ID !== item.LINE_LOCATION_ID
    );
    // this.asnLines.splice(j, 1);
    this.asnLots = this.asnLots.filter(
      (f) => f.LINE_LOCATION_ID !== item.LINE_LOCATION_ID
    );
    this.asnLines.forEach(x=>{
      this.totalTaxAmout = this.totalTaxAmout + (x.shipmentQuantity/x.ORDERED_QTY)*x.ASN_TAX_AMOUNT
    });
  }

  setDatePicker() {
    //this.showHide = false;
    let setDate1 = (i, value) => {
      //  console.log('CAlled Date 1',i,value)
      this.lotsEntered[i].MFG_DATE = value;
    };

    let setDate2 = (i, value) => {
      this.lotsEntered[i].EXPIRATION_DATE = value;
    };
    this.chRef.detectChanges();
    this.lotsEntered.forEach((f, i) => {
      (function ($, i, setDate1, setDate2) {
        //     console.log(i,"Called");
        $("#mfgDate" + i).datepicker({
          uiLibrary: "bootstrap4",
          format: "dd-mmm-yyyy",
          value: f.MFG_DATE,
          change(e) {
            setDate1(i, e.target.value);
          },
        });
        $("#expDate" + i).datepicker({
          uiLibrary: "bootstrap4",
          format: "dd-mmm-yyyy",
          value: f.EXPIRATION_DATE,
          change(e) {
            setDate2(i, e.target.value);
          },
        });
      })(jQuery, i, setDate1, setDate2);
    });
  }

  AddLineCalled() {
    this.selectedPO = "";
    this.currPOlines = [];
    this.lotsEntered = [];
    this.showlot = false;
    this.edit = false;
    // var baseamount= this.asnLines.Or
    // if(this.asnHeaders.BASE_AMOUNT ===  this.asnLines.ORDERED_QTY *this.asnLines.PRICE){
    //   this.notifier.notify("warning", "Base Amount doesn't match with price");
    // }
    this.beforeUpdateAsnLine = JSON.parse(JSON.stringify(this.asnLines));
    this.beforeUpdateAsnLots = JSON.parse(JSON.stringify(this.asnLots));
  }

  onModelClose() {
    this.asnLines = this.beforeUpdateAsnLine;
    this.asnLots = this.beforeUpdateAsnLots;
  }

  submitPage() {
    var mobileNo;
    // console.log("submitPage ",this.asnHeaders, this.asnLines, this.asnLots, this.email, this.asnTransporter);
    // console.log("totalTaxAmout : ", this.totalTaxAmout)
    let emailList = [];
    let uniqueEmailList = [];
    emailList = this.asnLines.map((m) => m.BUYER_EMAIL);
    // mobileNo =  Array.from(new Set(this.asnLines.map((m) => m.BUYER_PHONE_NUM)));
    // console.log("mobile no : ", mobileNo)
    mobileNo =  this.mobileNo.toString();
    // console.log("mobile no : ", mobileNo)
    uniqueEmailList = Array.from(new Set(emailList));
    var tot = 0;
    let lineCount =0;
    var totalamt =
      Number(this.asnHeaders.BASE_AMOUNT) +
      Number(this.asnHeaders.TAX_AMOUNT) +
      Number(this.asnHeaders.OTHER_AMOUNT);
    this.asnLines.forEach((x) => {
      tot = tot + Number(x.shipmentQuantity) * Number(x.PRICE);
      lineCount = lineCount+1;
    });
    tot =Number(tot.toFixed(2))
    this.totalTaxAmout = Number(this.totalTaxAmout.toFixed(2))
    totalamt = Number(totalamt.toFixed(2));
    // console.log("tot value : ", this.asnHeaders.TAX_AMOUNT,  this.totalTaxAmout)

    const days = environment.asnDays || 90;
    var dateOffset = (24 * 60 * 60 * 1000) * days;
    var pastDate = new Date();
    pastDate.setTime(pastDate.getTime() - dateOffset);
    if (new Date(this.asnHeaders.DELIVERY_ORDER_DATE) < pastDate) {
      this.finalSubmitButton = true;
      this.notifier.notify("error", `Invoice Date should not be earlier than ${days} days`);
    }
    else if(new Date(this.asnHeaders.DELIVERY_ORDER_DATE)>new Date(this.asnHeaders.EXPECTED_RECEIPT_DATE)){
      this.finalSubmitButton = true;
      this.notifier.notify("error", "Expected receipt Date should be greater than or equal to Invoice Date");
    }
    else if (Number(this.asnHeaders.BASE_AMOUNT) != tot) {
      this.finalSubmitButton = true;
      this.notifier.notify("warning", " Value Mismatch between ASN line value ("+tot+ ") and Base Amount");
    } else if(Number(this.asnHeaders.TAX_AMOUNT) != this.totalTaxAmout){
      this.finalSubmitButton = true;
      this.notifier.notify("warning", "TAX Amount ("+this.totalTaxAmout+") mismatch");
    } else if (Number(this.asnHeaders.TOTAL_VALUE) != totalamt) {
      this.finalSubmitButton = true;
      this.notifier.notify("warning", "ASN Total Value  ("+totalamt+") mismatch ");
    }
    // else if(this.asnHeaders.TOTAL_VALUE<=0){
    //   this.finalSubmitButton = true;
    //   this.notifier.notify("warning", "Please Check the ASN Amount !");
    // }
    else if(lineCount<=0){
        this.finalSubmitButton = true;
        this.notifier.notify("warning", "ASN doesnot have line(s)!");
      }
    else if(this.asnHeaders.FILE_ATTACHMENTS.length <=0){
      this.finalSubmitButton = true;
        this.notifier.notify("error", "File to be attached");
    }
   else {
      let query = {
        ASN: {
          HEADER: { ...this.asnHeaders, USER_NAME: this.email },
          LINES: {
            LINE: [
              ...this.asnLines.map((m) => {
                return {
                  LINE_LOCATION_ID: m.LINE_LOCATION_ID,
                  QUANTITY: m.shipmentQuantity,
                };
              }),
            ],
          },
          LOTS: {
            LOT: [...this.asnLots],
          },
          TRANSPORTER_INFOS: {
            TRANSPORTER_INFO: [...this.asnTransporter],
          },
          FILE_NAME: this.fileName,
        },
      };
      this.finalSubmitButton = false;
      this.spinner.show();
      this.purchaseService.saveASN(query).subscribe((data: any) => {
        this.spinner.hide();
        console.log("data : ",data)
        if (data.length >= 15) {
          this.finalSubmitButton = true;
          this.notifier.notify("error", data);
        }
        else{
            this.asn_id = data;
             this.notifier.notify("success", "ASN Created Sucessfully! Browser opens ASN pdf preview in web browser window (Pop-Up feature to be enabled)! You can print ASN copy using 'Advanced Shipment Notice' menu.");

          // console.log('ASN_ID',this.asn_id);
          // var file = new Blob([data], { type: 'application/pdf' })
          //   var fileURL = URL.createObjectURL(file);
          this.makePdf(
            emailList,
            `ASN ${this.asn_id} has been generated successfully generated by Supplier ${this.supplierName}`,
            `
      Dear Team,

ASN ${this.asn_id} has been generated successfully generated by Supplier ${this.supplierName}.

Please find attached ASN document for further details.

Regards,
${this.supplierName}

      `,
            data, 'ASN Creation', mobileNo, this.supplierName
          );
          // this.emailService.sendMail(email, subject, body, smsType, element.BUYER_PHONE_NUM, element.BUYER, element.PO_NO)

          this.resetFile();
          this.resetFile1();
          this.resetPage();
        }
          // this.router.navigate(['/craftsmanautomation/shipment/advanceShipments'])
        },
        (error) => {
          this.spinner.hide();
          console.log(error);
        }
      );
      // console.log(query);
    }
  }

  resetPage() {
    this.asnHeaders = {};
    this.asnLines = [];
    this.asnLots = [];
    this.asnTransporter = [];
    this.selectedPO = "";
    this.filenameWithoutno = [];
    this.filenameWithoutno1 = [];
    this.getPendingPurchase();
  }

  setPo(value) {
    let a = this.uniquePOs.filter((f) => f === value);
    if (a.length === 1) {
      this.selectedPO = value;
      this.setLinesList();
    }
  }

  fileUpload() {
    this.spinner.show();
    const query = {
      DOCUMENT_ID: "",
      FILE_NAME: this.fileName,
      TYPE: "ASN_DETAILS",
      TITLE: "",
      DESCRIPTION: "Test file loading",
    };
    this.purchaseService.FileUpload(query).subscribe(
      (data: any) => {
        var file1 = data.filename;
        var data1 = data.path;
        this.loginService.type = data1;
        this.loginService.fileType = "Yes";
        if (data.o === "S") {
          this.fileData = this.fileInput1.target.files as File;
          const length = this.fileInput1.target.files.length;
          const file3 = new FormData();
          for (let i = 0; i < length; i++) {
            file3.append("file", this.fileData[0], file1);
          }
          this.loginService.FileUpload(file3).subscribe(
            (data) => {
              // console.log("dataaaa : ", data);
              this.spinner.hide();
              let i;
              // this.upload.file = this.fileData[0].name;
            },
            (err) => {
              this.spinner.hide();
              this.resetFile();
              if (err) {
              } else {
              }
            }
          );
          this.notifier.notify("success", "File being uploaded!");
          // this.resetFile();
        }
        this.spinner.hide();
      },
      (error) => {
        console.log(error);
        this.spinner.hide();
      }
    );
  }
  resetFile() {
    this.myInputVariable.nativeElement.value = "";
    this.filenameWithno =[];
    this.filenameWithoutno=[];
  }
  resetFile1() {
    this.myInputVariable1.nativeElement.value = "";
    this.filenameWithoutno1 = [];
    this.filenameWithno1 = [];
  }

  onFilesAdded(fileInput: any) {
    this.spinner.show();
    this.fileInput1 = fileInput;
    this.fileData = fileInput.target.files as File;
    const length = fileInput.target.files.length;

    let file = new FormData();
    for (let i = 0; i < length; i++) {
      file = new FormData();
      this.loginService.type = "/temp";
      this.loginService.fileType = "No";
      const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      file.append("file", this.fileData[0], S4+"_"+this.fileData[0].name);
      this.loginService.FileUpload(file).subscribe(
        (data) => {
          if (i === length - 1) {
            // this.asnHeaders.file1 =
            //   this.asnHeaders.file1 + this.fileData[i].name;

            this.filenameWithoutno.push(this.fileData[i].name);
            this.filenameWithno.push("/temp/"+S4+"_"+this.fileData[i].name)
          }
          this.spinner.hide();
          // console.log("filename : ", this.filenameWithno.length)
          this.asnHeaders.FILE_ATTACHMENTS=this.filenameWithno;
          // this.resetFile();
          this.myInputVariable.nativeElement.value = "";
          this.finalSubmitButton = true;

        },
        //     this.fileName = this.fileData[0].name;
        //     this.fileUpload();
        //   },
        (err) => {
          this.spinner.hide();
          if (i === length - 1) {
            this.resetFile();
          }
          if (err) {
          } else {
          }
        }
      );
    }
  }

  onFilesAdded1(fileInput: any) {
    this.spinner.show();
    this.fileInput1 = fileInput;
    this.fileData1 = fileInput.target.files as File;
    const length = fileInput.target.files.length;
    let file = new FormData();
    for (let i = 0; i < length; i++) {
      file = new FormData();
      this.loginService.type = "/temp";
      this.loginService.fileType = "No";
      const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      file.append("file", this.fileData1[0], S4+"_"+this.fileData1[0].name);
      this.loginService.FileUpload(file).subscribe(
        (data) => {
          if (i === length - 1) {
            // this.asnHeaders.file1 =
            //   this.asnHeaders.file1 + this.fileData[i].name;

            this.filenameWithoutno1.push(this.fileData1[i].name);
            this.filenameWithno1.push("/temp/"+S4+"_"+this.fileData1[i].name)
            this.enableAdd = true;
          }
          this.spinner.hide();
          // console.log("filename1 : ", this.filenameWithno1)
          this.transporters.FILE_ATTACHMENTS=this.filenameWithno1;
          this.myInputVariable1.nativeElement.value = "";
          // this.resetFile();
            // this.asnTransporter.push({'FILE_ATTACHMENTS': this.filenameWithno1});
        },
        //     this.fileName = this.fileData[0].name;
        //     this.fileUpload();
        //   },
        (err) => {
          this.spinner.hide();
          if (i === length - 1) {
            this.resetFile1();
          }
          if (err) {
          } else {
          }
        }
      );
    }
  }

  removeFile(i){
    this.filenameWithno.splice(i, 1);
    this.filenameWithoutno.splice(i, 1);
  }
  removeFile1(i){
    this.filenameWithno1.splice(i, 1);
    this.filenameWithoutno1.splice(i, 1);
  }

  getAllFreightTerms() {
    // var query = { email: this.email, purchaseType: "ALL_FREIGHT_TERMS" };
    const details = this.purchaseService.inputJSON('ALL_FREIGHT_TERMS', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe(
      (data: any) => {
        // console.log("Fright createASN", data);
        this.frightList = data
          ? data.FREIGHT_TERMS.map((x) => x.FREIGHT_TERM)
          : [];
        // this.frightList = Array.isArray(data.FREIGHT_TERMS.FREIGHT_TERM) ? data.FREIGHT_TERMS.FREIGHT_TERM : (data.FREIGHT_TERMS.FREIGHT_TERM) ? [data.FREIGHT_TERMS.FREIGHT_TERM] : [];
      },
      (error) => {
        console.log(error);
      }
    );
  }

  //////////////////////////////FOR INVOICE //////////////////////////////

  makePdf(email, subject, body, asn_id, smsType, mobileNo, supplierName) {
    //   const doc = new jspdf();
    // const DATA = this.content.nativeElement;
    //   doc.addHTML(DATA, () => {
    //     doc.save('f2bOrder.pdf');
    //   });
    // }
    const documentDefinition = this.getDocumentDefinition();
    const temp = JSON.stringify(documentDefinition);
    const json = JSON.parse(temp);
    pdfMake.createPdf(json).open();
    // pdfMake.createPdf(json).getBase64().open();
    // pdfMake.save('ASN.pdf'); //('F2bOrderInvoice.pdf');
    // console.log("ASN>pdf : ", ASN.pdf)
    pdfMake.createPdf(json).getBase64((encodedString) => {
      this.emailService
        .sendMailWithAttachments(email, subject, body, "ASN.pdf", encodedString, smsType, mobileNo, asn_id, supplierName)
        .subscribe(
          (message) => {
            // console.log("AAAAAAAAAAA", message);
            this.resetPage();
            // this.spinner.hide();
            if (message === "OK") {
              const id = 1;
              // this.router.navigate(['/auth/view'], { queryParams: { id } });
              // this.text = false;
              // this.email = '';
            } else {
              // this.notifier.notify('warning', 'Email Send Failed');
            }
          },
          (error) => {
            this.spinner.hide();
          }
        );
    });
  }

  getDocumentDefinition() {
    return {
      content: [

        this.getQRRow(),

        // {
        //   text: this.asn_id,
        //   fontSize: this.bodyFontSize + 2,
        //   bold: true,
        //   alignment: "right",
        // },

        this.getFirstTable(),
        {
          text: "ASN Items",
          fontSize: this.bodyFontSize + 2,
          bold: true,
          margin: [-30, 10, 0, 0],
        },
        this.getSecondTable(),
        {
          text: this.asnLots.length ? "Lot Details" : "",
          fontSize: this.bodyFontSize + 2,
          bold: true,
          margin: [20, 20, 0, 0],
        },
        this.asnLots.length ? this.getThirdTable() : "",
        {
          text: this.asnTransporter.length ? "Transporter Details" : "",
          fontSize: this.bodyFontSize + 2,
          bold: true,
          margin: [20, 20, 0, 0],
        },
        this.asnTransporter.length ? this.getFourthTable() : "",
      ],
      info: {
        title: "ASN INVOICE",
      },
      styles: {
        header: {
          fontSize: 16,
          bold: true,
          margin: [0, 20, 0, 10],
          decoration: "underline",
        },
        alignment: {
          alignment: "right",
        },
        tableHeader: {
          bold: true,
        },
        tableExample: {
          margin: [0, 5, 0, 15],
        },
      },
    };
  }

  getQRRow() {
    return { qr: "" + this.asn_id, fit: "75", alignment: "right" }
  }

  getFirstTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths: [136, 136, 136, 136],
        body: [
          [
            {
              text: "Advance Shipment Notice",
              bold: true,
              fontSize: this.bodyFontSize + 2,
              colSpan: 4,
              alignment: "center",
            },
            "",
            "",
            "",
          ],
          [
            {
              text: " Unit Name : Craftsman Automation Limited - "+this.ORG_NAME +"\t\t\t\t\t\t     ASN Number : "+this.asn_id,
              bold: true,
              italic: true,
              fontSize: this.bodyFontSize + 2,
              colSpan: 4,
              alignment: "left",
            },

          ],
          [
            { text: "Supplier", bold: true, fontSize: this.bodyFontSize },
            {
              text: "Invoice Number",
              bold: true,
              fontSize: this.bodyFontSize,
            },
            {
              text: "Invoice Date",
              bold: true,
              fontSize: this.bodyFontSize,
            },
            {
              text: "Expected Receipt Date",
              bold: true,
              fontSize: this.bodyFontSize,
            },
          ],
          [
            { text: this.supplierName || "-", fontSize: this.bodyFontSize },
            {
              text: this.asnHeaders.DELIVERY_ORDER_NUMBER || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.asnHeaders.DELIVERY_ORDER_DATE || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.asnHeaders.EXPECTED_RECEIPT_DATE || "-",
              fontSize: this.bodyFontSize,
            },
          ],
          [
            { text: "Bill of Lading", bold: true, fontSize: this.bodyFontSize },
            { text: "Waybill Number", bold: true, fontSize: this.bodyFontSize },
            // { text: "Freight Terms", bold: true, fontSize: this.bodyFontSize },
            { text: "E-Invoice No", bold: true, fontSize: this.bodyFontSize },
            {
              text: "Shipment Method",
              bold: true,
              fontSize: this.bodyFontSize,
            },
          ],

          [
            {
              text: this.asnHeaders.BILL_OF_LADING || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.asnHeaders.WAYBILL_NUMBER || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.asnHeaders.EINVOICE_NUMBER || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.asnHeaders.SHIPPING_METHOD || "-",
              fontSize: this.bodyFontSize,
            },
          ],
          [
            { text: "Line Amount", bold: true, fontSize: this.bodyFontSize },
            {
              text: "Other Amount",
              bold: true,
              fontSize: this.bodyFontSize,
            },
            {
              text: "Tax Amount",
              bold: true,
              fontSize: this.bodyFontSize,
            },
            {
              text: "Total Value",
              bold: true,
              fontSize: this.bodyFontSize,
            },
          ],
          [
            { text: this.asnHeaders.BASE_AMOUNT || "-", fontSize: this.bodyFontSize },
            {
              text: this.asnHeaders.OTHER_AMOUNT || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.asnHeaders.TAX_AMOUNT || "-",
              fontSize: this.bodyFontSize,
            },
            {
              text: this.asnHeaders.TOTAL_VALUE || "-",
              fontSize: this.bodyFontSize,
            },
          ],
        ],
      },
      layout: {
        hLineWidth(i, node) {
          return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return i === 0 || i === node.table.body.length ? "white" : "white";
        },
        vLineColor(i, node) {
          return i === 0 || i === node.table.widths.length ? "white" : "white";
        },
      },
    };
  }

  getSecondTableBody() {
    return this.asnLines.map((m) => {
      return [
        { text: m.DOCUMENT_NO || "-", fontSize: this.bodyFontSize },
        { text: m.PO_LINE_NUMBER || "-", fontSize: this.bodyFontSize },
        // { text: m.PO_SHIPMENT_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.ITEM_DESCRIPTION || "-", fontSize: this.bodyFontSize },
        // { text: m.ORDERED_QTY || "-", fontSize: this.bodyFontSize },
        { text: m.UOM || "-", fontSize: this.bodyFontSize },
        { text: m.shipmentQuantity || "-", fontSize: this.bodyFontSize },
        { text: (m.PRICE *m.shipmentQuantity).toFixed(2) || "-", fontSize: this.bodyFontSize },
      ];
    });
  }

  getSecondTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        // widths: [55, 40, 190, 65, 55, 55],
        widths: [120, 40, 190, 45, 65, 65],
        // widths: [136,136,136,136],
        body: [
          [
            { text: "PO Number", fontSize: this.bodyFontSize, bold: true },
            { text: "Line No.", fontSize: this.bodyFontSize, bold: true },
            // { text: "Shipment No.", fontSize: this.bodyFontSize, bold: true },
            { text: "Item", fontSize: this.bodyFontSize, bold: true },
            // { text: "Ordered Qty", fontSize: this.bodyFontSize, bold: true },
            { text: "UOM", fontSize: this.bodyFontSize, bold: true },
            { text: "Shipped Qty", fontSize: this.bodyFontSize, bold: true },
            { text: "Line Amount", fontSize: this.bodyFontSize, bold: true },
          ],
          ...this.getSecondTableBody(),
        ],
      },
      layout: {
        hLineWidth(i, node) {
          return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return i === 0 || i === node.table.body.length ? "white" : "white";
        },
        vLineColor(i, node) {
          return i === 0 || i === node.table.widths.length ? "white" : "white";
        },
      },
    };
  }

  getThirdBody() {
    return this.asnLots.map((m) => {
      return [
        { text: m.PO_NO || "-", fontSize: this.bodyFontSize },
        { text: m.PO_LINE_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.PO_SHIPMENT_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.LOT_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.GRADE || "-", fontSize: this.bodyFontSize },
        { text: m.QUANTITY || "-", fontSize: this.bodyFontSize },
        { text: m.REMARKS || "-", fontSize: this.bodyFontSize },
      ];
    });
  }

  getThirdTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        // widths: [55, 40, 65, 190, 55, 40, 55],
        widths: [80, 40, 65, 180, 55, 40, 55],
        // widths: [136,136,136,136],
        body: [
          [
            { text: "PO Number", fontSize: this.bodyFontSize, bold: true },
            { text: "Line No.", fontSize: this.bodyFontSize, bold: true },
            { text: "Shipment No.", fontSize: this.bodyFontSize, bold: true },
            { text: "Lot", fontSize: this.bodyFontSize, bold: true },
            { text: "Grade", fontSize: this.bodyFontSize, bold: true },
            { text: "Quantity", fontSize: this.bodyFontSize, bold: true },
            { text: "Remarks", fontSize: this.bodyFontSize, bold: true },
          ],
          ...this.getThirdBody(),
        ],
      },
      layout: {
        hLineWidth(i, node) {
          return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return i === 0 || i === node.table.body.length ? "white" : "white";
        },
        vLineColor(i, node) {
          return i === 0 || i === node.table.widths.length ? "white" : "white";
        },
      },
    };
  }
  getFourthTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        // widths: [55, 40, 65, 80, 55, 40, 55, 80],
        widths: [55, 40, 65, 80, 65, 62, 60, 80],
        // widths: [136,136,136,136],
        body: [
          [
            { text: "Transporter", fontSize: this.bodyFontSize, bold: true },
            { text: "Vehicle No.", fontSize: this.bodyFontSize, bold: true },
            { text: "E-Way Bill No.", fontSize: this.bodyFontSize, bold: true },
            { text: "LR No.", fontSize: this.bodyFontSize, bold: true },
            { text: "LR Date", fontSize: this.bodyFontSize, bold: true },
            { text: "Driver", fontSize: this.bodyFontSize, bold: true },
            { text: "Contact No.", fontSize: this.bodyFontSize, bold: true },
            { text: "Remarks", fontSize: this.bodyFontSize, bold: true },
          ],
          ...this.getFourthBody(),
        ],
      },
      layout: {
        hLineWidth(i, node) {
          return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return i === 0 || i === node.table.body.length ? "white" : "white";
        },
        vLineColor(i, node) {
          return i === 0 || i === node.table.widths.length ? "white" : "white";
        },
      },
    };
  }

  getFourthBody() {
    return this.asnTransporter.map((m) => {
      return [
        { text: m.TRANSPORTER_NAME || "-", fontSize: this.bodyFontSize },
        { text: m.VEHICLE_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.EWAY_BILL_NO || "-", fontSize: this.bodyFontSize },
        { text: m.LR_NO || "-", fontSize: this.bodyFontSize },
        { text: m.LR_DATE || "-", fontSize: this.bodyFontSize },
        { text: m.DRIVER_NAME || "-", fontSize: this.bodyFontSize },
        { text: m.DRIVER_CONTACT_NO || "-", fontSize: this.bodyFontSize },
        { text: m.REMARKS || "-", fontSize: this.bodyFontSize },
      ];
    });
  }

  AddTransporter() {
    // console.log("called AddTransporter()");
  }

  onModelClose1() {
    //  console.log('Called');
    // this.asnLines = this.beforeUpdateAsnLine;
    // this.asnLots = this.beforeUpdateAsnLots;
  }
  AddButtonClicked1() {
    this.asnTransporter = JSON.parse(JSON.stringify(this.transporters));
    if (!this.edit) {
      // let linesSelected = [];
      // let selectedLineIds = [];
      // let filedLot = [];
      // linesSelected = this.currPOlines.filter(
      //   (f) => f.selected && Number(f.shipmentQuantity) > 0
      // );
      // selectedLineIds = linesSelected.map((m) => m.LINE_LOCATION_ID);
      // filedLot = this.allLotList.filter((f) =>
      //   selectedLineIds.includes(f.LINE_LOCATION_ID)
      // );
      // this.asnLines.push(...linesSelected);
      // this.asnLots.push(...filedLot);
    } else {
      // this.edit= false;
      // let selectedLineIds = [];
      // this.asnLines = this.asnLines.filter(
      //   (f) => f.selected && Number(f.shipmentQuantity) > 0
      // );
      // selectedLineIds = this.asnLines.map((m) => m.LINE_LOCATION_ID);
      // this.asnLots = this.asnLots.filter((f) =>
      //   selectedLineIds.includes(f.LINE_LOCATION_ID)
      // );
      // console.log('LEN',this.asnLines.length,'LEN!',this.asnLots.length);
    }
  }
  addMoreButtonClicked() {
    if (!this.enableEdit) {
      if(this.transporters.FILE_ATTACHMENTS.length <=0){
        // this.enableEdit = false;
        this.enableAdd = true;
        this.notifier.notify("error", "File to be attached");
      }
      else{
      // console.log("this.transporters", this.transporters);
      this.asnTransporter.push(this.transporters);
      this.transporters = {};
      this.closebutton.nativeElement.click();
      // this.enableAdd =false;
      // console.log("this.transporter : ", this.asnTransporter)
      }
    } else {
      this.asnTransporter[this.enableEditindex] = this.transporters;
      this.transporters = {};
      this.enableEdit = false;
      this.enableAdd = false;
      this.closebutton.nativeElement.click();
      // console.log("after this.transporter : ", this.asnTransporter)
    }
  }

  deleteTransporter(index) {
    this.asnTransporter.splice(index, 1);
  }
  editTransporter(item, k) {
    this.enableEditindex = k;
    this.enableEdit = true;
    this.enableAdd = true;
    this.transporters = this.asnTransporter.find(
      (f) => f.VEHICLE_NUMBER === item.VEHICLE_NUMBER
    );
  }
  getDocumentList() {
    //   this.spinner.show();
    //   console.log("ASN_DETAILS : ", this.attachmentString)
    //   const details = { email: this.email, purchaseType: 'ATTACHMENT_LIST', attachmentString: this.attachmentString };
    // // const details = { email: this.email, purchaseType: 'ASN_ATTACHMENTS', headerId: this.headerId,
    // // attachmentString: this.attachmentString };
    //   this.purchaseService.getDocs(details).subscribe((data: any) => {
    //     console.log("indice : ASN_DETAILS : ", data)
    //     this.attachments = data.ATTACHMENTS? data.ATTACHMENTS.map(x=>(x.ATTACHMENT)) :[]
    //     // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
    //     //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
    //     this.spinner.hide();
    //   }, error => {
    //     console.log(error);
    //     this.spinner.hide();
    //   });
    // }
  }
  viewTaxButton1(element, j){
    this.action = false;
    this.asnLines[j].action = false;

  }
  viewTaxButton2(element,j){
    this.action = true;
    this.asnLines[j].action = true
    var taxIdentificationString= element.TAX_IDENTIFICATION_STRING+'~'+element.shipmentQuantity
    // const details = { email: this.email, attachmentString: taxIdentificationString, purchaseType: 'TAX_LINES'};
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
    var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', taxIdentificationString)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.myGst = this.taxData[0].MY_TAX_REG_NO

      this.asnLines[j].taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.asnLines[j].taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.asnLines[j].taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.asnLines[j].myGst = this.taxData[0].MY_TAX_REG_NO
      this.asnLines[j].hsnsacCode = this.taxData[0].HSN_SAC_CODE
      this.asnLines[j].hsnsacCodeDesc = this.taxData[0].HSN_SAC_CODE_DESC
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
  gobackASNShipements(){
   this.router.navigateByUrl('/craftsmanautomation/shipment/advanceShipments', {skipLocationChange: profileData.hideURL})

  }
}
