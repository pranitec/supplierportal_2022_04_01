import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { LoginService } from '../../auth/service/login.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
import pdfMake from 'pdfmake/build/pdfmake';
import { EmailService } from '../../auth/service/email.service';
import { ActivatedRoute } from '@angular/router';
declare var jQuery: any;

@Component({
  selector: 'app-edit-asn',
  templateUrl: './edit-asn.component.html',
  styleUrls: ['./edit-asn.component.css']
})
export class EditAsnComponent implements OnInit {

  public email = '';
  public asnHeaderAll;
  public asnHeader;
  public asnLineAll;
  public asnLine;
  public asnLotsAll;
  public asnLot;

  public asnTransporterAll = []
  public poHeaderAll = [];
  public poLineAll = [];
  public poHeader = [];
  public poLine = [];

  public uniquePOs = [];
  public selectedPO = '';
  public currPOlines = [];
  public selectedPOLines = [];
  public frightList = [];

  public fileName = '';
  public bodyFontSize = 10;
  public fromDate;
  public toDate;


  fileData: File = null;

  public lineClicked: any = {}

  public lotsEntered = [];

  public asnHeaders: any = {};

  public asnLines: any[] = [];

  public asnLots: any[] = [];


  public showlot = false;

  public currentLotTotal = 0;

  public addLotButton = false;

  public allLotList = [];
  public edit = false;

  public showHide = true;

  public beforeUpdateAsnLine = [];

  public beforeUpdateAsnLots = [];

  public supplierName = '';

  public asn_id = '';

  public ASN_ID;

  public editReason;
  public asnTransporter: any[] = [];
  public transporters: any = {};
  public enableEdit = false;
  public enableAdd = false;
  public enableEditindex = null;
  public finalSubmitButton = true;
  public filenameWithoutno =[];
  public filenameWithno =[];
  public filenameWithoutno1 =[];
  public filenameWithno1 =[];
  public totalTaxAmout =0;
  public action= false;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst;
  fileData1 : File =null
  public fileInput1;
  public lotbutton =true;
  public mobileNo=[];
  public ASN_NUMBER;

  private notifier: NotifierService;

  @ViewChild("myFileInput", { static: false }) myInputVariable: ElementRef;
  @ViewChild("myFileInput1", { static: false }) myInputVariable1: ElementRef;
  @ViewChild("closebutton") closebutton;

  constructor(private purchaseService: PurchaseService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService, private route: ActivatedRoute,
    private breadCrumbServices: BreadcrumbService, private router: Router, private rsaService: RsaService, private notifierService: NotifierService, private loginService: LoginService, public emailService: EmailService) {
    this.notifier = notifierService;
    this.route.queryParams.subscribe((data) => {
      this.ASN_ID = data['headerId'];
      this.editReason = data['reason'];
      this.fromDate = data['f'];
      this.toDate = data['t'];
    });
  }

  ngOnInit(): void {
    this.supplierName = this.breadCrumbServices.filteredSelection[0].VENDOR_NAME;
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.getPendingPurchase();
    this.getASNDetails();

    let setOrderDate = (date) => {
      this.asnHeaders.DELIVERY_ORDER_DATE = date;
    }

    let setExpectDate = (date) => {
      this.asnHeaders.EXPECTED_RECEIPT_DATE = date;
    }
    let setLRDate = (date) => {
      this.transporters.LR_DATE = date;
    };

    (function ($, setOrderDate, setExpectDate, setLRDate) {
      $(document).ready(function () {
        //   $('.js-example-basic-single').select2({

        //   });

        //   $('#documentNo').on('select2:select', function (e) {
        //     var data = e.params.data;
        //     console.log(data.text);
        // });

        $(".asnlot").hide();


        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          change(e) {


            setOrderDate(e.target.value);
          }

        });

        $('#datepicker2').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          change(e) {


            setExpectDate(e.target.value);
          }
        });

        $("#datepicker3").datepicker({
          uiLibrary: "bootstrap4",
          format: "dd-mmm-yyyy",
          change(e) {
            setLRDate(e.target.value);
          },
        });

        // $(".datepicker").datepicker();

        $("#test").click(function () {

          //console.log('Called');

          $(".asnlot").toggle();

        });


        $("#test1").click(function () {
          //console.log('Called');


          $(".asnlot").toggle();

        });
      });
    })(jQuery, setOrderDate, setExpectDate, setLRDate);
  }

  getASNDetails() {
    let header, line,lot, transport;
    this.spinner.show();
    // const details = { email: this.email, purchaseType: 'ASN_DETAILS', date1: this.fromDate, date2: this.toDate };
    const details = this.purchaseService.inputJSON('ASN_DETAILS_FOR_ORDER', undefined, undefined, 'ASN_ID', this.ASN_ID)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('kkk ggg', data);
      this.spinner.hide();
      this.asnHeaderAll = data ? data.ASNDETAILS.ASN_HEADERS.map(x =>x.ASN_HEADER) : [];
      this.asnLineAll = data ? data.ASNDETAILS.ASN_LINES.map(x =>x.ASN_LINE) : [];
      this.asnLotsAll = data.ASNDETAILS.ASN_LOTS ? data.ASNDETAILS.ASN_LOTS.map(x =>x.ASN_LOT) : [];
      this.asnTransporterAll = data.ASNDETAILS.TRANSPORTER_INFOS? data.ASNDETAILS.TRANSPORTER_INFOS.map(x =>x.TRANSPORTER_INFO) : []
      // this.asnHeaderAll = Array.isArray(data.ASNDETAILS.ASN_HEADERS.ASN_HEADER) ? data.ASNDETAILS.ASN_HEADERS.ASN_HEADER : (data.ASNDETAILS.ASN_HEADERS.ASN_HEADER) ? [data.ASNDETAILS.ASN_HEADERS.ASN_HEADER] : [];
      // this.asnLineAll = Array.isArray(data.ASNDETAILS.ASN_LINES.ASN_LINE) ? data.ASNDETAILS.ASN_LINES.ASN_LINE : (data.ASNDETAILS.ASN_LINES.ASN_LINE) ? [(data.ASNDETAILS.ASN_LINES.ASN_LINE)] : [];
      // this.asnLotsAll = Array.isArray(data.ASNDETAILS.ASN_LOTS.ASN_LOT) ? data.ASNDETAILS.ASN_LOTS.ASN_LOT : (data.ASNDETAILS.ASN_LOTS.ASN_LOT) ? [data.ASNDETAILS.ASN_LOTS.ASN_LOT] : [];

      // this.asnHeaders = this.asnHeaderAll;
      // this.asnLines = this.asnLineAll;
      // this.asnLots = this.asnLotsAll;
      this.ASN_NUMBER =  (this.asnHeaderAll.map(x=>x.ASN_NUMBER)).toString()
      this.asnHeaders = this.asnHeaderAll.find(x  =>x.ASN_ID == this.ASN_ID);
          // this.asnLines = this.asnLineAll.find(y this.asnLineAll.forEach((x) => {
      line = this.asnLineAll.filter(f => f.ASN_ID == this.ASN_ID);
      this.mobileNo=line.map(x=>x.BUYER_PHONE_NUM);
      lot = this.asnLotsAll.filter(f => f.ASN_ID == this.ASN_ID);

      transport= this.asnTransporterAll.filter(f => f.ASN_ID == this.ASN_ID);

      this.asnLines = line.map(f=>{ return {...f,shipmentQuantity:f.RECEIVED_QTY,DOCUMENT_NO:f.PO_NUMBER, selected:true}});
      this.asnLines.forEach(x=>{
        this.totalTaxAmout = this.totalTaxAmout + x.ASN_TAX_AMOUNT
      });
      this.allLotList = lot;
    this.asnLots = [...lot];
    this.asnTransporter = [...transport]
      //  this.setFilteredData();

    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }


  getPendingPurchase() {
    this.spinner.show();
    // const details = { email: this.email, purchaseType: 'ASN_PENDING_PO' };
    const details = this.purchaseService.inputJSON('ASN_PENDING_PO', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('edit ASN', data);
      this.spinner.hide();
      this.getAllFreightTerms();

      this.poHeaderAll = data ? data.PENDINGPO.POHEADERS.map(x =>x.POHEADER) : [];
      this.poLineAll = data ? data.PENDINGPO.POLINES.map(x =>x.POLINE) : [];

      // this.poHeaderAll = Array.isArray(data.PENDINGPO.POHEADERS.POHEADER) ? data.PENDINGPO.POHEADERS.POHEADER : (data.PENDINGPO.POHEADERS.POHEADER) ? [data.PENDINGPO.POHEADERS.POHEADER] : [];
      // this.poLineAll = Array.isArray(data.PENDINGPO.POLINES.POLINE) ? data.PENDINGPO.POLINES.POLINE : (data.PENDINGPO.POLINES.POLINE) ? [(data.PENDINGPO.POLINES.POLINE)] : [];
      this.poHeader = this.poHeaderAll;
      this.poLine = this.poLineAll;
      this.poHeader.forEach(
        (x, i) => {
          this.poHeader[i].filteredList = this.poLine.filter(y => y.PO_HEADER_ID === x.PO_HEADER_ID);
          this.poHeader[i].filteredList = this.poHeader[i].filteredList.map(y => {
            y.BUYER_EMAIL = this.poHeader[i].BUYER_EMAIL;
            y.BUYER_PHONE_NUM = this.poHeader[i].BUYER_PHONE_NUM
            return y;
          });
        }
      );

      this.uniquePOs = this.poHeader.map(m => m.PO_NO)
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  setLinesList() {
    let line = this.poHeader.find(f => f.PO_NO === this.selectedPO);
    this.currPOlines = line.filteredList ? line.filteredList : [];
    this.currPOlines.forEach((x,i) => {
      let a = this.asnLines.find(f => f.LINE_LOCATION_ID === x.LINE_LOCATION_ID);
      if(a){
        this.currPOlines[i].selected = true;
        this.currPOlines[i].shipmentQuantity = a.shipmentQuantity;
      }
    })
  }

  checkQuantityAndUpdate(i) {
    if (Number(this.currPOlines[i].shipmentQuantity) > (Number(this.currPOlines[i].ORDERED_QTY) - Number(this.currPOlines[i].RECEIVED_QTY))) {

      this.notifier.notify('error', 'Shipment Qty Exceeds Order Qty');
      this.currPOlines[i].shipmentQuantity = 0;
    }
  }

  addLotClicked(i) {
    this.showlot = true;
    this.lineClicked = JSON.parse(JSON.stringify(this.currPOlines[i]));
    //  this.lotsEntered = this.asnLots.filter(f=>f.LINE_LOCATION_ID ===this.currPOlines[i].LINE_LOCATION_ID )

    this.lotsEntered = this.allLotList.filter(f => f.LINE_LOCATION_ID === this.currPOlines[i].LINE_LOCATION_ID);
    this.calculateTotal()
    this.setDatePicker();
  }

  checkValue(value, index) {
    //console.log(value,index);
    if (!value) {
      this.currPOlines[index].shipmentQuantity = undefined;
      this.showlot = false;
      this.lotsEntered = this.lotsEntered.filter(f => f.LINE_LOCATION_ID !== this.lotsEntered[index].LINE_LOCATION_ID)
      this.allLotList = this.allLotList.filter(f => f.LINE_LOCATION_ID !== this.lotsEntered[index].LINE_LOCATION_ID)
      this.setDatePicker()
    } else {
      this.currPOlines[index].shipmentQuantity = Number(this.currPOlines[index].ORDERED_QTY) - Number(this.currPOlines[index].RECEIVED_QTY)
    }
  }

  addLotButtonClicked() {
    this.allLotList.push({ LINE_LOCATION_ID: this.lineClicked.LINE_LOCATION_ID, PO_NO: this.selectedPO })
    this.lotsEntered.push(this.allLotList[this.allLotList.length - 1]);
    this.setDatePicker();
    this.calculateTotal()
  }


  changedLotQuantity(i) {
    let lotquantity = 0;
    this.lotsEntered.forEach(f => {
      lotquantity = lotquantity + Number(f.QUANTITY);
    });

    if (lotquantity > this.lineClicked.shipmentQuantity) {
      this.notifier.notify('error', 'Lot Qunatity Exceeds Shipment Quantity')
      this.lotsEntered[i].QUANTITY = 0;
    }
    this.calculateTotal();
  }

  calculateTotal() {
    let lotquantity = 0;
    this.lotsEntered.forEach(f => {
      lotquantity = lotquantity + Number(f.QUANTITY);
    });

    this.currentLotTotal = lotquantity;
    if (lotquantity >= this.lineClicked.shipmentQuantity) {
      this.addLotButton = true;
    }
    else {
      this.addLotButton = false;
    }
  }

  AddButtonClicked() {
    // this.totalTaxAmout =0;
    if (!this.edit) {
      let linesSelected = [];
      let selectedLineIds = [];
      let filedLot = [];
      linesSelected = this.currPOlines.filter(f => f.selected && Number(f.shipmentQuantity) > 0);
      linesSelected.forEach(x=>{
        this.totalTaxAmout = this.totalTaxAmout + (x.shipmentQuantity/x.ORDERED_QTY)*x.ASN_TAX_AMOUNT
      })

      selectedLineIds = linesSelected.map(m => m.LINE_LOCATION_ID);
      filedLot = this.allLotList.filter(f => selectedLineIds.includes(f.LINE_LOCATION_ID));
      // console.log('filedLot', filedLot);
      this.asnLines.push(...linesSelected);
      this.asnLots.push(...filedLot);
      // this.asnLines.forEach(x=>{
      //   this.totalTaxAmout = this.totalTaxAmout + (x.shipmentQuantity/x.ORDERED_QTY)*x.ASN_TAX_AMOUNT
      // })
    }
    else {
      // this.edit= false;
      let selectedLineIds = [];
      let linesSelected = this.currPOlines.filter(f => f.selected && Number(f.shipmentQuantity) > 0);
      this.asnLines = this.asnLines.filter(f => f.selected && Number(f.shipmentQuantity) > 0);
      this.asnLines.forEach((f,i)=>{
        let a = linesSelected.find(x=>f.LINE_LOCATION_ID === x.LINE_LOCATION_ID);
        if(a){
          this.asnLines[i].shipmentQuantity = a.shipmentQuantity;
          this.totalTaxAmout = this.totalTaxAmout + (f.shipmentQuantity/f.ORDERED_QTY)*f.ASN_TAX_AMOUNT
        }
      })
      selectedLineIds = this.asnLines.map(m => m.LINE_LOCATION_ID);
      this.asnLots = this.allLotList.filter(f => selectedLineIds.includes(f.LINE_LOCATION_ID));
    }
    this.asnLots.forEach(x=>{
      if(x.MFG_DATE ){
        if(new Date(x.MFG_DATE) >new Date(this.asnHeaders.DELIVERY_ORDER_DATE)){
        this.notifier.notify("warning", "Manufacturing date ("+x.MFG_DATE +") should not exceed Invoice date ("+this.asnHeaders.DELIVERY_ORDER_DATE+")");
        this.lotbutton = false;
        }
      }
    })
  }

  checkmfgdate(){
    this.lotbutton = true;
  }

  editLine(item) {
    this.selectedPO = item.DOCUMENT_NO;
    this.edit = true;
    this.showlot = false;
    this.beforeUpdateAsnLine = JSON.parse(JSON.stringify(this.asnLines));
    this.beforeUpdateAsnLots = JSON.parse(JSON.stringify(this.asnLots));
    this.setLinesList()
  }

  deleteLine(item) {
    this.asnLines = this.asnLines.filter(f => f.LINE_LOCATION_ID !== item.LINE_LOCATION_ID);
    this.asnLots = this.asnLots.filter(f => f.LINE_LOCATION_ID !== item.LINE_LOCATION_ID);
  }

  setDatePicker() {
    //this.showHide = false;
    let setDate1 = (i, value) => {
      //  console.log('CAlled Date 1',i,value)
      this.lotsEntered[i].MFG_DATE = value;
    }

    let setDate2 = (i, value) => {
      this.lotsEntered[i].EXPIRATION_DATE = value;
    }
    this.chRef.detectChanges();
    this.lotsEntered.forEach((f, i) => {
      (function ($, i, setDate1, setDate2) {
        //     console.log(i,"Called");
        $("#mfgDate" + i).datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          value: f.MFG_DATE,
          change(e) {
            setDate1(i, e.target.value);
          }
        });
        $("#expDate" + i).datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          value: f.EXPIRATION_DATE,
          change(e) {


            setDate2(i, e.target.value);
          }
        });

      })(jQuery, i, setDate1, setDate2);
    })
  }


  AddLineCalled() {
    this.selectedPO = '';
    this.currPOlines = [];
    this.lotsEntered = [];
    this.showlot = false;
    this.edit = false;
    this.beforeUpdateAsnLine = JSON.parse(JSON.stringify(this.asnLines));
    this.beforeUpdateAsnLots = JSON.parse(JSON.stringify(this.asnLots));
  }

  onModelClose() {
    //  console.log('Called');
    this.asnLines = this.beforeUpdateAsnLine;
    this.asnLots = this.beforeUpdateAsnLots;
  }

  submitPage() {
    var mobileNo;
    // console.log("submitPage ",this.asnHeaders, this.asnLines, this.asnLots, this.email, this.asnTransporter);

    //console.log(this.asnHeaders,this.asnLines,this.asnLots,this.email);
    let emailList = [];
    let uniqueEmailList = [];
    emailList = this.asnLines.map(m => m.BUYER_EMAIL);
    mobileNo =  Array.from(new Set(this.mobileNo));
    mobileNo =  mobileNo.toString();
    uniqueEmailList = Array.from(new Set(emailList));
    var tot = 0;
    var totalamt =
      Number(this.asnHeaders.BASE_AMOUNT) +
      Number(this.asnHeaders.TAX_AMOUNT) +
      Number(this.asnHeaders.OTHER_AMOUNT);
    this.asnLines.forEach((x) => {
      tot = tot + Number(x.shipmentQuantity) * Number(x.PRICE);
    });

    // this.asnLines.forEach(x=>{
    //   this.totalTaxAmout = this.totalTaxAmout + x.ASN_TAX_AMOUNT
    // });
    totalamt = Number(totalamt.toFixed(2));
    tot =Number(tot.toFixed(2))
    this.totalTaxAmout = Number(this.totalTaxAmout.toFixed(2))
    if(new Date(this.asnHeaders.DELIVERY_ORDER_DATE)>new Date(this.asnHeaders.EXPECTED_RECEIPT_DATE)){
      this.finalSubmitButton = true;
      this.notifier.notify("error", "Expected receipt Date should be greater than or equal to Invoice Date");
    } else if (Number(this.asnHeaders.BASE_AMOUNT) != tot) {
      this.finalSubmitButton = false;
      this.notifier.notify("warning", " Value Mismatch between ASN line value ("+tot+ ") and Base Amount");
     } else if(Number(this.asnHeaders.TAX_AMOUNT) != this.totalTaxAmout){
      this.finalSubmitButton = false;
      this.notifier.notify("warning", "TAX Amount ("+this.totalTaxAmout+") mismatch");
    } else if (Number(this.asnHeaders.TOTAL_VALUE) != totalamt) {
      this.finalSubmitButton = false;
      this.notifier.notify("warning", " ASN Total Value mismatch");
    }
    else {
    let query = {
      ASN: {
        HEADER: { ...this.asnHeaders, USER_NAME: this.email, OLD_ASN_ID: this.ASN_ID, REASON_DESCRIPTION: this.editReason },
        LINES: {
          LINE: [...this.asnLines.map(m => { return { LINE_LOCATION_ID: m.LINE_LOCATION_ID, QUANTITY: m.shipmentQuantity } })]
        },
        LOTS: {
          LOT: [...this.asnLots]
        },  TRANSPORTER_INFOS: {
          TRANSPORTER_INFO: [...this.asnTransporter],
        },
        FILE_NAME: this.fileName
      }
    };

    this.spinner.show()
    this.purchaseService.saveASN(query).subscribe((data: any) => {
      if (data) {
        this.asn_id = data;
      }
      //console.log('ASN_ID',this.asn_id);
      this.makePdf(emailList, `ASN ${this.asn_id} has been generated successfully generated by Supplier ${this.supplierName}`, `
      Dear Team,

ASN ${this.asn_id} has been generated successfully generated by Supplier ${this.supplierName}.

Please find attached ASN document for further details.

Regards,
${this.supplierName}

      `, data, 'ASN Edit', mobileNo, this.supplierName, this.ASN_NUMBER);
      this.notifier.notify('success', 'Submitted Sucessfully');
    }, error => {

      console.log(error);
    })
    console.log(query);
  }
  }

  resetPage() {
    this.asnHeaders = {};
    this.asnLines = [];
    this.asnLots = [];
    this.selectedPO = '';
    this.getPendingPurchase();
  }

  setPo(value) {
    let a = this.uniquePOs.filter(f => f === value)
    if (a.length === 1) {
      this.selectedPO = value;
      this.setLinesList();
    }
  }

  fileUpload() {
    this.spinner.show();
    const query = {
      DOCUMENT_ID: '', FILE_NAME: this.fileName, TYPE: 'ASN_DETAILS',
      TITLE: '', DESCRIPTION: 'Test file loading'
    };
    this.purchaseService.FileUpload(query).subscribe((data) => {
      // console.log(data);
      if (data === 'S') {
        this.resetFile();
      }
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });
  }
  resetFile() {
    this.myInputVariable.nativeElement.value = '';
  }

  // onFilesAdded(fileInput: any) {
  //   this.spinner.show();
  //   this.fileData = fileInput.target.files as File;
  //   const length = fileInput.target.files.length;
  //   const file = new FormData();
  //   for (let i = 0; i < length; i++) {
  //     file.append('file', this.fileData[0], this.fileData[0].name);
  //   }
  //   this.loginService.FileUpload(file).subscribe((data) => {
  //     this.spinner.hide();
  //     let i;
  //     //console.log('iiii');
  //     // this.fileUpload();
  //     this.fileName = this.fileData[0].name;
  //   }, err => {
  //     this.spinner.hide();
  //     this.resetFile();
  //     if (err.error.message) {

  //     } else {
  //     }
  //   });

  // }
  getAllFreightTerms() {
    // var query = { email: this.email, purchaseType: 'ALL_FREIGHT_TERMS' }
    const details = this.purchaseService.inputJSON('ALL_FREIGHT_TERMS', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('Fright',data);
      this.frightList = data? data.FREIGHT_TERMS.map(x =>x.FREIGHT_TERM) : []
      // this.frightList = Array.isArray(data.FREIGHT_TERMS.FREIGHT_TERM) ? data.FREIGHT_TERMS.FREIGHT_TERM : (data.FREIGHT_TERMS.FREIGHT_TERM) ? [data.FREIGHT_TERMS.FREIGHT_TERM] : [];
    }, error => {
      console.log(error);
    })
  }

  number(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  //////////////////////////////FOR INVOICE //////////////////////////////

  makePdf(email, subject, body, asn_id,  smsType, mobileNo, supplierName, oldasn_id) {
    // console.log("asn_id,  smsType, mobileNo, supplierName, oldasn_id : ", asn_id,  smsType, mobileNo, supplierName, oldasn_id)
    //   const doc = new jspdf();
    // const DATA = this.content.nativeElement;
    //   doc.addHTML(DATA, () => {
    //     doc.save('f2bOrder.pdf');
    //   });
    // }
    const documentDefinition = this.getDocumentDefinition();
    const temp = JSON.stringify(documentDefinition);
    const json = JSON.parse(temp);
    // pdfMake.createPdf(json).open();
    pdfMake.createPdf(json).open()//('F2bOrderInvoice.pdf');

    pdfMake.createPdf(json).getBase64((encodedString) => {
      this.emailService.sendMailWithAttachments(email, subject, body, 'ASN.pdf', encodedString, smsType, mobileNo, asn_id, supplierName, oldasn_id).
        subscribe(message => {
          // console.log('AAAAAAAAAAA', message);
          this.resetPage();
          this.spinner.hide();
          if (message === 'OK') {
            const id = 1;
            // this.router.navigate(['/auth/view'], { queryParams: { id } });
            // this.text = false;
            // this.email = '';
          } else {
            // this.notifier.notify('warning', 'Email Send Failed');
          }
        }, error => {
          this.spinner.hide();
        });
    });
  }

  getDocumentDefinition() {
    return {
      content: [
        this.getQRRow(),
        {
          text: this.asn_id,
          fontSize: this.bodyFontSize + 2,
          bold: true,
          alignment: "right",
        },
        this.getFirstTable(),
        { text: 'Shipment Lines', fontSize: this.bodyFontSize + 2, bold: true, margin: [20, 20, 0, 0] },
        this.getSecondTable(),
        { text: (this.asnLots.length) ? 'Lot Details' : '', fontSize: this.bodyFontSize + 2, bold: true, margin: [20, 20, 0, 0] },
        (this.asnLots.length) ? (this.getThirdTable()) : '',
        {
          text: this.asnTransporter.length ? "Transporter Details" : "",
          fontSize: this.bodyFontSize + 2,
          bold: true,
          margin: [20, 20, 0, 0],
        },
        this.asnTransporter.length ? this.getFourthTable() : "",
      ],
      info: {
        title: 'ASN INVOICE',
      },
      styles: {
        header: {
          fontSize: 16,
          bold: true,
          margin: [0, 20, 0, 10],
          decoration: 'underline',
        },
        alignment: {
          alignment: 'right',
        },
        tableHeader: {
          bold: true,
        },
        tableExample: {
          margin: [0, 5, 0, 15],
        },
      },
    };
  }

  getQRRow() {

    return { qr: '' + this.asn_id, fit: '75', alignment: 'right' }
  }

  getFirstTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths: [136, 136, 136, 136],
        body: [
          [{
            text: 'Advance Shipment Notice', bold: true, fontSize: this.bodyFontSize + 2, colSpan: 4, alignment: 'center'

          }, '', '', ''],
          [{
            text: 'Supplier', bold: true, fontSize: this.bodyFontSize
          }, {
            text: 'Invoice Number', bold: true, fontSize: this.bodyFontSize
          }, {
            text: 'Delivery Date', bold: true, fontSize: this.bodyFontSize
          }, {
            text: 'Expected Receipt Date', bold: true, fontSize: this.bodyFontSize
          }],
          [{
            text: (this.supplierName || '-'), fontSize: this.bodyFontSize
          }, {
            text: (this.asnHeaders.DELIVERY_ORDER_NUMBER || '-'), fontSize: this.bodyFontSize
          }, {
            text: (this.asnHeaders.DELIVERY_ORDER_DATE || '-'), fontSize: this.bodyFontSize
          }, {
            text: (this.asnHeaders.EXPECTED_RECEIPT_DATE || '-'), fontSize: this.bodyFontSize
          }],
          [{
            text: 'Bill of Lading', bold: true, fontSize: this.bodyFontSize
          }, {
            text: 'Waybill Number', bold: true, fontSize: this.bodyFontSize
          }, {
            text: 'Freight Terms', bold: true, fontSize: this.bodyFontSize
          }, {
            text: 'Shipment Method', bold: true, fontSize: this.bodyFontSize
          }],

          [{
            text: (this.asnHeaders.BILL_OF_LADING || '-'), fontSize: this.bodyFontSize
          }, {
            text: (this.asnHeaders.WAYBILL_NUMBER || '-'), fontSize: this.bodyFontSize
          }, {
            text: (this.asnHeaders.FREIGHT_TERMS || '-'), fontSize: this.bodyFontSize
          }, {
            text: (this.asnHeaders.SHIPPING_METHOD || '-'), fontSize: this.bodyFontSize
          }
          ],
        ]
      }, layout: {
        hLineWidth(i, node) {
          return (i === 0 || i === node.table.body.length) ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return (i === 0 || i === node.table.widths.length) ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
        },
        vLineColor(i, node) {
          return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
        }
      },
    }

  }


  getSecondTableBody() {
    return this.asnLines.map(m => {
      return [
        { text: (m.DOCUMENT_NO || '-'), fontSize: this.bodyFontSize },
        { text: (m.PO_LINE_NUMBER || '-'), fontSize: this.bodyFontSize },
        { text: (m.PO_SHIPMENT_NUMBER || '-'), fontSize: this.bodyFontSize },
        { text: (m.ITEM_DESCRIPTION || '-'), fontSize: this.bodyFontSize },
        { text: (m.ORDERED_QTY || '-'), fontSize: this.bodyFontSize },
        { text: (m.UOM || '-'), fontSize: this.bodyFontSize },
        { text: (m.shipmentQuantity || '-'), fontSize: this.bodyFontSize }
      ]
    })
  }

  getSecondTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths: [55, 40, 65, 190, 55, 40, 55],
        // widths: [136,136,136,136],
        body: [[{ text: 'PO Number', fontSize: this.bodyFontSize, bold: true }, { text: 'Line No.', fontSize: this.bodyFontSize, bold: true }, { text: 'Shipment No.', fontSize: this.bodyFontSize, bold: true }, { text: 'Item', fontSize: this.bodyFontSize, bold: true }, { text: 'Ordered Qty', fontSize: this.bodyFontSize, bold: true }, { text: 'UOM', fontSize: this.bodyFontSize, bold: true }, { text: 'Shipped Qty', fontSize: this.bodyFontSize, bold: true }], ...this.getSecondTableBody()]
      }, layout: {
        hLineWidth(i, node) {
          return (i === 0 || i === node.table.body.length) ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return (i === 0 || i === node.table.widths.length) ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
        },
        vLineColor(i, node) {
          return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
        }
      },
    }
  }

  getThirdBody() {
    return this.asnLots.map(m => {
      return [
        { text: (m.PO_NO || '-'), fontSize: this.bodyFontSize },
        { text: (m.PO_LINE_NUMBER || '-'), fontSize: this.bodyFontSize },
        { text: (m.PO_SHIPMENT_NUMBER || '-'), fontSize: this.bodyFontSize },
        { text: (m.LOT_NUMBER || '-'), fontSize: this.bodyFontSize },
        { text: (m.GRADE || '-'), fontSize: this.bodyFontSize },
        { text: (m.QUANTITY || '-'), fontSize: this.bodyFontSize },
        { text: (m.REMARKS || '-'), fontSize: this.bodyFontSize }
      ]
    })
  }

  getThirdTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths: [55, 40, 65, 190, 55, 40, 55],
        // widths: [136,136,136,136],
        body: [[{ text: 'PO Number', fontSize: this.bodyFontSize, bold: true }, { text: 'Line No.', fontSize: this.bodyFontSize, bold: true }, { text: 'Shipment No.', fontSize: this.bodyFontSize, bold: true }, { text: 'Lot', fontSize: this.bodyFontSize, bold: true }, { text: 'Grade', fontSize: this.bodyFontSize, bold: true }, { text: 'Quantity', fontSize: this.bodyFontSize, bold: true }, { text: 'Remarks', fontSize: this.bodyFontSize, bold: true }], ...this.getThirdBody()]
      }, layout: {
        hLineWidth(i, node) {
          return (i === 0 || i === node.table.body.length) ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return (i === 0 || i === node.table.widths.length) ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
        },
        vLineColor(i, node) {
          return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
        }
      },
    }
  }

getFourthTable() {
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths: [55, 40, 65, 80, 55, 40, 55, 80],
        // widths: [136,136,136,136],
        body: [
          [
            { text: "Transporter", fontSize: this.bodyFontSize, bold: true },
            { text: "Vehicle No.", fontSize: this.bodyFontSize, bold: true },
            { text: "E-Way Bill No.", fontSize: this.bodyFontSize, bold: true },
            { text: "LR No.", fontSize: this.bodyFontSize, bold: true },
            { text: "LR Date", fontSize: this.bodyFontSize, bold: true },
            { text: "Driver", fontSize: this.bodyFontSize, bold: true },
            { text: "Contact No.", fontSize: this.bodyFontSize, bold: true },
            { text: "Remarks", fontSize: this.bodyFontSize, bold: true },
          ],
          ...this.getFourthBody(),
        ],
      },
      layout: {
        hLineWidth(i, node) {
          return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
        },
        vLineWidth(i, node) {
          return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
        },
        hLineColor(i, node) {
          return i === 0 || i === node.table.body.length ? "white" : "white";
        },
        vLineColor(i, node) {
          return i === 0 || i === node.table.widths.length ? "white" : "white";
        },
      },
    };
  }

  getFourthBody() {
    return this.asnTransporter.map((m) => {
      return [
        { text: m.TRANSPORTER_NAME || "-", fontSize: this.bodyFontSize },
        { text: m.VEHICLE_NUMBER || "-", fontSize: this.bodyFontSize },
        { text: m.EWAY_BILL_NO || "-", fontSize: this.bodyFontSize },
        { text: m.LR_NO || "-", fontSize: this.bodyFontSize },
        { text: m.LR_DATE || "-", fontSize: this.bodyFontSize },
        { text: m.DRIVER_NAME || "-", fontSize: this.bodyFontSize },
        { text: m.DRIVER_CONTACT_NO || "-", fontSize: this.bodyFontSize },
        { text: m.REMARKS || "-", fontSize: this.bodyFontSize },
      ];
    });
  }

  viewTaxButton1(element, j){
    this.action = false;
    this.asnLines[j].action = false;

  }
  viewTaxButton2(element,j){
    this.action = true;
    this.asnLines[j].action = true
    var taxIdentificationString= element.TAX_IDENTIFICATION_STRING+'~'+element.shipmentQuantity
    // const details = { email: this.email, attachmentString: taxIdentificationString, purchaseType: 'TAX_LINES'};
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
      var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', taxIdentificationString)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.myGst = this.taxData[0].MY_TAX_REG_NO

      this.asnLines[j].taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.asnLines[j].taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.asnLines[j].taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.asnLines[j].myGst = this.taxData[0].MY_TAX_REG_NO
      this.asnLines[j].hsnsacCode = this.taxData[0].HSN_SAC_CODE
      this.asnLines[j].hsnsacCodeDesc = this.taxData[0].HSN_SAC_CODE_DESC
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  onFilesAdded(fileInput: any) {
    this.spinner.show();
    this.fileInput1 = fileInput;
    this.fileData = fileInput.target.files as File;
    const length = fileInput.target.files.length;

    let file = new FormData();
    for (let i = 0; i < length; i++) {
      file = new FormData();
      this.loginService.type = "/temp";
      this.loginService.fileType = "No";
      const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      file.append("file", this.fileData[0], S4+"_"+this.fileData[0].name);
      this.loginService.FileUpload(file).subscribe(
        (data) => {
          if (i === length - 1) {
            // this.asnHeaders.file1 =
            //   this.asnHeaders.file1 + this.fileData[i].name;

            this.filenameWithoutno.push(this.fileData[i].name);
            this.filenameWithno.push("/temp/"+S4+"_"+this.fileData[i].name)
          }
          this.spinner.hide();
          // console.log("filename : ", this.filenameWithno)
          this.asnHeaders.FILE_ATTACHMENTS=this.filenameWithno;
          this.resetFile();
        },
        //     this.fileName = this.fileData[0].name;
        //     this.fileUpload();
        //   },
        (err) => {
          this.spinner.hide();
          if (i === length - 1) {
            this.resetFile();
          }
          if (err) {
          } else {
          }
        }
      );
    }
  }

  onFilesAdded1(fileInput: any) {
    this.spinner.show();
    this.fileInput1 = fileInput;
    this.fileData1 = fileInput.target.files as File;
    const length = fileInput.target.files.length;
    let file = new FormData();
    for (let i = 0; i < length; i++) {
      file = new FormData();
      this.loginService.type = "/temp";
      this.loginService.fileType = "No";
      const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      file.append("file", this.fileData1[0], S4+"_"+this.fileData1[0].name);
      this.loginService.FileUpload(file).subscribe(
        (data) => {
          if (i === length - 1) {
            // this.asnHeaders.file1 =
            //   this.asnHeaders.file1 + this.fileData[i].name;

            this.filenameWithoutno1.push(this.fileData1[i].name);
            this.filenameWithno1.push("/temp/"+S4+"_"+this.fileData1[i].name)
            this.enableAdd = true;
          }
          this.spinner.hide();
          // console.log("filename1 : ", this.filenameWithno1)
          this.transporters.FILE_ATTACHMENTS=this.filenameWithno1;
          // this.resetFile();
            // this.asnTransporter.push({'FILE_ATTACHMENTS': this.filenameWithno1});
        },
        //     this.fileName = this.fileData[0].name;
        //     this.fileUpload();
        //   },
        (err) => {
          this.spinner.hide();
          if (i === length - 1) {
            this.resetFile1();
          }
          if (err) {
          } else {
          }
        }
      );
    }
  }

  removeFile(i){
    this.filenameWithno.splice(i, 1);
    this.filenameWithoutno.splice(i, 1);
  }
  removeFile1(i){
    this.filenameWithno1.splice(i, 1);
    this.filenameWithoutno1.splice(i, 1);
  }

   resetFile1() {
    this.myInputVariable1.nativeElement.value = "";
  }

  addMoreButtonClicked() {

    if (!this.enableEdit) {
      this.asnTransporter.push(this.transporters);
      this.transporters = {};
      // this.enableAdd =false;
      this.enableAdd = true;

      // console.log("this.transporter : ", this.asnTransporter)
    } else {
      this.asnTransporter[this.enableEditindex] = this.transporters;
      this.transporters = {};
      this.enableEdit = false;
      this.enableAdd = false;
      this.closebutton.nativeElement.click();
      // console.log("after this.transporter : ", this.asnTransporter)
    }

  }

  deleteTransporter(index) {
    this.asnTransporter.splice(index, 1);
  }
  editTransporter(item, k) {
    this.enableEditindex = k;
    this.enableEdit = true;
    this.enableAdd = true;
    this.transporters = this.asnTransporter.find(
      (f) => f.VEHICLE_NUMBER === item.VEHICLE_NUMBER
    );
  }
}
