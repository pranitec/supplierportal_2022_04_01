import { Component, Input, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-invoice-asn',
  templateUrl: './invoice-asn.component.html',
  styleUrls: ['./invoice-asn.component.css']
})
export class InvoiceASNComponent implements OnInit {



  @Input() asnHeader :any;

  @Input() asnLots:any[];
  public supplierName='';
  public bodyFontSize = 10;
  constructor( private spinner: NgxSpinnerService,  public breadCrumbServices: BreadcrumbService) {
  };



  ngOnInit(): void {
    this.supplierName = this.breadCrumbServices.filteredSelection[0].VENDOR_NAME;

  }

  getDocumentList() {
    this.spinner.show();
    // 247380

      this.makePdf();





  }



  // tslint:disable-next-line: max-line-length
  public arrayChunks = (array, chunk_size) => Array(Math.ceil(array.length / chunk_size)).fill('').map((_, index) => index * chunk_size).map(begin => array.slice(begin, begin + chunk_size));

  // dynamicTable() {
  //   const splitted = this.arrayChunks(this.poLines, 4);
  //   const content = [];
  //   splitted.forEach((m,i) => {
  //     if(!(i === (splitted.length-1)) ){
  //       content.push(
  //         {
  //           image: (this.logo === 'Green Prospect Sdn. Bhd. (567515-D)')? this.logogp:this.logoyty,
  //           width:475,
  //           alignment : 'center'
  //         },
  //        this.getHeaderRows(),
  //          this.getProductObject(m,i),

  //         this.getTableRowView(),
  //         this.getTableRowView1(),
  //         this.getSingleBoxData(),{text:'Store receiving goods only from (Mon-Fri) 8:00am to 5:00pm. ',fontSize:8,margin:[10,10,10,10]},
  //        // {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
  //         this.getSignTableWithPageBreak(),
  //         );
  //     }else{
  //       content.push(
  //         {
  //           image:(this.logo === 'Green Prospect Sdn. Bhd. (567515-D)')? this.logogp:this.logoyty,
  //           width:475,
  //           alignment : 'center'
  //         },
  //        this.getHeaderRows(),
  //          this.getProductObject(m,i),

  //         this.getTableRowView(),
  //         this.getTableRowView1(),
  //         this.getSingleBoxData(),{text:'Store receiving goods only from (Mon-Fri) 8:00am to 5:00pm. ',fontSize:8,margin:[10,10,10,10]},
  //        // {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
  //         this.getSignTable(),
  //         );
  //     }

  //   });
  //   return {
  //     margin: [-20, -35, 0, 0],
  //     table: {
  //       widths: [550],
  //       body: [
  //         [content]
  //       ]
  //     }, layout: {defaultBorder: false}
  //   //   , pageBreak: 'after'
  //   };
  // }

  getQRRow(){

return { qr: this.asnHeader.ASN_ID, fit: '75',alignment:'right' }
  }

  getFirstTable(){
    return {
          margin: [-30, 10, 0, 0],
          table: {
            // heights:['*'],
            widths: [136,136,136,136],
            body: [
              [{text:'Advance Shipment Notice',bold:true,fontSize:this.bodyFontSize+2,colSpan: 4,	alignment: 'center'

            },'','',''] ,
              [{text:'Supplier',bold:true,fontSize:this.bodyFontSize
            },{text:'Invoice Number',bold:true,fontSize:this.bodyFontSize
        },{text:'Delivery Date',bold:true,fontSize:this.bodyFontSize
      },{text:'Expected Receipt Date',bold:true,fontSize:this.bodyFontSize
    }],
              [{text:(this.supplierName||'-'),fontSize:this.bodyFontSize
            },{text:(this.asnHeader.DELIVERY_ORDER_NUMBER||'-'),fontSize:this.bodyFontSize
        },{text:(this.asnHeader.DELIVERY_ORDER_DATE||'-'),fontSize:this.bodyFontSize
      },{text:(this.asnHeader.EXPECTED_RECEIPT_DATE||'-'),fontSize:this.bodyFontSize
    }],
              [{text:'Bill of Lading',bold:true,fontSize:this.bodyFontSize
          },{text:'Waybill Number',bold:true,fontSize:this.bodyFontSize
        },{text:'Freight Terms',bold:true,fontSize:this.bodyFontSize
      },{text:'Shipment Method',bold:true,fontSize:this.bodyFontSize
    }],

      [{text:(this.asnHeader.BILL_OF_LADING||'-'),fontSize:this.bodyFontSize
  },{text:(this.asnHeader.WAYBILL_NUMBER||'-'),fontSize:this.bodyFontSize
},{text:(this.asnHeader.FREIGHT_TERMS||'-'),fontSize:this.bodyFontSize
},{text:(this.asnHeader.SHIPPING_METHOD ||'-'),fontSize:this.bodyFontSize
}
],
            ]
          },     layout: {
                    hLineWidth(i, node) {
                        return (i === 0 || i === node.table.body.length) ? 0.1 : 0.1;
                    },
                    vLineWidth(i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 0.1 : 0.1;
                    },
                    hLineColor(i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                    },
                    vLineColor(i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
                    }
                },
        }

  }
getSecondTableBody(){
  return this.asnHeader.list.map(m=>{
    return [
      {text:(m.PO_NUMBER||'-'),fontSize:this.bodyFontSize},
      {text:(m.PO_LINE_NUMBER||'-'),fontSize:this.bodyFontSize},
      {text:(m.PO_SHIPMENT_NUMBER||'-'),fontSize:this.bodyFontSize},
      {text:(m.ITEM_DESCRIPTION||'-'),fontSize:this.bodyFontSize},
      {text:(m.ORDERED_QTY||'-'),fontSize:this.bodyFontSize},
      {text:(m.UOM||'-'),fontSize:this.bodyFontSize},
      {text:(m.BALANCE_QTY||'-'),fontSize:this.bodyFontSize}
    ]
  })
}

  getSecondTable(){
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths:[55,40,65,190,55,40,55],
       // widths: [136,136,136,136],
        body: [ [{text:'PO Number',fontSize:this.bodyFontSize,bold:true},{text:'Line No.',fontSize:this.bodyFontSize,bold:true},{text:'Shipment No.',fontSize:this.bodyFontSize,bold:true},{text:'Item',fontSize:this.bodyFontSize,bold:true},{text:'Ordered Qty',fontSize:this.bodyFontSize,bold:true},{text:'UOM',fontSize:this.bodyFontSize,bold:true},{text:'Shipped Qty',fontSize:this.bodyFontSize,bold:true}],...this.getSecondTableBody()  ]
      },     layout: {
                hLineWidth(i, node) {
                    return (i === 0 || i === node.table.body.length) ? 0.1 : 0.1;
                },
                vLineWidth(i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 0.1 : 0.1;
                },
                hLineColor(i, node) {
                    return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                },
                vLineColor(i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
                }
            },
          }
  }

  getThirdBody(){
    return this.asnLots.map(m=>{
      return [
        {text:(m.PO_NUMBER||'-'),fontSize:this.bodyFontSize},
        {text:(m.PO_LINE_NUMBER||'-'),fontSize:this.bodyFontSize},
        {text:(m.PO_SHIPMENT_NUMBER||'-'),fontSize:this.bodyFontSize},
        {text:(m.LOT_NUMBER||'-'),fontSize:this.bodyFontSize},
        {text:(m.GRADE||'-'),fontSize:this.bodyFontSize},
        {text:(m.QUANTITY||'-'),fontSize:this.bodyFontSize},
        {text:(m.REMARKS||'-'),fontSize:this.bodyFontSize}
      ]
    })
  }

  getThirdTable(){
    return {
      margin: [-30, 10, 0, 0],
      table: {
        // heights:['*'],
        widths:[55,40,65,190,55,40,55],
       // widths: [136,136,136,136],
        body: [ [{text:'PO Number',fontSize:this.bodyFontSize,bold:true},{text:'Line No.',fontSize:this.bodyFontSize,bold:true},{text:'Shipment No.',fontSize:this.bodyFontSize,bold:true},{text:'Lot',fontSize:this.bodyFontSize,bold:true},{text:'Grade',fontSize:this.bodyFontSize,bold:true},{text:'Quantity',fontSize:this.bodyFontSize,bold:true},{text:'Remarks',fontSize:this.bodyFontSize,bold:true}],...this.getThirdBody()  ]
      },     layout: {
                hLineWidth(i, node) {
                    return (i === 0 || i === node.table.body.length) ? 0.1 : 0.1;
                },
                vLineWidth(i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 0.1 : 0.1;
                },
                hLineColor(i, node) {
                    return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                },
                vLineColor(i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
                }
            },
          }
  }


  getDocumentDefinition() {
    return {
      content: [
        this.getQRRow(),
        {text:''+this.asnHeader.ASN_ID,alignment:'right',fontSize:this.bodyFontSize+2,bold:true,margin:[0,5,25,0]},
        this.getFirstTable(),
        {text:'Shipment Lines',fontSize:this.bodyFontSize+2,bold:true,margin:[20,20,0,0]},
        this.getSecondTable(),
        {text:(this.asnLots.length)?'Lot Details':'',fontSize:this.bodyFontSize+2,bold:true,margin:[20,20,0,0]},
        (this.asnLots.length)?(this.getThirdTable()):''


      ],
      info: {
        title: 'ASN INVOICE',
      },
      styles: {
        header: {
          fontSize: 16,
          bold: true,
          margin: [0, 20, 0, 10],
          decoration: 'underline',
        },
        alignment: {
          alignment: 'right',
        },
        tableHeader: {
          bold: true,
        },
        tableExample: {
          margin: [0, 5, 0, 15],
        },
      },
    };
  }

  // getHeaderRows() {
  //   return {
  //     table: {
  //       // headerRows: 1,
  //       heights: [14,10,10,10,10,10,10,10],
  //       widths: [300, 120,130],
  //       body: [
  //         [{text: '',
  //         fontSize: 16,
  //         bold: true,
  //         margin: [0, 20, 0, 10],
  //       }, {text: 'PURCHASE ORDER', fontSize: 14,
  //         bold: true, colSpan: 2,
  //         margin: [0, 20, 0, 10]},''],
  //         //2
  //         [this._textBody(this.poHeader.SUPPLIER_NAME ||'',9),this._textBody('No.',9),this._textBody(this.poHeader.PO_NUMBER||'',9)],
  //         //3
  //         [this._textview(this.poHeader.ADDRESS_LINE1||'',9),this._textview('Prep. By',9),this._textview(this.poHeader.REQUESTED_BY||'',9 )],
  //         //4
  //         [this._textview(this.poHeader.CITY||'',9),this._textview('Date',9),this._textview(this.poHeader.ORDER_DATE ||'',9)],
  //         //5
  //         [this._textview((this.poHeader.STATE||'-') +'-' +(this.poHeader.ZIP ||''),9),this._textview('PR No.',9),this._textview(this.poHeader.PR||'',9)],
  //         //6
  //         [this._textview('Tel:'+(this.poHeader.PHONE||''),9),this._textview('Quotation No.',9),this._textview(this.poHeader.QUOTE_NUMBER||'',9 )],
  //         //7
  //         [this._textview('Fax:'+(this.poHeader.FAX ||''),9),this._textview('Revision No/Date',9),this._textview((this.poHeader.REVISION_NUM||'-')+'/'+(this.poHeader.REVISION_DATE||''),9)],
  //         //8
  //         [this._textview('Attn:'+(this.poHeader.Attn||''),9),this._textview('Payment Terms',9),this._textview(this.poHeader.TERMS||'',9)]
  //       ]
  //     },
  //     layout: {defaultBorder: false},
  //   };
  // }
  _textview(txt, font) {
    return{text: txt, fontSize: font};
  }
getFooterView() {
  return {
    margin: [0, 12, 0, 75],
    table: {
      widths: ['*'],
      body: [
        // tslint:disable-next-line: max-line-length
        [{text: ' Customer note: All your Delivery issues or Replacement or Damage, Please mail to support@farm2bag.com or Whatsapp/text to 9445033734',
        fontSize: 10}]
      ]
    },
      layout: {
          hLineWidth(i, node) {
              return (i === 0 || i === node.table.body.length) ? 1 : 1;
          },
          vLineWidth(i, node) {
              return (i === 0 || i === node.table.widths.length) ? 1 : 1;
          },
          hLineColor(i, node) {
              return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
          },
          vLineColor(i, node) {
              return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
          }
      },
  };
}
//   getTableRowView() {
//     return {
//       margin: [40, 3, 0, 15],
//       table: {
//         widths: [300, '*'],
//         body: [
//           [{text:'Dimension:'+(this.poHeader.DIMENSION_H ||''),fontSize:this.bodyFontSize}],
//           [{text:'Quality:'+(this.poHeader.QUALITY_H ||''),fontSize:this.bodyFontSize}],
//           [{text:'Control No: '+(this.poHeader.CONTROL_NO_H ||''),fontSize:this.bodyFontSize}],
//           [{text:'PR No :'+(this.poHeader.PR ||''),fontSize:this.bodyFontSize}],
//           [{text:'Remarks :'+(this.poHeader.REMARKS_H ||''),fontSize:this.bodyFontSize}],

//         ]
//       },
//       layout: {defaultBorder: false},
//     //   layout: {
//     //     hLineWidth(i, node) {
//     //         return (i === 0 || i === node.table.body.length) ? 1 : 1;
//     //     },
//     //     vLineWidth(i, node) {
//     //         return (i === 0 || i === node.table.widths.length) ? 1 : 1;
//     //     },
//     //     hLineColor(i, node) {
//     //         return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
//     //     },
//     //     vLineColor(i, node) {
//     //         return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
//     //     }
//     // },
//   };
// }
// getTableRowView1() {
//   return {
//     margin: [0, 3, 0, 15],
//     table: {
//       widths: [300, 100,100],
//       body: [
//         [{text:'Gross Amount :',fontSize:this.bodyFontSize,bold:true},{text:this.poHeader.CURRENCY_CODE||'',fontSize:this.bodyFontSize,bold:true,alignment:'right'},{text:this.poHeader.PO_AMOUNT ||'',fontSize:this.bodyFontSize,bold:true,alignment:'right'}],
//         [{text:'(Nett. Amount in Words) :'+this.poHeader.PO_AMOUNT_TEXT ||'',fontSize:this.bodyFontSize,bold:true},{text:'Total  '+this.poHeader.CURRENCY_CODE||'',fontSize:this.bodyFontSize,alignment:'right',bold:true},{text:this.poHeader.PO_AMOUNT ||'',fontSize:this.bodyFontSize,alignment:'right',bold:true}],

//       ]
//     },
//     layout: {defaultBorder: false},
//   //   layout: {
//   //     hLineWidth(i, node) {
//   //         return (i === 0 || i === node.table.body.length) ? 1 : 1;
//   //     },
//   //     vLineWidth(i, node) {
//   //         return (i === 0 || i === node.table.widths.length) ? 1 : 1;
//   //     },
//   //     hLineColor(i, node) {
//   //         return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
//   //     },
//   //     vLineColor(i, node) {
//   //         return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
//   //     }
//   // },
// };
// }

getSingleBoxData(){
return {
  margin: [0, 3, 0, 0],
  table: {
    widths: [525],
    body: [
      [{text:'Please acknowledge us of the receiving of this P/O by fax; and further confirm with the delivery date or else the stated date will be deemed as feasible. Written notification is        required on any changes in raw materials, combination of contents, country of origin, process, forms, packaging and labelling methods and whatsoever, prior to the delivery.        COA is required for chemical goods. Thank you',fontSize:7,bold:true}],

    ]
  },
  //layout: {defaultBorder: false},
//   layout: {
//     hLineWidth(i, node) {
//         return (i === 0 || i === node.table.body.length) ? 1 : 1;
//     },
//     vLineWidth(i, node) {
//         return (i === 0 || i === node.table.widths.length) ? 1 : 1;
//     },
//     hLineColor(i, node) {
//         return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
//     },
//     vLineColor(i, node) {
//         return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
//     }
// },
};

}

getSignTable(){
return {
  margin: [0, 3, 0, 15],
  table: {
    widths: [175,175,175],
    body: [
      [{text:'Meganathan, Ms. Letchumi',fontSize:8,bold:true,alignment:'center'},{text:'',fontSize:8,bold:true,alignment:'center'},{text:'Pahwa, Mr. Rakesh',fontSize:8,bold:true,alignment:'center'}],
      [ {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 110, y2: 5, lineWidth: 1 }],alignment:'center'}, {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 110, y2: 5, lineWidth: 1 }],alignment:'center'}, {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 110, y2: 5, lineWidth: 1 }],alignment:'center'},],
      [{text:'Ordered By ',fontSize:10,bold:true,alignment:'center'},{text:'Checked By',fontSize:10,bold:true,alignment:'center'},{text:'Approved',fontSize:10,bold:true,alignment:'center'}],
    ]
  },
  layout: {defaultBorder: false},

};
}
getSignTableWithPageBreak(){
return {
  margin: [0, 3, 0, 15],
  table: {
    widths: [175,175,175],
    body: [
      [{text:'Meganathan, Ms. Letchumi',fontSize:8,bold:true,alignment:'center'},{text:'',fontSize:8,bold:true,alignment:'center'},{text:'Pahwa, Mr. Rakesh',fontSize:8,bold:true,alignment:'center'}],
      [ {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 110, y2: 5, lineWidth: 1 }],alignment:'center'}, {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 110, y2: 5, lineWidth: 1 }],alignment:'center'}, {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 110, y2: 5, lineWidth: 1 }],alignment:'center'},],
      [{text:'Ordered By ',fontSize:10,bold:true,alignment:'center'},{text:'Checked By',fontSize:10,bold:true,alignment:'center'},{text:'Approved',fontSize:10,bold:true,alignment:'center'}],
    ]
  },
  layout: {defaultBorder: false},
pageBreak:"after"
};
}

  //   getProductObject(productList,i) {
  //   return {
  //     margin: [0, 15, 0, 0],
  //     table: {
  //       widths: [30, 160, '*', '*', '*','*','*','*'],
  //       body: [
  //         [{
  //           text: 'S.NO',
  //           style: 'tableHeader',
  //           alignment: 'center',
  //           fontSize:this.bodyFontSize
  //         },
  //         {
  //           text: 'Item Description',
  //           style: 'tableHeader',
  //           alignment: 'center',
  //           fontSize:this.bodyFontSize
  //         },
  //         {
  //           text: 'ETA',
  //           style: 'tableHeader',
  //           alignment: 'center',
  //           fontSize:this.bodyFontSize
  //         },
  //         {
  //           text: 'Qty ',
  //           style: 'tableHeader',
  //           alignment: 'center',
  //           fontSize:this.bodyFontSize
  //         },
  //         {
  //           text: 'UOM',
  //           style: 'tableHeader',
  //           alignment: 'center',
  //           fontSize:this.bodyFontSize
  //         },
  //         {
  //           text: 'Disc%',
  //           style: 'tableHeader',
  //           alignment: 'center',
  //           fontSize:this.bodyFontSize
  //         },
  //         {
  //           text: 'Unit Price\n'+(this.poHeader.CURRENCY_CODE?'('+this.poHeader.CURRENCY_CODE +')':''),
  //           style: 'tableHeader',
  //           alignment: 'center',
  //           fontSize:this.bodyFontSize
  //         },
  //         {
  //           text: 'Amount\n'+(this.poHeader.CURRENCY_CODE?'('+this.poHeader.CURRENCY_CODE +')':''),
  //           style: 'tableHeader',
  //           alignment: 'center',
  //           fontSize:this.bodyFontSize
  //         },
  //         ],

  //         ...productList.map((product, index) => {
  //           // tslint:disable-next-line: max-line-length
  //           return [{text: index + 1 +(i*4), alignment: 'center', fontSize:this.bodyFontSize}, {text:[product.ITEM_DESCRIPTION ||'',
  //           //'\n ','\n '
  //           // {text:'\nLot No:',bold:true},' 542512',{text:'\nMfg Date :',bold:true},
  //           // ' 2020-07'
  //         ], fontSize:this.bodyFontSize }, {text: product.ETA ||'08-JUL-20', alignment: 'right', fontSize:this.bodyFontSize}, {text: product.QUANTITY||'' , alignment: 'right', fontSize:this.bodyFontSize}, {text: product.UNIT_MEAS_LOOKUP_CODE ||'', alignment: 'right', fontSize:this.bodyFontSize},'',{text: product.UNIT_PRICE||'' , alignment: 'right', fontSize:this.bodyFontSize},{text: product.AMOUNT||'' , alignment: 'right', fontSize:this.bodyFontSize}];
  //         })
  //       ]
  //     }
  //   };
  // }

    _textBody(txt, font) {
    return{text: txt, fontSize: font, bold: true};
  }
    makePdf() {
  //   const doc = new jspdf();
  // const DATA = this.content.nativeElement;
  //   doc.addHTML(DATA, () => {
  //     doc.save('f2bOrder.pdf');
  //   });
  // }
    const documentDefinition = this.getDocumentDefinition();
    const temp = JSON.stringify(documentDefinition);
    const json = JSON.parse(temp);
    // pdfMake.createPdf(json).open();
    pdfMake.createPdf(json).open()//('F2bOrderInvoice.pdf');
    this.spinner.hide();
  }

generatePdf(){
  this.getDocumentList();
  // const documentDefinition = { content: 'This is an sample PDF printed with pdfMake' };
  // pdfMake.createPdf(documentDefinition).open();
 }

}
