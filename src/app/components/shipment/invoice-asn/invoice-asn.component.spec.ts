import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceASNComponent } from './invoice-asn.component';

describe('InvoiceASNComponent', () => {
  let component: InvoiceASNComponent;
  let fixture: ComponentFixture<InvoiceASNComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceASNComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceASNComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
