import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-view-delivery-performance',
  templateUrl: './view-delivery-performance.component.html',
  styleUrls: ['./view-delivery-performance.component.css']
})
export class ViewDeliveryPerformanceComponent implements OnInit {

  public valuek = 0;

  /*line graph*/
  lineChartData: ChartDataSets[] = [
    {
      data: [0, 4, 3, 8, 15, 10, 7, 10, 12, 14, 17, 20],
      // label: 'Earnings',
      lineTension: 0.3,
      // backgroundColor: '#8bdeac',
      // borderColor: '#8bdeac',
      pointRadius: 3,
      pointBackgroundColor: '#dc5257',
      pointBorderColor: '#dc5257',
      pointHoverRadius: 3,
      pointHoverBackgroundColor: 'rgba(78, 115, 223, 1)',
      pointHoverBorderColor: 'rgba(78, 115, 223, 1)',
      pointHitRadius: 10,
      pointBorderWidth: 2,
    },
  ];

  lineChartLabels: Label[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  lineChartOptions = {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 25,
        bottom: 0
      }
    },
    tooltips: {
      backgroundColor: 'rgb(255,255,255)',
      bodyFontColor: '#858796',
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
    },
    responsive: true,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7,
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          callback: function (value, index, values) {
            return value + '%';
          }
        },
        gridLines: {
          display: false
        }
      }]
    }
  };


  lineChartColors: Color[] = [
    {

      borderColor: '#8bdeac',
      backgroundColor: 'white',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  /*line graph*/

  lineChartData2: ChartDataSets[] = [
    {
      data: [0, 4, 3, 8, 15, 20],
      // label: 'Earnings',
      lineTension: 0.3,
      // backgroundColor: '#8bdeac',
      // borderColor: '#8bdeac',
      pointRadius: 3,
      pointBackgroundColor: '#dc5257',
      pointBorderColor: '#dc5257',
      pointHoverRadius: 3,
      pointHoverBackgroundColor: 'rgba(78, 115, 223, 1)',
      pointHoverBorderColor: 'rgba(78, 115, 223, 1)',
      pointHitRadius: 10,
      pointBorderWidth: 2,
    },
  ];

  lineChartLabels2: Label[] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"];

  lineChartOptions2 = {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 25,
        bottom: 0
      }
    },
    tooltips: {
      backgroundColor: 'rgb(255,255,255)',
      bodyFontColor: '#858796',
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
    },
    responsive: true,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7,
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          callback: function (value, index, values) {
            return value;
          }
        },
        gridLines: {
          display: false
        }
      }]
    }
  };


  lineChartColors2: Color[] = [
    {

      borderColor: '#8bdeac',
      backgroundColor: 'white',
    },
  ];

  lineChartLegend2 = true;
  lineChartPlugins2 = [];
  lineChartType2 = 'line';

  /*line graph1*/

  /*Bar graph*/

  // public barChartOptions: ChartOptions = {
  //   maintainAspectRatio: false,
  //   responsive: true,
  //   layout: {
  //     padding: {
  //       left: 10,
  //       right: 25,
  //       top: 25,
  //       bottom: 0
  //     }
  //   },
  //   scales: {
  //     xAxes: [{
  //       stacked: true,
  //       position: 'bottom',
  //       display: true,
  //       gridLines: {
  //         display: false,
  //         drawBorder: false,
  //       },
  //       ticks: {
  //         display: true, //this will remove only the label
  //         maxTicksLimit: 7
  //       }
  //     }],
  //     yAxes: [{
  //       stacked: true,
  //       display: true,

  //       ticks: {
  //         maxTicksLimit: 7,
  //         padding: 10,
  //         beginAtZero: true,
  //         callback: function (value) {
  //           return value;
  //         }

  //       },
  //     }]
  //   },
  //   legend: {
  //     display: true,
  //     position: 'top',

  //     labels: {
  //       fontSize: 15,
  //       boxWidth: 40,
  //     }
  //   },
  //   tooltips: {

  //     backgroundColor: "#858796",
  //     bodyFontColor: "rgb(255,255,255)",
  //     titleMarginBottom: 10,
  //     titleFontColor: 'rgb(255,255,255)',
  //     titleFontSize: 14,
  //     borderColor: '#dddfeb',
  //     borderWidth: 1,
  //     xPadding: 15,
  //     yPadding: 15,
  //     displayColors: false,
  //     intersect: false,
  //     mode: 'label',
  //     caretPadding: 10,

  //   },
  //   plugins: {
  //     datalabels: {
  //       display: false,
  //       align: 'center',
  //       anchor: 'center'
  //     }
  //   },

  // };
  // public barChartLabels: Label[] = ["item 1", "item 2", "item 3", "item 4", "item 5"];
  // public barChartType: ChartType = 'bar';
  // public barChartLegend = true;
  // public barChartPlugins = [];

  // public barChartData: ChartDataSets[] = [

  //   {
  //     data: [21421, 22145, 15014, 15441, 15441], label: 'Reject', backgroundColor: "#6a8d92",
  //     hoverBackgroundColor: "#6a8d92"
  //   },
  //   {
  //     data: [6310, 5742, 4044, 5564, 15441], label: 'Impact', backgroundColor: "#8bdeac",
  //     hoverBackgroundColor: "#8bdeac"
  //   },
  //   {
  //     data: [11542, 12400, 12510, 11450, 15441], label: 'No Impact', backgroundColor: "#dc5257",
  //     hoverBackgroundColor: "#dc5257"
  //   },


  // ];


  /*Bar graph*/

  /*Bar graph3*/

  // public barChartOptions3: ChartOptions = {
  //   maintainAspectRatio: false,
  //   responsive: true,
  //   layout: {
  //     padding: {
  //       left: 10,
  //       right: 25,
  //       top: 25,
  //       bottom: 0
  //     }
  //   },
  //   scales: {
  //     xAxes: [{
  //       stacked: true,
  //       position: 'bottom',
  //       display: true,
  //       gridLines: {
  //         display: false,
  //         drawBorder: false,
  //       },
  //       ticks: {
  //         display: true, //this will remove only the label
  //         maxTicksLimit: 10
  //       }
  //     }],
  //     yAxes: [{
  //       stacked: true,
  //       display: true,
  //       // gridLines: {
  //       //   color: "rgb(234, 236, 244)",
  //       //   zeroLineColor: "rgb(234, 236, 244)",
  //       //   drawBorder: false,
  //       //   borderDash: [2],
  //       //   zeroLineBorderDash: [2]
  //       // },
  //       ticks: {
  //         maxTicksLimit: 10,
  //         padding: 10,
  //         beginAtZero: true,
  //         callback: function (value) {
  //           return value;
  //         }

  //       },
  //     }]
  //   },
  //   legend: {
  //     display: true,
  //     position: 'top',
  //     // align:'end',
  //     labels: {
  //       fontSize: 15,
  //       boxWidth: 40,
  //     }
  //   },
  //   tooltips: {

  //     backgroundColor: "#858796",
  //     bodyFontColor: "rgb(255,255,255)",
  //     titleMarginBottom: 10,
  //     titleFontColor: 'rgb(255,255,255)',
  //     titleFontSize: 14,
  //     borderColor: '#dddfeb',
  //     borderWidth: 1,
  //     xPadding: 15,
  //     yPadding: 15,
  //     displayColors: false,
  //     intersect: false,
  //     mode: 'label',
  //     caretPadding: 10,

  //   },
  //   plugins: {
  //     datalabels: {
  //       display: false,
  //       align: 'center',
  //       anchor: 'center'
  //     }
  //   },

  // };
  // public barChartLabels3: Label[] = ["item 1", "item 2", "item 3", "item 4", "item 5"];
  // public barChartType3: ChartType = 'horizontalBar';
  // public barChartLegend3 = true;
  // public barChartPlugins3 = [];

  // public barChartData3: ChartDataSets[] = [

  //   {
  //     data: [17, 11, 22, 18, 12], label: 'Early', backgroundColor: "#8bdeac",
  //     hoverBackgroundColor: "#8bdeac"
  //   },
  //   {
  //     data: [19, 22, 17, 15, 14], label: 'On Time', backgroundColor: "#dc5257",
  //     hoverBackgroundColor: "#dc5257",
  //   },
  //   {
  //     data: [40, 47, 44, 38, 27], label: 'Late', backgroundColor: "#fae377",
  //     hoverBackgroundColor: "#fae377",
  //   },


  // ];

  /*Bar graph3*/

  /*Bar graph2*/

  public barChartOptions2: ChartOptions = {
    maintainAspectRatio: false,
    responsive: true,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        stacked: true,
        position: 'bottom',
        display: true,
        gridLines: {
          display: false,
          drawBorder: false,
        },
        ticks: {
          display: true, //this will remove only the label
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        stacked: true,
        display: true,
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        },
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          callback: function (value, index, values) {
            return value + '%';
          }
        },
      }]
    },
    legend: {
      display: false
    },
    tooltips: {

      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,

    },
    plugins: {
      datalabels: {
        display: false,
        align: 'center',
        anchor: 'center'
      }
    }
  };
  public barChartLabels2: Label[] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  public barChartType2: ChartType = 'bar';
  public barChartLegend2 = true;
  public barChartPlugins2 = [];

  public barChartData2: ChartDataSets[] = [
    {
      label: '',
      data: [0, 4, 3, 8, 15, 20, 24, 14, 34, 4, 24, 33],
      backgroundColor: "#8bdeac",
      borderColor: "#8bdeac",
      pointRadius: 3,
      pointBackgroundColor: "#dc5257",
      pointBorderColor: "#dc5257",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      lineTension: 0.3,
      borderWidth: 1,
      fill: false
    }
  ];


  /*Bar graph2*/

  public email;
  public defectAll = [];
  public defect = [];
  public defectPeriodAll = [];
  public defectPeriod = [];
  public ontimeAll = [];
  public ontime = [];
  public ontimePeriod = [];
  public ontimePeriodAll = [];
  public lead = [];
  public leadAll = [];
  public leadPeriodAll = [];
  public leadPeriod = [];
  public defectCount = 0;
  public ontimeCount = 0;
  public leadCount = 0;

  constructor(public breadCrumbServices: BreadcrumbService, private router: Router,
    private purchaseService: PurchaseService, public rsaService: RsaService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/shipment/viewDelivery') {
        this.setFilteredData();
      }
    });
    // this.spinner.show();
    this.getDefectRate();
    this.getDefectRatePeriod();
    this.getOnTime();
    this.getOnTimePeriod();
    this.getLeadTime();
    this.getLeadTimePeriod();
  }



  getDefectRate() {
    this.spinner.show();
    let defectCount = 0;
    let noOfLines1 =0
    // const details = { email: this.email, purchaseType: 'DB_PO_DEFECT_RATE' };
    var details = this.purchaseService.inputJSON('DB_PO_DEFECT_RATE',  undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.spinner.hide();
      // console.log('defect', data);
      this.defectAll = data ? data.DBDEFECTRATES.map(x=>(x.DBDEFECTRATE)) :[]
      // this.defectAll = Array.isArray(data.DBDEFECTRATES.DBDEFECTRATE) ? data.DBDEFECTRATES.DBDEFECTRATE : (data.DBDEFECTRATES.DBDEFECTRATE) ? [data.DBDEFECTRATES.DBDEFECTRATE] : [];
      this.defect = this.setFilteredContent(this.defectAll);
      // this.defect.forEach(f => {
      //   if (defectCount) {
      //     defectCount = (defectCount + Number(f.DEFECT_RATE)) / 2;
      //   }
      //   else {
      //     defectCount = defectCount + Number(f.DEFECT_RATE);
      //   }
      // });
      this.defect.forEach(f => {
        if(f.DEFECT_RATE){
          defectCount = defectCount + Number(f.DEFECT_RATE)
        noOfLines1 = noOfLines1 +1
        }
      });
      if(noOfLines1 ===0 ){
        defectCount =0
      }else{
        defectCount = Math.round(defectCount / noOfLines1);
      }
      this.defectCount = defectCount;
    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }

  getDefectRatePeriod() {
    this.spinner.show();
    // const details = { email: this.email, purchaseType: 'DB_DEFECT_RATE_PERIODWISE' };
    var details = this.purchaseService.inputJSON('DB_DEFECT_RATE_PERIODWISE',  undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('defectPeriod', data);
      this.spinner.hide();
      this.defectPeriodAll = data.DBDEFECTRATEPERIODS ? data.DBDEFECTRATEPERIODS.map(x=>(x.DBDEFECTRATEPERIOD)) :[]
      // this.defectPeriodAll = Array.isArray(data.DBDEFECTRATEPERIODS.DBDEFECTRATEPERIOD) ? data.DBDEFECTRATEPERIODS.DBDEFECTRATEPERIOD : (data.DBDEFECTRATEPERIODS.DBDEFECTRATEPERIOD) ? [data.DBDEFECTRATEPERIODS.DBDEFECTRATEPERIOD] : [];
      this.defectPeriod = JSON.parse(JSON.stringify(this.setFilteredContent(this.defectPeriodAll.sort((a, b) => Number(a.PERIOD_NUM) - Number(b.PERIOD_NUM)))));
      let arr = {};
      this.defectPeriod.forEach((f, i) => {
        if (arr[f.PAYMENT_PERIOD]) {
          arr[f.PAYMENT_PERIOD].DEFECT_RATE = (Number(arr[f.PAYMENT_PERIOD].DEFECT_RATE) + Number(f.DEFECT_RATE)) / 2;
        } else {
          arr[f.PAYMENT_PERIOD] = f;
        }
        if (i === this.defectPeriod.length - 1) {
          this.setGraphData(arr);
        }
      });
    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }

  setGraphData(arr) {
    // console.log('INSIDE', arr, Object.entries(arr));
    this.lineChartData[0].data = (Object.entries(arr)).map((m: any) => m[1].DEFECT_RATE);
    this.lineChartLabels = (Object.entries(arr)).map(m => m[0]);
  }

  getOnTime() {
    let ontimeCount = 0;
    let noOfLines =0;
    this.spinner.show();
    // const details = { email: this.email, purchaseType: 'DB_ONTIME_SUPPLY' };
    var details = this.purchaseService.inputJSON('DB_ONTIME_SUPPLY',  undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      console.log('Ontime', data);
      if( data.DBONTIMESUPPLIES){
      this.spinner.hide();
      this.ontimeAll = data.DBONTIMESUPPLIES ? data.DBONTIMESUPPLIES.map(x=>(x.DBONTIMESUPPLY)) :[]
      // this.ontimeAll = Array.isArray(data.DBONTIMESUPPLIES.DBONTIMESUPPLY) ? data.DBONTIMESUPPLIES.DBONTIMESUPPLY : (data.DBONTIMESUPPLIES.DBONTIMESUPPLY) ? [data.DBONTIMESUPPLIES.DBONTIMESUPPLY] : [];
      this.ontime = this.setFilteredContent(this.ontimeAll);
      // this.ontime.forEach(f => {
      //   if (ontimeCount) {
      //     ontimeCount = ontimeCount + Number(f.ONTIME_SUPPLY) / 2;
      //   }
      //   else {
      //     ontimeCount = ontimeCount + Number(f.ONTIME_SUPPLY);
      //   }
      // });
      this.ontime.forEach(f => {
        if(f.ONTIME_SUPPLY){
          ontimeCount = ontimeCount + Number(f.ONTIME_SUPPLY)
          noOfLines = noOfLines +1
        }
      });
    }else{
      ontimeCount = 0;
      }
      if(noOfLines ===0 ){
        ontimeCount =0
      }else{
        ontimeCount = Math.round(ontimeCount / noOfLines);
      }
      this.ontimeCount = ontimeCount;
    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }

  getOnTimePeriod() {
    this.spinner.show();
    // const details = { email: this.email, purchaseType: 'DB_ONTIME_SUPPLY_PERIODWISE' };
    var details = this.purchaseService.inputJSON('DB_ONTIME_SUPPLY_PERIODWISE',  undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('ontimeperiod', data);
      this.ontimePeriodAll = data.DBONTIMESUPPLYPERIODS ? data.DBONTIMESUPPLYPERIODS.map(x=>(x.DBONTIMESUPPLYPERIOD)) :[]
      // this.ontimePeriodAll = Array.isArray(data.DBONTIMESUPPLYPERIODS.DBONTIMESUPPLYPERIOD) ? data.DBONTIMESUPPLYPERIODS.DBONTIMESUPPLYPERIOD : (data.DBONTIMESUPPLYPERIODS.DBONTIMESUPPLYPERIOD) ? [data.DBONTIMESUPPLYPERIODS.DBONTIMESUPPLYPERIOD] : [];
      this.ontimePeriod = JSON.parse(JSON.stringify(this.setFilteredContent(this.ontimePeriodAll.sort((a, b) => Number(a.PERIOD_NUM) - Number(b.PERIOD_NUM)))));
      let arr = {};
      this.ontimePeriod.forEach((f, i) => {
        if (arr[f.ONTIME_PERIOD]) {
          arr[f.ONTIME_PERIOD].ONTIME_SUPPLY = (Number(arr[f.ONTIME_PERIOD].ONTIME_SUPPLY) + Number(f.ONTIME_SUPPLY)) / 2;
        } else {
          arr[f.ONTIME_PERIOD] = f;
        }
        if (i === this.ontimePeriod.length - 1) {
          this.setBarData(arr);
        }
      });
    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }

  setBarData(arr) {
    // console.log('INSIDE', arr, Object.entries(arr));
    this.barChartData2[0].data = (Object.entries(arr)).map((m: any) => m[1].ONTIME_SUPPLY);
    this.barChartLabels2 = (Object.entries(arr)).map(m => m[0]);
  }

  getLeadTime() {
    let leadCount = 0;
    let noOfLines =0
    this.spinner.show();
    // const details = { email: this.email, purchaseType: 'DB_PO_LEAD_TIME' };
    var details = this.purchaseService.inputJSON('DB_PO_LEAD_TIME',  undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('lead', data);
      this.spinner.hide();
      if(data.DBPOLEADTIMES){
      this.leadAll = data.DBPOLEADTIMES ? data.DBPOLEADTIMES.map(x=>(x.DBPOLEADTIME)) :[]
      // this.leadAll = Array.isArray(data.DBPOLEADTIMES.DBPOLEADTIME) ? data.DBPOLEADTIMES.DBPOLEADTIME : (data.DBPOLEADTIMES.DBPOLEADTIME) ? [data.DBPOLEADTIMES.DBPOLEADTIME] : [];
      this.lead = this.setFilteredContent(this.leadAll);
      // this.lead.forEach(f => {
      //   if (leadCount) {
      //   leadCount = (leadCount + Number(f.LEAD_TIME)) / 2 ;
      //   }else{
      //     leadCount = leadCount + Number(f.LEAD_TIME);
      //   }
      // });
      this.lead.forEach(f => {
        if(f.LEAD_TIME){
          leadCount = leadCount + Number(f.LEAD_TIME)
        noOfLines = noOfLines +1
        }
      });
    }else{
      leadCount = 0;
      }
      if(noOfLines ===0 ){
        leadCount =0
      }else{
        leadCount = Math.round(leadCount / noOfLines);
      }
      this.leadCount = leadCount;
    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }

  getLeadTimePeriod() {
    this.spinner.show();
    // const details = { email: this.email, purchaseType: 'DB_LEAD_TIME_PERIODWISE' };
    var details = this.purchaseService.inputJSON('DB_LEAD_TIME_PERIODWISE',  undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log('leadPeriod', data);
      this.spinner.hide();
      this.leadPeriodAll = data.DBLEADTIMEPERIODS ? data.DBLEADTIMEPERIODS.map(x=>(x.DBLEADTIMEPERIOD)) :[]
      // this.leadPeriodAll = Array.isArray(data.DBLEADTIMEPERIODS.DBLEADTIMEPERIOD) ? data.DBLEADTIMEPERIODS.DBLEADTIMEPERIOD : (data.DBLEADTIMEPERIODS.DBLEADTIMEPERIOD) ? [data.DBLEADTIMEPERIODS.DBLEADTIMEPERIOD] : [];
      this.leadPeriod = JSON.parse(JSON.stringify(this.setFilteredContent(this.leadPeriodAll.sort((a, b) => Number(a.PERIOD_NUM) - Number(b.PERIOD_NUM)))));
      let arr = {};
      this.leadPeriod.forEach((f, i) => {
        if (arr[f.LEAD_PERIOD]) {
          arr[f.LEAD_PERIOD].LEAD_TIME = Number(arr[f.LEAD_PERIOD].LEAD_TIME) + Number(f.LEAD_TIME);
        } else {
          arr[f.LEAD_PERIOD] = f;
        }
        if (i === this.leadPeriod.length - 1) {
          this.setGraphData1(arr);
        }
      });

    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }

  setGraphData1(arr) {
    // console.log('INSIDE', arr, Object.entries(arr));
    this.lineChartData2[0].data = (Object.entries(arr)).map((m: any) => m[1].LEAD_TIME);
    this.lineChartLabels2 = (Object.entries(arr)).map(m => m[0]);
  }

  setFilteredData() {
    // defectCount
    let defectCount = 0;
    let noOfLines1 =0
    this.defect = this.setFilteredContent(this.defectAll);
    // this.defect.forEach(f => {
    //   if (defectCount) {
    //     defectCount = (defectCount + Number(f.DEFECT_RATE)) / 2;
    //   }
    //   else {
    //     defectCount = defectCount + Number(f.DEFECT_RATE);
    //   }
    // });
    this.defect.forEach(f => {
      if(f.DEFECT_RATE){
        defectCount = defectCount + Number(f.DEFECT_RATE)
      noOfLines1 = noOfLines1 +1
      }
    });
    if(noOfLines1 ===0 ){
      defectCount =0
    }else{
      defectCount = Math.round(defectCount / noOfLines1);
    }

    this.defectCount = defectCount;

    //onTime
    let ontimeCount = 0;
    let noOfLines =0
    this.ontime = this.setFilteredContent(this.ontimeAll);
    // this.ontime.forEach(f => {
    //   if (ontimeCount) {
    //     ontimeCount = ontimeCount + Number(f.ONTIME_SUPPLY) / 2;
    //   }
    //   else {
    //     ontimeCount = ontimeCount + Number(f.ONTIME_SUPPLY);
    //   }
    // });
    this.ontime.forEach(f => {
      if(f.ONTIME_SUPPLY){
        ontimeCount = ontimeCount + Number(f.ONTIME_SUPPLY)
      noOfLines = noOfLines +1
      }
    });

    if(noOfLines ===0 ){
      ontimeCount =0
    }else{
      ontimeCount = Math.round(ontimeCount / noOfLines);
    }

    this.ontimeCount = ontimeCount;

    //leadTime
    let leadCount = 0;
    this.lead = this.setFilteredContent(this.leadAll);
    this.lead.forEach(f => {

        leadCount = leadCount + Number(f.LEAD_TIME);

      });
    this.leadCount = leadCount;

    //linegraph1

    this.defectPeriod = JSON.parse(JSON.stringify(this.setFilteredContent(this.defectPeriodAll.sort((a, b) => Number(a.PERIOD_NUM) - Number(b.PERIOD_NUM)))));
    let arr = {};
    this.defectPeriod.forEach((f, i) => {
      if (arr[f.PAYMENT_PERIOD]) {
        arr[f.PAYMENT_PERIOD].DEFECT_RATE = (Number(arr[f.PAYMENT_PERIOD].DEFECT_RATE) + Number(f.DEFECT_RATE)) / 2;
      } else {
        arr[f.PAYMENT_PERIOD] = f;
      }
      if (i === this.defectPeriod.length - 1) {
        this.setGraphData(arr);
      }
    });

   //bargraph1

    this.ontimePeriod = JSON.parse(JSON.stringify(this.setFilteredContent(this.ontimePeriodAll.sort((a, b) => Number(a.PERIOD_NUM) - Number(b.PERIOD_NUM)))));
    let arr1 = {};
    this.ontimePeriod.forEach((f, i) => {
      if (arr1[f.ONTIME_PERIOD]) {
        arr1[f.ONTIME_PERIOD].ONTIME_SUPPLY = (Number(arr1[f.ONTIME_PERIOD].ONTIME_SUPPLY) + Number(f.ONTIME_SUPPLY)) / 2;
      } else {
        arr1[f.ONTIME_PERIOD] = f;
      }
      if (i === this.ontimePeriod.length - 1) {
        this.setBarData(arr1);
      }
    });

    //linegraph2

    this.leadPeriod = JSON.parse(JSON.stringify(this.setFilteredContent(this.leadPeriodAll.sort((a, b) => Number(a.PERIOD_NUM) - Number(b.PERIOD_NUM)))));
    let arr3 = {};
    this.leadPeriod.forEach((f, i) => {
      if (arr3[f.LEAD_PERIOD]) {
        arr3[f.LEAD_PERIOD].LEAD_TIME = Number(arr3[f.LEAD_PERIOD].LEAD_TIME) + Number(f.LEAD_TIME);
      } else {
        arr3[f.LEAD_PERIOD] = f;
      }
      if (i === this.leadPeriod.length - 1) {
        this.setGraphData1(arr3);
      }
    });

  }

  setFilteredContent(data) {

    return data.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }

      return a;

    });

  }

}
