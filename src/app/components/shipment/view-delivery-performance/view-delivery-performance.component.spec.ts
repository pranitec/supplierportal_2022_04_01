import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDeliveryPerformanceComponent } from './view-delivery-performance.component';

describe('ViewDeliveryPerformanceComponent', () => {
  let component: ViewDeliveryPerformanceComponent;
  let fixture: ComponentFixture<ViewDeliveryPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDeliveryPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDeliveryPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
