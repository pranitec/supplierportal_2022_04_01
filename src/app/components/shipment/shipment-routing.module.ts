import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewReceiptsComponent } from './view-receipts/view-receipts.component';
import { ViewReturnsComponent } from './view-returns/view-returns.component';
import { ViewDeliveryPerformanceComponent } from './view-delivery-performance/view-delivery-performance.component';
import { AdvanceShipmentNoticeComponent } from './advance-shipment-notice/advance-shipment-notice.component';
import { CreateAsnComponent } from './create-asn/create-asn.component';
import { EditAsnComponent} from './edit-asn/edit-asn.component';
import { IntransitASNComponent}from './intransit-asn/intransit-asn.component'
import { AuthGuardService } from 'src/app/shared/service/auth-guard.service';

const routes: Routes = [
  {
    path: 'viewreceipts',
    component: ViewReceiptsComponent,
    data: {
      title: 'View Receipts',
      breadcrumb: 'View Receipts'
    },
    canActivate: [AuthGuardService]
  },

  {
    path: 'viewreturns',
    component: ViewReturnsComponent,
    data: {
      title: 'View Returns',
      breadcrumb: 'View Returns'
    },
    canActivate: [AuthGuardService]
  },

  {
    path: 'viewDelivery',
    component: ViewDeliveryPerformanceComponent,
    data: {
      title: 'View Delivery Performance',
      breadcrumb: 'View Delivery Performance'
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'advanceShipments',
    component: AdvanceShipmentNoticeComponent,
    data: {
      title: 'Advance Shipment Notice',
      breadcrumb: 'Advance Shipment Notice'
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'createasn',
    component: CreateAsnComponent,
    data: {
      title: 'Create ASN',
      breadcrumb: 'Create ASN'
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'editasn',
    component: EditAsnComponent,
    data: {
      title: 'Edit ASN',
      breadcrumb: 'Edit ASN'
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'intransitAsn',
    component: IntransitASNComponent,
    data: {
      title: 'Intransit ASN',
      breadcrumb: 'Intransit ASN'
    },
    canActivate: [AuthGuardService]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShipmentRoutingModule { }
