import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvanceShipmentNoticeComponent } from './advance-shipment-notice.component';

describe('AdvanceShipmentNoticeComponent', () => {
  let component: AdvanceShipmentNoticeComponent;
  let fixture: ComponentFixture<AdvanceShipmentNoticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvanceShipmentNoticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceShipmentNoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
