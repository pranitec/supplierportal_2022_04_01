import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
import pdfMake from "pdfmake/build/pdfmake";
import { EmailService } from "../../auth/service/email.service";
import { SidebarService } from 'src/app/shared/service/sidebar.service';
import { NotifierService } from 'angular-notifier';
import { profileData } from 'src/app/config/config'

declare var jQuery: any;
@Component({
  selector: 'app-advance-shipment-notice',
  templateUrl: './advance-shipment-notice.component.html',
  styleUrls: ['./advance-shipment-notice.component.css']
})
export class AdvanceShipmentNoticeComponent implements OnInit {
  public email = '';
  public asnHeaderAll = [];
  public ASN_ID;
  public editReason;
  public deleteReason;
  public asnLineAll = [];
  public asnHeader = [];
  public asnLine = [];
  public asnLotsAll = [];
  public asnLots = [];
  public asnTransporterAll =[];
  public asnTransporter = [];
  public lotListView = true;
  public lotDetails = [];
  public searchText = '';
  public accountType;
  public viewCount = 10;
  public page =1;
  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();

  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
  // public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate()));

  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth() -1]
  // + '-' + this.oneMonthBefore.getFullYear();
  // public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

  public action = false;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst
  public bodyFontSize = 10;
  public asn_id;
  public supplierName;
  public asnHeader1;
  public asnLines1=[];
  public asnlots1 =[];
  public asnTransporterbyASNID;
  public printdocumentFlag;
  public gateNo ;
  public gateDate;
  public viewAsnFlag= false;
  createAsnflag;
  private notifier: NotifierService;
  public noOfMonths;
  public selectedmenu;
  public selectedmenuFilter =[];
  public oneyearstartingDate : Date;
  public docid;

  constructor(private purchaseService: PurchaseService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService,
    private breadCrumbServices: BreadcrumbService, private router: Router, private rsaService: RsaService,
    public emailService: EmailService, private route: ActivatedRoute,  public sidebarService : SidebarService,
    private notifierService: NotifierService){
      this.notifier = notifierService;
      this.route.queryParams.subscribe((data) => {
        this.docid = data['docid'];
      });
        }

  ngOnInit(): void {
    this.createAsnflag= this.sidebarService.checkMenuAccess('CREATE_ASN');
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.accountType = this.rsaService.decrypt(localStorage.getItem('6'));
    // this.supplierName = this.breadCrumbServices.filteredSelection[0].VENDOR_NAME;
    // this.accountType = 'SUPPLIER';
    this.selectedmenu= this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
    this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(({MENUACCESS})=> MENUACCESS.MENU_CODE === 'VIEW_ASN');
    this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START)
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.noOfMonths  = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE /31)
    localStorage.setItem('ASNDF', this.fromDate);
    localStorage.setItem('ASNDT', this.toDate);
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/shipment/advanceShipments') {
        this.setFilteredData();
      }
    });
    (function ($, _this) {

      $(document).ready(function () {
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        const today = new Date();
        const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' + months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();

        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
          value: _this.fromDate,
          minDate : _this.oneyearstartingDate,
          change(e) {
            // $('#datepicker').val(e.target.value);
            // $('#datepicker').trigger('input');
            // $('#datepicker').trigger('ngModelChange');
            localStorage.setItem('ASNDF', e.target.value);
            _this.fromDate = e.target.value;
          }

        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
          value: _this.toDate,
          maxDate: new Date(),
          change(e) {
            localStorage.setItem('ASNDT', e.target.value);
            _this.toDate = e.target.value
          }
        });
        //   $('#datepicker').datepicker({
        //     uiLibrary: 'bootstrap4',
        //     value: '01-Jan-2020'
        // });

        //       $('#datepicker1').datepicker({
        //     uiLibrary: 'bootstrap4',
        //     value: '31-May-2020'
        // });


        // $("#goback").hide();

        //       $("#goback").click(function() {
        //           $("#list-sections").hide();
        //           $("#grid-sections").show();
        //       });


        //card view


      })
    })(jQuery, this);
    this.getASNDetails();
  }

  getASNDetails() {
    this.spinner.show();
    // this.fromDate = localStorage.getItem('ASNDF');
    // this.toDate = localStorage.getItem('ASNDT');
    // const details = { email: this.email, purchaseType: 'ASN_DETAILS', date1: this.fromDate, date2: this.toDate };
    var time_difference =  Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
    var months_difference =  Math.floor(time_difference / (1000 * 60 * 60 * 24)/31);

    if((-(months_difference)) >this.noOfMonths){
      // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
      this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
      this.spinner.hide();
    } else{
    const details = this.purchaseService.inputJSON('ASN_DETAILS', this.fromDate, this.toDate, 'ASN_DETAILS', this.docid)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      let length = data.ASNDETAILS.ASN_HEADERS ? data.ASNDETAILS.ASN_HEADERS.length : 0
      this.spinner.hide();
      if(length<=0){
        this.viewAsnFlag= true;
      }else{
        this.viewAsnFlag = false;
      this.spinner.hide();
      this.asnHeaderAll = data? data.ASNDETAILS.ASN_HEADERS.map(x =>x.ASN_HEADER) : []
      this.asnLineAll = data? data.ASNDETAILS.ASN_LINES.map(x =>x.ASN_LINE) : []
      this.asnLotsAll = data.ASNDETAILS.ASN_LOTS? data.ASNDETAILS.ASN_LOTS.map(x =>x.ASN_LOT) : []
      this.asnTransporterAll = data.ASNDETAILS.TRANSPORTER_INFOS? data.ASNDETAILS.TRANSPORTER_INFOS.map(x =>x.TRANSPORTER_INFO) : []
            // this.asnHeaderAll = Array.isArray(data.ASNDETAILS.ASN_HEADERS.ASN_HEADER) ? data.ASNDETAILS.ASN_HEADERS.ASN_HEADER : (data.ASNDETAILS.ASN_HEADERS.ASN_HEADER) ? [data.ASNDETAILS.ASN_HEADERS.ASN_HEADER] : [];
      // this.asnLineAll = Array.isArray(data.ASNDETAILS.ASN_LINES.ASN_LINE) ? data.ASNDETAILS.ASN_LINES.ASN_LINE : (data.ASNDETAILS.ASN_LINES.ASN_LINE) ? [(data.ASNDETAILS.ASN_LINES.ASN_LINE)] : [];
      // this.asnLotsAll = Array.isArray(data.ASNDETAILS.ASN_LOTS.ASN_LOT) ? data.ASNDETAILS.ASN_LOTS.ASN_LOT : (data.ASNDETAILS.ASN_LOTS.ASN_LOT) ? [data.ASNDETAILS.ASN_LOTS.ASN_LOT] : [];
      this.asnHeader = this.asnHeaderAll;
      this.asnLine = this.asnLineAll;
      this.asnLots = this.asnLotsAll;
      this.asnTransporter= this.asnTransporterAll
      this.asnHeader.forEach((f, i) => {
        this.asnHeader[i].list = this.asnLineAll.filter(x => x.ASN_ID == f.ASN_ID);
        this.asnHeader[i].list1= this.asnTransporterAll.filter(x => x.ASN_ID == f.ASN_ID);
        this.asnHeader[i].list2= this.asnLotsAll.filter(x => x.ASN_ID == f.ASN_ID);
      });


      // this.asnHeaderAll.forEach(y =>{
      //   this.asnTransporterAll.forEach(x=>(x.ASN_ID == y.ASN_ID))
      // })

      // this.asnHeaderAll.forEach((y,i) =>{
      //   console.log("y : ", i, y)
      //   this.asnTransporter[i]= this.asnTransporterAll.forEach(x=>(x.ASN_ID == y.ASN_ID))
      // })

      // this.asnHeaderAll.forEach((f, i) => {
      //   this.asnTransporterbyASNID[i]= this.asnTransporterAll.filter(x => x.ASN_ID == f.ASN_ID);
      //   console.log("asnTransporter: ", this.asnTransporterbyASNID[i])
      // });
      this.setFilteredData();
    }
    }, error => {
      this.viewAsnFlag = false;
      this.spinner.hide();
      console.log(error);
    });
  }
  }

  asnStatus(gateNo, gateDate){
    this.gateNo = gateNo;
    this.gateDate = gateDate
  }
  lotView(element) {
    let data = element;
    this.lotDetails = this.asnLotsAll.filter(x => x.ASN_ID === data.ASN_ID && x.LINE_LOCATION_ID === data.LINE_LOCATION_ID);
    this.lotListView = false;
  }

  goBack() {
    this.lotListView = true;
  }

  fromDateChanged() {

  }

  setAsDataTable() {
    this.chRef.detectChanges();
    this.asnHeader.forEach((f, i) => {
      (function ($) {
        $('#test' + i).DataTable({
          responsive: true,
          bLengthChange: false,
          retrieve: true,
          pageLength: 5,
        });
      })(jQuery);

    });
    this.chRef.detectChanges();
  }

  setFilteredData() {
    this.asnHeader = this.asnHeaderAll.filter(f => {
      let a = true;

      // if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
      //   a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      // }
      // if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
      //   a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      // }
      // if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
      //   a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      // }


      // if (this.fromDate) {
      //   a = a && this.fromDate < f.PO_DATE;
      // }

      // if (this.toDate) {
      //   a = a && this.toDate > f.PO_DATE;
      // }

      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }
      return a;

    });

    //   this.poHeadersList = this.poHeadersList.slice(0, 20);
    this.asnHeader.forEach((f, i) => {
      this.asnHeader[i].list = this.asnLineAll.filter(x => x.ASN_ID == f.ASN_ID);
    });
    this.setAsDataTable();
  }


  getAsnLots(elements) {
    let lots = [];
    elements.list.forEach(a => {
      this.asnLotsAll.forEach(f => {
        if (a.LINE_LOCATION_ID === f.LINE_LOCATION_ID && f.ASN_ID === elements.ASN_ID)
          lots.push({ ...f, PO_NUMBER: a.PO_NUMBER, PO_LINE_NUMBER: a.PO_LINE_NUMBER, PO_SHIPMENT_NUMBER: a.PO_SHIPMENT_NUMBER })
      });
    });
    //  console.log(linelocationIds,lots,elements.ASN_ID);
    return lots;
  }

  // editASN() {
  //   console.log('edit asn ID : ', this.ASN_ID)
  //   this.router.navigate(['/craftsmanautomation/shipment/editasn'], { queryParams: { headerId: this.ASN_ID, reason: this.editReason, f: this.fromDate, t: this.toDate } });
  // }

  // deleteASN() {

  //   let query = {
  //     ASN: {
  //       HEADER: { OLD_ASN_ID: this.ASN_ID, REASON_DESCRIPTION: this.deleteReason },
  //     }
  //   };
  //   console.log("delete ASN : ", query )
  //   this.spinner.show();
  //   this.purchaseService.saveASN(query).subscribe((data: any) => {
  //     this.spinner.hide();
  //     console.log('data', data);
  //     this.getASNDetails();
  //   });
  // }

  // setHeaderId(id) {
  //   this.ASN_ID = id;
  // }
  public onPageChanged(event) {
    this.page = event;
  }

  resetPage() {
    // this.asnHeaders = {};
    // this.asnLines = [];
    // this.asnLots = [];
    // this.selectedPO = "";
  }

  // generatePdf(asnid){
  //   console.log("generate pdf : ", asnid)
  //   this.asnHeader1 = this.asnHeaderAll.filter(x => x.ASN_ID === asnid)
  //   console.log("this.asnHeader1[0].list, this.asnLineAll", this.asnHeader1[0].list, this.asnLotsAll);
  //   this.asnLines1 = this.asnHeader1[0].list.filter((f) => f.ASN_ID === asnid);
  //   this.asnlots1 = this.asnLotsAll.filter((f) => f.ASN_ID === asnid);
  //   console.log("asnHeader1 : ", this.asnHeader1,this. asnLines1, this.asnlots1)
  //   this.asn_id=asnid
  //   const documentDefinition = this.getDocumentDefinition();
  //   const temp = JSON.stringify(documentDefinition);
  //   const json = JSON.parse(temp);
  //   pdfMake.createPdf(json).open();
  //   this.router.navigate(['/craftsmanautomation/shipment/advanceShipments']);

  // }
  // getDocumentDefinition() {
  //   console.log(this.asnLots.length);
  //   return {
  //     content: [
  //       this.getQRRow(),
  //       this.getFirstTable(),
  //       {
  //         text: "ASN Items",
  //         fontSize: this.bodyFontSize + 2,
  //         bold: true,
  //         margin: [20, 20, 0, 0],
  //       },
  //       this.getSecondTable(),
  //       {
  //         text: this.asnLots.length ? "Lot Details" : "",
  //         fontSize: this.bodyFontSize + 2,
  //         bold: true,
  //         margin: [20, 20, 0, 0],
  //       },
  //       this.asnLots.length ? this.getThirdTable() : "",
  //       {
  //         text: this.asnTransporter.length ? "Transporter Details" : "",
  //         fontSize: this.bodyFontSize + 2,
  //         bold: true,
  //         margin: [20, 20, 0, 0],
  //       },
  //       this.asnTransporter.length ? this.getFourthTable() : "",
  //     ],
  //     info: {
  //       title: "ASN INVOICE",
  //     },
  //     styles: {
  //       header: {
  //         fontSize: 16,
  //         bold: true,
  //         margin: [0, 20, 0, 10],
  //         decoration: "underline",
  //       },
  //       alignment: {
  //         alignment: "right",
  //       },
  //       tableHeader: {
  //         bold: true,
  //       },
  //       tableExample: {
  //         margin: [0, 5, 0, 15],
  //       },
  //     },
  //   };
  // }

  // getQRRow() {
  //   console.log("QR asnID : ", this.asn_id)
  //   return { qr: " " + this.asn_id, fit: "75", alignment: "right"};
  // }

  // getFirstTable() {
  //   console.log("pdf make 1 : ", this.asnHeader1)
  //   return {
  //     margin: [-30, 10, 0, 0],
  //     table: {
  //       // heights:['*'],
  //       widths: [136, 136, 136, 136],
  //       body: [
  //         [
  //           {
  //             text: "Advance Shipment Notice",
  //             bold: true,
  //             fontSize: this.bodyFontSize + 2,
  //             colSpan: 4,
  //             alignment: "center",
  //           },
  //           "",
  //           "",
  //           "",
  //         ],
  //         [
  //           { text: "Supplier", bold: true, fontSize: this.bodyFontSize },
  //           {
  //             text: "Invoice Number",
  //             bold: true,
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: "Invoice Date",
  //             bold: true,
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: "Expected Receipt Date",
  //             bold: true,
  //             fontSize: this.bodyFontSize,
  //           },
  //         ],
  //         [
  //           { text: this.supplierName || "-", fontSize: this.bodyFontSize },
  //           {
  //             text: this.asnHeader1[0].DELIVERY_ORDER_NUMBER || "-",
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: this.asnHeader1[0].DELIVERY_ORDER_DATE || "-",
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: this.asnHeader1[0].EXPECTED_RECEIPT_DATE || "-",
  //             fontSize: this.bodyFontSize,
  //           },

  //         ],
  //         [
  //           { text: "Bill of Lading", bold: true, fontSize: this.bodyFontSize },
  //           { text: "Waybill Number", bold: true, fontSize: this.bodyFontSize },
  //           { text: "E-Invoice No", bold: true, fontSize: this.bodyFontSize },
  //           {
  //             text: "Shipment Method",
  //             bold: true,
  //             fontSize: this.bodyFontSize,
  //           },
  //         ],
  //         [
  //           {
  //             text: this.asnHeader1[0].BILL_OF_LADING || "-",
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: this.asnHeader1[0].WAYBILL_NUMBER || "-",
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: this.asnHeader1[0].EINVOICE_NUMBER || "-",
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: this.asnHeader1[0].SHIPPING_METHOD || "-",
  //             fontSize: this.bodyFontSize,
  //           },
  //         ],
  //         [
  //           { text: "Base Amount", bold: true, fontSize: this.bodyFontSize },
  //           {
  //             text: "Other Amount",
  //             bold: true,
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: "Tax Amount",
  //             bold: true,
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: "Total Value",
  //             bold: true,
  //             fontSize: this.bodyFontSize,
  //           },
  //         ],
  //         [
  //           { text: this.asnHeader1[0].BASE_AMOUNT || "-", fontSize: this.bodyFontSize },
  //           {
  //             text: this.asnHeader1[0].OTHER_AMOUNT || "-",
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: this.asnHeader1[0].TAX_AMOUNT || "-",
  //             fontSize: this.bodyFontSize,
  //           },
  //           {
  //             text: this.asnHeader1[0].TOTAL_VALUE || "-",
  //             fontSize: this.bodyFontSize,
  //           },
  //         ],
  //       ],
  //     },
  //     layout: {
  //       hLineWidth(i, node) {
  //         return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
  //       },
  //       vLineWidth(i, node) {
  //         return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
  //       },
  //       hLineColor(i, node) {
  //         return i === 0 || i === node.table.body.length ? "white" : "white";
  //       },
  //       vLineColor(i, node) {
  //         return i === 0 || i === node.table.widths.length ? "white" : "white";
  //       },
  //     },
  //   };
  // }

  // getSecondTableBody() {
  //   return this.asnLines1.map((m) => {
  //     return [
  //       { text: m.PO_NUMBER || "-", fontSize: this.bodyFontSize },
  //       { text: m.PO_LINE_NUMBER || "-", fontSize: this.bodyFontSize },
  //       // { text: m.PO_SHIPMENT_NUMBER || "-", fontSize: this.bodyFontSize },
  //       { text: m.ITEM_DESCRIPTION || "-", fontSize: this.bodyFontSize },
  //       // { text: m.ORDERED_QTY || "-", fontSize: this.bodyFontSize },
  //       { text: m.UOM || "-", fontSize: this.bodyFontSize },
  //       { text: m.RECEIVED_QTY || "-", fontSize: this.bodyFontSize },
  //       { text: m.PRICE *m.RECEIVED_QTY || "-", fontSize: this.bodyFontSize },
  //     ];
  //   });
  // }

  // getSecondTable() {
  //   return {
  //     margin: [-30, 10, 0, 0],
  //     table: {
  //       // heights:['*'],
  //       widths: [55, 40, 190, 65, 55, 55],
  //       // widths: [136,136,136,136],
  //       body: [
  //         [
  //           { text: "PO Number", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Line No.", fontSize: this.bodyFontSize, bold: true },
  //           // { text: "Shipment No.", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Item", fontSize: this.bodyFontSize, bold: true },
  //           // { text: "Ordered Qty", fontSize: this.bodyFontSize, bold: true },
  //           { text: "UOM", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Shipped Qty", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Price", fontSize: this.bodyFontSize, bold: true },
  //         ],
  //         ...this.getSecondTableBody(),
  //       ],
  //     },
  //     layout: {
  //       hLineWidth(i, node) {
  //         return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
  //       },
  //       vLineWidth(i, node) {
  //         return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
  //       },
  //       hLineColor(i, node) {
  //         return i === 0 || i === node.table.body.length ? "white" : "white";
  //       },
  //       vLineColor(i, node) {
  //         return i === 0 || i === node.table.widths.length ? "white" : "white";
  //       },
  //     },
  //   };
  // }

  // getThirdBody() {
  //   console.log("lots", this.asnlots1);
  //   return this.asnlots1.map((m) => {
  //     return [
  //       { text: m.PO_NO || "-", fontSize: this.bodyFontSize },
  //       { text: m.PO_LINE_NUMBER || "-", fontSize: this.bodyFontSize },
  //       { text: m.PO_SHIPMENT_NUMBER || "-", fontSize: this.bodyFontSize },
  //       { text: m.LOT_NUMBER || "-", fontSize: this.bodyFontSize },
  //       { text: m.GRADE || "-", fontSize: this.bodyFontSize },
  //       { text: m.QUANTITY || "-", fontSize: this.bodyFontSize },
  //       { text: m.REMARKS || "-", fontSize: this.bodyFontSize },
  //     ];
  //   });
  // }

  // getThirdTable() {
  //   return {
  //     margin: [-30, 10, 0, 0],
  //     table: {
  //       // heights:['*'],
  //       widths: [55, 40, 65, 190, 55, 40, 55],
  //       // widths: [136,136,136,136],
  //       body: [
  //         [
  //           { text: "PO Number", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Line No.", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Shipment No.", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Lot", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Grade", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Quantity", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Remarks", fontSize: this.bodyFontSize, bold: true },
  //         ],
  //         ...this.getThirdBody(),
  //       ],
  //     },
  //     layout: {
  //       hLineWidth(i, node) {
  //         return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
  //       },
  //       vLineWidth(i, node) {
  //         return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
  //       },
  //       hLineColor(i, node) {
  //         return i === 0 || i === node.table.body.length ? "white" : "white";
  //       },
  //       vLineColor(i, node) {
  //         return i === 0 || i === node.table.widths.length ? "white" : "white";
  //       },
  //     },
  //   };
  // }

  // getFourthTable() {
  //   return {
  //     margin: [-30, 10, 0, 0],
  //     table: {
  //       // heights:['*'],
  //       widths: [55, 40, 65, 80, 55, 40, 55, 80],
  //       // widths: [136,136,136,136],
  //       body: [
  //         [
  //           { text: "Transporter", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Vehicle No.", fontSize: this.bodyFontSize, bold: true },
  //           { text: "E-Way Bill No.", fontSize: this.bodyFontSize, bold: true },
  //           { text: "LR No.", fontSize: this.bodyFontSize, bold: true },
  //           { text: "LR Date", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Driver", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Contact No.", fontSize: this.bodyFontSize, bold: true },
  //           { text: "Remarks", fontSize: this.bodyFontSize, bold: true },
  //         ],
  //         ...this.getFourthBody(),
  //       ],
  //     },
  //     layout: {
  //       hLineWidth(i, node) {
  //         return i === 0 || i === node.table.body.length ? 0.1 : 0.1;
  //       },
  //       vLineWidth(i, node) {
  //         return i === 0 || i === node.table.widths.length ? 0.1 : 0.1;
  //       },
  //       hLineColor(i, node) {
  //         return i === 0 || i === node.table.body.length ? "white" : "white";
  //       },
  //       vLineColor(i, node) {
  //         return i === 0 || i === node.table.widths.length ? "white" : "white";
  //       },
  //     },
  //   };
  // }

  // getFourthBody() {
  //   return this.asnTransporter.map((m) => {
  //     return [
  //       { text: m.TRANSPORTER_NAME || "-", fontSize: this.bodyFontSize },
  //       { text: m.VEHICLE_NUMBER || "-", fontSize: this.bodyFontSize },
  //       { text: m.EWAY_BILL_NO || "-", fontSize: this.bodyFontSize },
  //       { text: m.LR_NO || "-", fontSize: this.bodyFontSize },
  //       { text: m.LR_DATE || "-", fontSize: this.bodyFontSize },
  //       { text: m.DRIVER_NAME || "-", fontSize: this.bodyFontSize },
  //       { text: m.DRIVER_CONTACT_NO || "-", fontSize: this.bodyFontSize },
  //       { text: m.REMARKS || "-", fontSize: this.bodyFontSize },
  //     ];
  //   });
  // }

  viewTaxButton1(element, i, j){
    this.action = false;
    this.asnHeader[i].list[j].action = false;
  }
  viewTaxButton2(element,i, j){
    this.action = true;
    this.asnHeader[i].list[j].action = true
    // const details = { email: this.email, attachmentString: element.TAX_IDENTIFICATION_STRING, purchaseType: 'TAX_LINES'};
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
      var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', element.TAX_IDENTIFICATION_STRING)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      // console.log("tax details : ", data)
      this.taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.myGst = this.taxData[0].MY_TAX_REG_NO

      this.asnHeader[i].list[j].taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.asnHeader[i].list[j].taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.asnHeader[i].list[j].taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.asnHeader[i].list[j].myGst = this.taxData[0].MY_TAX_REG_NO
      this.asnHeader[i].list[j].hsnsacCode = this.taxData[0].HSN_SAC_CODE
      this.asnHeader[i].list[j].hsnsacCodeDesc = this.taxData[0].HSN_SAC_CODE_DESC
      // console.log("zzz : ", this.asnHeader[i].list)

      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
  moveTo(){
    this.router.navigateByUrl('/craftsmanautomation/shipment/createasn', {skipLocationChange: profileData.hideURL})
  }
}
