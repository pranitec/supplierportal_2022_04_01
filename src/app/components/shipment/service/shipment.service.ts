import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';


@Injectable({
  providedIn: 'root'
})
export class ShipmentService {


  public baseUrl = environment.apiBaseUrl + '/shipment' ;

  constructor(private http: HttpClient) { }

  getShipments(email) {
    return this.http.post(`${this.baseUrl}`, email);
  }

  getExcel(data){
    // return this.http.post(`${this.baseUrl}/getExcel`, data);
    const title = 'Pending Report';
    return this.http.post(`${this.baseUrl}/getExcel`, data, {
      params: new HttpParams().append('token', localStorage.getItem('1')),
      observe: 'response', responseType: 'text'
    }).subscribe(r => { saveAs(new Blob([r.body], { type: 'text/csv' }), title + '.csv'); });

  }
  getShipmentsById(email){
    return this.http.put(`${this.baseUrl}`,email);
  }
}
