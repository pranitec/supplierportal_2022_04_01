import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ShipmentService } from '../service/shipment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { NotifierService } from 'angular-notifier';
import { ExcelService} from 'src/app/shared/service/excel.service'
declare var jQuery: any;
@Component({
  selector: 'app-view-receipts',
  templateUrl: './view-receipts.component.html',
  styleUrls: ['./view-receipts.component.css']
})
export class ViewReceiptsComponent implements OnInit {

  public email;
  public poHeadersList = [];
  public fullpoHeaderList = [];
  public poLineList = [];
  public fullpolines = [];
  public fullSummary = [];
  public viewShipmenList = [];

  public searchText = '';
  public page =1;
  public viewCount = 10;
  public viewGrid = true;
  public ordersReceived = 0;
  public orderedQuantity = 0;
  public deliveredQuantity = 0;
  public pendingQuantity = 0;
  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public today = new Date();
  // public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
  public uom = ''
  public action = false;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst
  public lineId;
  public fieldName;
  public selectedmenu;
  public selectedmenuFilter =[];
  public oneyearstartingDate : Date;
  private notifier: NotifierService;
  public noOfMonths;

  public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

  constructor(private shipmentService: ShipmentService, private chRef: ChangeDetectorRef, private notifierService: NotifierService,
    public breadCrumbServices: BreadcrumbService, private router: Router, public rsaService: RsaService, private spinner: NgxSpinnerService,
    private purchaseService : PurchaseService, private route: ActivatedRoute, private excelService : ExcelService) {
      this.notifier = notifierService;
      this.route.queryParams.subscribe((data) => {
        this.lineId = data['lineiId'];
        this.fieldName = data['fieldName'];
      });
     }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    localStorage.setItem('PODF', this.fromDate);
    localStorage.setItem('PODT', this.toDate);

    this.selectedmenu= this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
    this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(({MENUACCESS})=> MENUACCESS.MENU_CODE === 'VIEW_RECEIPTS');
    this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START)
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.noOfMonths  = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE /31)

    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/shipment/viewreceipts') {
        this.setFilteredData();
      }
    });

    this.getViewReceipts();

    (function ($, _this) {

      $(document).ready(function () {
        // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        // const today = new Date();
        // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' + months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
        // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();

        $('#datepicker').datepicker({
          uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
          // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
          value: _this.fromDate,
          minDate : _this.oneyearstartingDate,
          change(e) {
            // $('#datepicker').val(e.target.value);
            // $('#datepicker').trigger('input');
            // $('#datepicker').trigger('ngModelChange');
            localStorage.setItem('PODF', e.target.value);
            _this.fromDate = e.target.value;
          }

        });
        $('#datepicker1').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'dd-mmm-yyyy',
          // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
          value: _this.toDate,
          maxDate: new Date(),
          change(e) {
            localStorage.setItem('PODT', e.target.value);
            _this.toDate = e.target.value
          }
        });
        // $('#grid-sections').show();
        // $('#list-sections').hide();,m
        // $('#grid').click(function() {
        //   $('#list-sections').hide();
        //   $('#grid-sections').show();
        //   $('#list').removeClass('btn-success');
        //   $('#list').addClass('btn-light');
        //   $(this).removeClass('btn-light');
        //   $(this).addClass('btn-success');
        // });
        // $('#list').click(function() {
        //   $('#list-sections').show();
        //   $('#grid-sections').hide();

        //   $('#Past-details-view').hide();
        //   $('#Past-details').show();

        //   $('#grid').removeClass('btn-success');
        //   $('#grid').addClass('btn-light');

        //   $(this).removeClass('btn-light');
        //   $(this).addClass('btn-success');
        // });
        // $('#Past-details-view').hide();
        // $('#goback').hide();

        // $('#goback').click(function() {
        //   $('#Past-details-view').hide();
        //   $('#Past-details').show();
        // });
      });
    })(jQuery, this);
  }

  getViewReceipts() {
    this.spinner.show();
    // this.fromDate = localStorage.getItem('PODF');
    // this.toDate = localStorage.getItem('PODT');
    var time_difference =  Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
    var months_difference =  Math.floor(time_difference / (1000 * 60 * 60 * 24)/31);

    if((-(months_difference)) >this.noOfMonths){
      // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
      this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
      this.spinner.hide();
    } else{
    // const details = { email: this.email, shipmentType: 'PO_RECEIPT', date1: this.fromDate, date2: this.toDate };
    // this.shipmentService.getShipments(details).subscribe((data: any) => {
      const details= this.purchaseService.inputJSON('PO_RECEIPT', this.fromDate, this.toDate, this.fieldName, this.lineId)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      console.log("getViewReceipts", data)
      this.spinner.hide();
      if (data) {
        // this.fullpoHeaderList = Array.isArray(data.PORECEIPT.RECEIPTHEADERS.RECEIPTHEADER) ? data.PORECEIPT.RECEIPTHEADERS.RECEIPTHEADER : (data.PORECEIPT.RECEIPTHEADERS.RECEIPTHEADER) ? [data.PORECEIPT.RECEIPTHEADERS.RECEIPTHEADER] : [];
        // this.fullpolines = Array.isArray(data.PORECEIPT.RECEIPTLINES.RECEIPTLINE) ? data.PORECEIPT.RECEIPTLINES.RECEIPTLINE : (data.PORECEIPT.RECEIPTLINES.RECEIPTLINE) ? [(data.PORECEIPT.RECEIPTLINES.RECEIPTLINE)] : [];
        // this.fullSummary = Array.isArray(data.PORECEIPT.RECEIPTLOTS.RECEIPTLOT) ? data.PORECEIPT.RECEIPTLOTS.RECEIPTLOT : (data.PORECEIPT.RECEIPTLOTS.RECEIPTLOT) ? [data.PORECEIPT.RECEIPTLOTS.RECEIPTLOT] : [];
        this.fullpoHeaderList = data.PORECEIPT.RECEIPTHEADERS ? data.PORECEIPT.RECEIPTHEADERS.map(x=>(x.RECEIPTHEADER)) :[]
        this.fullpolines = data.PORECEIPT.RECEIPTLINES ? data.PORECEIPT.RECEIPTLINES.map(x=>(x.RECEIPTLINE)) :[]
        this.fullSummary = data.PORECEIPT.RECEIPTLOTS ? data.PORECEIPT.RECEIPTLOTS.map(x=>(x.RECEIPTLOT)) :[]
        this.setFilteredData();
        this.setSummaryData();
        console.log("fullpolines : ", this.fullpolines);

      } else {
        console.error('NULL VALUE');
      }

    }, error => {
      this.spinner.hide();
      console.log(error);
    });
  }
  }

  setFilteredData() {
    this.poHeadersList = this.fullpoHeaderList.filter(f => {
      let a = true;

      if (this.breadCrumbServices.select1 && this.breadCrumbServices.select1 !== 'All') {
        a = a && this.breadCrumbServices.select1 === f.BUYER_UNIT;

      }
      if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
      }
      if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
        a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
      }


      // if (this.fromDate) {
      //   a = a && this.fromDate < f.PO_DATE;
      // }

      // if (this.toDate) {
      //   a = a && this.toDate > f.PO_DATE;
      // }

      if (this.searchText) {
        a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
      }

      return a;

    });
    //   this.poHeadersList = this.poHeadersList.slice(0, 20);
    this.poHeadersList.forEach((f, i) => {
      this.poHeadersList[i].list = this.fullpolines.filter(x => x.SHIPMENT_HEADER_ID == f.SHIPMENT_HEADER_ID);
    });
    console.log("this.poHeadersList : ", this.poHeadersList);

    this.setAsDataTable();
  }

  setAsDataTable() {
    this.chRef.detectChanges();
    this.poHeadersList.forEach((f, i) => {
      (function ($) {
        $('#test' + i).DataTable({
          responsive: true,
          bLengthChange: false,
          retrieve: true,
          pageLength: 5,
        });
      })(jQuery);
    });
  }

  // setAsDataTableList() {
  //   this.chRef.detectChanges();
  //   (function ($) {
  //     $('#Past-table').DataTable({
  //       responsive: true,
  //       iDisplayLength: 5,
  //       bLengthChange: false,
  //       retrieve: true,
  //       pageLength: 5,
  //     });

  //     $('#Past-table').draw();
  //   })(jQuery);

  // }

  setAsDataTableList() {
    this.chRef.detectChanges();
    (function($) {
      $.noConflict();
      $('#Past-table').DataTable({
        responsive: true,
        iDisplayLength: 5,
        bLengthChange: false,
        retrieve: true,
        pageLength: 5,
      });

      $('#Past-table').draw();
    })(jQuery);

  }

  public onPageChanged(event) {
    this.page = event;
  }
  onViewGrid() {
    this.viewGrid = true;
    this.searchText = '';
    this.setAsDataTable();

  }

  onViewList() {
    this.viewGrid = false;
    this.searchText = '';
    this.setAsDataTableList();

  }

  exportExcel() {
    const poLines = [];
    const data= [];
    var sNo;
    this.poHeadersList.forEach(f => {
      poLines.push(...f.list);
    });
    const details = { POLINES: poLines, title: 'View Receipts Report' };
    // this.purchaseService.getExcel(details);
    console.log("poLines : ", poLines);
    this.poHeadersList.forEach((x,index)=>{
      sNo = index + 1;
      x.list.forEach(y=>{
        data.push({
          'S.No':sNo,
          'Organization':x.BUYER_ORGANIZATION,
          'Unit':x.BUYER_UNIT,
          'Receipt No':x.RECEIPT_NUM,
          'Receipt Date':x.RECEIPT_DATE,
          'Invoice No': x.INVOICE_NUM,
          'Supplier Invoice No': x.SUPPLIER_INV_NUM,
          'Supplier Invoice Date': x.SUPPLIER_INV_DATE,
          'Total Line Amount':x.LINE_AMOUNT,
          'Total Tax Amount':x.TAX_AMOUNT,
          'Total Receipt Amount':x.TOTAL_AMOUNT,
          'Document No' :y.DOCUMENT_NO,
          'Line No':y.LINE_NO,
          'Item Code':y.ITEM_CODE,
          'Item Description':y.ITEM_DESCRIPTION,
          'UOM':y.UNIT_OF_MEASURE,
          'Ordered Qty':y.ORDERED_QTY,
          'Delivered Qty':y.DELIVERED_QTY,
          'Balance Qty':y.BALANCE_QTY,
          'Returned Qty':y.RETURNED_QTY,
          'Line Amount':y.LINE_AMOUNT,
          'Tax Amount':y.TAX_AMOUNT,
          'Division':y.DIVISION,
          'Division Code':y.DIVISION_CODE
        });
      })
    })
    console.log("polines data : ",data);

    this.excelService.exportAsExcelFile(data, 'View Receipts Report ')
    // const poLines = [];
    // this.poHeadersList.forEach(f => {
    //   poLines.push(...f.list);
    // });
    // const details = { POLINES: poLines , title: 'Past Purchase Report' };
    // this.purchaseService.getExcel(details);

  }


  setSummaryData() {
    let pendingQuantity = 0, orderedQuantity = 0, ordersReceived = 0, deliveredQuantity = 0;

    this.fullSummary.forEach(f => {
      pendingQuantity = pendingQuantity + Number(f.PENDING_PO_QUANTITY);
      orderedQuantity = orderedQuantity + Number(f.ORDERED_QUANTITY);
      ordersReceived = ordersReceived + Number(f.PENDING_PO_COUNT);
      deliveredQuantity = deliveredQuantity + Number(f.DELIVERED_QUANTITY);
      if (f.UOM) {
        this.uom = f.UOM;
      }
    });

    this.pendingQuantity = pendingQuantity;
    this.orderedQuantity = orderedQuantity;
    this.ordersReceived = ordersReceived;
    this.deliveredQuantity = deliveredQuantity;
  }
  fromDateChanged() {

  }


  changeView(shipment_id) {

    this.viewShipmenList = this.fullSummary.filter(f => f.SHIPMENT_LINE_ID === shipment_id)
    this.viewGrid = false;
  }
  goBack() {
    this.viewGrid = true;
  }

  viewTaxButton1(element, i, j){
    this.action = false;
    this.poHeadersList[i].list[j].action = false;

  }
  viewTaxButton2(element,i, j){
    this.action = true;
    this.poHeadersList[i].list[j].action = true
    // const details = { email: this.email, attachmentString: element.TAX_IDENTIFICATION_STRING, purchaseType: 'TAX_LINES'};
    // this.purchaseService.getDocs(details).subscribe((data: any) => {
      var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', element.TAX_IDENTIFICATION_STRING)
      this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
      this.taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.myGst = this.taxData[0].MY_TAX_REG_NO

      this.poHeadersList[i].list[j].taxData = data? data.TAX_DETAILS.map(x=>(x.TAX_DETAIL)) :[]
      this.poHeadersList[i].list[j].taxPayerId= this.taxData[0].CUSTOMER_TAX_PAYER_ID;
      this.poHeadersList[i].list[j].taxRegNo= this.taxData[0].CUSTOMER_TAX_REG_NO;
      this.poHeadersList[i].list[j].myGst = this.taxData[0].MY_TAX_REG_NO
      this.poHeadersList[i].list[j].hsnsacCode = this.taxData[0].HSN_SAC_CODE
      this.poHeadersList[i].list[j].hsnsacCodeDesc = this.taxData[0].HSN_SAC_CODE_DESC

      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

}
