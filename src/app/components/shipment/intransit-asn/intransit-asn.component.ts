import { Component, OnInit, ChangeDetectorRef, ViewChild} from '@angular/core';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { PurchaseService } from '../../purchase/service/purchase.service';
import { Router } from '@angular/router';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';
// import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TaxComponent } from 'src/app/shared/components/tax/tax.component';
import { ExcelService } from 'src/app/shared/service/excel.service';
// import {NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { NotifierService } from 'angular-notifier';
import { Subject } from 'rxjs';
declare var jQuery: any;

@Component({
  selector: 'app-intransit-asn',
  templateUrl: './intransit-asn.component.html',
  styleUrls: ['./intransit-asn.component.css']
})
export class IntransitASNComponent implements OnInit {

  public email;
  public shipmentView = false;
  public lineId = '';
  public intransitAsn = [];
  public poSummary = [];
  public poHeaderAll = [];
  public poLineAll = [];
  public poHeader = [];
  public poLine = [];
  public viewCount = 10;
  public orderedQty = 0;
  public deliveredQty = 0;
  public pendingQty = 0;
  public pendingCount = 0;
  public poCountsAll = [];
  public poCounts = []
  // public viewLength: number;
  public page =  1;
  public searchText = '';
  public tableView = false;
  public modal;
  public modalHeader;
  public modalLine1;
  public modalLine2;
  public modalLine3;
  public modalLine4;
  public action= false;
  public taxIdentificationString;
  public taxData=[];
  public taxPayerId;
  public taxRegNo;
  public myGst
  public hsnsacCode;
  public fieldName =''
  public unitName=''
  // public vendorName1=''
  public vendorSite=''
  public OrganisationList=[]
  public vendorNameList =[];
  public vendorSiteList=[];
  public pageLength=5;
  private notifier: NotifierService;
  public intransitAsn1 =[]
  public suppliername;
  public len = 0;
  public viewFlag = false;

  // public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  // public today = new Date();
  // public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
  // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
  // public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

  // @ViewChild(DataTableDirective)
  // dtElement: DataTableDirective;
  // dtOptions: DataTables.Settings = {};
  // dtTrigger: Subject<any> = new Subject();
  constructor(private purchaseService: PurchaseService, private chRef: ChangeDetectorRef, private spinner: NgxSpinnerService,
              private breadCrumbServices: BreadcrumbService, private router: Router, private rsaService: RsaService,
              private excelService: ExcelService,public datepipe: DatePipe,  private notifierService: NotifierService,
              ) {
                this.notifier = notifierService;
                this.page = 1;
  }

  ngOnInit(): void {
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.tableView = true;
    this.breadCrumbServices.selectValuesChanged.subscribe(() => {
      if (this.router.url === '/craftsmanautomation/shipment/intransitAsn') {
        this.setFilteredData();
      }
    });
    this.getIntransitASN();
    // this.spinner.show();
  }

  getIntransitASN() {
    console.log("getIntransitASN");

    const deliveredQty = 0, pendingQty = 0, orderedQty = 0, pendingCount = 0;
    this.spinner.show()
    var query = this.purchaseService.inputJSON('INTRANSIT_ASN', undefined, undefined)
    // const details = { email: this.email, purchaseType: 'PENDING_PO'};
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      console.log('ppp getIntransitASN', data, data.length);
      this.intransitAsn = data? data.INTRANSIT_ASNS.map(x=>(x.INTRANSIT_ASN)) :[]
      this.intransitAsn.forEach(x=> x.ASN_DATE=new Date(x.ASN_DATE))
      let temp= this.intransitAsn.map(x=> (x.ORG_NAME))
      let temp1= this.intransitAsn.map(x=> (x.VENDOR_NAME))
      let temp2= this.intransitAsn.map(x=> (x.SITE_CODE))
      this.intransitAsn1 =this.intransitAsn
      this.len = this.intransitAsn.length

      this.OrganisationList=Array.from(new Set(temp))
      this.vendorNameList=Array.from(new Set(temp1))
      this.vendorSiteList=Array.from(new Set(temp2))
      this.OrganisationList = this.OrganisationList.sort((a,b) => 0 - (a > b ? -1 : 1));
      this.vendorNameList = this.vendorNameList.sort((a,b) => 0 - (a > b ? -1 : 1));
      this.vendorSiteList = this.vendorSiteList.sort((a,b) => 0 - (a > b ? -1 : 1));
      // this.listView();
      if(this.intransitAsn1){
        this.viewFlag = true
        this.setAsDataTable1();
      }
      this.spinner.hide();
    // }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  setAsDataTable1() {
    this.chRef.detectChanges();
    (function($) {
      $('#Pending-table').DataTable({
        responsive: false,
        iDisplayLength: 5,
        bLengthChange: false,
        retrieve: true,
        pageLength: 10,
        paging : false,
        // stateSave : false,
       info : false
      });
    })(jQuery);
  }

  public onPageChanged(event) {
    this.page = event;
  }

  exportExcel() {
      const data = [];
      var sNo;
      let date=new Date();
      let latest_date =this.datepipe.transform(date, 'dd-MM-yyyy');
      if( this.intransitAsn.length>0){
      this.intransitAsn.forEach((element, index) => {
          sNo = index + 1;
        data.push({
          SerialNo: sNo,
           Unit: element.ORG_NAME,
           Vendor_Name: element.VENDOR_NAME,
           Vendor_Site: element.SITE_CODE,
           ASN_Number: element.ASN_NUMBER,
           ASN_Date: element.ASN_DATE,
           PO_Number : element.PO_NUMBER,
           Line_No: element.LINE_NUMBER,
           Item_Code: element.PO_LINE_ITEM,
           Item_Description : element.ITEM_DESCRIPTION,
           Qunatity: element.QUANTITY,
           Invoice_No: element.INVOICE_NUMBER,
           Invoice_Date: this.datepipe.transform(element.INVOICE_DATE, 'dd-MM-yyyy'),
           Division: element.DIVISION,
           Division_code: element.DIVISION_CODE
        });
      });
      this.excelService.exportAsExcelFile(data,  'intransitAsn ' + latest_date);
    }
    else{
      this.notifier.notify("error", "No data found");
    }
  }
  getUnitName(unit){
    this.suppliername ='';  this.vendorSite=''; this.vendorNameList =[]; this.vendorSiteList =[]
    let temp = this.intransitAsn1.filter(x=> (x.ORG_NAME === unit ))
    this.vendorNameList =Array.from(new Set(temp.map(x=> (x.VENDOR_NAME))))
    this.vendorNameList= this.vendorNameList.sort((a,b) => 0 - (a > b ? -1 : 1));

    // console.log("vendorName ,",this.unitName, this.vendorName, this.vendorNameList)
  }
  getvendorName(vendorname){
    // this.suppliername =''
    this.vendorSite='';
    this.vendorSiteList =[]
    let temp = this.intransitAsn1.filter(x=> (x.ORG_NAME === this.unitName || x.VENDOR_NAME === vendorname))
    this.vendorSiteList =Array.from(new Set(temp.map(x=> (x.SITE_CODE))))
    this.vendorSiteList = this.vendorSiteList.sort((a,b) => 0 - (a > b ? -1 : 1));
  }

  setFilteredData(){
    this.spinner.show();
    this.intransitAsn =[]
    if(this.unitName || this.suppliername || this.vendorSite){
      this.intransitAsn = this.intransitAsn1.filter(x=>
       (
         this.unitName === '' || x.ORG_NAME === this.unitName
       )
         &&
        (
          this.suppliername === '' || x.VENDOR_NAME === this.suppliername
        ) &&
        (
          this.vendorSite === '' || x.SITE_CODE === this.vendorSite
        )
       )
    }
    if(this.intransitAsn) {
      this.spinner.hide();
      this.setAsDataTable1();
    }

    // this.vendorSiteList =Array.from(new Set(temp.map(x=> (x.ORG_NAME))))
    // console.log("OrganisationList : ", this.vendorSiteList)

  }

  clear(){
    this.vendorSite='';
    this.unitName='';
    this.suppliername='';
    this.vendorSiteList =[];
    this.vendorNameList =[];
    // console.log("this.intransitAsn1 : ", this.intransitAsn1)
    // this.intransitAsn = this.intransitAsn1
    // console.log("this.intransitAsn : ", this.intransitAsn)
    this.getIntransitASN()
    // this.setAsDataTable1();
  }
}
