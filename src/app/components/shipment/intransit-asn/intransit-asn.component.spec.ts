import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntransitASNComponent } from './intransit-asn.component';

describe('IntransitASNComponent', () => {
  let component: IntransitASNComponent;
  let fixture: ComponentFixture<IntransitASNComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntransitASNComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntransitASNComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
