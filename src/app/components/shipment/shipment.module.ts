import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { ShipmentRoutingModule } from './shipment-routing.module';
import { ViewReceiptsComponent } from './view-receipts/view-receipts.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewReturnsComponent } from './view-returns/view-returns.component';
import { ViewDeliveryPerformanceComponent } from './view-delivery-performance/view-delivery-performance.component';
import { AdvanceShipmentNoticeComponent } from './advance-shipment-notice/advance-shipment-notice.component';
import { CreateAsnComponent } from './create-asn/create-asn.component';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { InvoiceASNComponent } from './invoice-asn/invoice-asn.component';
import { PurchaseModule } from '../purchase/purchase.module';
import { EditAsnComponent } from './edit-asn/edit-asn.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { IntransitASNComponent } from './intransit-asn/intransit-asn.component';
import { DatePipe } from '@angular/common';
// import {PurchaseModule} from '../purchase/purchase.module';

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'middle',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 99,
      gap: 15
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 1200,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 2800,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 1000,
      easing: 'ease',
      offset: 150
    },
    shift: {
      speed: 500,
      easing: 'ease'
    },
    overlap: 90
  }
};

@NgModule({
  declarations: [ViewReceiptsComponent, ViewReturnsComponent, ViewDeliveryPerformanceComponent,
    AdvanceShipmentNoticeComponent, CreateAsnComponent, InvoiceASNComponent, EditAsnComponent, IntransitASNComponent],
  imports: [
    CommonModule,
    FormsModule,
    ShipmentRoutingModule,
    NgxPaginationModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxSpinnerModule,
    ChartsModule,
    PurchaseModule,
    SharedModule,
    ReactiveFormsModule,
    // MatInputModule
  ],
  providers:[DatePipe]
})
export class ShipmentModule { }
