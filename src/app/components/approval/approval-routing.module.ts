import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupplierApprovalComponent } from './supplier-approval/supplier-approval.component';


const routes: Routes = [{
  path:'supplier',component:SupplierApprovalComponent,data:{
    title: 'Approve Supplier',
    breadcrumb: 'Approve Supplier'
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovalRoutingModule { }
