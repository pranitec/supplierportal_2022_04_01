import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-supplier-approval',
  templateUrl: './supplier-approval.component.html',
  styleUrls: ['./supplier-approval.component.css']
})
export class SupplierApprovalComponent implements OnInit {

  constructor() { }
  public supplierRequests=[{RequestType:'New',Supplier:'s',SupplierType:'',Country:'',justification:''},
  {RequestType:'Update',Supplier:'s',SupplierType:'',Country:'',justification:''}]
  ngOnInit(): void {
  }

}
