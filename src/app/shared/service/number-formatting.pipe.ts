import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberFormatting'
})
export class NumberFormattingPipe implements PipeTransform {

  transform(value: any): any {
    //  var userLang = navigator.language;
    //  return new Intl.NumberFormat('userLang').format(value);
      // return new Intl.NumberFormat('en-IN').format(value);

  const formatter = new Intl.NumberFormat('en-US', {
    minimumFractionDigits: 2,
    maximumFractionDigits :2
  })
  return formatter.format(value);
}

}
