import { Injectable, Inject, HostListener } from '@angular/core';
import { WINDOW } from './windows.service';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  constructor(@Inject(WINDOW) private window) {
    // this.onResize();
    if (this.screenWidth < 991) {
      this.collapseSidebar = true;
    }
  }

  public screenWidth: any;
  public collapseSidebar = false;


  // Windows width
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
  }
}
