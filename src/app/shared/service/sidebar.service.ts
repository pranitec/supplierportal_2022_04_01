import { Injectable } from "@angular/core";
import { RsaService } from "./rsa.service";

@Injectable({
  providedIn: "root",
})
export class SidebarService {
  public createASNFlag;
  public menu;
  constructor(public rsaService: RsaService) {}
  checkMenuAccess(menuCode) {
    this.menu = JSON.parse(
      this.rsaService.decrypt(localStorage.getItem("MENUACCESS"))
    );
    let temp = this.menu.find((x) => x.MENUACCESS.MENU_CODE == menuCode);
    if(temp) return true;
  }
}
