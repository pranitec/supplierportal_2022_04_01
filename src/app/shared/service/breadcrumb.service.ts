import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { PurchaseService } from 'src/app/components/purchase/service/purchase.service';
import { environment } from 'src/environments/environment';
import { RsaService } from './rsa.service';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  private _refreshNeeded$ = new Subject<void>();

  get refreshNeeded$() {
    return this._refreshNeeded$;
  }

  selectValuesChanged = new EventEmitter();
  subsVar: Subscription;


  public baseUrl = environment.apiBaseUrl + '/users';
  public screenWidth: any;
  public collapseSidebar = false;

  public selectValues = [];
  public filteredSelection = [];

  public partyList = [];
  public uniquePartyList = [];
  public partySelected='';
  public partyNameSelected = '';
  public partyId = '';

  public select1 = 'All';
  public select2 = 'All';
  public select3 = 'All';
  constructor(private rsaService: RsaService,private http: HttpClient,public router:Router, private purchaseService :PurchaseService) { }


  setSelectValues(data,count) {
    this.selectValues = data;
    this.filteredSelection = JSON.parse(JSON.stringify(data));
    if (this.filteredSelection.length === 1) {
      this.select1 = this.filteredSelection[0].ERP_ORGANIZATION_NAME;
      this.select2 = this.filteredSelection[0].VENDOR_NAME;
      this.select3 = this.filteredSelection[0].VENDOR_SITE_CODE;
    }

    if(count>0){
      this.getAllPartyList({
        email:this.rsaService.decrypt(localStorage.getItem('3')),
      });
      this.partySelected = this.filteredSelection[0].VENDOR_ID;
      this.partyNameSelected = this.filteredSelection[0].VENDOR_NAME;
    }
    this.setLocalStorage();

  }

    getSelectValues() {
    this.selectValues = JSON.parse(this.rsaService.decrypt(localStorage.getItem('USERACCESS')));
    this.filteredSelection = JSON.parse(this.rsaService.decrypt(localStorage.getItem('USERACCESS')));
    // this.filteredSelection = this.filteredSelection.sort((a,b)=> a.ERP_ORGANIZATION_NAME.localeCompare(b.ERP_ORGANIZATION_NAME));
    // this.select1 = localStorage.getItem('select1') ? localStorage.getItem('select1') : 'All';
    // this.select2 = localStorage.getItem('select2') ? localStorage.getItem('select2') : 'All';
    // this.select3 = localStorage.getItem('select3') ? localStorage.getItem('select3') : 'All';
    this.partySelected = this.filteredSelection[0].VENDOR_ID;
    this.partyNameSelected = this.filteredSelection[0].VENDOR_NAME;
    this.getAllPartyList({
      email:this.rsaService.decrypt(localStorage.getItem('3'))
    });
    return true;
  }

  onFirstSelectChange(value) {
    this.select1 = value;
    // this.selectValuesChanged.emit();
    return this.filterValues(1);
  }

  onSecondSelectChange(value) {
    this.select2 = value;
    // this.selectValuesChanged.emit();
    return this.filterValues(2);
  }

  onThirdSelectChange(value) {
    this.select3 = value;
    // this.selectValuesChanged.emit();
    return this.filterValues(3);
  }

  filterValues(selected) {
    this.filteredSelection =[]
    // console.log("selected: ",selected, this.selectValues, this.select1 )
    this.filteredSelection = this.selectValues.filter(val => {
      let a = true;
      if(selected === 1){
        this.select3 = 'All'
      }
      if (this.select1 !== 'All') {
        a = a && this.select1 === val.ERP_ORGANIZATION_NAME;
      }

      if (this.select2 !== 'All') {
        a = a && this.select2 === val.VENDOR_NAME;
      }

      if (this.select3 !== 'All') {
        a = a && this.select3 === val.VENDOR_SITE_CODE;
      }
      return a;
    });
    // console.log("filteredSelection: ",this.filteredSelection )
    if (this.filteredSelection.length === 1) {
      this.select1 = this.filteredSelection[0].ERP_ORGANIZATION_NAME;
      this.select2 = this.filteredSelection[0].VENDOR_NAME;
      this.select3 = this.filteredSelection[0].VENDOR_SITE_CODE;
    } else {
      if (selected === 1) {
        this.select2 = 'All';
        this.select3 = 'All';
      }
      if (selected === 2) {
        this.select3 = 'All';
      }
      if (!selected) {
      }
    }
    // console.log("this.select1: ", this.select1 , this.select2, this.select3,  )
    this.setLocalStorage();

    this.selectValuesChanged.emit();
       return this.filteredSelection;

  }

setLocalStorage() {
    localStorage.setItem('select1', this.select1);
    localStorage.setItem('select2', this.select2);
    localStorage.setItem('select3', this.select3);
  }

  // this.partyList = Array.isArray(data.VENDORS.VENDOR) ? data.VENDORS.VENDOR : (data.VENDORS.VENDOR) ? [data.VENDORS.VENDOR] : [];
       //this.http.post(`${this.baseUrl}/supplierList`,query).subscribe((data:any)=>{
getAllPartyList(query){
  // console.log("getAllpatyList")
const details = this.purchaseService.inputJSON('SUPPLIER_LIST', undefined, undefined)
    this.purchaseService.getPurchaseOrder(details).subscribe((data: any) => {
    // console.log('res',data);
    if(data){
        this.partyList = (data.VENDORS) ? data.VENDORS.map(x=>(x.VENDOR)) : [];
        this.partyList = this.partyList.sort();

      }
    })
}

getPartyDetails(partyID){
      var query ={email:this.rsaService.decrypt(localStorage.getItem('3')),supplierId:partyID}
      this.http.post(`${this.baseUrl}/bySupplierId`,query).subscribe((data:any)=>{
      // console.log('Deatils',data,query);
      // localStorage.setItem('USERACCESS', this.rsaService.encrypt(JSON.stringify(data.USERDETAILS.USERACCESSES.map(x=>x.USERACCESS) || [])));
      // this.setSelectValues(data.USERDETAILS.USERACCESSES.map(x =>(x.USERACCESS))||[],data.USERDETAILS.USER.NO_OF_PARTIES ||0);
this.redirectTo(this.router.url)
      })
}

    redirectTo(uri) {
      // console.log("URL : ", uri)
      this.select1 = 'All';
      this.select3 = 'All';
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
      this.router.navigate([uri]));
    }


    onSelectParty(party_id){
      this.partySelected = party_id;
      this.partyNameSelected = (this.partyList.find(f=>f.VENDOR_ID === party_id)).VENDOR_NAME;
      this.getPartyDetails(party_id)

    }
}
