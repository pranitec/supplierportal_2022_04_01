import { Injectable, Inject, LOCALE_ID  } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { DecimalPipe,formatNumber } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class RsaService {

  encryptMode: boolean;
  textToConvert: string;
  password = 'Shine@321';
  conversionOutput: string;
  numberValue : number
  name:string;

  constructor(@Inject(LOCALE_ID) private locale: string) {
    var formattedNumber = formatNumber(this.numberValue,this.locale,'1.2-2');
    console.log("formatNumber ", formattedNumber)
   }


  encrypt(textToConvert) {
    if(textToConvert)
    return this.conversionOutput = CryptoJS.AES.encrypt(textToConvert.trim(), this.password.trim()).toString();
    else
    return '';
  }

  decrypt(textToConvert) {
    if(textToConvert)
    return this.conversionOutput = CryptoJS.AES.decrypt(textToConvert.trim(), this.password.trim()).toString(CryptoJS.enc.Utf8);
    else
    return '';
  }

}
