import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree  } from '@angular/router';
import { LoginService } from 'src/app/components/auth/service/login.service';
import { PurchaseService } from 'src/app/components/purchase/service/purchase.service';
import { profileData } from 'src/app/config/config'
import { RsaService } from './rsa.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private loginService: LoginService, private router: Router, private purchaseService : PurchaseService,
    public rsaService: RsaService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree {
       let url: string = state.url;

           return this.checkLogin(url);
       }

       checkLogin(url: string): true | UrlTree {
          var menu;
          let val: string = localStorage.getItem('2');
           menu = this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
           let menuSelection = menu ?menu.MENUACCESS :[]
          console.log("auth guard : ", );

         let temp =  menu.includes(url);
         console.log("auth guard : ", temp, url);
         if(!temp){
           alert("This menu is not available for you. Please contact Administrator !");
           this.router.navigateByUrl('/auth/login');
         }
        var details = this.purchaseService.inputJSON('SESSION_CHECK', undefined, undefined)
        this.purchaseService.saveASN(details).subscribe((data: any) => {
          if(data == 'L' ){
            alert("session timeout")
            this.router.navigateByUrl('/auth/login');
          }
        }, error => {
          console.log(error);
        });

          if(val != null && val){
             if(url == "auth/login"){
                this.router.navigateByUrl('/craftsmanautomation/dashboard', {skipLocationChange : profileData.hideURL});
             }
             else
                return true;
          } else {
             return this.router.parseUrl('/auth/login');
          }
       }
}
