import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private httpClient: HttpClient;

  constructor(handler: HttpBackend) {
    this.httpClient = new HttpClient(handler);
  }

  getPincodeBasedAddress(pincode) {
    return this.httpClient.get('https://api.postalpincode.in/pincode/' + pincode);
  }

  getBankDetails(ifsc) {
    return this.httpClient.get('https://ifsc.firstatom.org/key/05B5ur4w8KGPoxtijEE14K76C/ifsc/' + ifsc);
  }
}
