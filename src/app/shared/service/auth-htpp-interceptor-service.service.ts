import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';
import { RsaService } from './rsa.service';
import { profileData } from 'src/app/config/config'
// import { BnNgIdleService} from 'bn-ng-idle'
@Injectable({
  providedIn: 'root'
})
export class AuthHtppInterceptorServiceService {

  public downtimeFlag = false;
  public downtimeText=''

  constructor(private router: Router, private rsaService: RsaService, private httpClient : HttpClient)
  {
    // private bnIdle: BnNgIdleService) {
    //   console.log("profileData.sessionTimeOut : ", profileData.hideURL);

    //   this.bnIdle.startWatching(profileData.hideURL).subscribe((res) => {
    //     if(res) {
    //         console.log("session expired", res);
    //         localStorage.clear();
    //         return this.router.navigate(['/auth/login']);
    //     }
    //   })

  }
  public customer;


  handelError(error: HttpErrorResponse) {
    return throwError(error);
  }


  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if (localStorage.getItem('1')) {
      req = req.clone({
        setHeaders: {
          Authorization: localStorage.getItem('1')
        }
      });
    } else {

    }
    return next.handle(req)
    .pipe(
      catchError((error: HttpErrorResponse) => {
        console.log("error dtsatus ")
        if (error.status === 400) {
          localStorage.clear();
          return this.router.navigate(['auth/login']);
        }
        // else if (error.status === 0) {
          // console.log("error dtsatus : ", error.status)
          // console.log("errorddddd.status :", error.status)
          // this.httpClient.get('assets/downtime.txt', { responseType: 'text' })
          // .subscribe(data => {
          //   if(data.length<=4) {
          //     return this.router.navigate(['auth/downtime']);
          //   }
          //   else{
          //     this.router.navigate(['']);
          //   }
          //    });
        // }
        else {
          return throwError(error);
        }
      })
    );
  }
}
