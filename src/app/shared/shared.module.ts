import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';
import { NavService } from './service/nav.service';
import { WINDOW_PROVIDERS } from './service/windows.service';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
import { NgxSpinnerModule } from 'ngx-spinner';
import { GTranslateModule } from 'ngx-g-translate';
import { TaxComponent } from './components/tax/tax.component';
import { NumberFormattingPipe } from './service/number-formatting.pipe';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { PushNotificationService } from './service/push-notification.service';
import { AppnumberdecimalDirective } from './validation/appnumberdecimal.directive';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [HeaderComponent, FooterComponent, BreadcrumbComponent, SidebarComponent, ContentLayoutComponent, TaxComponent, NumberFormattingPipe, ToolbarComponent, AppnumberdecimalDirective],
  imports: [
    CommonModule, RouterModule, FormsModule, ReactiveFormsModule, NgxSpinnerModule,    GTranslateModule,
    // NgbModule
  ],
  // entryComponents:[
  //   TaxComponent
  // ],
  exports: [ TaxComponent, NumberFormattingPipe, ToolbarComponent, AppnumberdecimalDirective],
  providers: [NavService, WINDOW_PROVIDERS, PushNotificationService],
})
export class SharedModule { }
