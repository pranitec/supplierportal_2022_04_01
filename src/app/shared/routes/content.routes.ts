import { Routes } from '@angular/router';

export const content: Routes = [{
  path: '',
  redirectTo: 'dashboard',
  pathMatch: 'full'},
  {
    path: 'dashboard',
    loadChildren: () => import('../../components/dashboard/dashboard.module').then(m => m.DashboardModule),
  },
  {
    path: 'purchase',
    loadChildren: () => import('../../components/purchase/purchase.module').then(m => m.PurchaseModule),
  },
  {
    path: 'finance',
    loadChildren: () => import('../../components/finance/finance.module').then(m => m.FinanceModule),
  },
  {
    path: 'shipment',
    loadChildren: () => import('../../components/shipment/shipment.module').then(m => m.ShipmentModule),
  },
  {
    path: 'profile',
    loadChildren: () => import('../../components/profile/profile.module').then(m => m.ProfileModule),
  }  ,
  {
    path: 'settings',
    loadChildren: () => import('../../components/settings/settings.module').then(m => m.SettingsModule),
    // data: {
    //   title: 'Settings',
    //   breadcrumb: 'Settings'
    // }
  },
  {
    path: 'administration',
    loadChildren: () => import('../../components/administration/administration.module').then(m => m.AdministrationModule),
  },
  {
    path:'gate',
    loadChildren:() => import('../../components/gate-entry/gate-entry.module').then(m => m.GateEntryModule),
  },
  {
    path:'approval',
    loadChildren:() => import('../../components/approval/approval.module').then(m => m.ApprovalModule),
  }
];
