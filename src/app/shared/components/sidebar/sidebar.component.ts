import { Component, OnInit } from '@angular/core';
import { NavService } from '../../service/nav.service';
import { RsaService } from 'src/app/shared/service/rsa.service';
import { PurchaseService } from 'src/app/components/purchase/service/purchase.service';
import { SidebarService } from '../../service/sidebar.service';
import { Router } from '@angular/router';
import { profileData } from 'src/app/config/config'

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public accountType;
  public email;
  public role;
  public menuSelection = [];
  public parentMenuList = [];
  public parentMenuFlagwithNoSubmenu= true;
  public menuaccess;

  constructor(public navServices: NavService, public rsaService: RsaService,private sidebarService : SidebarService,
    public purchaseService : PurchaseService, private router : Router) { }

  ngOnInit(): void {
    this.getMenus();
    this.accountType = this.rsaService.decrypt(localStorage.getItem('6'));
    this.email = this.rsaService.decrypt(localStorage.getItem('3'));
    this.role= this.rsaService.decrypt(localStorage.getItem('8'))
  }
  getsubMenu(parentId){
    // this.parentMenuList = this.menuSelection.filter(y=> (Number(x.MENU_ID) == Number(y.PARENT_MENU_ID)))
  }

  getMenus(){
    var details = this.purchaseService.inputJSON('USER_MENU_ACCESS', undefined, undefined)
         this.purchaseService.getPurchaseOrder(details).subscribe((data1: any) => {
          //  console.log("get MENUACCESS : ", data1)
        if(data1){
          localStorage.setItem('MENUACCESS', this.rsaService.encrypt(JSON.stringify(data1.MENUACCESSES)));
          this.menuSelection = data1 ?(data1.MENUACCESSES.map(x=>x.MENUACCESS)) :[]
          // console.log("this.menuSelection : ", this.menuSelection)
          this.setMenus();
        }
      }, err => {
        console.log(err);
      });

  }
  setMenus(){
    // console.log("set MENUACCESS : ")
    if(this.menuSelection){
      this.parentMenuList = this.menuSelection.filter(x=> (x.PARENT_MENU_ID ===null));
        this.parentMenuList.forEach((x,i) =>{
        this.parentMenuList[i].submenuList =this.menuSelection.filter(y=> (Number(x.MENU_ID) == Number(y.PARENT_MENU_ID)))
      })
      let temp1 =  this.menuSelection.find(x=> x.DISPLAY_FLAG ==="N")
      this.parentMenuList.forEach((x,i)=>{
        if( x.submenuList.length <=0){
          x.parentMenuFlagwithNoSubmenu = false
        }else{
          x.parentMenuFlagwithNoSubmenu = true
        }
      })
    }
  }
  menuRedirectTo(path){
    // console.log("path url : ", path);
    this.router.navigateByUrl(path, {skipLocationChange : profileData.hideURL});
  }
  collapseSidebar() {
// this.open = !this.open;
    // this.navServices.collapseSidebar = !this.navServices.collapseSidebar;
    this.navServices.collapseSidebar = false;
  }
}
