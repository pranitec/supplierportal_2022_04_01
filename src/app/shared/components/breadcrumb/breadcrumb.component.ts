import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, PRIMARY_OUTLET } from '@angular/router';
import { PurchaseService } from 'src/app/components/purchase/service/purchase.service';

import { filter } from 'rxjs/operators';
import { map } from 'rxjs/internal/operators';
import { BreadcrumbService } from '../../service/breadcrumb.service';
import { RsaService } from '../../service/rsa.service';
import { profileData } from 'src/app/config/config'

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  public breadcrumbs;
  public title: string;
  public selectOneValues =[];
  public selectTwoValues =[];
  public selectThreeValues =[];
  public accountType;
  public buyerList;

  constructor(private activatedRoute: ActivatedRoute, public purchaseService : PurchaseService,
              private router: Router,public breadCrumService:BreadcrumbService, public rsaService: RsaService,) {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .pipe(map(() => this.activatedRoute))
      .pipe(map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }))
      .pipe(filter(route => route.outlet === PRIMARY_OUTLET))
      .subscribe(route => {
        const snapshot = this.router.routerState.snapshot;
        const title = route.snapshot.data.title;
        const parent = route.parent.snapshot.data.breadcrumb;
        const child = route.snapshot.data.breadcrumb;
        this.breadcrumbs = {};
        this.title = title;
        this.breadcrumbs = {
          parentBreadcrumb: parent,
          childBreadcrumb: child
        };
      });
  }

  ngOnInit() {
    this.getUserAccess();
    this.accountType = this.rsaService.decrypt(localStorage.getItem('6'));

  }

  ngOnDestroy() { }

  // onFirstSelect(value){


  //   let valueSelected = this.breadCrumService.selectValues.filter(f=>f.ERP_ORGANIZATION_NAME === value.target.value)

  //   console.log(valueSelected.length, valueSelected.length ===1)
  //   if(valueSelected.length ===1 || value.target.value === 'All'){
  //     let a = this.breadCrumService.onFirstSelectChange(value.target.value);

  //     this.selectTwoValues = Array.from(new Set(a.map(x=>x.VENDOR_NAME)))
  //     this.selectThreeValues = Array.from(new Set(a.map(x=>x.VENDOR_SITE_CODE)))

  //   }


  // }

  // onSecondSelect(value){
  //   let valueSelected =  this.selectTwoValues .filter(f=>f === value.target.value)

  //   if(valueSelected.length ===1 || value.target.value === 'All'){
  //   let b = this.breadCrumService.onSecondSelectChange(value.target.value);
  //   this.selectThreeValues = Array.from(new Set(b.map(x=>x.VENDOR_SITE_CODE)))

  //   }
  // }


  // onThirdSelect(value){

  //   let valueSelected =  this.selectThreeValues .filter(f=>f === value.target.value)

  //   if(valueSelected.length ===1 || value.target.value === 'All'){
  //   let c=this.breadCrumService.onThirdSelectChange(value.target.value);
  //   }
  // }

  onFirstSelect(value){
    let a = this.breadCrumService.onFirstSelectChange(value);
    // console.log("abbb : ", a)
    this.selectTwoValues = Array.from(new Set(a.map(x=>x.VENDOR_NAME)))
    // console.log("buuuu : ", this.selectTwoValues)
    this.selectThreeValues = Array.from(new Set(a.map(x=>x.VENDOR_SITE_CODE)))
    // console.log("buuuu2323 : ", this.selectThreeValues)
     //this.selectOneValues = Array.from(new Set(a.map(x=>x.ERP_ORGANIZATION_NAME)))
   }

   onSecondSelect(value){
     let b = this.breadCrumService.onSecondSelectChange(value);
     this.selectThreeValues = Array.from(new Set(b.map(x=>x.VENDOR_SITE_CODE)))
     //this.selectTwoValues = Array.from(new Set(b.map(x=>x.VENDOR_NAME)))
   }


   onThirdSelect(value){
     let c=this.breadCrumService.onThirdSelectChange(value);
     // this.selectThreeValues = Array.from(new Set(c.map(x=>x.VENDOR_SITE_CODE)))
   }

  initialSelectSetup(){
    this.onFirstSelect(this.breadCrumService.select1);
    this.onSecondSelect(this.breadCrumService.select2);
    this.onThirdSelect(this.breadCrumService.select3);

  }

  getUserAccess(){
    var details = this.purchaseService.inputJSON('USER_ACCESS', undefined, undefined)
         this.purchaseService.getPurchaseOrder(details).subscribe((data1: any) => {
           console.log("get userAccess : ", data1)
        if(data1){
          var useraccess = (data1.USERACCESSES.map(x=>x.USERACCESS))
      useraccess.sort((a, b) =>a.ERP_ORGANIZATION_NAME < b.ERP_ORGANIZATION_NAME ? -1 : a.ERP_ORGANIZATION_NAME > b.ERP_ORGANIZATION_NAME ? 1 : 0);
      localStorage.setItem('USERACCESS', this.rsaService.encrypt(JSON.stringify(useraccess)));
      this.breadCrumService.getSelectValues();
      this.buyerList =  Array.from(new Set(this.breadCrumService.selectValues.map(x =>x.ERP_ORGANIZATION_NAME)));
      this.initialSelectSetup();
          // localStorage.setItem('menuACCESS', this.rsaService.encrypt(JSON.stringify(data1.MENUACCESSES)));
          // this.useraccess = data1 ?(data1.MENUACCESSES.map(x=>x.MENUACCESS)) :[]
          // console.log("this.menuSelection : ", this.menuSelection)
          // this.setMenus();
        }
      }, err => {
        console.log(err);
      });

  }
  clickBreadcrumbs(){
    this.router.navigateByUrl('/craftsmanautomation/dashboard', {skipLocationChange : profileData.hideURL})
  }


}
