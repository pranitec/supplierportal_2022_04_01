import { Component, OnInit } from '@angular/core';
import {profileData } from '../../../config/config'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  public copyright='';
  public website =''
  constructor() { }

  ngOnInit(): void {
    this.copyright = profileData.copyright
    this.website = profileData.websiteURL
  }

}
