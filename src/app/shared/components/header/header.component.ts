import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PurchaseService } from 'src/app/components/purchase/service/purchase.service';
import { profileData } from 'src/app/config/config';
import { BreadcrumbService } from '../../service/breadcrumb.service';
import { NavService } from '../../service/nav.service';
import { RsaService } from '../../service/rsa.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { saveAs } from 'file-saver';
import { PushNotificationService } from '../../../shared/service/push-notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public right_sidebar: boolean = false;
  public open: boolean = false;
  public openNav: boolean = false;
  public isOpenMobile: boolean;
  public role;
  public userName = '';
  public party = '';
  public logo;
  public notificationList = [];
  public count = 0;
  public markallReadIds = [];
  public  greet;
  public icon;
  public noofParties=0
  // public notifier : NotifierService;

  constructor(public navServices: NavService, public rsaService: RsaService, private router: Router, public breadCrumService: BreadcrumbService,
    private purchaseService: PurchaseService, private spinner: NgxSpinnerService,
    private pushnotificationService: PushNotificationService) {
  }

  ngOnInit(): void {
    this.userName = this.rsaService.decrypt(localStorage.getItem('4'));
    this.noofParties = Number(localStorage.getItem('5'));
    this.logo = profileData.LogoFile
    this.greetings();
    // this.notify();
    this.pushNotification();
  }

  // getMenu(){
  //   console.log("inn menu")
  //   var details = this.purchaseService.inputJSON('USER_MENU_ACCESS', undefined, undefined)
  //     this.purchaseService.getPurchaseOrder(details).subscribe((data1: any) => {
  //       var menuaccess = (data1.MENUACCESSES.map(x=>x.MENUACCESS))
  //     console.log("menuaccess : ", menuaccess)
  //     localStorage.setItem('MENUACCESS', this.rsaService.encrypt(JSON.stringify(menuaccess)));
  //     }, err => {
  //       this.spinner.hide();
  //       console.log(err);
  //     });
  // }
  collapseSidebar() {
    // console.log("collapseSidebar ", open, this.navServices.collapseSidebar);
    this.open = !this.open;
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar;
    // this.navServices.collapseSidebar = false;
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['']);
  }
  setParty(value) {

    let a = this.breadCrumService.partyList.filter(f => f.VENDOR_NAME === value)
    if (a.length === 1) {
      this.party = a[0].VENDOR_ID;

      // this.setLinesList();
    }
    else {
      this.party = '';
    }
  }

  // callHeader(i){
  //   this.navServices.getView(i);
  // }
  confirmParty() {
    this.breadCrumService.onSelectParty(this.party);
  }

  pushNotification() {
    this.spinner.show();
    this.markallReadIds = [];
    var query = this.purchaseService.inputJSON('NOTIFICATION_LIST', undefined, undefined)
    this.purchaseService.getPurchaseOrder(query).subscribe((data: any) => {
      console.log('ppp NOTIFICATION_LIST', data);
      this.notificationList = data.NOTIFICATION_LISTS ? data.NOTIFICATION_LISTS.map(x => x.NOTIFICATION_LIST) : [];
      this.count = data.NOTIFICATION_COUNT;
      console.log("count  : ", this.count);

      this.spinner.hide();
      let markallread = this.notificationList.filter(y => (y.NOTIFICATION_STATUS === 'U'))
      markallread.forEach(x => {
        this.markallReadIds.push(x.NOTIFICATION_ID)
      });
      // console.log('ppp markallReadIds', this.markallReadIds);
    }, err => {
      this.spinner.hide();
      console.log(err);
    });

  }
  markAllRead() {
    let query = { NOTIFICATION_LIST: { NOTIFICATION_ID: this.markallReadIds } };
    // console.log(" notification serevice : ", query)
    this.purchaseService.saveASN(query).subscribe((data: any) => {
      // console.log("notified data: ", data)

    },
      (error) => {
        console.log(error);
      });
  }
  userGuide() {

  }
  greetings() {
    var myDate = new Date();
    var hrs = myDate.getHours();
      if (hrs < 12){
        this.icon="fas fa-cloud-sun"
        this.greet = 'Good Morning';
      }
    else if (hrs >= 12 && hrs <= 17){
      this.greet = 'Good Afternoon';
      this.icon="fas fa-sun"
    }
    else if (hrs >= 17 && hrs <= 24){
      this.greet = 'Good Evening';
      this.icon="fas fa-moon"
      // this.icon="fas fa-cloud-sun"
    }
    // console.log("this.greet : ", this.greet, this.icon)
  }
  viewMore(){
    console.log("inn")
    this.router.navigateByUrl('craftsmanautomation/administration/listNotification', {skipLocationChange : profileData.hideURL});
  }
  notify() {
    console.log("notificatino alert")
    let data: Array < any >= [];
    data.push({
        'title': 'Approval',
        'alertContent': 'This is First Alert -- By Debasis Saha'
    });
    data.push({
        'title': 'Request',
        'alertContent': 'This is Second Alert -- By Debasis Saha'
    });
    data.push({
        'title': 'Leave Application',
        'alertContent': 'This is Third Alert -- By Debasis Saha'
    });
    data.push({
        'title': 'Approval',
        'alertContent': 'This is Fourth Alert -- By Debasis Saha'
    });
    data.push({
        'title': 'To Do Task',
        'alertContent': 'This is Fifth Alert -- By Debasis Saha'
    });
    this.pushnotificationService.generateNotification(data);
}

clickedLink(id,url, docid) {

  let query = { NOTIFICATION_LIST: { NOTIFICATION_ID: id} };
  this.purchaseService.saveASN(query).subscribe((data: any) => {
    console.log("notified data: ", data)
  },
    (error) => {
      console.log(error);
    });

   this.router.navigate([url], { queryParams: { docid: docid }, skipLocationChange : profileData.hideURL });
}
clicklogo(){
  console.log("clicklogo");

  this.router.navigateByUrl('/craftsmanautomation/dashboard', {skipLocationChange : profileData.hideURL})
}
}
