import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, PRIMARY_OUTLET } from '@angular/router';
import { PurchaseService } from 'src/app/components/purchase/service/purchase.service';
import { filter } from 'rxjs/operators';
import { map } from 'rxjs/internal/operators';
import { BreadcrumbService } from '../../service/breadcrumb.service';
import { RsaService } from '../../service/rsa.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  public fromDate;
  public toDate;
  public searchText;
  constructor() { }

  ngOnInit(): void {
  }

  fromDateChanged(){

  }
  getPastPurchases(){

  }
  setFilteredData(){

  }

  exportExcel(){

  }

}
