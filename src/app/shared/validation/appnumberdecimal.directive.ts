import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { nextTick } from 'process';

@Directive({
  selector: '[appAppnumberdecimal]'
})
export class AppnumberdecimalDirective {

  // Allow decimal numbers and negative values
  //  private regex: RegExp = new RegExp(/^\d*\.?\d{0,3}$/g);
  private regex: RegExp;
  // Allow key codes for special events. Reflect :
  // Backspace, tab, end, home
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight', 'Del', 'Delete'];
  @Input('appAppnumberdecimal') params: number;
  constructor(private el: ElementRef) {
  }
  ngOnInit() {
  }
  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    if (this.params == 0) {
      this.regex = new RegExp(/^\d*$/g);
    }
    else if (this.params == 1) {
      this.regex = new RegExp(/^\d*\.?\d{0,1}$/g);
    }
    else if (this.params == 2) {
      this.regex = new RegExp(/^\d*\.?\d{0,2}$/g);
    }
    else if (this.params == 3) {
      this.regex = new RegExp(/^\d*\.?\d{0,3}$/g);
      // private regex: RegExp = new RegExp(/^\d*\.?\d{0,2}$/g);
    }
    else if (this.params == 4) {
      this.regex = new RegExp(/^\d*\.?\d{0,4}$/g);
    }
    else if (this.params == 5) {
      this.regex = new RegExp(/^\d*\.?\d{0,5}$/g);
    }
    else if (this.params == 6) {
      this.regex = new RegExp(/^\d*\.?\d{0,6}$/g);
    }
    let current: string = this.el.nativeElement.value;
    const position = this.el.nativeElement.selectionStart;
    const next: string = [current.slice(0, position), event.key == 'Decimal' ? '.' : event.key, current.slice(position)].join('');
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }
}
